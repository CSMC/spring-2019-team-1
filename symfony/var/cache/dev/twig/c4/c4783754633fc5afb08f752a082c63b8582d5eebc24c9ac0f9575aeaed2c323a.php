<?php

/* role/admin/ip/list.html.twig */
class __TwigTemplate_2e97fe373a2085574cc715b9f6d98e6deb5d4635fdf1fb8fe878717fa1a375d4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/ip/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/ip/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/ip/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"clearfix\"></div>

    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h3>IP Address
                        <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./addAddress'\"
                                style=\"float:right\">New IP Address
                        </button>
                    </h3>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <h4>CSMC</h4>
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>


                        <tbody>
                        ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["csmc"]) || array_key_exists("csmc", $context) ? $context["csmc"] : (function () { throw new Twig_Error_Runtime('Variable "csmc" does not exist.', 42, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ip"]) {
            // line 43
            echo "                            <tr>
                                <td>";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "address", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 45
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "room", []) == null)) {
                // line 46
                echo "                                        -
                                    ";
            } else {
                // line 48
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "room", []), "html", null, true);
                echo "
                                    ";
            }
            // line 50
            echo "                                </td>
                                <td>
                                    ";
            // line 52
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == 1)) {
                // line 53
                echo "                                        Yes
                                    ";
            } else {
                // line 55
                echo "                                        No
                                    ";
            }
            // line 57
            echo "                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id=";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>


                            </tr>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ip'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "
                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>UTD CS</h4>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        ";
        // line 96
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utdcs"]) || array_key_exists("utdcs", $context) ? $context["utdcs"] : (function () { throw new Twig_Error_Runtime('Variable "utdcs" does not exist.', 96, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ip"]) {
            // line 97
            echo "                            <tr>
                                <td>";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "address", []), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 100
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == true)) {
                // line 101
                echo "                                        (Ip Blocked)
                                    ";
            } else {
                // line 103
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "room", []), "html", null, true);
                echo "
                                    ";
            }
            // line 105
            echo "                                </td>
                                <td>
                                    ";
            // line 107
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == 1)) {
                // line 108
                echo "                                        Yes
                                    ";
            } else {
                // line 110
                echo "                                        No
                                    ";
            }
            // line 112
            echo "                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id=";
            // line 115
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>


                            </tr>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ip'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "
                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>UTD</h4>
                    <table id=\"datatable1\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        ";
        // line 151
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["utd"]) || array_key_exists("utd", $context) ? $context["utd"] : (function () { throw new Twig_Error_Runtime('Variable "utd" does not exist.', 151, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ip"]) {
            // line 152
            echo "                            <tr>
                                <td>";
            // line 153
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "address", []), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 155
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == true)) {
                // line 156
                echo "                                        (Ip Blocked)
                                    ";
            } else {
                // line 158
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "room", []), "html", null, true);
                echo "
                                    ";
            }
            // line 160
            echo "                                </td>
                                <td>
                                    ";
            // line 162
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == 1)) {
                // line 163
                echo "                                        Yes
                                    ";
            } else {
                // line 165
                echo "                                        No
                                    ";
            }
            // line 167
            echo "                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id=";
            // line 170
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>


                            </tr>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ip'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 178
        echo "
                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>Others</h4>
                    <table id=\"datatable2\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        ";
        // line 206
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["others"]) || array_key_exists("others", $context) ? $context["others"] : (function () { throw new Twig_Error_Runtime('Variable "others" does not exist.', 206, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["ip"]) {
            // line 207
            echo "                            <tr>
                                <td>";
            // line 208
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "address", []), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 210
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == true)) {
                // line 211
                echo "                                        (Ip Blocked)
                                    ";
            } else {
                // line 213
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "room", []), "html", null, true);
                echo "
                                    ";
            }
            // line 215
            echo "                                </td>
                                <td>
                                    ";
            // line 217
            if ((twig_get_attribute($this->env, $this->source, $context["ip"], "blocked", []) == 1)) {
                // line 218
                echo "                                        Yes
                                    ";
            } else {
                // line 220
                echo "                                        No
                                    ";
            }
            // line 222
            echo "                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id=";
            // line 225
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["ip"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>


                            </tr>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ip'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 233
        echo "
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/ip/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  411 => 233,  397 => 225,  392 => 222,  388 => 220,  384 => 218,  382 => 217,  378 => 215,  372 => 213,  368 => 211,  366 => 210,  361 => 208,  358 => 207,  354 => 206,  324 => 178,  310 => 170,  305 => 167,  301 => 165,  297 => 163,  295 => 162,  291 => 160,  285 => 158,  281 => 156,  279 => 155,  274 => 153,  271 => 152,  267 => 151,  237 => 123,  223 => 115,  218 => 112,  214 => 110,  210 => 108,  208 => 107,  204 => 105,  198 => 103,  194 => 101,  192 => 100,  187 => 98,  184 => 97,  180 => 96,  150 => 68,  136 => 60,  131 => 57,  127 => 55,  123 => 53,  121 => 52,  117 => 50,  111 => 48,  107 => 46,  105 => 45,  101 => 44,  98 => 43,  94 => 42,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"clearfix\"></div>

    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h3>IP Address
                        <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./addAddress'\"
                                style=\"float:right\">New IP Address
                        </button>
                    </h3>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <h4>CSMC</h4>
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>


                        <tbody>
                        {% for ip in csmc %}
                            <tr>
                                <td>{{ ip.address }}</td>
                                <td>{% if ip.room == NULL %}
                                        -
                                    {% else %}
                                        {{ ip.room }}
                                    {% endif %}
                                </td>
                                <td>
                                    {% if ip.blocked == 1 %}
                                        Yes
                                    {% else %}
                                        No
                                    {% endif %}
                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id={{ ip.id }}'\">Edit
                                    </button>
                                </td>


                            </tr>

                        {% endfor %}

                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>UTD CS</h4>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        {% for ip in utdcs %}
                            <tr>
                                <td>{{ ip.address }}</td>
                                <td>
                                    {% if ip.blocked == true %}
                                        (Ip Blocked)
                                    {% else %}
                                        {{ ip.room }}
                                    {% endif %}
                                </td>
                                <td>
                                    {% if ip.blocked == 1 %}
                                        Yes
                                    {% else %}
                                        No
                                    {% endif %}
                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id={{ ip.id }}'\">Edit
                                    </button>
                                </td>


                            </tr>

                        {% endfor %}

                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>UTD</h4>
                    <table id=\"datatable1\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        {% for ip in utd %}
                            <tr>
                                <td>{{ ip.address }}</td>
                                <td>
                                    {% if ip.blocked == true %}
                                        (Ip Blocked)
                                    {% else %}
                                        {{ ip.room }}
                                    {% endif %}
                                </td>
                                <td>
                                    {% if ip.blocked == 1 %}
                                        Yes
                                    {% else %}
                                        No
                                    {% endif %}
                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id={{ ip.id }}'\">Edit
                                    </button>
                                </td>


                            </tr>

                        {% endfor %}

                    </table>
                </div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <h4>Others</h4>
                    <table id=\"datatable2\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">IP Address
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Room
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Blocked
                            </th>
                            <th style=\"width:20%\" class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\"
                                colspan=\"1\">Edit
                            </th>

                        </thead>

                        <tbody>
                        {% for ip in others %}
                            <tr>
                                <td>{{ ip.address }}</td>
                                <td>
                                    {% if ip.blocked == true %}
                                        (Ip Blocked)
                                    {% else %}
                                        {{ ip.room }}
                                    {% endif %}
                                </td>
                                <td>
                                    {% if ip.blocked == 1 %}
                                        Yes
                                    {% else %}
                                        No
                                    {% endif %}
                                </td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./addressEdit?id={{ ip.id }}'\">Edit
                                    </button>
                                </td>


                            </tr>

                        {% endfor %}

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

{% endblock %}", "role/admin/ip/list.html.twig", "/code/templates/role/admin/ip/list.html.twig");
    }
}
