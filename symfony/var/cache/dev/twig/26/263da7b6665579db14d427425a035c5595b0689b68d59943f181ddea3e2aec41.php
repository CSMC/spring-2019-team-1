<?php

/* role/admin/announcement/edit.html.twig */
class __TwigTemplate_5ba41143d2c7a5160da35d8947c316ec494c1f9561c85ea53bf638f50dd24b4f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/announcement/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Announcement?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    ";
        // line 40
        echo "                    ";
        // line 41
        echo "                            ";
        // line 42
        echo "                    ";
        // line 43
        echo "                    <a class=\"btn btn-danger\" id=\"deleteItem\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_announcement_delete");
        echo "\"></a>
                </div>
            </div>
        </div>
    </div>


    <div class=\"center_col\" role=\"main\">

        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Announcement (";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["subject"]) || array_key_exists("subject", $context) ? $context["subject"] : (function () { throw new Twig_Error_Runtime('Variable "subject" does not exist.', 56, $this->source); })()), "html", null, true);
        echo ") </h2>
                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">
                        <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                    </span>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">
                        ";
        // line 66
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 66, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 67
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 69
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 70
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "
                        ";
        // line 77
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 77, $this->source); })()), "vars", []), "valid", [])) {
            echo "a\\
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 79
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 79, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 82
        echo "                        <br/>

                        ";
        // line 84
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                        ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 85, $this->source); })()), 'errors');
        echo "

                        ";
        // line 91
        echo "

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 95, $this->source); })()), "subject", []), 'label');
        echo "
                            </div>
                            ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 97, $this->source); })()), "subject", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 99, $this->source); })()), "subject", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 105, $this->source); })()), "message", []), 'label');
        echo "
                            </div>
                            ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 107, $this->source); })()), "message", []), 'errors');
        echo "

                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <div class=\"btn-toolbar editor\" data-role=\"editor-toolbar\" data-target=\"#editor-one\">
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font\"><i
                                                    class=\"fas fa-font\"></i><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu\">
                                        </ul>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font Size\"><i
                                                    class=\"fas fa-text-height\"></i>&nbsp;<b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu\">
                                            <li>
                                                <a data-edit=\"fontSize 5\">
                                                    <p style=\"font-size:17px\">Huge</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-edit=\"fontSize 3\">
                                                    <p style=\"font-size:14px\">Normal</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-edit=\"fontSize 1\">
                                                    <p style=\"font-size:11px\">Small</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"bold\" title=\"Bold (Ctrl/Cmd+B)\"><i
                                                    class=\"fas fa-bold\"></i></a>
                                        <a class=\"btn\" data-edit=\"italic\" title=\"Italic (Ctrl/Cmd+I)\"><i
                                                    class=\"fas fa-italic\"></i></a>
                                        <a class=\"btn\" data-edit=\"strikethrough\" title=\"Strikethrough\"><i
                                                    class=\"fas fa-strikethrough\"></i></a>
                                        <a class=\"btn\" data-edit=\"underline\" title=\"Underline (Ctrl/Cmd+U)\"><i
                                                    class=\"fas fa-underline\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"insertunorderedlist\" title=\"Bullet list\"><i
                                                    class=\"fas fa-list-ul\"></i></a>
                                        <a class=\"btn\" data-edit=\"insertorderedlist\" title=\"Number list\"><i
                                                    class=\"fas fa-list-ol\"></i></a>
                                        <a class=\"btn\" data-edit=\"outdent\" title=\"Reduce indent (Shift+Tab)\"><i
                                                    class=\"fas fa-dedent\"></i></a>
                                        <a class=\"btn\" data-edit=\"indent\" title=\"Indent (Tab)\"><i
                                                    class=\"fas fa-indent\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"justifyleft\" title=\"Align Left (Ctrl/Cmd+L)\"><i
                                                    class=\"fas fa-align-left\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifycenter\" title=\"Center (Ctrl/Cmd+E)\"><i
                                                    class=\"fas fa-align-center\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifyright\" title=\"Align Right (Ctrl/Cmd+R)\"><i
                                                    class=\"fas fa-align-right\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifyfull\" title=\"Justify (Ctrl/Cmd+J)\"><i
                                                    class=\"fas fa-align-justify\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Hyperlink\"><i
                                                    class=\"fas fa-link\"></i></a>
                                        <div class=\"dropdown-menu input-append\">
                                            <input class=\"span2\" placeholder=\"URL\" type=\"text\" data-edit=\"createLink\"/>
                                            <button class=\"btn\" type=\"button\">Add</button>
                                        </div>
                                        <a class=\"btn\" data-edit=\"unlink\" title=\"Remove Hyperlink\"><i
                                                    class=\"fas fa-cut\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" title=\"Insert picture (or just drag & drop)\" id=\"pictureBtn\"><i
                                                    class=\"fas fa-picture-o\"></i></a>
                                        <input type=\"file\" data-role=\"magic-overlay\" data-target=\"#pictureBtn\"
                                               data-edit=\"insertImage\"/>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"undo\" title=\"Undo (Ctrl/Cmd+Z)\"><i
                                                    class=\"fas fa-undo\"></i></a>
                                        <a class=\"btn\" data-edit=\"redo\" title=\"Redo (Ctrl/Cmd+Y)\"><i
                                                    class=\"fas fa-repeat\"></i></a>
                                    </div>
                                </div>

                                ";
        // line 193
        echo "                                <div id=\"editor-one\" class=\"editor-wrapper\"></div>
                                ";
        // line 194
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 194, $this->source); })()), "message", []), 'widget', ["attr" => ["style" => "display:none;", "class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                <br/>
                            </div>
                        </div>

                        ";
        // line 210
        echo "
                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 213
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 213, $this->source); })()), "roles", []), 'label');
        echo "
                            </div>
                            ";
        // line 215
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 215, $this->source); })()), "roles", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 217
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 217, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "size" => "7"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 223
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 223, $this->source); })()), "userGroups", []), 'label');
        echo "
                            </div>
                            ";
        // line 225
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 225, $this->source); })()), "userGroups", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 227
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 227, $this->source); })()), "userGroups", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "size" => "7"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 233
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 233, $this->source); })()), "startDate", []), 'label');
        echo "
                            </div>
                            ";
        // line 235
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 235, $this->source); })()), "startDate", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 237
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 237, $this->source); })()), "startDate", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 243
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 243, $this->source); })()), "endDate", []), 'label');
        echo "
                            </div>
                            ";
        // line 245
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 245, $this->source); })()), "endDate", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 247
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 247, $this->source); })()), "endDate", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"ln_solid\"></div>


                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <button class=\"btn btn-secondary\" type=\"button\"
                                        onclick=\"location.href='/listAnnouncements'\">Cancel
                                </button>
                                ";
        // line 259
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 259, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "
                            </div>
                        </div>

                        ";
        // line 263
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 263, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>


    ";
        // line 271
        $this->displayBlock('javascripts', $context, $blocks);
        // line 316
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 271
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 272
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "


        <script type=\"text/javascript\">

            var storedMessage = document.getElementById(\"form_message\").value;

            \$('#editor-one').wysiwyg();


            // \$( \"#editor-one\").html( \"<span class='red'>Hello <b>Again</b></span>\" );
            \$(\"#editor-one\").html(storedMessage);

            \$('#form_save').click(function () {
                document.getElementById(\"form_message\").value = \$('#editor-one').html();
            });


            ";
        // line 297
        echo "

            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });


            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });


            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });


        </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/announcement/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  466 => 297,  444 => 272,  435 => 271,  424 => 316,  422 => 271,  411 => 263,  404 => 259,  389 => 247,  384 => 245,  379 => 243,  370 => 237,  365 => 235,  360 => 233,  351 => 227,  346 => 225,  341 => 223,  332 => 217,  327 => 215,  322 => 213,  317 => 210,  309 => 194,  306 => 193,  218 => 107,  213 => 105,  204 => 99,  199 => 97,  194 => 95,  188 => 91,  183 => 85,  179 => 84,  175 => 82,  169 => 79,  163 => 77,  160 => 75,  153 => 73,  144 => 70,  139 => 69,  135 => 68,  132 => 67,  127 => 66,  115 => 56,  98 => 43,  96 => 42,  94 => 41,  92 => 40,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Announcement?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    {# Change the delete URL #}
                    {#<button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"#}
                            {#onclick=\"location.href='/deleteAnnouncement?id={{ deleteId }}'\">Delete#}
                    {#</button>#}
                    <a class=\"btn btn-danger\" id=\"deleteItem\" href=\"{{ path('admin_announcement_delete') }}\"></a>
                </div>
            </div>
        </div>
    </div>


    <div class=\"center_col\" role=\"main\">

        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Announcement ({{ subject }}) </h2>
                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">
                        <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                    </span>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">
                        {# Displaying Flash message for foreign key constraint violation#}
                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}

                        {# Checking for form errors and displaying them#}
                        {% if not form.vars.valid %}a\\
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>

                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left' }}) }}
                        {{ form_errors(form) }}

                        {#
                       <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
                       how to apply this? -- data-parsley-validate
                       #}


                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.subject) }}
                            </div>
                            {{ form_errors(form.subject) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.subject, {'attr': {'class': 'form-control col-md-7 col-xs-12' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.message) }}
                            </div>
                            {{ form_errors(form.message) }}

                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <div class=\"btn-toolbar editor\" data-role=\"editor-toolbar\" data-target=\"#editor-one\">
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font\"><i
                                                    class=\"fas fa-font\"></i><b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu\">
                                        </ul>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font Size\"><i
                                                    class=\"fas fa-text-height\"></i>&nbsp;<b class=\"caret\"></b></a>
                                        <ul class=\"dropdown-menu\">
                                            <li>
                                                <a data-edit=\"fontSize 5\">
                                                    <p style=\"font-size:17px\">Huge</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-edit=\"fontSize 3\">
                                                    <p style=\"font-size:14px\">Normal</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-edit=\"fontSize 1\">
                                                    <p style=\"font-size:11px\">Small</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"bold\" title=\"Bold (Ctrl/Cmd+B)\"><i
                                                    class=\"fas fa-bold\"></i></a>
                                        <a class=\"btn\" data-edit=\"italic\" title=\"Italic (Ctrl/Cmd+I)\"><i
                                                    class=\"fas fa-italic\"></i></a>
                                        <a class=\"btn\" data-edit=\"strikethrough\" title=\"Strikethrough\"><i
                                                    class=\"fas fa-strikethrough\"></i></a>
                                        <a class=\"btn\" data-edit=\"underline\" title=\"Underline (Ctrl/Cmd+U)\"><i
                                                    class=\"fas fa-underline\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"insertunorderedlist\" title=\"Bullet list\"><i
                                                    class=\"fas fa-list-ul\"></i></a>
                                        <a class=\"btn\" data-edit=\"insertorderedlist\" title=\"Number list\"><i
                                                    class=\"fas fa-list-ol\"></i></a>
                                        <a class=\"btn\" data-edit=\"outdent\" title=\"Reduce indent (Shift+Tab)\"><i
                                                    class=\"fas fa-dedent\"></i></a>
                                        <a class=\"btn\" data-edit=\"indent\" title=\"Indent (Tab)\"><i
                                                    class=\"fas fa-indent\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"justifyleft\" title=\"Align Left (Ctrl/Cmd+L)\"><i
                                                    class=\"fas fa-align-left\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifycenter\" title=\"Center (Ctrl/Cmd+E)\"><i
                                                    class=\"fas fa-align-center\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifyright\" title=\"Align Right (Ctrl/Cmd+R)\"><i
                                                    class=\"fas fa-align-right\"></i></a>
                                        <a class=\"btn\" data-edit=\"justifyfull\" title=\"Justify (Ctrl/Cmd+J)\"><i
                                                    class=\"fas fa-align-justify\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Hyperlink\"><i
                                                    class=\"fas fa-link\"></i></a>
                                        <div class=\"dropdown-menu input-append\">
                                            <input class=\"span2\" placeholder=\"URL\" type=\"text\" data-edit=\"createLink\"/>
                                            <button class=\"btn\" type=\"button\">Add</button>
                                        </div>
                                        <a class=\"btn\" data-edit=\"unlink\" title=\"Remove Hyperlink\"><i
                                                    class=\"fas fa-cut\"></i></a>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" title=\"Insert picture (or just drag & drop)\" id=\"pictureBtn\"><i
                                                    class=\"fas fa-picture-o\"></i></a>
                                        <input type=\"file\" data-role=\"magic-overlay\" data-target=\"#pictureBtn\"
                                               data-edit=\"insertImage\"/>
                                    </div>
                                    <div class=\"btn-group\">
                                        <a class=\"btn\" data-edit=\"undo\" title=\"Undo (Ctrl/Cmd+Z)\"><i
                                                    class=\"fas fa-undo\"></i></a>
                                        <a class=\"btn\" data-edit=\"redo\" title=\"Redo (Ctrl/Cmd+Y)\"><i
                                                    class=\"fas fa-repeat\"></i></a>
                                    </div>
                                </div>

                                {# TODO: HOW DO We Pass the JavaScript Value to the Textarea??? #}
                                <div id=\"editor-one\" class=\"editor-wrapper\"></div>
                                {{ form_widget(form.message, {'attr': {'style': 'display:none;', 'class': 'form-control col-md-7 col-xs-12' }}) }}
                                <br/>
                            </div>
                        </div>

                        {#
                                                <div class=\"form-group\">
                                                    <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                                        {{ form_label(form.active) }}
                                                    </div>
                                                    {{ form_errors(form.active) }}
                                                    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                                        {{ form_widget(form.active, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                                    </div>
                                                </div>
                        #}

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.roles) }}
                            </div>
                            {{ form_errors(form.roles) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.roles, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'size': '7' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.userGroups) }}
                            </div>
                            {{ form_errors(form.userGroups) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.userGroups, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'size': '7' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.startDate) }}
                            </div>
                            {{ form_errors(form.startDate) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.startDate, {'attr': {'class': 'form-control col-md-7 col-xs-12' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.endDate) }}
                            </div>
                            {{ form_errors(form.endDate) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.endDate, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>

                        <div class=\"ln_solid\"></div>


                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <button class=\"btn btn-secondary\" type=\"button\"
                                        onclick=\"location.href='/listAnnouncements'\">Cancel
                                </button>
                                {{ form_widget(form.save, {'attr': {'class': 'btn btn-success', 'data-toggle':'modal','data-target':'#myModalEdit' }}) }}
                            </div>
                        </div>

                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {% block javascripts %}
        {{ parent() }}


        <script type=\"text/javascript\">

            var storedMessage = document.getElementById(\"form_message\").value;

            \$('#editor-one').wysiwyg();


            // \$( \"#editor-one\").html( \"<span class='red'>Hello <b>Again</b></span>\" );
            \$(\"#editor-one\").html(storedMessage);

            \$('#form_save').click(function () {
                document.getElementById(\"form_message\").value = \$('#editor-one').html();
            });


            {#
            function myFunction() {
                alert(\"FORM loaded\");

                return true;
            }
            #}


            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });


            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });


            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });


        </script>
    {% endblock %}

{% endblock %}
", "role/admin/announcement/edit.html.twig", "/code/templates/role/admin/announcement/edit.html.twig");
    }
}
