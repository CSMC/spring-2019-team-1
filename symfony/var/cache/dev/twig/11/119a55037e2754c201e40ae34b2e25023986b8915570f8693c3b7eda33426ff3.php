<?php

/* role/admin/user_group/edit.html.twig */
class __TwigTemplate_bd3b6ef0b8a732b92f0d42a098c1882883c37aa43d7c60e3560f6a56406a956b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user_group/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>

            </div>
        </div>
    </div>



    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete this User Group?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    ";
        // line 43
        echo "                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteUserGroup?id=";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["deleteId"]) || array_key_exists("deleteId", $context) ? $context["deleteId"] : (function () { throw new Twig_Error_Runtime('Variable "deleteId" does not exist.', 44, $this->source); })()), "html", null, true);
        echo "'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit User Group (";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["groupName"]) || array_key_exists("groupName", $context) ? $context["groupName"] : (function () { throw new Twig_Error_Runtime('Variable "groupName" does not exist.', 58, $this->source); })()), "html", null, true);
        echo ")</h2>

                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">

                       <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                               data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                        </span>

                        <div class=\"clearfix\"></div>
                    </div>


                    <div class=\"x_content\">

                        ";
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 72, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 73
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 74
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 75
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 76
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "
                        ";
        // line 82
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 82, $this->source); })()), "vars", []), "valid", [])) {
            // line 83
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 84
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 87
        echo "                        <br/>
                        ";
        // line 88
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 88, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "

                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 96, $this->source); })()), "name", []), 'label');
        echo "
                                </div>
                                ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 98, $this->source); })()), "name", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 100, $this->source); })()), "name", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 107, $this->source); })()), "description", []), 'label');
        echo "
                                </div>
                                ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 109, $this->source); })()), "description", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 111, $this->source); })()), "description", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 119, $this->source); })()), "users", []), 'label');
        echo "
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 122, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>

                            </div>


                            <div class=\"form-group\">
                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\" data-color=\"white\">
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 132, $this->source); })()), "users", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>
                            </div>


                            <div class=\"ln_solid\"></div>
                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">


                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userGroupShowAll'\">Cancel
                                    </button>

                                    ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 146, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "

                                </div>
                            </div>

                        </form>
                        ";
        // line 152
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 152, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>





    ";
        // line 163
        $this->displayBlock('javascripts', $context, $blocks);
        // line 259
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 163
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 164
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();

            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();

            });


        </script>

        <script type=\"text/javascript\">
            \$(function () {

                \$('#form_users').multiSelect({
                    selectableHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    selectionHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",


                    afterInit: function (ms) {
                        var that = this,
                            \$selectableSearch = that.\$selectableUl.prev(),
                            \$selectionSearch = that.\$selectionUl.prev(),
                            selectableSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = \$selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.\$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = \$selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.\$selectionUl.focus();
                                    return false;
                                }
                            });
                        that.qs3 = \$('#form_roles').quicksearch(selectableSearchString, {
                            'testQuery': function (query, txt, row) {
                                var v = \$(row).data(\"roles\");
                                if (v === \"\" || Array.isArray(v)) {
                                    return query[0] === \"\";
                                } else {
                                    console.log(\$(row).data(\"roles\"));
                                    // change single quotes to double (double is required for JSON), because we had to change them to play nice with form style
                                    var d = \$(row).data(\"roles\").replace(/'/g, \"\\\"\");
                                    var r = JSON.parse(d);
                                    for (var i = 0; i < r.length; i++) {
                                        console.log(r[i]);
                                        if (r[i] == query[0]) {
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                            }
                        }).on('change', function (e) {
                            if (e.which === 40) {
                                that.\$selectableUl.focus();
                                return false;
                            }
                        });


                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                });
                //change the size of the two selectable tables

                \$('.ms-container').css('width', '100%');
                \$('.ms-list').addClass('form-control');
            });

        </script>



    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user_group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  316 => 164,  307 => 163,  294 => 259,  292 => 163,  278 => 152,  269 => 146,  252 => 132,  239 => 122,  233 => 119,  222 => 111,  217 => 109,  212 => 107,  202 => 100,  197 => 98,  192 => 96,  181 => 88,  178 => 87,  172 => 84,  169 => 83,  167 => 82,  164 => 81,  157 => 79,  148 => 76,  143 => 75,  139 => 74,  136 => 73,  132 => 72,  115 => 58,  98 => 44,  95 => 43,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>

            </div>
        </div>
    </div>



    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete this User Group?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    {# Change the delete URL #}
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteUserGroup?id={{ deleteId }}'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit User Group ({{ groupName }})</h2>

                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">

                       <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                               data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                        </span>

                        <div class=\"clearfix\"></div>
                    </div>


                    <div class=\"x_content\">

                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}

                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>
                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}

                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.name) }}
                                </div>
                                {{ form_errors(form.name) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.name, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.description) }}
                                </div>
                                {{ form_errors(form.description) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.description, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.users) }}
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.roles, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>

                            </div>


                            <div class=\"form-group\">
                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\" data-color=\"white\">
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.users, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>
                            </div>


                            <div class=\"ln_solid\"></div>
                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">


                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userGroupShowAll'\">Cancel
                                    </button>

                                    {{ form_widget(form.save, {'attr': {'class': 'btn btn-success', 'data-toggle':'modal','data-target':'#myModalEdit' }}) }}

                                </div>
                            </div>

                        </form>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>





    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();

            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();

            });


        </script>

        <script type=\"text/javascript\">
            \$(function () {

                \$('#form_users').multiSelect({
                    selectableHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    selectionHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",


                    afterInit: function (ms) {
                        var that = this,
                            \$selectableSearch = that.\$selectableUl.prev(),
                            \$selectionSearch = that.\$selectionUl.prev(),
                            selectableSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = \$selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.\$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = \$selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.\$selectionUl.focus();
                                    return false;
                                }
                            });
                        that.qs3 = \$('#form_roles').quicksearch(selectableSearchString, {
                            'testQuery': function (query, txt, row) {
                                var v = \$(row).data(\"roles\");
                                if (v === \"\" || Array.isArray(v)) {
                                    return query[0] === \"\";
                                } else {
                                    console.log(\$(row).data(\"roles\"));
                                    // change single quotes to double (double is required for JSON), because we had to change them to play nice with form style
                                    var d = \$(row).data(\"roles\").replace(/'/g, \"\\\"\");
                                    var r = JSON.parse(d);
                                    for (var i = 0; i < r.length; i++) {
                                        console.log(r[i]);
                                        if (r[i] == query[0]) {
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                            }
                        }).on('change', function (e) {
                            if (e.which === 40) {
                                that.\$selectableUl.focus();
                                return false;
                            }
                        });


                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                });
                //change the size of the two selectable tables

                \$('.ms-container').css('width', '100%');
                \$('.ms-list').addClass('form-control');
            });

        </script>



    {% endblock %}



{% endblock %}", "role/admin/user_group/edit.html.twig", "/code/templates/role/admin/user_group/edit.html.twig");
    }
}
