<?php

/* role/admin/user/add.html.twig */
class __TwigTemplate_14c9dce8744fec4c0805103ed04757f1858140770fa5f4f1bd918bb0ed685582 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user/add.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/add.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"page-title\">
                        <div class=\"title_left\">
                            <h3>New Users </h3>
                        </div>

                    </div>

                    <div class=\"clearfix\"></div>

                    <div>

                        ";
        // line 21
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->source); })()), "vars", []), "valid", [])) {
            // line 22
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 23
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 23, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 26
        echo "
                        <br/>
                        ";
        // line 28
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->source); })()), "username", []), 'label');
        echo "
                                </div>
                                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 35, $this->source); })()), "username", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->source); })()), "username", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 44, $this->source); })()), "firstName", []), 'label');
        echo "
                                </div>
                                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 46, $this->source); })()), "firstName", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 48, $this->source); })()), "firstName", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 55, $this->source); })()), "lastName", []), 'label');
        echo "
                                </div>
                                ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 57, $this->source); })()), "lastName", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 59, $this->source); })()), "lastName", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 66
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 66, $this->source); })()), "cardId", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                                </div>
                                ";
        // line 68
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 68, $this->source); })()), "cardId", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 70, $this->source); })()), "cardId", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 77, $this->source); })()), "scancode", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "

                                </div>
                                ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 80, $this->source); })()), "scancode", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 82, $this->source); })()), "scancode", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 89, $this->source); })()), "roles", []), 'label');
        echo "
                                </div>
                                ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 91, $this->source); })()), "roles", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 93, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required", "size" => "15"]]);
        // line 96
        echo "
                                </div>
                            </div>


                            <div class=\"ln_solid\"></div>
                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userShowAll'\">Cancel
                                    </button>
                                    ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 107, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "

                                </div>
                            </div>

                        </form>
                        ";
        // line 113
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 113, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 113,  222 => 107,  209 => 96,  207 => 93,  202 => 91,  197 => 89,  187 => 82,  182 => 80,  176 => 77,  166 => 70,  161 => 68,  156 => 66,  146 => 59,  141 => 57,  136 => 55,  126 => 48,  121 => 46,  116 => 44,  106 => 37,  101 => 35,  96 => 33,  88 => 28,  84 => 26,  78 => 23,  75 => 22,  72 => 21,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"page-title\">
                        <div class=\"title_left\">
                            <h3>New Users </h3>
                        </div>

                    </div>

                    <div class=\"clearfix\"></div>

                    <div>

                        {# Checking for form errors and displaying them#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}

                        <br/>
                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}
                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">
                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.username) }}
                                </div>
                                {{ form_errors(form.username) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.username, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.firstName) }}
                                </div>
                                {{ form_errors(form.firstName) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.firstName, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.lastName) }}
                                </div>
                                {{ form_errors(form.lastName) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.lastName, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.cardId,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                                </div>
                                {{ form_errors(form.cardId) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.cardId, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.scancode,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}

                                </div>
                                {{ form_errors(form.scancode) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.scancode, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.roles) }}
                                </div>
                                {{ form_errors(form.roles) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.roles, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required',
                                        'size':'15'

                                    }}) }}
                                </div>
                            </div>


                            <div class=\"ln_solid\"></div>
                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userShowAll'\">Cancel
                                    </button>
                                    {{ form_widget(form.save, {'attr': {'class': 'btn btn-success'}}) }}

                                </div>
                            </div>

                        </form>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
", "role/admin/user/add.html.twig", "/code/templates/role/admin/user/add.html.twig");
    }
}
