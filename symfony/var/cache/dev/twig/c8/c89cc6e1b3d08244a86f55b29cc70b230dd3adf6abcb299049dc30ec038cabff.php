<?php

/* role/admin/user_group/add.html.twig */
class __TwigTemplate_ab641a7134f62e13f6ec4de358d854b2a06aecb6b33850cbd72512beb80f7b2d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user_group/add.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/add.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">

                    <div class=\"x_title\">
                        <h2>New User Group </h2>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">

                        ";
        // line 18
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->source); })()), "vars", []), "valid", [])) {
            // line 19
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 23
        echo "                        <br/>

                        ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 25, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "

                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->source); })()), "name", []), 'label');
        echo "
                                </div>
                                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 35, $this->source); })()), "name", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->source); })()), "name", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 44, $this->source); })()), "description", []), 'label');
        echo "
                                </div>
                                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 46, $this->source); })()), "description", []), 'errors');
        echo "
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 48, $this->source); })()), "description", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                                </div>
                            </div>


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 56, $this->source); })()), "users", []), 'label');
        echo "
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 59, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>

                            </div>


                            <div class=\"form-group\">
                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\" data-color=\"white\">
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 69, $this->source); })()), "users", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                                </div>
                            </div>


                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userGroupShowAll'\">Cancel
                                    </button>
                                    ";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 79, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "
                                </div>
                            </div>

                        </form>
                        ";
        // line 84
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>




    ";
        // line 94
        $this->displayBlock('javascripts', $context, $blocks);
        // line 181
        echo "

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 94
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 95
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            \$(function () {

                \$('#form_users').multiSelect({
                    selectableHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    selectionHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    cssClass: \"form-group\",

                    afterInit: function (ms) {
                        var that = this,
                            \$selectableSearch = that.\$selectableUl.prev(),
                            \$selectionSearch = that.\$selectionUl.prev(),
                            selectableSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = \$selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.\$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = \$selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.\$selectionUl.focus();
                                    return false;
                                }
                            });
                        that.qs3 = \$('#form_roles').quicksearch(selectableSearchString, {
                            'testQuery': function (query, txt, row) {
                                //use v to store data from roles
                                var v = \$(row).data(\"roles\");
                                if (v === \"\" || Array.isArray(v)) {
                                    return query[0] === \"\";
                                } else {
                                    console.log(\$(row).data(\"roles\"));
                                    // change single quotes to double (double is required for JSON), because we had to change them to play nice with form style
                                    var d = \$(row).data(\"roles\").replace(/'/g, \"\\\"\");
                                    var r = JSON.parse(d);
                                    for (var i = 0; i < r.length; i++) {
                                        console.log(r[i]);
                                        if (r[i] == query[0]) {
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                            }
                        }).on('change', function (e) {
                            if (e.which === 40) {
                                that.\$selectableUl.focus();
                                return false;
                            }
                        });


                        var values = \$('#searchable').val();
                        var variableToSend = values;

                        //  alert(values);

                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },

                });
                //change the size of the two selectable tables
                \$('.ms-container').css('width', '100%');
                \$('.ms-list').addClass('form-control');

            });

        </script>



    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user_group/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 95,  205 => 94,  193 => 181,  191 => 94,  178 => 84,  170 => 79,  157 => 69,  144 => 59,  138 => 56,  127 => 48,  122 => 46,  117 => 44,  107 => 37,  102 => 35,  97 => 33,  86 => 25,  82 => 23,  76 => 20,  73 => 19,  70 => 18,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">

                    <div class=\"x_title\">
                        <h2>New User Group </h2>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">

                        {# Checking for form errors and displaying them#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>

                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}

                        <form id=\"demo-form2\" data-parsley-validate class=\"form-horizontal form-label-left\">


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.name) }}
                                </div>
                                {{ form_errors(form.name) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.name, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>

                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.description) }}
                                </div>
                                {{ form_errors(form.description) }}
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.description, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                                </div>
                            </div>


                            <div class=\"form-group\">

                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                    {{ form_label(form.users) }}
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.roles, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>

                            </div>


                            <div class=\"form-group\">
                                <div class=\"control-label col-md-3 col-sm-3 col-xs-12\" data-color=\"white\">
                                </div>
                                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                    {{ form_widget(form.users, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                                </div>
                            </div>


                            <div class=\"form-group\">
                                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                    <button class=\"btn btn-secondary\" type=\"button\"
                                            onclick=\"location.href='./userGroupShowAll'\">Cancel
                                    </button>
                                    {{ form_widget(form.save, {'attr': {'class': 'btn btn-success'}}) }}
                                </div>
                            </div>

                        </form>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>




    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">
            \$(function () {

                \$('#form_users').multiSelect({
                    selectableHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    selectionHeader: \"<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>\",
                    cssClass: \"form-group\",

                    afterInit: function (ms) {
                        var that = this,
                            \$selectableSearch = that.\$selectableUl.prev(),
                            \$selectionSearch = that.\$selectionUl.prev(),
                            selectableSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.\$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = \$selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.\$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = \$selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.\$selectionUl.focus();
                                    return false;
                                }
                            });
                        that.qs3 = \$('#form_roles').quicksearch(selectableSearchString, {
                            'testQuery': function (query, txt, row) {
                                //use v to store data from roles
                                var v = \$(row).data(\"roles\");
                                if (v === \"\" || Array.isArray(v)) {
                                    return query[0] === \"\";
                                } else {
                                    console.log(\$(row).data(\"roles\"));
                                    // change single quotes to double (double is required for JSON), because we had to change them to play nice with form style
                                    var d = \$(row).data(\"roles\").replace(/'/g, \"\\\"\");
                                    var r = JSON.parse(d);
                                    for (var i = 0; i < r.length; i++) {
                                        console.log(r[i]);
                                        if (r[i] == query[0]) {
                                            return true;
                                        }
                                    }
                                    return false;
                                }
                            }
                        }).on('change', function (e) {
                            if (e.which === 40) {
                                that.\$selectableUl.focus();
                                return false;
                            }
                        });


                        var values = \$('#searchable').val();
                        var variableToSend = values;

                        //  alert(values);

                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },

                });
                //change the size of the two selectable tables
                \$('.ms-container').css('width', '100%');
                \$('.ms-list').addClass('form-control');

            });

        </script>



    {% endblock %}


{% endblock %}
", "role/admin/user_group/add.html.twig", "/code/templates/role/admin/user_group/add.html.twig");
    }
}
