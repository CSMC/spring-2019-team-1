<?php

/* role/instructor/session/history_by_time.html.twig */
class __TwigTemplate_77664999555f3162e970f662a21ae25bd5f6527358bb19137ad0e7c5872105fb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/history_by_time.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/history_by_time.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/history_by_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"content_head\">
        <h2 class=\"content_title\">Scheduled Sessions</h2>
    </div>
    <table class=\"content_table\">
        <tr class=\"content_table_head\">
            <th class=\"content_table_cell\">Section(s)</th>
            <th class=\"content_table_cell\">Topic</th>
            <th class=\"content_table_cell\">Description</th>
            <th class=\"content_table_cell\">Grades</th>
        </tr>
        ";
        // line 13
        if (twig_test_empty((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 13, $this->source); })()))) {
            // line 14
            echo "            <tr class=\"content_table_data\">
                <td class=\"content_table_cell\" colspan=\"7\">No sessions.</td>
            </tr>
        ";
        } else {
            // line 18
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 18, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
                // line 19
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">
                        ";
                // line 21
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["session"], "sections", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                    // line 22
                    echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                    // line 23
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                        // line 24
                        echo ", ";
                    }
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                    </td>
                    <td class=\"content_table_cell\">";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "description", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">
                        <a href=\"";
                // line 31
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades", ["id" => twig_get_attribute($this->env, $this->source, $context["session"], "id", [])]), "html", null, true);
                echo "\">
                            <div class=\"button green_button\">View</div>
                        </a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "        ";
        }
        // line 38
        echo "    </table>
    <br>
    <div class=\"content_head\">
        <h2 class=\"content_title\">Quizzes</h2>
    </div>
    <table class=\"content_table\">
        <tr class=\"content_table_head\">
            <th class=\"content_table_cell\">Section(s)</th>
            <th class=\"content_table_cell\">Topic</th>
            <th class=\"content_table_cell\">Description</th>
            <th class=\"content_table_cell\">Grades</th>
            <th class=\"content_table_cell\">Check In Time</th>
            <th class=\"content_table_cell\">Check Out Time</th>
            <th class=\"content_table_cell\">Grade</th>
        </tr>
        ";
        // line 53
        if (twig_test_empty((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 53, $this->source); })()))) {
            // line 54
            echo "            <tr class=\"content_table_data\">
                <td class=\"content_table_cell\" colspan=\"7\">No quizzes.</td>
            </tr>
        ";
        } else {
            // line 58
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 58, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
                // line 59
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">
                        ";
                // line 61
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["quiz"], "sections", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                    // line 62
                    echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                    // line 63
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                        // line 64
                        echo ", ";
                    }
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 67
                echo "                    </td>
                    <td class=\"content_table_cell\">";
                // line 68
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 69
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">
                        <a href=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades", ["id" => twig_get_attribute($this->env, $this->source, $context["quiz"], "id", [])]), "html", null, true);
                echo "\">
                            <div class=\"button green_button\">Edit</div>
                        </a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "        ";
        }
        // line 78
        echo "    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/history_by_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 78,  239 => 77,  227 => 71,  222 => 69,  218 => 68,  215 => 67,  200 => 64,  198 => 63,  196 => 62,  179 => 61,  175 => 59,  170 => 58,  164 => 54,  162 => 53,  145 => 38,  142 => 37,  130 => 31,  125 => 29,  121 => 28,  118 => 27,  103 => 24,  101 => 23,  99 => 22,  82 => 21,  78 => 19,  73 => 18,  67 => 14,  65 => 13,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <div class=\"content_head\">
        <h2 class=\"content_title\">Scheduled Sessions</h2>
    </div>
    <table class=\"content_table\">
        <tr class=\"content_table_head\">
            <th class=\"content_table_cell\">Section(s)</th>
            <th class=\"content_table_cell\">Topic</th>
            <th class=\"content_table_cell\">Description</th>
            <th class=\"content_table_cell\">Grades</th>
        </tr>
        {% if sessions is empty %}
            <tr class=\"content_table_data\">
                <td class=\"content_table_cell\" colspan=\"7\">No sessions.</td>
            </tr>
        {% else %}
            {% for session in sessions %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">
                        {% for section in session.sections %}
                            {{- section -}}
                            {%- if not loop.last -%}
                                {{- \", \" -}}
                            {%- endif -%}
                        {% endfor %}
                    </td>
                    <td class=\"content_table_cell\">{{ session.topic }}</td>
                    <td class=\"content_table_cell\">{{ session.description }}</td>
                    <td class=\"content_table_cell\">
                        <a href=\"{{ path('session_grades', {'id': session.id}) }}\">
                            <div class=\"button green_button\">View</div>
                        </a>
                    </td>
                </tr>
            {% endfor %}
        {% endif %}
    </table>
    <br>
    <div class=\"content_head\">
        <h2 class=\"content_title\">Quizzes</h2>
    </div>
    <table class=\"content_table\">
        <tr class=\"content_table_head\">
            <th class=\"content_table_cell\">Section(s)</th>
            <th class=\"content_table_cell\">Topic</th>
            <th class=\"content_table_cell\">Description</th>
            <th class=\"content_table_cell\">Grades</th>
            <th class=\"content_table_cell\">Check In Time</th>
            <th class=\"content_table_cell\">Check Out Time</th>
            <th class=\"content_table_cell\">Grade</th>
        </tr>
        {% if quizzes is empty %}
            <tr class=\"content_table_data\">
                <td class=\"content_table_cell\" colspan=\"7\">No quizzes.</td>
            </tr>
        {% else %}
            {% for quiz in quizzes %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">
                        {% for section in quiz.sections %}
                            {{- section -}}
                            {%- if not loop.last -%}
                                {{- \", \" -}}
                            {%- endif -%}
                        {% endfor %}
                    </td>
                    <td class=\"content_table_cell\">{{ quiz.topic }}</td>
                    <td class=\"content_table_cell\">{{ quiz.description }}</td>
                    <td class=\"content_table_cell\">
                        <a href=\"{{ path('session_grades', {'id': quiz.id}) }}\">
                            <div class=\"button green_button\">Edit</div>
                        </a>
                    </td>
                </tr>
            {% endfor %}
        {% endif %}
    </table>
{% endblock %}", "role/instructor/session/history_by_time.html.twig", "/code/templates/role/instructor/session/history_by_time.html.twig");
    }
}
