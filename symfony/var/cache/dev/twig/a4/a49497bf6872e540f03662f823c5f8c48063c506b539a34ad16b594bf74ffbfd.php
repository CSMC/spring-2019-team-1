<?php

/* role/admin/specialty.html.twig */
class __TwigTemplate_c835cb3d06ee7afc9226208707fce27f14b20dec734b615d07011c9c4fabf992 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/specialty.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/specialty.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/specialty.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<style>

</style>


<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/view_summary.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
<div class=\"container\">
    <div class=\"row title\">
        <div class=\"col-md-4\">
        <h1>Mentor Management Specialty</h1>
        </div>
        <div class=\"col-md-8 text-right pr-1\">

        <div class=\"search-wrap\">
           <div class=\"search\">
              <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
              <button type=\"submit\" class=\"searchButton\">
                <i class=\"fa fa-search\"></i>
             </button>
           </div>
        </div>
        </div>
    </div>
    <div class=\"\">
      <div class=\"row\">
        <div id=\"summary\">
            <div class=\"row table-outer\">
                <table class=\"table table-hover\" id=\"summaryTable\" width=\"100%\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Preferred Name</th>
                            ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["subjects"]) || array_key_exists("subjects", $context) ? $context["subjects"] : (function () { throw new Twig_Error_Runtime('Variable "subjects" does not exist.', 37, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
            // line 38
            echo "                                <th class=\"subjects\" id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subject"], "id", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subject"], "name", []), "html", null, true);
            echo "</th>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                            <th class=\"col-md-2\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mentors"]) || array_key_exists("mentors", $context) ? $context["mentors"] : (function () { throw new Twig_Error_Runtime('Variable "mentors" does not exist.', 44, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
            // line 45
            echo "                        <tr id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "id", []), "html", null, true);
            echo "\">
                            <td class=\"col-md-2\">
                                ";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "firstName", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "lastName", []), "html", null, true);
            echo "
                            </td>

                            <td class=\"col-md-2\">
                                <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
            echo "\"
                                   target=\"_blank\">";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "preferredName", []), "html", null, true);
            echo "
                                </a>
                            </td>

                            ";
            // line 56
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["subjects"]) || array_key_exists("subjects", $context) ? $context["subjects"] : (function () { throw new Twig_Error_Runtime('Variable "subjects" does not exist.', 56, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
                // line 57
                echo "                                ";
                $context["break"] = false;
                // line 58
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "specialties", []));
                foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                    if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new Twig_Error_Runtime('Variable "break" does not exist.', 58, $this->source); })())) {
                        // line 59
                        echo "                                    ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "name", []) == twig_get_attribute($this->env, $this->source, $context["subject"], "name", []))) {
                            // line 60
                            echo "                                        <td class=\"col-md-1 ";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subject"], "id", []), "html", null, true);
                            echo "\" rowspan=\"1\" align=\"center\">
                                            <div class=\"specialty-level\" style=\"display:inline\">
                                                ";
                            // line 62
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["specialty"], "rating", []), "html", null, true);
                            echo "
                                            </div>
                                            <div style=\"display:inline\">
                                                <p class=\"glyphicon glyphicon-plus-sign modify-specialty increase-specialty-level\"
                                                data-toggle=\"tooltip\" title=\"Increase specialty level\" style=\"font-size:small;display:none\"></p>
                                                <p class=\"glyphicon glyphicon-minus-sign modify-specialty decrease-specialty-level\"
                                                data-toggle=\"tooltip\" title=\"Decrease specialty level\" style=\"font-size:small;display:none\"></p>
                                            </div>
                                        </td>
                                        ";
                            // line 71
                            $context["break"] = true;
                            // line 72
                            echo "                                    ";
                        }
                        // line 73
                        echo "                                ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo "                                ";
                if ( !(isset($context["break"]) || array_key_exists("break", $context) ? $context["break"] : (function () { throw new Twig_Error_Runtime('Variable "break" does not exist.', 74, $this->source); })())) {
                    // line 75
                    echo "                                    <td class=\"col-md-1 ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subject"], "id", []), "html", null, true);
                    echo "\" rowspan=\"1\" align=\"center\">
                                        <div class=\"specialty-level\" style=\"display:inline\">
                                            -
                                        </div>
                                        <div style=\"display:inline\">
                                            <p class=\"glyphicon glyphicon-plus-sign modify-specialty increase-specialty-level\"
                                            data-toggle=\"tooltip\" title=\"Increase specialty level\" style=\"font-size:small;display:none\"></p>
                                            <p class=\"glyphicon glyphicon-minus-sign modify-specialty decrease-specialty-level\"
                                            data-toggle=\"tooltip\" title=\"Decrease specialty level\" style=\"font-size:small;display:none\"></p>
                                        </div>
                                    </td>
                                ";
                }
                // line 87
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "                            <td>
                                <div class=\"col-md-1 actions\" style=\"display:inline\">
                                    <button class=\"btn btn-success edit-specialty\" style=\"display:inline\">Edit</button>
                                    <button class=\"btn btn-success apply-changes\" style=\"display:none\">Apply</button>
                                    <button class=\"btn btn-default cancel-changes\" style=\"display:none\">Cancel</button>
                                    <p class=\"response-message-short\" style=\"display:none\"></p>
                                </div>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "                    </tbody>
                </table>
            </div>
      </div>
    </div>
</div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 108
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 109
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

    <script>
    \$(document).ready(function() {
        var register_actions = function() {
            \$('.edit-specialty').on('click', function() {
                \$(this).hide();
                \$(this).closest('tr').find('.modify-specialty').show();
                \$(this).closest('tr').find('.apply-changes').show();
                \$(this).closest('tr').find('.cancel-changes').show();
            });

            \$('.increase-specialty-level').on('click', function() {
                var specialty = \$(this).closest('td').find('.specialty-level');
                var level = specialty.text();
                if (\$.isNumeric(level)) {
                    if (level != 5) {
                        level++;
                        specialty.text(level)
                    }
                }
                else {
                    level = 1;
                    specialty.text(level)
                }
            });

            \$('.decrease-specialty-level').on('click', function() {
                var specialty = \$(this).closest('td').find('.specialty-level');
                var level = specialty.text();
                if (\$.isNumeric(level)) {
                    if (level != 1) {
                        level--;
                        specialty.text(level)
                    }
                }
                else {
                    level = 1;
                    specialty.text(level)
                }
            });

            \$('.apply-changes').on('click', function() {
                var s = {};
                var subjects = \$('.subjects')
                for (i = 0; i < subjects.length; i++) {
                    var k = '.'+subjects[i].id+' .specialty-level';
                    var v = \$(this).closest('tr').find(k).text().trim();
                    if (\$.isNumeric(v)) {
                        s[subjects[i].textContent] = v;
                    }
                }

                var specialties = JSON.stringify(s);
                var message = \$(this).closest('tr').find('.response-message-short');
                var apply = \$(this).closest('tr').find('.apply-changes');
                var cancel = \$(this).closest('tr').find('.cancel-changes');
                var editBtn = \$(this).closest('tr').find('.edit-specialty');
                var modifyBtns = \$(this).closest('tr').find('.modify-specialty');
                \$.ajax({
                    type: 'POST',
                    url: \"";
        // line 171
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_modify_specialties");
        echo "\",
                    data: {
                        'user': \$(this).closest('tr').attr('id'),
                        'specialties': specialties
                    },
                    complete: function(response) {
                        message.text(response.responseText).show();
                        message.fadeOut(1500);
                        modifyBtns.hide();
                        apply.hide();
                        cancel.hide();
                        editBtn.show();
                    }
                });
            });

            \$('.cancel-changes').on('click', function() {
                \$(this).hide();
                \$(this).closest('tr').find('.apply-changes').hide();
                \$(this).closest('tr').find('.edit-specialty').show();
                \$(this).closest('tr').find('.modify-specialty').hide();
            });
        };

        let summaryTable = \$('#summaryTable').DataTable({
            aaSorting: [],
            language: {
                \"paginate\": {
                    \"previous\": \"<<\",
                    \"next\": \">>\"
                },

            },
            dom: '<clear>rtp',
            lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, \"All\"] ],
            draw: register_actions()
        });
        
        \$(\".searchTerm\").on( 'keyup change', function () {
            summaryTable.search( this.value ).draw();
        } );
    });
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/specialty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 171,  256 => 109,  247 => 108,  228 => 98,  213 => 88,  207 => 87,  191 => 75,  188 => 74,  181 => 73,  178 => 72,  176 => 71,  164 => 62,  158 => 60,  155 => 59,  149 => 58,  146 => 57,  142 => 56,  135 => 52,  131 => 51,  122 => 47,  116 => 45,  112 => 44,  106 => 40,  95 => 38,  91 => 37,  61 => 10,  54 => 5,  45 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends 'role/admin/base.html.twig' %}

{% block body %}
<style>

</style>


<link href=\"{{ asset('build/css/view_summary.css') }}\" rel=\"stylesheet\" />
<div class=\"container\">
    <div class=\"row title\">
        <div class=\"col-md-4\">
        <h1>Mentor Management Specialty</h1>
        </div>
        <div class=\"col-md-8 text-right pr-1\">

        <div class=\"search-wrap\">
           <div class=\"search\">
              <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
              <button type=\"submit\" class=\"searchButton\">
                <i class=\"fa fa-search\"></i>
             </button>
           </div>
        </div>
        </div>
    </div>
    <div class=\"\">
      <div class=\"row\">
        <div id=\"summary\">
            <div class=\"row table-outer\">
                <table class=\"table table-hover\" id=\"summaryTable\" width=\"100%\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Preferred Name</th>
                            {% for subject in subjects %}
                                <th class=\"subjects\" id=\"{{ subject.id }}\">{{ subject.name }}</th>
                            {% endfor %}
                            <th class=\"col-md-2\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for mentor in mentors %}
                        <tr id=\"{{ mentor.id }}\">
                            <td class=\"col-md-2\">
                                {{ mentor.firstName }} {{ mentor.lastName }}
                            </td>

                            <td class=\"col-md-2\">
                                <a href=\"{{ path('profile', {'username': mentor.username}) }}\"
                                   target=\"_blank\">{{ mentor.preferredName }}
                                </a>
                            </td>

                            {% for subject in subjects %}
                                {% set break = false %}
                                {% for specialty in mentor.profile.specialties if not break %}
                                    {% if specialty.subject.name == subject.name %}
                                        <td class=\"col-md-1 {{ subject.id }}\" rowspan=\"1\" align=\"center\">
                                            <div class=\"specialty-level\" style=\"display:inline\">
                                                {{ specialty.rating }}
                                            </div>
                                            <div style=\"display:inline\">
                                                <p class=\"glyphicon glyphicon-plus-sign modify-specialty increase-specialty-level\"
                                                data-toggle=\"tooltip\" title=\"Increase specialty level\" style=\"font-size:small;display:none\"></p>
                                                <p class=\"glyphicon glyphicon-minus-sign modify-specialty decrease-specialty-level\"
                                                data-toggle=\"tooltip\" title=\"Decrease specialty level\" style=\"font-size:small;display:none\"></p>
                                            </div>
                                        </td>
                                        {% set break = true %}
                                    {% endif %}
                                {% endfor %}
                                {% if not break %}
                                    <td class=\"col-md-1 {{ subject.id }}\" rowspan=\"1\" align=\"center\">
                                        <div class=\"specialty-level\" style=\"display:inline\">
                                            -
                                        </div>
                                        <div style=\"display:inline\">
                                            <p class=\"glyphicon glyphicon-plus-sign modify-specialty increase-specialty-level\"
                                            data-toggle=\"tooltip\" title=\"Increase specialty level\" style=\"font-size:small;display:none\"></p>
                                            <p class=\"glyphicon glyphicon-minus-sign modify-specialty decrease-specialty-level\"
                                            data-toggle=\"tooltip\" title=\"Decrease specialty level\" style=\"font-size:small;display:none\"></p>
                                        </div>
                                    </td>
                                {% endif %}
                            {% endfor %}
                            <td>
                                <div class=\"col-md-1 actions\" style=\"display:inline\">
                                    <button class=\"btn btn-success edit-specialty\" style=\"display:inline\">Edit</button>
                                    <button class=\"btn btn-success apply-changes\" style=\"display:none\">Apply</button>
                                    <button class=\"btn btn-default cancel-changes\" style=\"display:none\">Cancel</button>
                                    <p class=\"response-message-short\" style=\"display:none\"></p>
                                </div>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
      </div>
    </div>
</div>



{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

    <script>
    \$(document).ready(function() {
        var register_actions = function() {
            \$('.edit-specialty').on('click', function() {
                \$(this).hide();
                \$(this).closest('tr').find('.modify-specialty').show();
                \$(this).closest('tr').find('.apply-changes').show();
                \$(this).closest('tr').find('.cancel-changes').show();
            });

            \$('.increase-specialty-level').on('click', function() {
                var specialty = \$(this).closest('td').find('.specialty-level');
                var level = specialty.text();
                if (\$.isNumeric(level)) {
                    if (level != 5) {
                        level++;
                        specialty.text(level)
                    }
                }
                else {
                    level = 1;
                    specialty.text(level)
                }
            });

            \$('.decrease-specialty-level').on('click', function() {
                var specialty = \$(this).closest('td').find('.specialty-level');
                var level = specialty.text();
                if (\$.isNumeric(level)) {
                    if (level != 1) {
                        level--;
                        specialty.text(level)
                    }
                }
                else {
                    level = 1;
                    specialty.text(level)
                }
            });

            \$('.apply-changes').on('click', function() {
                var s = {};
                var subjects = \$('.subjects')
                for (i = 0; i < subjects.length; i++) {
                    var k = '.'+subjects[i].id+' .specialty-level';
                    var v = \$(this).closest('tr').find(k).text().trim();
                    if (\$.isNumeric(v)) {
                        s[subjects[i].textContent] = v;
                    }
                }

                var specialties = JSON.stringify(s);
                var message = \$(this).closest('tr').find('.response-message-short');
                var apply = \$(this).closest('tr').find('.apply-changes');
                var cancel = \$(this).closest('tr').find('.cancel-changes');
                var editBtn = \$(this).closest('tr').find('.edit-specialty');
                var modifyBtns = \$(this).closest('tr').find('.modify-specialty');
                \$.ajax({
                    type: 'POST',
                    url: \"{{ path('admin_modify_specialties') }}\",
                    data: {
                        'user': \$(this).closest('tr').attr('id'),
                        'specialties': specialties
                    },
                    complete: function(response) {
                        message.text(response.responseText).show();
                        message.fadeOut(1500);
                        modifyBtns.hide();
                        apply.hide();
                        cancel.hide();
                        editBtn.show();
                    }
                });
            });

            \$('.cancel-changes').on('click', function() {
                \$(this).hide();
                \$(this).closest('tr').find('.apply-changes').hide();
                \$(this).closest('tr').find('.edit-specialty').show();
                \$(this).closest('tr').find('.modify-specialty').hide();
            });
        };

        let summaryTable = \$('#summaryTable').DataTable({
            aaSorting: [],
            language: {
                \"paginate\": {
                    \"previous\": \"<<\",
                    \"next\": \">>\"
                },

            },
            dom: '<clear>rtp',
            lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, \"All\"] ],
            draw: register_actions()
        });
        
        \$(\".searchTerm\").on( 'keyup change', function () {
            summaryTable.search( this.value ).draw();
        } );
    });
    </script>

{% endblock %}", "role/admin/specialty.html.twig", "/code/templates/role/admin/specialty.html.twig");
    }
}
