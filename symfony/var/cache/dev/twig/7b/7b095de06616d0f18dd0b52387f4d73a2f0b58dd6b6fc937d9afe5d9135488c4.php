<?php

/* role/mentor/schedule/timesheet.html.twig */
class __TwigTemplate_c9e4f2d60ddf2bd87da717606e9e97da56e968ed9e93abc790f8c7b3c4e0d00b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/schedule/timesheet.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/timesheet.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/timesheet.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"content_head\">
        <h2 class=\"content_title\">Times for Week of ";
        // line 4
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 4, $this->source); })()), 0, [], "array"), "day", [], "array"), "m/d/Y"), "html", null, true);
        echo "</h2> ";
        // line 5
        echo "        <div class=\"content_options\">
            ";
        // line 6
        $context["prev"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 6, $this->source); })()), 0, [], "array"), "day", [], "array");
        // line 7
        echo "            ";
        $context["prev"] = twig_date_modify_filter($this->env, (isset($context["prev"]) || array_key_exists("prev", $context) ? $context["prev"] : (function () { throw new Twig_Error_Runtime('Variable "prev" does not exist.', 7, $this->source); })()), "-7 day");
        // line 8
        echo "            ";
        $context["next"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 8, $this->source); })()), 0, [], "array"), "day", [], "array");
        // line 9
        echo "            ";
        $context["next"] = twig_date_modify_filter($this->env, (isset($context["next"]) || array_key_exists("next", $context) ? $context["next"] : (function () { throw new Twig_Error_Runtime('Variable "next" does not exist.', 9, $this->source); })()), "+7 day");
        // line 10
        echo "            <a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule_timesheet", ["date" => twig_date_format_filter($this->env, (isset($context["prev"]) || array_key_exists("prev", $context) ? $context["prev"] : (function () { throw new Twig_Error_Runtime('Variable "prev" does not exist.', 10, $this->source); })()), "Y-m-d")]), "html", null, true);
        echo "\">
                <div class=\"content_option button green_button\">Previous</div>
            </a>
            <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule_timesheet", ["date" => twig_date_format_filter($this->env, (isset($context["next"]) || array_key_exists("next", $context) ? $context["next"] : (function () { throw new Twig_Error_Runtime('Variable "next" does not exist.', 13, $this->source); })()), "Y-m-d")]), "html", null, true);
        echo "\">
                <div class=\"content_option button green_button\">Next</div>
            </a>
        </div>
    </div>
    ";
        // line 19
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 19, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["day"]) {
            // line 20
            echo "        <div class=\"content_head\">
            <h3 class=\"content_title\">";
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["day"], "day", [], "array"), "l, m/d/Y"), "html", null, true);
            echo "</h3>
        </div>
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <td class=\"content_table_cell\">Time In</td>
                <td class=\"content_table_cell\">Time Out</td>
            </tr>
            ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["day"], "times", [], "array"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["time"]) {
                // line 29
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">";
                // line 30
                echo twig_escape_filter($this->env, (( !(null === twig_get_attribute($this->env, $this->source, $context["time"], "timeIn", []))) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "timeIn", []), "g:i A")) : ("")), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 31
                echo twig_escape_filter($this->env, (( !(null === twig_get_attribute($this->env, $this->source, $context["time"], "timeOut", []))) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "timeOut", []), "g:i A")) : ("")), "html", null, true);
                echo "</td>
                </tr>
            ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 34
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"2\">You have no times</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['time'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "        </table>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['day'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/schedule/timesheet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 38,  126 => 34,  118 => 31,  114 => 30,  111 => 29,  106 => 28,  96 => 21,  93 => 20,  88 => 19,  80 => 13,  73 => 10,  70 => 9,  67 => 8,  64 => 7,  62 => 6,  59 => 5,  56 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <div class=\"content_head\">
        <h2 class=\"content_title\">Times for Week of {{ timesheet[0]['day']|date('m/d/Y') }}</h2> {# TODO: better title #}
        <div class=\"content_options\">
            {% set prev = timesheet[0]['day'] %}
            {% set prev = prev|date_modify(\"-7 day\") %}
            {% set next = timesheet[0]['day'] %}
            {% set next = next|date_modify(\"+7 day\") %}
            <a href=\"{{ path('schedule_timesheet', {'date': prev|date('Y-m-d')}) }}\">
                <div class=\"content_option button green_button\">Previous</div>
            </a>
            <a href=\"{{ path('schedule_timesheet', {'date': next|date('Y-m-d')}) }}\">
                <div class=\"content_option button green_button\">Next</div>
            </a>
        </div>
    </div>
    {#<div class=\"message\">#}{# TODO: add message about times #}{#</div>#}
    {% for day in timesheet %}
        <div class=\"content_head\">
            <h3 class=\"content_title\">{{ day['day']|date('l, m/d/Y') }}</h3>
        </div>
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <td class=\"content_table_cell\">Time In</td>
                <td class=\"content_table_cell\">Time Out</td>
            </tr>
            {% for time in day['times'] %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">{{ time.timeIn is not null ? time.timeIn|date('g:i A') }}</td>
                    <td class=\"content_table_cell\">{{ time.timeOut is not null ? time.timeOut|date('g:i A') }}</td>
                </tr>
            {% else %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"2\">You have no times</td>
                </tr>
            {% endfor %}
        </table>
    {% endfor %}
{% endblock %}", "role/mentor/schedule/timesheet.html.twig", "/code/templates/role/mentor/schedule/timesheet.html.twig");
    }
}
