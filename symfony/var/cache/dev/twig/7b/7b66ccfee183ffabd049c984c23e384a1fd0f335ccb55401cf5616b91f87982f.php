<?php

/* shared/swipe/walk_in.html.twig */
class __TwigTemplate_7eb1f82f1f4fea4609c0a3659e5ff0513ea9c6c3830dac80173f5fde0f70a1d8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/swipe/walk_in.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/swipe/walk_in.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/favicon.png"), "html", null, true);
        echo "\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/global.css"), "html", null, true);
        echo "\"/>
</head>
<body style=\"background: #FFFFFF url(";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/swipe-background.png"), "html", null, true);
        echo ") no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/logo-csmc.png"), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"row\">
                            <div class=\"col-xs-3\"></div>
                            <div class=\"col-xs-6\">
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h2>Welcome! Sign In Here</h2>
                                    </div>
                                    <div class=\"panel-body\">
                                        ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 45, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                                        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 46, $this->source); })()), 'errors');
        echo "
                                        <div id=\"success\" class=\"alert alert-success\" style=\"display: none;\"></div>
                                        <div class=\"form-group\">
                                            ";
        // line 50
        echo "                                            ";
        // line 51
        echo "                                            ";
        // line 52
        echo "                                            ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 52, $this->source); })()), "scancode", []), 'errors');
        echo "
                                            <div>
                                                <div id=\"loader\" class=\"text-center\" style=\"display: none;\">
                                                    <img src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                                                </div>
                                                ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 57, $this->source); })()), "scancode", []), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Please swipe your Comet Card"]]);
        echo "
                                            </div>
                                        </div>
                                        ";
        // line 60
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 60, $this->source); })()), 'form_end');
        echo "
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-3\"></div>
                        </div>
                        <div class=\"col-xs-3\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"entry-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"entry-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"entry-modal-label\">Entry Survey</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"entry-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"entry-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                    </div>
                    ";
        // line 86
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 86, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 89, $this->source); })()), "topic", []), 'label');
        echo "
                        </div>
                        ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 91, $this->source); })()), "topic", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 93, $this->source); })()), "topic", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 99, $this->source); })()), "activity", []), 'label');
        echo "
                        </div>
                        ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 101, $this->source); })()), "activity", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 103, $this->source); })()), "activity", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 109, $this->source); })()), "course", []), 'label');
        echo "
                        </div>
                        ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 111, $this->source); })()), "course", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 113
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 113, $this->source); })()), "course", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 119, $this->source); })()), "quiz", []), 'label');
        echo "
                        </div>
                        ";
        // line 121
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 121, $this->source); })()), "quiz", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 123
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 123, $this->source); })()), "quiz", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            ";
        // line 125
        echo "                        </div>
                    </div>
                    ";
        // line 127
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["entry_form"]) || array_key_exists("entry_form", $context) ? $context["entry_form"] : (function () { throw new Twig_Error_Runtime('Variable "entry_form" does not exist.', 127, $this->source); })()), 'form_end');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"entry-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"exit-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exit-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"exit-modal-label\">Exit Survey</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"exit-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"exit-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                    </div>
                    ";
        // line 150
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 150, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 153
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 153, $this->source); })()), "mentors", []), 'label');
        echo "
                        </div>
                        ";
        // line 155
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 155, $this->source); })()), "mentors", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 157
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 157, $this->source); })()), "mentors", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 163
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 163, $this->source); })()), "feedback", []), 'label');
        echo "
                        </div>
                        ";
        // line 165
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 165, $this->source); })()), "feedback", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 167
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 167, $this->source); })()), "feedback", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        </div>
                    </div>
                    ";
        // line 170
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["exit_form"]) || array_key_exists("exit_form", $context) ? $context["exit_form"] : (function () { throw new Twig_Error_Runtime('Variable "exit_form" does not exist.', 170, $this->source); })()), 'form_end');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"exit-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"register-modal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"register-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"register-modal-label\">Register Comet Card</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"register-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"register-warning\" class=\"alert alert-warning\">
                        <strong>Card not registered!</strong>
                        Please enter your NetID and password to register this card.
                    </div>
                    <div id=\"register-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                    </div>
                    ";
        // line 198
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 198, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 201
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 201, $this->source); })()), "username", []), 'label');
        echo "
                        </div>
                        ";
        // line 203
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 203, $this->source); })()), "username", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 205
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 205, $this->source); })()), "username", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 211
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 211, $this->source); })()), "password", []), 'label');
        echo "
                        </div>
                        ";
        // line 213
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 213, $this->source); })()), "password", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 215
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 215, $this->source); })()), "password", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    ";
        // line 219
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 219, $this->source); })()), 'form_end');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"register-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"error-modal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"error-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"error-modal-label\">Error</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"error-message\" class=\"alert alert-danger\" role=\"alert\">

                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Acknowledge</button>
                </div>
            </div>
        </div>
    </div>

    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
    <script src=\"https://cdn.datatables.net/plug-ins/1.10.16/features/pageResize/dataTables.pageResize.min.js\"></script>
    <script>
        \$(function () {
            var success = \$('#success');

            var register_warning = \$('#register-warning');
            var register_error = \$('#register-error');

            var loader = \$('#loader');
            var register_loader = \$('#register-loader');

            var input = \$('#swipe_scancode');
            input.prop('autofocus', true);
            input.get(0).focus();

            var swipe_form = \$('[name=\"swipe\"]');
            var entry_form = \$('[name=\"walk_in_entry\"]');
            var exit_form = \$('[name=\"walk_in_exit\"]');
            var register_form = \$('[name=\"card_register\"]');

            // swipe
            swipe_form.on('submit', function (e) {
                e.preventDefault();

                success.hide();

                var swipe = input.val();

                register_form.children('#card_register_swipe').val(swipe);

                \$.ajax({
                    url: swipe_form.prop('action'),
                    data: {
                        'scancode': swipe,
                        'session': \$('#swipe_session').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        input.hide();
                        loader.show();
                    }
                }).always(function () {
                    loader.hide();
                    input.show();
                    input.get(0).focus();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'entrance':
                            \$('#entry-modal').modal('show');

                            \$('#walk_in_entry_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'exit':
                            \$('#exit-modal').modal('show');

                            \$('#walk_in_exit_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'mentor_in':

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                        case 'mentor_out':
                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user + '!');
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'malformed_scan':
                            // rescan card again
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Scan failed!</strong> Please scan your card again.');
                            break;
                        case 'unregistered_user':
                            // register card
                            \$('#register-modal').modal('show');
                            break;
                        case 'invalid':
                            // shouldn't happen
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });

                this.reset();
            });

            // entry survey
            entry_form.on('submit', function (e) {
                e.preventDefault();

                \$.ajax({
                    url: entry_form.prop('action'),
                    data: {
                        topic: \$('#walk_in_entry_topic').val(),
                        activity: \$('#walk_in_entry_activity').val(),
                        course: \$('#walk_in_entry_course').val(),
                        user: \$('#walk_in_entry_user').val(),
                        quiz: \$('#walk_in_entry_quiz').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        entry_form.hide();
                        \$('#entry-loader').show();
                    }
                }).always(function () {
                    input.get(0).focus();
                    \$('#entry-loader').hide();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'attendee_in':
                            \$('#entry-modal').modal('hide');

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    //TODO
                });
            });

            \$('#entry-submit').on('click', function () {
                entry_form.submit();
            });

            \$('#entry-modal').on('shown.bs.modal', function () {
                entry_form.show();

                var topic = \$('#walk_in_entry_topic');
                topic.val('');
                topic.get(0).focus();

                var activity = \$('#walk_in_entry_activity');
                activity.val('').change();

                var course = \$('#walk_in_entry_course');
                course.val('').change();

                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]');
                quiz_label.hide();

                var quiz = \$('#walk_in_entry_quiz');
                quiz.val('').change();
                quiz.hide();

                var other_label = \$('label[for=\"walk_in_entry_otherCourse');
                other_label.hide();

                var other = \$('#walk_in_entry_otherCourse');
                other.val('').change();
                other.hide();
            });

            \$('#entry-modal').on('hidden.bs.modal', function () {
                var topic = \$('#walk_in_entry_topic');
                topic.val('');

                var activity = \$('#walk_in_entry_activity');
                activity.val('').change();

                var course = \$('#walk_in_entry_course');
                course.val('').change();

                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]');
                quiz_label.hide();

                var quiz = \$('#walk_in_entry_quiz');
                quiz.val('').change();
                quiz.hide();

                var other_label = \$('label[for=\"walk_in_entry_otherCourse');
                other_label.hide();

                var other = \$('#walk_in_entry_otherCourse');
                other.val('').change();
                other.hide();

                var user = \$('#walk_in_entry_user');
                user.val('');

                input.get(0).focus();
            });

            \$('#walk_in_entry_activity').on('change', function() {
                var option = \$('#walk_in_entry_activity option:selected').text();
                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]')
                var quiz = \$('#walk_in_entry_quiz');
                if(option == 'Take a Quiz') {
                    quiz_label.show();
                    quiz.show();
                } else {;
                    quiz_label.hide();

                    quiz.val('').change();
                    quiz.hide();
                }
            });

            // exit survey
            exit_form.on('submit', function (e) {
                e.preventDefault();

                \$.ajax({
                    url: exit_form.prop('action'),
                    data: {
                        mentors: \$('#walk_in_exit_mentors').val(),
                        feedback: \$('#walk_in_exit_feedback').val(),
                        user: \$('#walk_in_exit_user').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        exit_form.hide();
                        \$('#exit-loader').show();
                    }
                }).always(function () {
                    \$('#exit-loader').hide();
                    input.get(0).focus();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'attendee_out':
                            \$('#exit-modal').modal('hide');

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user + '!');
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // TODO
                });

            });

            \$('#exit-submit').on('click', function () {
                exit_form.submit();
            });

            \$('#exit-modal').on('shown.bs.modal', function () {
                exit_form.show();

                var mentors = \$('#walk_in_exit_mentors');
                mentors.val('').change();

                var feedback = \$('#walk_in_exit_feedback');
                feedback.val('');
            });

            \$('#exit-modal').on('hidden.bs.modal', function () {
                var mentors = \$('#walk_in_exit_mentors');
                mentors.val('').change();

                var feedback = \$('#walk_in_exit_feedback');
                feedback.val('');

                var user = \$('#walk_in_exit_user');
                user.val('');

                input.get(0).focus();
            });

            // register
            register_form.on('submit', function (e) {
                e.preventDefault();

                var username_input = \$('#card_register_username');
                var username = username_input.val();
                // console.log(username);

                var password_input = \$('#card_register_password');
                var password = password_input.val();
                password_input.val('');

                var swipe = \$('#card_register_swipe').val();
                \$.ajax({
                    url: register_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': null,
                        'swipe': swipe
                    },
                    type: 'POST',
                    beforeSend: function () {
                        register_form.hide();
                        register_loader.show();
                    }
                }).always(function () {
                    register_loader.hide();
                }).done(function (data, textStatus, jqXHR) {
                    username_input.val('');
                    \$('#register-modal').modal('hide');

                    ";
        // line 566
        echo "                    switch (jqXHR.responseJSON.message) {
                        case 'entrance':
                            \$('#entry-modal').modal('show');

                            \$('#walk_in_entry_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'mentor_in':
                        ";
        // line 574
        echo "                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Your card has been registered and welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                            ";
        // line 579
        echo "                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    register_form.show();
                    register_warning.hide();
                    register_error.show();

                    // console.log(jqXHR.responseJSON);

                    switch (jqXHR.responseJSON) {
                        case 'unregistered_user':
                        ";
        // line 591
        echo "                            register_error.html('<strong>Card could not be registered!</strong> Please try again.');
                            break;
                        case 'no_user':
                        ";
        // line 595
        echo "                            break;
                        case 'ineligible':
                            // hide modal
                            // normal error
                            break;
                        case 'bad_credentials':
                            register_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        case 'invalid':
                        ";
        // line 605
        echo "                            register_error.html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            register_error.html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });
            });

            \$('#register-submit').on('click', function () {
                register_form.submit();
            });

            var register_enter_pressed = function (e) {
                if (e.keyCode == '13') {
                    register_form.submit();
                }
            };

            \$('#card_register_username').on('keyup', register_enter_pressed);
            \$('#card_register_password').on('keyup', register_enter_pressed);

            register_form.parsley({
                errorsMessagesDisabled: true,
            });

            \$('#register-modal').on('shown.bs.modal', function () {
                register_form.show();
                var username_input = \$('#card_register_username');
                username_input.val('');
                username_input.get(0).focus();

                var password_input = \$('#card_register_password');
                password_input.val('');
            });

            \$('#register-modal').on('hidden.bs.modal', function () {
                var username_input = \$('#card_register_username');
                username_input.val('');

                var password_input = \$('#card_register_password');
                password_input.val('');

                \$('#register-error').hide();
                \$('#register-warning').show();

                input.get(0).focus();
            });

            // floating labels
            var floatingLabel;

            floatingLabel = function (onload) {
                var \$input;
                \$input = \$(this);
                if (onload) {
                    \$.each(\$('.bt-flabels__wrapper input'), function (index, value) {
                        var \$current_input;
                        \$current_input = \$(value);
                        if (\$current_input.val()) {
                            \$current_input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                        }
                    });
                }

                setTimeout((function () {
                    if (\$input.val()) {
                        \$input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                    } else {
                        \$input.closest('.bt-flabels__wrapper').removeClass('bt-flabel__float');
                    }
                }), 1);
            };

            \$('.bt-flabels__wrapper input').keydown(floatingLabel);
            \$('.bt-flabels__wrapper input').change(floatingLabel);

            window.addEventListener('load', floatingLabel(true), false);
            var error = function () {
                \$.each(this.fields, function (key, field) {
                    if (field.validationResult !== true) {
                        field.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                    }
                });
            };

            var validated = function () {
                if (this.validationResult === true) {
                    this.\$element.closest('.bt-flabels__wrapper').removeClass('bt-flabels__error');
                } else {
                    this.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                }
            };

            // no_card_form.parsley().on('form:error', error);
            // no_card_form.parsley().on('field:validated', validated);

            register_form.parsley().on('form:error', error);
            register_form.parsley().on('field:validated', validated);
            // end floating labels
        });
    </script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/swipe/walk_in.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  771 => 605,  760 => 595,  755 => 591,  742 => 579,  736 => 574,  727 => 566,  378 => 219,  371 => 215,  366 => 213,  361 => 211,  352 => 205,  347 => 203,  342 => 201,  336 => 198,  331 => 196,  302 => 170,  296 => 167,  291 => 165,  286 => 163,  277 => 157,  272 => 155,  267 => 153,  261 => 150,  256 => 148,  232 => 127,  228 => 125,  224 => 123,  219 => 121,  214 => 119,  205 => 113,  200 => 111,  195 => 109,  186 => 103,  181 => 101,  176 => 99,  167 => 93,  162 => 91,  157 => 89,  151 => 86,  146 => 84,  119 => 60,  113 => 57,  108 => 55,  101 => 52,  99 => 51,  97 => 50,  91 => 46,  87 => 45,  67 => 28,  56 => 20,  51 => 18,  38 => 8,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('build/images/favicon.png') }}\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/global.css') }}\"/>
</head>
<body style=\"background: #FFFFFF url({{ asset('build/images/swipe-background.png') }}) no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <img src=\"{{ asset(\"build/images/logo-csmc.png\") }}\">
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"row\">
                            <div class=\"col-xs-3\"></div>
                            <div class=\"col-xs-6\">
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h2>Welcome! Sign In Here</h2>
                                    </div>
                                    <div class=\"panel-body\">
                                        {{ form_start(swipe_form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}
                                        {{ form_errors(swipe_form) }}
                                        <div id=\"success\" class=\"alert alert-success\" style=\"display: none;\"></div>
                                        <div class=\"form-group\">
                                            {#<div class=\"control-label\">#}
                                            {#{{ form_label(form.scancode) }}#}
                                            {#</div>#}
                                            {{ form_errors(swipe_form.scancode) }}
                                            <div>
                                                <div id=\"loader\" class=\"text-center\" style=\"display: none;\">
                                                    <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                                                </div>
                                                {{ form_widget(swipe_form.scancode, {'attr': {'class': 'form-control', 'placeholder': 'Please swipe your Comet Card'}}) }}
                                            </div>
                                        </div>
                                        {{ form_end(swipe_form) }}
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-3\"></div>
                        </div>
                        <div class=\"col-xs-3\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"entry-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"entry-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"entry-modal-label\">Entry Survey</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"entry-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"entry-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                    </div>
                    {{ form_start(entry_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(entry_form.topic) }}
                        </div>
                        {{ form_errors(entry_form.topic) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(entry_form.topic, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(entry_form.activity) }}
                        </div>
                        {{ form_errors(entry_form.activity) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(entry_form.activity, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(entry_form.course) }}
                        </div>
                        {{ form_errors(entry_form.course) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(entry_form.course, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(entry_form.quiz) }}
                        </div>
                        {{ form_errors(entry_form.quiz) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(entry_form.quiz, {'attr': {'class': 'form-control'}}) }}
                            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
                        </div>
                    </div>
                    {{ form_end(entry_form) }}
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"entry-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"exit-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exit-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"exit-modal-label\">Exit Survey</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"exit-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"exit-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                    </div>
                    {{ form_start(exit_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(exit_form.mentors) }}
                        </div>
                        {{ form_errors(exit_form.mentors) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(exit_form.mentors, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(exit_form.feedback) }}
                        </div>
                        {{ form_errors(exit_form.feedback) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(exit_form.feedback, {'attr': {'class': 'form-control'}}) }}
                        </div>
                    </div>
                    {{ form_end(exit_form) }}
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"exit-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"register-modal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"register-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"register-modal-label\">Register Comet Card</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"register-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                    <div id=\"register-warning\" class=\"alert alert-warning\">
                        <strong>Card not registered!</strong>
                        Please enter your NetID and password to register this card.
                    </div>
                    <div id=\"register-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                    </div>
                    {{ form_start(register_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(register_form.username) }}
                        </div>
                        {{ form_errors(register_form.username) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(register_form.username, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(register_form.password) }}
                        </div>
                        {{ form_errors(register_form.password) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(register_form.password, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>
                    {{ form_end(register_form) }}
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button id=\"register-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal fade\" id=\"error-modal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"error-modal-label\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                aria-hidden=\"true\">&times;</span></button>
                    <h4 class=\"modal-title\" id=\"error-modal-label\">Error</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"error-message\" class=\"alert alert-danger\" role=\"alert\">

                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Acknowledge</button>
                </div>
            </div>
        </div>
    </div>

    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
    <script src=\"https://cdn.datatables.net/plug-ins/1.10.16/features/pageResize/dataTables.pageResize.min.js\"></script>
    <script>
        \$(function () {
            var success = \$('#success');

            var register_warning = \$('#register-warning');
            var register_error = \$('#register-error');

            var loader = \$('#loader');
            var register_loader = \$('#register-loader');

            var input = \$('#swipe_scancode');
            input.prop('autofocus', true);
            input.get(0).focus();

            var swipe_form = \$('[name=\"swipe\"]');
            var entry_form = \$('[name=\"walk_in_entry\"]');
            var exit_form = \$('[name=\"walk_in_exit\"]');
            var register_form = \$('[name=\"card_register\"]');

            // swipe
            swipe_form.on('submit', function (e) {
                e.preventDefault();

                success.hide();

                var swipe = input.val();

                register_form.children('#card_register_swipe').val(swipe);

                \$.ajax({
                    url: swipe_form.prop('action'),
                    data: {
                        'scancode': swipe,
                        'session': \$('#swipe_session').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        input.hide();
                        loader.show();
                    }
                }).always(function () {
                    loader.hide();
                    input.show();
                    input.get(0).focus();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'entrance':
                            \$('#entry-modal').modal('show');

                            \$('#walk_in_entry_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'exit':
                            \$('#exit-modal').modal('show');

                            \$('#walk_in_exit_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'mentor_in':

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                        case 'mentor_out':
                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user + '!');
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'malformed_scan':
                            // rescan card again
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Scan failed!</strong> Please scan your card again.');
                            break;
                        case 'unregistered_user':
                            // register card
                            \$('#register-modal').modal('show');
                            break;
                        case 'invalid':
                            // shouldn't happen
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });

                this.reset();
            });

            // entry survey
            entry_form.on('submit', function (e) {
                e.preventDefault();

                \$.ajax({
                    url: entry_form.prop('action'),
                    data: {
                        topic: \$('#walk_in_entry_topic').val(),
                        activity: \$('#walk_in_entry_activity').val(),
                        course: \$('#walk_in_entry_course').val(),
                        user: \$('#walk_in_entry_user').val(),
                        quiz: \$('#walk_in_entry_quiz').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        entry_form.hide();
                        \$('#entry-loader').show();
                    }
                }).always(function () {
                    input.get(0).focus();
                    \$('#entry-loader').hide();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'attendee_in':
                            \$('#entry-modal').modal('hide');

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    //TODO
                });
            });

            \$('#entry-submit').on('click', function () {
                entry_form.submit();
            });

            \$('#entry-modal').on('shown.bs.modal', function () {
                entry_form.show();

                var topic = \$('#walk_in_entry_topic');
                topic.val('');
                topic.get(0).focus();

                var activity = \$('#walk_in_entry_activity');
                activity.val('').change();

                var course = \$('#walk_in_entry_course');
                course.val('').change();

                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]');
                quiz_label.hide();

                var quiz = \$('#walk_in_entry_quiz');
                quiz.val('').change();
                quiz.hide();

                var other_label = \$('label[for=\"walk_in_entry_otherCourse');
                other_label.hide();

                var other = \$('#walk_in_entry_otherCourse');
                other.val('').change();
                other.hide();
            });

            \$('#entry-modal').on('hidden.bs.modal', function () {
                var topic = \$('#walk_in_entry_topic');
                topic.val('');

                var activity = \$('#walk_in_entry_activity');
                activity.val('').change();

                var course = \$('#walk_in_entry_course');
                course.val('').change();

                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]');
                quiz_label.hide();

                var quiz = \$('#walk_in_entry_quiz');
                quiz.val('').change();
                quiz.hide();

                var other_label = \$('label[for=\"walk_in_entry_otherCourse');
                other_label.hide();

                var other = \$('#walk_in_entry_otherCourse');
                other.val('').change();
                other.hide();

                var user = \$('#walk_in_entry_user');
                user.val('');

                input.get(0).focus();
            });

            \$('#walk_in_entry_activity').on('change', function() {
                var option = \$('#walk_in_entry_activity option:selected').text();
                var quiz_label = \$('label[for=\"walk_in_entry_quiz\"]')
                var quiz = \$('#walk_in_entry_quiz');
                if(option == 'Take a Quiz') {
                    quiz_label.show();
                    quiz.show();
                } else {;
                    quiz_label.hide();

                    quiz.val('').change();
                    quiz.hide();
                }
            });

            // exit survey
            exit_form.on('submit', function (e) {
                e.preventDefault();

                \$.ajax({
                    url: exit_form.prop('action'),
                    data: {
                        mentors: \$('#walk_in_exit_mentors').val(),
                        feedback: \$('#walk_in_exit_feedback').val(),
                        user: \$('#walk_in_exit_user').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        exit_form.hide();
                        \$('#exit-loader').show();
                    }
                }).always(function () {
                    \$('#exit-loader').hide();
                    input.get(0).focus();
                }).done(function (data, textStatus, jqXHR) {
                    switch (jqXHR.responseJSON.message) {
                        case 'attendee_out':
                            \$('#exit-modal').modal('hide');

                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user + '!');
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    // TODO
                });

            });

            \$('#exit-submit').on('click', function () {
                exit_form.submit();
            });

            \$('#exit-modal').on('shown.bs.modal', function () {
                exit_form.show();

                var mentors = \$('#walk_in_exit_mentors');
                mentors.val('').change();

                var feedback = \$('#walk_in_exit_feedback');
                feedback.val('');
            });

            \$('#exit-modal').on('hidden.bs.modal', function () {
                var mentors = \$('#walk_in_exit_mentors');
                mentors.val('').change();

                var feedback = \$('#walk_in_exit_feedback');
                feedback.val('');

                var user = \$('#walk_in_exit_user');
                user.val('');

                input.get(0).focus();
            });

            // register
            register_form.on('submit', function (e) {
                e.preventDefault();

                var username_input = \$('#card_register_username');
                var username = username_input.val();
                // console.log(username);

                var password_input = \$('#card_register_password');
                var password = password_input.val();
                password_input.val('');

                var swipe = \$('#card_register_swipe').val();
                \$.ajax({
                    url: register_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': null,
                        'swipe': swipe
                    },
                    type: 'POST',
                    beforeSend: function () {
                        register_form.hide();
                        register_loader.show();
                    }
                }).always(function () {
                    register_loader.hide();
                }).done(function (data, textStatus, jqXHR) {
                    username_input.val('');
                    \$('#register-modal').modal('hide');

                    {# success implies card was registered #}
                    switch (jqXHR.responseJSON.message) {
                        case 'entrance':
                            \$('#entry-modal').modal('show');

                            \$('#walk_in_entry_user').val(jqXHR.responseJSON.user);
                            break;
                        case 'mentor_in':
                        {# TODO better message #}
                            success.show();
                            success.delay(9000).hide(\"fade\", {}, 1000, null);
                            success.html('<strong>Success!</strong> Your card has been registered and welcome ' + jqXHR.responseJSON.user + '!');
                            break;
                            {# other success messages should NOT occur, if they do something went really wrong #}
                    }

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    register_form.show();
                    register_warning.hide();
                    register_error.show();

                    // console.log(jqXHR.responseJSON);

                    switch (jqXHR.responseJSON) {
                        case 'unregistered_user':
                        {# shouldn't happen #}
                            register_error.html('<strong>Card could not be registered!</strong> Please try again.');
                            break;
                        case 'no_user':
                        {# TODO figure out how to handle #}
                            break;
                        case 'ineligible':
                            // hide modal
                            // normal error
                            break;
                        case 'bad_credentials':
                            register_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        case 'invalid':
                        {# shouldn't happen #}
                            register_error.html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            register_error.html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });
            });

            \$('#register-submit').on('click', function () {
                register_form.submit();
            });

            var register_enter_pressed = function (e) {
                if (e.keyCode == '13') {
                    register_form.submit();
                }
            };

            \$('#card_register_username').on('keyup', register_enter_pressed);
            \$('#card_register_password').on('keyup', register_enter_pressed);

            register_form.parsley({
                errorsMessagesDisabled: true,
            });

            \$('#register-modal').on('shown.bs.modal', function () {
                register_form.show();
                var username_input = \$('#card_register_username');
                username_input.val('');
                username_input.get(0).focus();

                var password_input = \$('#card_register_password');
                password_input.val('');
            });

            \$('#register-modal').on('hidden.bs.modal', function () {
                var username_input = \$('#card_register_username');
                username_input.val('');

                var password_input = \$('#card_register_password');
                password_input.val('');

                \$('#register-error').hide();
                \$('#register-warning').show();

                input.get(0).focus();
            });

            // floating labels
            var floatingLabel;

            floatingLabel = function (onload) {
                var \$input;
                \$input = \$(this);
                if (onload) {
                    \$.each(\$('.bt-flabels__wrapper input'), function (index, value) {
                        var \$current_input;
                        \$current_input = \$(value);
                        if (\$current_input.val()) {
                            \$current_input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                        }
                    });
                }

                setTimeout((function () {
                    if (\$input.val()) {
                        \$input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                    } else {
                        \$input.closest('.bt-flabels__wrapper').removeClass('bt-flabel__float');
                    }
                }), 1);
            };

            \$('.bt-flabels__wrapper input').keydown(floatingLabel);
            \$('.bt-flabels__wrapper input').change(floatingLabel);

            window.addEventListener('load', floatingLabel(true), false);
            var error = function () {
                \$.each(this.fields, function (key, field) {
                    if (field.validationResult !== true) {
                        field.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                    }
                });
            };

            var validated = function () {
                if (this.validationResult === true) {
                    this.\$element.closest('.bt-flabels__wrapper').removeClass('bt-flabels__error');
                } else {
                    this.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                }
            };

            // no_card_form.parsley().on('form:error', error);
            // no_card_form.parsley().on('field:validated', validated);

            register_form.parsley().on('form:error', error);
            register_form.parsley().on('field:validated', validated);
            // end floating labels
        });
    </script>
</body>
</html>
", "shared/swipe/walk_in.html.twig", "/code/templates/shared/swipe/walk_in.html.twig");
    }
}
