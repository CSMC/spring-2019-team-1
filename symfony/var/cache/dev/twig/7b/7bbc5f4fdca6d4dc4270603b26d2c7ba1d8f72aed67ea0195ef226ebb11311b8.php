<?php

/* role/developer/files.html.twig */
class __TwigTemplate_d823b6e20cb1e8f96f10f92efd57b5f4c44bf609aef6fa20ee5995d6fd4103e4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/developer/base.html.twig", "role/developer/files.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/developer/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/developer/files.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/developer/files.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "<table id=\"table\" class=\"table table-bordered table-striped\">
    <thead>
        <tr>
            <th>Name</th>
            <th>Owner</th>
            <th>Extension</th>
            <th>MIME Type</th>
            <th>Size</th>
            <th>Path</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 20
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function() {
            var render_owner = function(data, type, row) {
                return row.owner.firstName + ' ' + row.owner.lastName;
            };

            var render_extension = function(data, type, row) {
                for(var i in row.metadata) {
                    if(row.metadata[i].key == \"extension\") {
                        return row.metadata[i].value;
                    }
                }

                return '';
            };

            var render_mime = function(data, type, row) {
                for(var i in row.metadata) {
                    if(row.metadata[i].key == \"mime\") {
                        return row.metadata[i].value;
                    }
                }

                return '';
            };

            var render_size = function(data, type, row) {
                return row.hash.size;
            };

            var render_path = function(data, type, row) {
                return row.hash.path;
            };

            var render_link = function(data, type, row) {
                return '<a href=\"/download/' + row.id + '\">Download</a>';
            };

           \$('#table').DataTable({
               ordering: true,
               searching: true,
               paging: true,
               ajax: {
                   url: '";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("dev_files_feed");
        echo "',
                   dataSrc: ''
                },
               columns: [
                   {data: 'name'},
                   {render: render_owner},
                   {render: render_extension},
                   {render: render_mime},
                   {render: render_size},
                   {render: render_path},
                   {render: render_link}
               ]
           });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/developer/files.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 64,  88 => 20,  79 => 19,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/developer/base.html.twig' %}
{% block body %}
<table id=\"table\" class=\"table table-bordered table-striped\">
    <thead>
        <tr>
            <th>Name</th>
            <th>Owner</th>
            <th>Extension</th>
            <th>MIME Type</th>
            <th>Size</th>
            <th>Path</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
{% endblock %}
{% block javascripts %}
{{ parent() }}
    <script>
        \$(function() {
            var render_owner = function(data, type, row) {
                return row.owner.firstName + ' ' + row.owner.lastName;
            };

            var render_extension = function(data, type, row) {
                for(var i in row.metadata) {
                    if(row.metadata[i].key == \"extension\") {
                        return row.metadata[i].value;
                    }
                }

                return '';
            };

            var render_mime = function(data, type, row) {
                for(var i in row.metadata) {
                    if(row.metadata[i].key == \"mime\") {
                        return row.metadata[i].value;
                    }
                }

                return '';
            };

            var render_size = function(data, type, row) {
                return row.hash.size;
            };

            var render_path = function(data, type, row) {
                return row.hash.path;
            };

            var render_link = function(data, type, row) {
                return '<a href=\"/download/' + row.id + '\">Download</a>';
            };

           \$('#table').DataTable({
               ordering: true,
               searching: true,
               paging: true,
               ajax: {
                   url: '{{ path('dev_files_feed') }}',
                   dataSrc: ''
                },
               columns: [
                   {data: 'name'},
                   {render: render_owner},
                   {render: render_extension},
                   {render: render_mime},
                   {render: render_size},
                   {render: render_path},
                   {render: render_link}
               ]
           });
        });
    </script>
{% endblock %}", "role/developer/files.html.twig", "/code/templates/role/developer/files.html.twig");
    }
}
