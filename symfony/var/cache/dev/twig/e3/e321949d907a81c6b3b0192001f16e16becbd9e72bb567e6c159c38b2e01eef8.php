<?php

/* role/admin/schedule/listOperationHours.html.twig */
class __TwigTemplate_17f2a3b97284c947d9592abe48744e82c00e65ef0d2082241d049eb3df6bd7c3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/schedule/listOperationHours.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/listOperationHours.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/listOperationHours.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Operation Hours
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./list_holidays'\"
                                    style=\"float:right\">Holidays
                            </button>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable_list\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Day</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["operationHours"]) || array_key_exists("operationHours", $context) ? $context["operationHours"] : (function () { throw new Twig_Error_Runtime('Variable "operationHours" does not exist.', 31, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["o"]) {
            // line 32
            echo "                            <tr>
                                <td>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "day", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 34
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "startTime", []), "g:i a"), "html", null, true);
            echo "</td>
                                <td>";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "endTime", []), "g:i a"), "html", null, true);
            echo "</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./edit_operation_hours?id=";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["o"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['o'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type=\"text/javascript\">
        ";
        // line 51
        echo "        \$(document).ready(function () {
            \$('#datatable_list').dataTable({
                \"aaSorting\": []
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/schedule/listOperationHours.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 51,  115 => 43,  104 => 38,  98 => 35,  94 => 34,  90 => 33,  87 => 32,  83 => 31,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Operation Hours
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./list_holidays'\"
                                    style=\"float:right\">Holidays
                            </button>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable_list\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Day</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for o in operationHours %}
                            <tr>
                                <td>{{ o.day }}</td>
                                <td>{{ o.startTime|date('g:i a') }}</td>
                                <td>{{ o.endTime|date('g:i a') }}</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./edit_operation_hours?id={{ o.id }}'\">Edit
                                    </button>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type=\"text/javascript\">
        {# disable default sorting while displaying datatable#}
        \$(document).ready(function () {
            \$('#datatable_list').dataTable({
                \"aaSorting\": []
            });
        });
    </script>
{% endblock %}
", "role/admin/schedule/listOperationHours.html.twig", "/code/templates/role/admin/schedule/listOperationHours.html.twig");
    }
}
