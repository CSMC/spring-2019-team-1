<?php

/* role/admin/section/list.html.twig */
class __TwigTemplate_5339a03df65572ef7cc9be1c34aa96fbfde7a912890b7d9071402d0fcd5ed345 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/section/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/section/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/section/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"clearfix\"></div>

    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">

                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Sections
                            <a class=\"btn btn-primary\"
                               href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_create");
        echo "\"
                               style=\"float:right\">New Section
                            </a>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Course<br>Number</th>
                            <th>Course<br>Name</th>
                            <th>Section</th>
                            <th>Semester</th>
                            <th>Instructors</th>
                            <th>TAs</th>
                            <th># Students</th>
                            <th>Admin<br>Notes</th>
                            <th>Edit</th>
                            <th>Roster</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sections"]) || array_key_exists("sections", $context) ? $context["sections"] : (function () { throw new Twig_Error_Runtime('Variable "sections" does not exist.', 39, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 40
            echo "                            <tr>
                                <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["s"], "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["s"], "course", []), "number", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["s"], "course", []), "name", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "number", []), "html", null, true);
            echo "</td>
                                <td>
                                    ";
            // line 45
            if ((twig_get_attribute($this->env, $this->source, $context["s"], "semester", []) != null)) {
                // line 46
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["s"], "semester", []), "year", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["s"], "semester", []), "season", []), "html", null, true);
                echo "
                                    ";
            }
            // line 48
            echo "
                                </td>
                                <td>
                                    ";
            // line 51
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["s"], "instructors", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["instructor"]) {
                // line 52
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["instructor"], "firstName", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["instructor"], "lastName", []), "html", null, true);
                echo "
                                        ";
                // line 53
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 54
                    echo "                                            ";
                    echo ", ";
                    echo "
                                        ";
                }
                // line 56
                echo "                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instructor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                                </td>
                                <td>
                                    ";
            // line 60
            echo "                                    ";
            // line 61
            echo "                                    ";
            // line 64
            echo "                                </td>
                                <td>
                                    ";
            // line 66
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "roster", [])), "html", null, true);
            echo "
                                </td>
                                <td>
                                    ";
            // line 69
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "description", [])) > 20)) {
                // line 70
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "description", []), 0, 20), "html", null, true);
                echo " ...
                                    ";
            } else {
                // line 72
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "description", []), "html", null, true);
                echo "
                                    ";
            }
            // line 74
            echo "                                </td>
                                <td>
                                    <a class=\"btn btn-success\"
                                       href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["s"], "id", [])]), "html", null, true);
            echo "\">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a class=\"btn btn-warning\"
                                       href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_roster", ["id" => twig_get_attribute($this->env, $this->source, $context["s"], "id", [])]), "html", null, true);
            echo "\">
                                        View
                                    </a>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 96
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 97
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/section/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 97,  252 => 96,  236 => 89,  224 => 83,  215 => 77,  210 => 74,  204 => 72,  198 => 70,  196 => 69,  190 => 66,  186 => 64,  184 => 61,  182 => 60,  178 => 57,  164 => 56,  158 => 54,  156 => 53,  149 => 52,  132 => 51,  127 => 48,  119 => 46,  117 => 45,  112 => 43,  108 => 42,  102 => 41,  99 => 40,  95 => 39,  66 => 13,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"clearfix\"></div>

    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">

                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Sections
                            <a class=\"btn btn-primary\"
                               href=\"{{ path('admin_section_create') }}\"
                               style=\"float:right\">New Section
                            </a>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Course<br>Number</th>
                            <th>Course<br>Name</th>
                            <th>Section</th>
                            <th>Semester</th>
                            <th>Instructors</th>
                            <th>TAs</th>
                            <th># Students</th>
                            <th>Admin<br>Notes</th>
                            <th>Edit</th>
                            <th>Roster</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for s in sections %}
                            <tr>
                                <td>{{ s.course.department.abbreviation }} {{ s.course.number }}</td>
                                <td>{{ s.course.name }}</td>
                                <td>{{ s.number }}</td>
                                <td>
                                    {% if s.semester != null %}
                                        {{ s.semester.year }} {{ s.semester.season }}
                                    {% endif %}

                                </td>
                                <td>
                                    {% for instructor in s.instructors %}
                                        {{ instructor.firstName }} {{ instructor.lastName }}
                                        {% if not loop.last %}
                                            {{ ', ' }}
                                        {% endif %}
                                    {% endfor %}
                                </td>
                                <td>
                                    {# TWIG array to string #}
                                    {#{{ s.teaching_assistants |join(', ') }}#}
                                    {#
                                    {{ s.teaching_assistants.firstName }} {{ s.teaching_assistants.lastName }}
                                    #}
                                </td>
                                <td>
                                    {{ s.roster|length }}
                                </td>
                                <td>
                                    {% if s.description|length > 20 %}
                                        {{ s.description[0:20] }} ...
                                    {% else %}
                                        {{ s.description }}
                                    {% endif %}
                                </td>
                                <td>
                                    <a class=\"btn btn-success\"
                                       href=\"{{ path('admin_section_edit', {'id': s.id}) }}\">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a class=\"btn btn-warning\"
                                       href=\"{{ path('admin_section_roster', {\"id\": s.id}) }}\">
                                        View
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
{% endblock %}", "role/admin/section/list.html.twig", "/code/templates/role/admin/section/list.html.twig");
    }
}
