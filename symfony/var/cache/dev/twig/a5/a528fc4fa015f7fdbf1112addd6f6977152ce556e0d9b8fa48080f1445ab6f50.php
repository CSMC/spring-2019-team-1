<?php

/* role/instructor/report/report_home.html.twig */
class __TwigTemplate_c56e1e02417fa2372715fbcc3f9c6639660f55726427ac5d92a49eb214532d6b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/report/report_home.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/report/report_home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/report/report_home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->source); })()), "course", []), 'label');
        echo "
        </div>
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->source); })()), "course", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->source); })()), "course", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 12
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->source); })()), "session", []), 'label');
        echo "
        </div>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->source); })()), "session", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), "session", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 22
        echo "        </div>
    </div>
    ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->source); })()), "submit", []), 'widget');
        echo "
    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 25, $this->source); })()), 'form_end');
        echo "

    ";
        // line 27
        if ( !(null === (isset($context["report"]) || array_key_exists("report", $context) ? $context["report"] : (function () { throw new Twig_Error_Runtime('Variable "report" does not exist.', 27, $this->source); })()))) {
            // line 28
            echo "        <h3>Report</h3>
        <table id=\"datatable\" class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>NetID</th>
                <th>Time In</th>
                <th>Time Out</th>
                ";
            // line 37
            if (((isset($context["report_type"]) || array_key_exists("report_type", $context) ? $context["report_type"] : (function () { throw new Twig_Error_Runtime('Variable "report_type" does not exist.', 37, $this->source); })()) == "session")) {
                // line 38
                echo "                    <th>Comet Card</th>
                    <th>Grade</th>
                ";
            } else {
                // line 41
                echo "                    <th>Topic</th>
                    <th>Activity</th>
                ";
            }
            // line 44
            echo "            </tr>
            </thead>
            <tbody>
            ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["report"]) || array_key_exists("report", $context) ? $context["report"] : (function () { throw new Twig_Error_Runtime('Variable "report" does not exist.', 47, $this->source); })()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 48
                echo "                <tr>
                ";
                // line 49
                if (((isset($context["report_type"]) || array_key_exists("report_type", $context) ? $context["report_type"] : (function () { throw new Twig_Error_Runtime('Variable "report_type" does not exist.', 49, $this->source); })()) == "session")) {
                    // line 50
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", [], "array"), "lastName", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 51
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", [], "array"), "firstName", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 52
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", [], "array"), "username", []), "html", null, true);
                    echo "</td>
                    ";
                    // line 53
                    if ( !(null === twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"))) {
                        // line 54
                        echo "                        <td>";
                        echo twig_escape_filter($this->env, ((twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "timeIn", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "timeIn", []), "m/d/y g:i A"))), "html", null, true);
                        echo "</td>
                        <td>";
                        // line 55
                        echo twig_escape_filter($this->env, ((twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "timeOut", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "timeOut", []), "g:i A"))), "html", null, true);
                        echo "</td>
                        <td></td>
                        <td>";
                        // line 57
                        echo twig_escape_filter($this->env, (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "grade", []) != null)) ? (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "attendance", [], "array"), "grade", [])) : ("")), "html", null, true);
                        echo "</td>
                    ";
                    } else {
                        // line 59
                        echo "                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    ";
                    }
                    // line 64
                    echo "                ";
                } else {
                    // line 65
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", []), "lastName", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 66
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", []), "firstName", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 67
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "user", []), "username", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 68
                    echo twig_escape_filter($this->env, ((twig_test_empty(twig_get_attribute($this->env, $this->source, $context["row"], "timeIn", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "timeIn", []), "m/d/y g:i A"))), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 69
                    echo twig_escape_filter($this->env, ((twig_test_empty(twig_get_attribute($this->env, $this->source, $context["row"], "timeOut", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "timeOut", []), "g:i A"))), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 70
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "topic", []), "html", null, true);
                    echo "</td>
                    <td>";
                    // line 71
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["row"], "activity", []), "name", []), "html", null, true);
                    echo "</td>
                ";
                }
                // line 73
                echo "                </tr>
            ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 75
                echo "            ";
                // line 76
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "            </tbody>
        </table>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 81
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 82
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                paging: false,
                ordering: true,
                searching: true,
                buttons: [
                    'csv'
                ]
            });

            table.buttons().container()
                .appendTo( \$('#datatable_wrapper .col-sm-6:eq(0)') );
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/report/report_home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 82,  245 => 81,  232 => 77,  226 => 76,  224 => 75,  218 => 73,  213 => 71,  209 => 70,  205 => 69,  201 => 68,  197 => 67,  193 => 66,  188 => 65,  185 => 64,  178 => 59,  173 => 57,  168 => 55,  163 => 54,  161 => 53,  157 => 52,  153 => 51,  148 => 50,  146 => 49,  143 => 48,  138 => 47,  133 => 44,  128 => 41,  123 => 38,  121 => 37,  110 => 28,  108 => 27,  103 => 25,  99 => 24,  95 => 22,  91 => 20,  86 => 18,  81 => 16,  75 => 12,  71 => 10,  66 => 8,  61 => 6,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ form_start(form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.course) }}
        </div>
        {{ form_errors(form.course) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.course, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.session) }}
        </div>
        {{ form_errors(form.session) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.session, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    {{ form_widget(form.submit) }}
    {{ form_end(form) }}

    {% if report is not null %}
        <h3>Report</h3>
        <table id=\"datatable\" class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>NetID</th>
                <th>Time In</th>
                <th>Time Out</th>
                {% if report_type == 'session' %}
                    <th>Comet Card</th>
                    <th>Grade</th>
                {% else %}
                    <th>Topic</th>
                    <th>Activity</th>
                {% endif %}
            </tr>
            </thead>
            <tbody>
            {% for row in report %}
                <tr>
                {% if report_type == 'session' %}
                    <td>{{ row['user'].lastName }}</td>
                    <td>{{ row['user'].firstName }}</td>
                    <td>{{ row['user'].username }}</td>
                    {% if row['attendance'] is not null %}
                        <td>{{ row['attendance'].timeIn is empty ? \"\" : row['attendance'].timeIn|date('m/d/y g:i A') }}</td>
                        <td>{{ row['attendance'].timeOut is empty ? \"\" : row['attendance'].timeOut|date('g:i A') }}</td>
                        <td></td>
                        <td>{{ row['attendance'].grade != null ? row['attendance'].grade }}</td>
                    {% else %}
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    {% endif %}
                {% else %}
                    <td>{{ row.user.lastName }}</td>
                    <td>{{ row.user.firstName }}</td>
                    <td>{{ row.user.username }}</td>
                    <td>{{ row.timeIn is empty ? \"\" : row.timeIn|date('m/d/y g:i A') }}</td>
                    <td>{{ row.timeOut is empty ? \"\" : row.timeOut|date('g:i A') }}</td>
                    <td>{{ row.topic }}</td>
                    <td>{{ row.activity.name }}</td>
                {% endif %}
                </tr>
            {% else %}
            {#<tr><td colspan=\"7\">No data</td></tr>#}
            {% endfor %}
            </tbody>
        </table>
    {% endif %}
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                paging: false,
                ordering: true,
                searching: true,
                buttons: [
                    'csv'
                ]
            });

            table.buttons().container()
                .appendTo( \$('#datatable_wrapper .col-sm-6:eq(0)') );
        });
    </script>
{% endblock %}", "role/instructor/report/report_home.html.twig", "/code/templates/role/instructor/report/report_home.html.twig");
    }
}
