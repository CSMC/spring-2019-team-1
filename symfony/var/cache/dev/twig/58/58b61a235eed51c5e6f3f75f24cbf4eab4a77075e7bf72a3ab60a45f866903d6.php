<?php

/* role/admin/room/list.html.twig */
class __TwigTemplate_4274a0cf57253ad645c0861e29f1a9e473b9a2e08f158a06f67f8114a92c44fc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/room/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/room/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/room/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h3>Rooms
                        <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./roomAdd'\"
                                style=\"float:right\">New Room
                        </button>
                    </h3>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">Room
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">
                                Description
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">
                                Capacity
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">Active
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">Edit</th>
                        </thead>
                        <tbody>
                        ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["room"]) || array_key_exists("room", $context) ? $context["room"] : (function () { throw new Twig_Error_Runtime('Variable "room" does not exist.', 35, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["rm"]) {
            // line 36
            echo "                            <tr>
                                <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rm"], "__toString", [], "method"), "html", null, true);
            echo "</td>
                                <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rm"], "description", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rm"], "capacity", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 40
            if ((twig_get_attribute($this->env, $this->source, $context["rm"], "active", []) == 1)) {
                // line 41
                echo "                                        Yes
                                    ";
            } else {
                // line 43
                echo "                                        No
                                    ";
            }
            // line 44
            echo "</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./roomEdit?id=";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rm"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rm'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 60
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 61
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function() {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/room/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 61,  150 => 60,  133 => 52,  122 => 47,  117 => 44,  113 => 43,  109 => 41,  107 => 40,  103 => 39,  99 => 38,  95 => 37,  92 => 36,  88 => 35,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h3>Rooms
                        <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./roomAdd'\"
                                style=\"float:right\">New Room
                        </button>
                    </h3>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr role=\"row\">
                            <th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\"
                                aria-label=\"Name: activate to sort column descending\" style=\"width: 189px;\"
                                aria-sort=\"ascending\">Room
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">
                                Description
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">
                                Capacity
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">Active
                            </th>
                            <th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\">Edit</th>
                        </thead>
                        <tbody>
                        {% for rm in room %}
                            <tr>
                                <td>{{ rm.__toString() }}</td>
                                <td>{{ rm.description }}</td>
                                <td>{{ rm.capacity }}</td>
                                <td>{% if rm.active == 1 %}
                                        Yes
                                    {% else %}
                                        No
                                    {% endif %}</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./roomEdit?id={{ rm.id }}'\">Edit
                                    </button>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function() {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
{% endblock %}", "role/admin/room/list.html.twig", "/code/templates/role/admin/room/list.html.twig");
    }
}
