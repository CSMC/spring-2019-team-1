<?php

/* role/admin/session/grades.html.twig */
class __TwigTemplate_cf58c5e671f25dbe82ea1b67ebd8f94dc10c492674685157627aab5a798c7366 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/session/grades.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/grades.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/grades.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        // line 4
        echo "    ";
        $this->loadTemplate("shared/component/flash_messages.html.twig", "role/admin/session/grades.html.twig", 4)->display($context);
        // line 5
        echo "    <div class=\"content_head\">
        <h2 class=\"content_title\">";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 6, $this->source); })()), "topic", []), "html", null, true);
        echo "</h2>
        <div class=\"content_options\">
            ";
        // line 8
        if ((isset($context["timeSlots"]) || array_key_exists("timeSlots", $context))) {
            // line 9
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_register", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 9, $this->source); })()), "id", [])]), "html", null, true);
            echo "\">
                    <div class=\"content_option button green_button\">Register</div>
                </a>
            ";
        }
        // line 13
        echo "            <a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_attend", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 13, $this->source); })()), "id", [])]), "html", null, true);
        echo "\">
                <div class=\"content_option button green_button\">Attend</div>
            </a>
        </div>
    </div>
    ";
        // line 18
        if ((isset($context["attendances"]) || array_key_exists("attendances", $context))) {
            // line 19
            echo "        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Name</th>
                <th class=\"content_table_cell\">NetID</th>
                <th class=\"content_table_cell\">Sign In Time</th>
                <th class=\"content_table_cell\">Sign Out Time</th>
                ";
            // line 25
            if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 25, $this->source); })()), "graded", [])) {
                // line 26
                echo "                    <th class=\"content_table_cell\">Grade</th>
                ";
            }
            // line 28
            echo "            </tr>
            ";
            // line 29
            if (twig_test_empty((isset($context["attendances"]) || array_key_exists("attendances", $context) ? $context["attendances"] : (function () { throw new Twig_Error_Runtime('Variable "attendances" does not exist.', 29, $this->source); })()))) {
                // line 30
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"";
                // line 31
                echo ((twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 31, $this->source); })()), "graded", [])) ? (6) : (5));
                echo "\">No attendees.</td>
                </tr>
            ";
            } else {
                // line 34
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["attendances"]) || array_key_exists("attendances", $context) ? $context["attendances"] : (function () { throw new Twig_Error_Runtime('Variable "attendances" does not exist.', 34, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                    // line 35
                    echo "                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\">";
                    // line 36
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "html", null, true);
                    echo "</td>
                        <td class=\"content_table_cell\">";
                    // line 37
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                    echo "</td>
                        ";
                    // line 38
                    $context["time_in"] = twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []);
                    // line 39
                    echo "                        ";
                    $context["time_in"] = (((isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 39, $this->source); })())) ? (twig_date_format_filter($this->env, (isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 39, $this->source); })()), "g:i A")) : (""));
                    // line 40
                    echo "                        ";
                    $context["time_out"] = twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []);
                    // line 41
                    echo "                        ";
                    $context["time_out"] = (((isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 41, $this->source); })())) ? (twig_date_format_filter($this->env, (isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 41, $this->source); })()), "g:i A")) : (""));
                    // line 42
                    echo "                        <td class=\"content_table_cell\">";
                    echo twig_escape_filter($this->env, (isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 42, $this->source); })()), "html", null, true);
                    echo "</td>
                        <td class=\"content_table_cell\">";
                    // line 43
                    echo twig_escape_filter($this->env, (isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 43, $this->source); })()), "html", null, true);
                    echo "</td>
                        ";
                    // line 44
                    if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 44, $this->source); })()), "graded", [])) {
                        // line 45
                        echo "                            <td class=\"content_table_cell\">
                                ";
                        // line 46
                        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 46, $this->source); })()), "numericGrade", [])) {
                            // line 47
                            echo "                                    <input value=\"";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []), "html", null, true);
                            echo "\"
                                           onchange=\"updateGrade(this, '";
                            // line 48
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                            echo "')\"
                                           type=\"number\"
                                           min=\"0\"
                                           max=\"100\">
                                ";
                        } else {
                            // line 53
                            echo "                                    <select onchange=\"updateGrade(this, '";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                            echo "')\">
                                        <option ";
                            // line 54
                            if ((null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []))) {
                                echo "selected";
                            }
                            echo "></option>
                                        <option value=\"1\" ";
                            // line 55
                            if ((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 1)) {
                                echo "selected";
                            }
                            echo ">Pass
                                        </option>
                                        <option value=\"0\"
                                                ";
                            // line 58
                            if (((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 0) &&  !(null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", [])))) {
                                echo "selected";
                            }
                            echo ">
                                            Fail
                                        </option>
                                    </select>
                                ";
                        }
                        // line 63
                        echo "                            </td>
                        ";
                    }
                    // line 65
                    echo "                    </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 67
                echo "            ";
            }
            // line 68
            echo "        </table>
    ";
        } elseif (        // line 69
(isset($context["timeSlots"]) || array_key_exists("timeSlots", $context))) {
            // line 70
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["timeSlots"]) || array_key_exists("timeSlots", $context) ? $context["timeSlots"] : (function () { throw new Twig_Error_Runtime('Variable "timeSlots" does not exist.', 70, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["timeSlot"]) {
                // line 71
                echo "            <div class=\"content_head\">
                <h3 class=\"content_title\">
                    ";
                // line 73
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeSlot"], "timeSlot", [], "array"), "startTime", []), "m/d g:i A"), "html", null, true);
                echo "
                    - ";
                // line 74
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeSlot"], "timeSlot", [], "array"), "endTime", []), "g:i A"), "html", null, true);
                echo " Mentors:
                    ";
                // line 75
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeSlot"], "timeSlot", [], "array"), "assignments", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
                    // line 76
                    echo "                        ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "mentor", []), "preferredName", []), "html", null, true);
                    echo "
                        ";
                    // line 77
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                        // line 78
                        echo "                            ";
                        echo ", ";
                        echo "
                        ";
                    }
                    // line 80
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 81
                echo "                </h3>
            </div>
            ";
                // line 83
                $context["students"] = twig_get_attribute($this->env, $this->source, $context["timeSlot"], "students", [], "array");
                // line 84
                echo "            <table class=\"content_table\">
                <tr class=\"content_table_head\">
                    <th class=\"content_table_cell\">Name</th>
                    <th class=\"content_table_cell\">NetID</th>
                    <th class=\"content_table_cell\">Registered</th>
                    <th class=\"content_table_cell\">Sign In Time</th>
                    <th class=\"content_table_cell\">Sign Out Time</th>
                    <th class=\"content_table_cell\">Comet Card</th>
                    ";
                // line 92
                if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 92, $this->source); })()), "graded", [])) {
                    // line 93
                    echo "                        <th class=\"content_table_cell\">Grade</th>
                    ";
                }
                // line 95
                echo "                </tr>
                ";
                // line 96
                if (twig_test_empty((isset($context["students"]) || array_key_exists("students", $context) ? $context["students"] : (function () { throw new Twig_Error_Runtime('Variable "students" does not exist.', 96, $this->source); })()))) {
                    // line 97
                    echo "                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\" colspan=\"";
                    // line 98
                    echo ((twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 98, $this->source); })()), "graded", [])) ? (7) : (6));
                    echo "\">No attendees or
                            registrations.
                        </td>
                    </tr>
                ";
                } else {
                    // line 103
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["students"]) || array_key_exists("students", $context) ? $context["students"] : (function () { throw new Twig_Error_Runtime('Variable "students" does not exist.', 103, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
                        // line 104
                        echo "                        <tr class=\"content_table_data\">
                            <td class=\"content_table_cell\">";
                        // line 105
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "user", []), "firstName", []), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "user", []), "lastName", []), "html", null, true);
                        echo "</td>
                            <td class=\"content_table_cell\">";
                        // line 106
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "user", []), "username", []), "html", null, true);
                        echo "</td>
                            <td class=\"content_table_cell\">";
                        // line 107
                        echo ((twig_get_attribute($this->env, $this->source, $context["student"], "registered", [])) ? ("Yes") : ("no"));
                        echo "</td>
                            ";
                        // line 108
                        if ( !(null === twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []))) {
                            // line 109
                            echo "                                ";
                            $context["time_in"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "timeIn", []);
                            // line 110
                            echo "                                ";
                            $context["time_in"] = (((isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 110, $this->source); })())) ? (twig_date_format_filter($this->env, (isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 110, $this->source); })()), "g:i A")) : (""));
                            // line 111
                            echo "                                ";
                            $context["time_out"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "timeOut", []);
                            // line 112
                            echo "                                ";
                            $context["time_out"] = (((isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 112, $this->source); })())) ? (twig_date_format_filter($this->env, (isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 112, $this->source); })()), "g:i A")) : (""));
                            // line 113
                            echo "                                <td class=\"content_table_cell\">";
                            echo twig_escape_filter($this->env, (isset($context["time_in"]) || array_key_exists("time_in", $context) ? $context["time_in"] : (function () { throw new Twig_Error_Runtime('Variable "time_in" does not exist.', 113, $this->source); })()), "html", null, true);
                            echo "</td>
                                <td class=\"content_table_cell\">";
                            // line 114
                            echo twig_escape_filter($this->env, (isset($context["time_out"]) || array_key_exists("time_out", $context) ? $context["time_out"] : (function () { throw new Twig_Error_Runtime('Variable "time_out" does not exist.', 114, $this->source); })()), "html", null, true);
                            echo "</td>
                            ";
                        } else {
                            // line 116
                            echo "                                <td class=\"content_table_cell\"></td>
                                <td class=\"content_table_cell\"></td>
                            ";
                        }
                        // line 119
                        echo "                            <td class=\"content_table_cell\">Not Yet Implemented</td>
                            ";
                        // line 120
                        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 120, $this->source); })()), "graded", [])) {
                            // line 121
                            echo "                                <td class=\"content_table_cell\">
                                    ";
                            // line 122
                            if ( !(null === twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []))) {
                                // line 123
                                echo "
                                        ";
                                // line 124
                                if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 124, $this->source); })()), "numericGrade", [])) {
                                    // line 125
                                    echo "                                            <input value=\"";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "grade", []), "html", null, true);
                                    echo "\"
                                                   onchange=\"updateGrade(this, '";
                                    // line 126
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["attendance"]) || array_key_exists("attendance", $context) ? $context["attendance"] : (function () { throw new Twig_Error_Runtime('Variable "attendance" does not exist.', 126, $this->source); })()), "id", []), "html", null, true);
                                    echo "')\"
                                                   type=\"number\"
                                                   min=\"0\"
                                                   max=\"100\">
                                        ";
                                } else {
                                    // line 131
                                    echo "                                            <select onchange=\"updateGrade(this, '";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "id", []), "html", null, true);
                                    echo "')\">
                                                <option ";
                                    // line 132
                                    if ((null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "grade", []))) {
                                        echo "selected";
                                    }
                                    echo "></option>
                                                <option value=\"1\" ";
                                    // line 133
                                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "grade", []) == 1)) {
                                        echo "selected";
                                    }
                                    echo ">Pass
                                                </option>
                                                <option value=\"0\"
                                                        ";
                                    // line 136
                                    if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "grade", []) == 0) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "attendance", []), "grade", [])))) {
                                        echo "selected";
                                    }
                                    echo ">
                                                    Fail
                                                </option>
                                            </select>
                                        ";
                                }
                                // line 141
                                echo "                                    ";
                            } else {
                                // line 142
                                echo "                                        N/A
                                    ";
                            }
                            // line 144
                            echo "                                </td>
                            ";
                        }
                        // line 146
                        echo "                        </tr>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 148
                    echo "                ";
                }
                // line 149
                echo "            </table>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeSlot'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 151
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 154
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 155
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        function updateGrade(obj, attendance) {
            ";
        // line 158
        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 158, $this->source); })()), "numericGrade", [])) {
            // line 159
            echo "            var grade = obj.value;
            ";
        } else {
            // line 161
            echo "            var grade = obj.options[obj.selectedIndex].value;
            ";
        }
        // line 163
        echo "            \$.ajax({
                type: 'POST',
                url: '";
        // line 165
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades_edit_ajax");
        echo "',
                data: {'grade': grade, 'attendance': attendance}
            }).done(function (data) {
                //success message
            }).fail(function (data) {
                //error message
            });
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/session/grades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  496 => 165,  492 => 163,  488 => 161,  484 => 159,  482 => 158,  475 => 155,  466 => 154,  455 => 151,  448 => 149,  445 => 148,  438 => 146,  434 => 144,  430 => 142,  427 => 141,  417 => 136,  409 => 133,  403 => 132,  398 => 131,  390 => 126,  385 => 125,  383 => 124,  380 => 123,  378 => 122,  375 => 121,  373 => 120,  370 => 119,  365 => 116,  360 => 114,  355 => 113,  352 => 112,  349 => 111,  346 => 110,  343 => 109,  341 => 108,  337 => 107,  333 => 106,  327 => 105,  324 => 104,  319 => 103,  311 => 98,  308 => 97,  306 => 96,  303 => 95,  299 => 93,  297 => 92,  287 => 84,  285 => 83,  281 => 81,  267 => 80,  261 => 78,  259 => 77,  254 => 76,  237 => 75,  233 => 74,  229 => 73,  225 => 71,  220 => 70,  218 => 69,  215 => 68,  212 => 67,  205 => 65,  201 => 63,  191 => 58,  183 => 55,  177 => 54,  172 => 53,  164 => 48,  159 => 47,  157 => 46,  154 => 45,  152 => 44,  148 => 43,  143 => 42,  140 => 41,  137 => 40,  134 => 39,  132 => 38,  128 => 37,  124 => 36,  121 => 35,  116 => 34,  110 => 31,  107 => 30,  105 => 29,  102 => 28,  98 => 26,  96 => 25,  88 => 19,  86 => 18,  77 => 13,  69 => 9,  67 => 8,  62 => 6,  59 => 5,  56 => 4,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/admin/base.html.twig' %}
{% block body %}
    {# TODO add download link for instructor/admin #}
    {% include('shared/component/flash_messages.html.twig') %}
    <div class=\"content_head\">
        <h2 class=\"content_title\">{{ session.topic }}</h2>
        <div class=\"content_options\">
            {% if timeSlots is defined %}
                <a href=\"{{ path('admin_session_register', {'id': session.id}) }}\">
                    <div class=\"content_option button green_button\">Register</div>
                </a>
            {% endif %}
            <a href=\"{{ path('admin_session_attend', {'id': session.id}) }}\">
                <div class=\"content_option button green_button\">Attend</div>
            </a>
        </div>
    </div>
    {% if attendances is defined %}
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Name</th>
                <th class=\"content_table_cell\">NetID</th>
                <th class=\"content_table_cell\">Sign In Time</th>
                <th class=\"content_table_cell\">Sign Out Time</th>
                {% if session.graded %}
                    <th class=\"content_table_cell\">Grade</th>
                {% endif %}
            </tr>
            {% if attendances is empty %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"{{ session.graded ? 6 : 5 }}\">No attendees.</td>
                </tr>
            {% else %}
                {% for attendance in attendances %}
                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\">{{ attendance.user }}</td>
                        <td class=\"content_table_cell\">{{ attendance.user.username }}</td>
                        {% set time_in = attendance.timeIn %}
                        {% set time_in = time_in ? time_in|date('g:i A') : '' %}
                        {% set time_out = attendance.timeOut %}
                        {% set time_out = time_out ? time_out|date('g:i A') : '' %}
                        <td class=\"content_table_cell\">{{ time_in }}</td>
                        <td class=\"content_table_cell\">{{ time_out }}</td>
                        {% if session.graded %}
                            <td class=\"content_table_cell\">
                                {% if session.numericGrade %}
                                    <input value=\"{{ attendance.grade }}\"
                                           onchange=\"updateGrade(this, '{{ attendance.id }}')\"
                                           type=\"number\"
                                           min=\"0\"
                                           max=\"100\">
                                {% else %}
                                    <select onchange=\"updateGrade(this, '{{ attendance.id }}')\">
                                        <option {% if attendance.grade is null %}selected{% endif %}></option>
                                        <option value=\"1\" {% if attendance.grade == 1 %}selected{% endif %}>Pass
                                        </option>
                                        <option value=\"0\"
                                                {% if attendance.grade == 0 and attendance.grade is not null %}selected{% endif %}>
                                            Fail
                                        </option>
                                    </select>
                                {% endif %}
                            </td>
                        {% endif %}
                    </tr>
                {% endfor %}
            {% endif %}
        </table>
    {% elseif timeSlots is defined %}
        {% for timeSlot in timeSlots %}
            <div class=\"content_head\">
                <h3 class=\"content_title\">
                    {{ timeSlot['timeSlot'].startTime|date('m/d g:i A') }}
                    - {{ timeSlot['timeSlot'].endTime|date('g:i A') }} Mentors:
                    {% for mentor in timeSlot['timeSlot'].assignments %}
                        {{ mentor.mentor.preferredName }}
                        {% if not loop.last %}
                            {{ \", \" }}
                        {% endif %}
                    {% endfor %}
                </h3>
            </div>
            {% set students = timeSlot['students'] %}
            <table class=\"content_table\">
                <tr class=\"content_table_head\">
                    <th class=\"content_table_cell\">Name</th>
                    <th class=\"content_table_cell\">NetID</th>
                    <th class=\"content_table_cell\">Registered</th>
                    <th class=\"content_table_cell\">Sign In Time</th>
                    <th class=\"content_table_cell\">Sign Out Time</th>
                    <th class=\"content_table_cell\">Comet Card</th>
                    {% if session.graded %}
                        <th class=\"content_table_cell\">Grade</th>
                    {% endif %}
                </tr>
                {% if students is empty %}
                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\" colspan=\"{{ session.graded ? 7 : 6 }}\">No attendees or
                            registrations.
                        </td>
                    </tr>
                {% else %}
                    {% for student in students %}
                        <tr class=\"content_table_data\">
                            <td class=\"content_table_cell\">{{ student.user.firstName }} {{ student.user.lastName }}</td>
                            <td class=\"content_table_cell\">{{ student.user.username }}</td>
                            <td class=\"content_table_cell\">{{ student.registered ? 'Yes' : 'no' }}</td>
                            {% if student.attendance is not null %}
                                {% set time_in = student.attendance.timeIn %}
                                {% set time_in = time_in ? time_in|date('g:i A') : '' %}
                                {% set time_out = student.attendance.timeOut %}
                                {% set time_out = time_out ? time_out|date('g:i A') : '' %}
                                <td class=\"content_table_cell\">{{ time_in }}</td>
                                <td class=\"content_table_cell\">{{ time_out }}</td>
                            {% else %}
                                <td class=\"content_table_cell\"></td>
                                <td class=\"content_table_cell\"></td>
                            {% endif %}
                            <td class=\"content_table_cell\">Not Yet Implemented</td>
                            {% if session.graded %}
                                <td class=\"content_table_cell\">
                                    {% if student.attendance is not null %}

                                        {% if session.numericGrade %}
                                            <input value=\"{{ student.attendance.grade }}\"
                                                   onchange=\"updateGrade(this, '{{ attendance.id }}')\"
                                                   type=\"number\"
                                                   min=\"0\"
                                                   max=\"100\">
                                        {% else %}
                                            <select onchange=\"updateGrade(this, '{{ student.attendance.id }}')\">
                                                <option {% if student.attendance.grade is null %}selected{% endif %}></option>
                                                <option value=\"1\" {% if student.attendance.grade == 1 %}selected{% endif %}>Pass
                                                </option>
                                                <option value=\"0\"
                                                        {% if student.attendance.grade == 0 and student.attendance.grade is not null %}selected{% endif %}>
                                                    Fail
                                                </option>
                                            </select>
                                        {% endif %}
                                    {% else %}
                                        N/A
                                    {% endif %}
                                </td>
                            {% endif %}
                        </tr>
                    {% endfor %}
                {% endif %}
            </table>
        {% endfor %}
    {% endif %}
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script>
        function updateGrade(obj, attendance) {
            {% if session.numericGrade %}
            var grade = obj.value;
            {% else %}
            var grade = obj.options[obj.selectedIndex].value;
            {% endif %}
            \$.ajax({
                type: 'POST',
                url: '{{ path('session_grades_edit_ajax') }}',
                data: {'grade': grade, 'attendance': attendance}
            }).done(function (data) {
                //success message
            }).fail(function (data) {
                //error message
            });
        }
    </script>
{% endblock %}", "role/admin/session/grades.html.twig", "/code/templates/role/admin/session/grades.html.twig");
    }
}
