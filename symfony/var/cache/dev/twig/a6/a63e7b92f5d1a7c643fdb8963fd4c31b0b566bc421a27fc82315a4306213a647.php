<?php

/* role/instructor/session/request/request.html.twig */
class __TwigTemplate_661f04924e7b3a2c09fc2a2701ddf6a2fbd7420dc7444b2460a6b2ea071288da extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/request/request.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/request.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"content_list\">
        <div class=\"content_list_label\">Requester</div>
        <div class=\"content_list_info\">";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 5, $this->source); })()), "user", []), "firstName", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 5, $this->source); })()), "user", []), "lastName", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Topic</div>
        <div class=\"content_list_info\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 7, $this->source); })()), "topic", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Type</div>
        <div class=\"content_list_info\">";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 9, $this->source); })()), "type", []), "name", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Time Requested</div>
        <div class=\"content_list_info\">";
        // line 11
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 11, $this->source); })()), "created", []), "m/d/y h:i A"), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Time Last Updated</div>
        <div class=\"content_list_info\">";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 13, $this->source); })()), "updated", []), "m/d/y h:i A"), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Dates Requested</div>
        <div class=\"content_list_info\">";
        // line 15
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 15, $this->source); })()), "startDate", []), "m/d/y"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 15, $this->source); })()), "endDate", []), "m/d/y"), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Course</div>
        <div class=\"content_list_info\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 17, $this->source); })()), "sections", []), 0, [], "array"), "course", []), "department", []), "abbreviation", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 17, $this->source); })()), "sections", []), 0, [], "array"), "course", []), "number", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Section(s)</div>
        <div class=\"content_list_info\">
            ";
        // line 20
        $context["count"] = twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 20, $this->source); })()), "sections", []));
        // line 21
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 21, $this->source); })()), "sections", []));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 22
            echo "                ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "number", []), "html", null, true);
            echo "
                ";
            // line 23
            if (((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 23, $this->source); })()) > 1)) {
                // line 24
                echo "                    ";
                echo ", ";
                echo "
                    ";
                // line 25
                $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 25, $this->source); })()) - 1);
                // line 26
                echo "                ";
            }
            // line 27
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "        </div>
        <div class=\"content_list_label\">Student Instructions</div>
        <div class=\"content_list_info\">";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 30, $this->source); })()), "studentInstructions", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Files</div>
        <div class=\"content_list_info\">
            ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["request"]) || array_key_exists("request", $context) ? $context["request"] : (function () { throw new Twig_Error_Runtime('Variable "request" does not exist.', 33, $this->source); })()), "files", []));
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 34
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["id" => twig_get_attribute($this->env, $this->source, $context["file"], "id", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["file"], "name", []), "html", null, true);
            echo "</a><br>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/request/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 36,  143 => 34,  139 => 33,  133 => 30,  129 => 28,  123 => 27,  120 => 26,  118 => 25,  113 => 24,  111 => 23,  106 => 22,  101 => 21,  99 => 20,  91 => 17,  84 => 15,  79 => 13,  74 => 11,  69 => 9,  64 => 7,  57 => 5,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <div class=\"content_list\">
        <div class=\"content_list_label\">Requester</div>
        <div class=\"content_list_info\">{{ request.user.firstName }} {{ request.user.lastName }}</div>
        <div class=\"content_list_label\">Topic</div>
        <div class=\"content_list_info\">{{ request.topic }}</div>
        <div class=\"content_list_label\">Type</div>
        <div class=\"content_list_info\">{{ request.type.name }}</div>
        <div class=\"content_list_label\">Time Requested</div>
        <div class=\"content_list_info\">{{ request.created|date('m/d/y h:i A') }}</div>
        <div class=\"content_list_label\">Time Last Updated</div>
        <div class=\"content_list_info\">{{ request.updated|date('m/d/y h:i A') }}</div>
        <div class=\"content_list_label\">Dates Requested</div>
        <div class=\"content_list_info\">{{ request.startDate|date('m/d/y') }} - {{ request.endDate|date('m/d/y') }}</div>
        <div class=\"content_list_label\">Course</div>
        <div class=\"content_list_info\">{{ request.sections[0].course.department.abbreviation }} {{ request.sections[0].course.number }}</div>
        <div class=\"content_list_label\">Section(s)</div>
        <div class=\"content_list_info\">
            {% set count = request.sections|length %}
            {% for section in request.sections %}
                {{ section.number }}
                {% if count > 1 %}
                    {{ \", \" }}
                    {% set count = count - 1 %}
                {% endif %}
            {% endfor %}
        </div>
        <div class=\"content_list_label\">Student Instructions</div>
        <div class=\"content_list_info\">{{ request.studentInstructions }}</div>
        <div class=\"content_list_label\">Files</div>
        <div class=\"content_list_info\">
            {% for file in request.files %}
                <a href=\"{{ path('download', {'id': file.id}) }}\">{{ file.name }}</a><br>
            {% endfor %}
        </div>
    </div>
{% endblock %}", "role/instructor/session/request/request.html.twig", "/code/templates/role/instructor/session/request/request.html.twig");
    }
}
