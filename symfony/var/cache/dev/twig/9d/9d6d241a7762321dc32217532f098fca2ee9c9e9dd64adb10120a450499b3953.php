<?php

/* role/mentor/schedule/weekly.html.twig */
class __TwigTemplate_44e12dfbc1b113c25322060d3e3e9da3bacf85b80673b36c40e7b666b1ff8cf8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/schedule/weekly.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/weekly.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/weekly.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        $this->loadTemplate("shared/component/flash_messages.html.twig", "role/mentor/schedule/weekly.html.twig", 3)->display($context);
        // line 4
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["date_period"]) || array_key_exists("date_period", $context) ? $context["date_period"] : (function () { throw new Twig_Error_Runtime('Variable "date_period" does not exist.', 4, $this->source); })()), "startDate", []), "M jS, Y"), "html", null, true);
        echo "
        - ";
        // line 5
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["date_period"]) || array_key_exists("date_period", $context) ? $context["date_period"] : (function () { throw new Twig_Error_Runtime('Variable "date_period" does not exist.', 5, $this->source); })()), "endDate", []), "-1 day"), "M jS, Y"), "html", null, true);
        echo "
        <div class=\"pull-right\">
            <a class=\"btn btn-success\"
               href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule_weekly", ["date" => twig_date_format_filter($this->env, twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["date_period"]) || array_key_exists("date_period", $context) ? $context["date_period"] : (function () { throw new Twig_Error_Runtime('Variable "date_period" does not exist.', 8, $this->source); })()), "startDate", []), "-7 day"), "Y-m-d")]), "html", null, true);
        echo "\">
                <i class=\"fa fa-arrow-left\"></i> Previous
            </a>
            <a class=\"btn btn-success\"
               href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule_weekly", ["date" => twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["date_period"]) || array_key_exists("date_period", $context) ? $context["date_period"] : (function () { throw new Twig_Error_Runtime('Variable "date_period" does not exist.', 12, $this->source); })()), "endDate", []), "Y-m-d")]), "html", null, true);
        echo "\">
                Next <i class=\"fa fa-arrow-right\"></i>
            </a>
        </div>
    </h2>
    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["date_period"]) || array_key_exists("date_period", $context) ? $context["date_period"] : (function () { throw new Twig_Error_Runtime('Variable "date_period" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["date"]) {
            // line 18
            echo "        <h3 class=\"content_title\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $context["date"], "l, M jS, Y"), "html", null, true);
            echo "</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td rowspan=\"2\">Time</td>
                <td colspan=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new Twig_Error_Runtime('Variable "rooms" does not exist.', 23, $this->source); })())), "html", null, true);
            echo "\">Rooms</td>
            </tr>

            <tr>
                ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new Twig_Error_Runtime('Variable "rooms" does not exist.', 27, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 28
                echo "                    <td>";
                echo twig_escape_filter($this->env, $context["room"], "html", null, true);
                echo "</td>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "            </tr>
            </thead>
            <tbody>
            ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["shifts"]) || array_key_exists("shifts", $context) ? $context["shifts"] : (function () { throw new Twig_Error_Runtime('Variable "shifts" does not exist.', 33, $this->source); })()), twig_date_format_filter($this->env, $context["date"], "w"), [], "array"), "times", [], "array"));
            foreach ($context['_seq'] as $context["key"] => $context["time"]) {
                // line 34
                echo "                <tr>
                    <td>
                        ";
                // line 36
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "start", [], "array"), "g:i A"), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "end", [], "array"), "g:i A"), "html", null, true);
                echo "
                    </td>
                    ";
                // line 38
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new Twig_Error_Runtime('Variable "rooms" does not exist.', 38, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                    // line 39
                    echo "                        <td style=\"padding: 0;\">
                            ";
                    // line 40
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["shifts"] ?? null), twig_date_format_filter($this->env, $context["date"], "w"), [], "array", false, true), "shifts", [], "array", false, true), twig_get_attribute($this->env, $this->source, $context["room"], "__toString", []), [], "array", false, true), $context["key"], [], "array", true, true)) {
                        // line 41
                        echo "                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["shifts"]) || array_key_exists("shifts", $context) ? $context["shifts"] : (function () { throw new Twig_Error_Runtime('Variable "shifts" does not exist.', 41, $this->source); })()), twig_date_format_filter($this->env, $context["date"], "w"), [], "array"), "shifts", [], "array"), twig_get_attribute($this->env, $this->source, $context["room"], "__toString", []), [], "array"), $context["key"], [], "array"));
                        foreach ($context['_seq'] as $context["_key"] => $context["shift"]) {
                            // line 42
                            echo "                                    <div>
                                        ";
                            // line 43
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($context["shift"]);
                            foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
                                // line 44
                                echo "                                            ";
                                if ((twig_get_attribute($this->env, $this->source, $context["subject"], "subject", [], "array") == "shift_leader")) {
                                    // line 45
                                    echo "                                                <div style=\"padding: 8px 8px 2px 8px; color: #ffffff; background-color: #337ab7;\">
                                                    <i class=\"fa fa-star\"></i> ";
                                    // line 46
                                    echo "Shift Leader: ";
                                    echo "
                                                    ";
                                    // line 47
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subject"], "assignments", [], "array"));
                                    foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
                                        // line 48
                                        echo "                                                        ";
                                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["assignment"], "mentor", []), "html", null, true);
                                        echo "
                                                    ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 50
                                    echo "                                                </div>
                                            ";
                                }
                                // line 52
                                echo "                                        ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 53
                            echo "                                        ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($context["shift"]);
                            foreach ($context['_seq'] as $context["_key"] => $context["subject"]) {
                                // line 54
                                echo "                                            ";
                                if ((twig_get_attribute($this->env, $this->source, $context["subject"], "subject", [], "array") != "shift_leader")) {
                                    // line 55
                                    echo "                                                <div style=\"padding: 0px 8px 2px 8px; color: #ffffff; background-color: ";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subject"], "subject", [], "array"), "color", []), "html", null, true);
                                    echo ";\">
                                                    ";
                                    // line 57
                                    echo "                                                    ";
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subject"], "assignments", [], "array"));
                                    $context['loop'] = [
                                      'parent' => $context['_parent'],
                                      'index0' => 0,
                                      'index'  => 1,
                                      'first'  => true,
                                    ];
                                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                                        $length = count($context['_seq']);
                                        $context['loop']['revindex0'] = $length - 1;
                                        $context['loop']['revindex'] = $length;
                                        $context['loop']['length'] = $length;
                                        $context['loop']['last'] = 1 === $length;
                                    }
                                    foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
                                        // line 58
                                        echo "                                                        ";
                                        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["assignment"], "absence", []))) {
                                            // line 59
                                            echo "                                                            ";
                                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["assignment"], "mentor", []), "html", null, true);
                                            echo " ";
                                            echo "(absent)";
                                        } else {
                                            // line 61
                                            echo "                                                            ";
                                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["assignment"], "mentor", []), "html", null, true);
                                        }
                                        // line 63
                                        echo "                                                        ";
                                        if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                                            // line 64
                                            echo ", ";
                                            echo "
                                                        ";
                                        }
                                        // line 66
                                        echo "                                                    ";
                                        ++$context['loop']['index0'];
                                        ++$context['loop']['index'];
                                        $context['loop']['first'] = false;
                                        if (isset($context['loop']['length'])) {
                                            --$context['loop']['revindex0'];
                                            --$context['loop']['revindex'];
                                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                                        }
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 67
                                    echo "                                                </div>
                                            ";
                                }
                                // line 69
                                echo "                                        ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subject'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 70
                            echo "                                    </div>
                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shift'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 72
                        echo "                            ";
                    }
                    // line 73
                    echo "                        </td>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 75
                echo "                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['time'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "            </tbody>
        </table>
        <h3 class=\"content_title\">Sessions</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td>Time</td>
                <td>Room</td>
                <td>Topic</td>
                <td>Mentors</td>
                <td>Materials</td>
                <td>Details</td>
            </tr>
            </thead>
            <tbody>
            ";
            // line 92
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 92, $this->source); })()), twig_date_format_filter($this->env, $context["date"], "w"), [], "array"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
                // line 93
                echo "                <tr>
                    <td>";
                // line 94
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "g:i A"), "html", null, true);
                echo "</td>
                    <td>";
                // line 95
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "location", []), "html", null, true);
                echo "</td>
                    ";
                // line 96
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", [], "any", false, true), "topic", [], "any", true, true)) {
                    // line 97
                    echo "                        <td>";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "topic", []), "html", null, true);
                    echo "</td>
                    ";
                } else {
                    // line 99
                    echo "                        <td></td>
                    ";
                }
                // line 101
                echo "                    <td>
                     ";
                // line 102
                if (twig_get_attribute($this->env, $this->source, $context["timeslot"], "assignments", [], "any", true, true)) {
                    // line 103
                    echo "                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["timeslot"], "assignments", []));
                    $context['_iterated'] = false;
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
                        // line 104
                        echo "                            ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["assignment"], "mentor", []), "html", null, true);
                        echo "
                            ";
                        // line 105
                        if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                            // line 106
                            echo "                                ";
                            echo ", ";
                            echo "
                            ";
                        }
                        // line 108
                        echo "                        ";
                        $context['_iterated'] = true;
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    if (!$context['_iterated']) {
                        // line 109
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 110
                    echo "                      ";
                }
                echo "  
                    </td>
                    <td>
                    ";
                // line 113
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", [], "any", false, true), "files", [], "any", true, true)) {
                    // line 114
                    echo "                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "files", []));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
                        // line 115
                        echo "                            <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["id" => twig_get_attribute($this->env, $this->source, $context["file"], "id", [])]), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["file"], "name", []), "html", null, true);
                        echo "</a>
                            ";
                        // line 116
                        if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                            // line 117
                            echo "                                <br>
                            ";
                        }
                        // line 119
                        echo "                        ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 120
                    echo "                    ";
                }
                echo "     
                    </td>
                    <td>
                        <a class=\"btn btn-success\" href=\"";
                // line 123
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_timeslot_view", ["tid" => twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", [])]), "html", null, true);
                echo "\">
                            View
                        </a>
                    </td>
                </tr>
            ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 129
                echo "                <tr>
                    <td colspan=\"6\">No sessions!</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 133
            echo "            </tbody>
        </table>
        <h3>Quizzes</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td>Topic</td>
                <td>Room</td>
                <td>Description</td>
                <td>Materials</td>
            </tr>
            </thead>
            <tbody>
            ";
            // line 146
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 146, $this->source); })()), twig_date_format_filter($this->env, $context["date"], "w"), [], "array"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
                // line 147
                echo "                <tr>
                    <td>";
                // line 148
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 149
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "location", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 150
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
                echo "</td>
                    <td>
                        ";
                // line 152
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["quiz"], "files", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
                    // line 153
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["fid" => twig_get_attribute($this->env, $this->source, $context["file"], "id", [])]), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["file"], "name", []), "html", null, true);
                    echo "</a>
                            ";
                    // line 154
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                        // line 155
                        echo "                                <br>
                            ";
                    }
                    // line 157
                    echo "                        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 158
                echo "                    </td>
                </tr>
            ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 161
                echo "                <tr>
                    <td colspan=\"4\">No quizzes!</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 165
            echo "            </tbody>
        </table>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['date'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/schedule/weekly.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 165,  558 => 161,  551 => 158,  537 => 157,  533 => 155,  531 => 154,  524 => 153,  507 => 152,  502 => 150,  498 => 149,  494 => 148,  491 => 147,  486 => 146,  471 => 133,  462 => 129,  451 => 123,  444 => 120,  430 => 119,  426 => 117,  424 => 116,  417 => 115,  399 => 114,  397 => 113,  390 => 110,  384 => 109,  371 => 108,  365 => 106,  363 => 105,  358 => 104,  339 => 103,  337 => 102,  334 => 101,  330 => 99,  324 => 97,  322 => 96,  318 => 95,  314 => 94,  311 => 93,  306 => 92,  289 => 77,  282 => 75,  275 => 73,  272 => 72,  265 => 70,  259 => 69,  255 => 67,  241 => 66,  236 => 64,  233 => 63,  229 => 61,  223 => 59,  220 => 58,  202 => 57,  197 => 55,  194 => 54,  189 => 53,  183 => 52,  179 => 50,  170 => 48,  166 => 47,  162 => 46,  159 => 45,  156 => 44,  152 => 43,  149 => 42,  144 => 41,  142 => 40,  139 => 39,  135 => 38,  128 => 36,  124 => 34,  120 => 33,  115 => 30,  106 => 28,  102 => 27,  95 => 23,  86 => 18,  82 => 17,  74 => 12,  67 => 8,  61 => 5,  56 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {% include('shared/component/flash_messages.html.twig') %}
    <h2>{{ date_period.startDate|date('M jS, Y') }}
        - {{ date_period.endDate|date_modify(\"-1 day\")|date('M jS, Y') }}
        <div class=\"pull-right\">
            <a class=\"btn btn-success\"
               href=\"{{ path('schedule_weekly', {'date': date_period.startDate|date_modify(\"-7 day\")|date('Y-m-d')}) }}\">
                <i class=\"fa fa-arrow-left\"></i> Previous
            </a>
            <a class=\"btn btn-success\"
               href=\"{{ path('schedule_weekly', {'date': date_period.endDate|date('Y-m-d')}) }}\">
                Next <i class=\"fa fa-arrow-right\"></i>
            </a>
        </div>
    </h2>
    {% for date in date_period %}
        <h3 class=\"content_title\">{{ date|date('l, M jS, Y') }}</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td rowspan=\"2\">Time</td>
                <td colspan=\"{{ rooms|length }}\">Rooms</td>
            </tr>

            <tr>
                {% for room in rooms %}
                    <td>{{ room }}</td>
                {% endfor %}
            </tr>
            </thead>
            <tbody>
            {% for key, time in shifts[date|date('w')]['times'] %}
                <tr>
                    <td>
                        {{ time['start']|date('g:i A') }} - {{ time['end']|date('g:i A') }}
                    </td>
                    {% for room in rooms %}
                        <td style=\"padding: 0;\">
                            {% if shifts[date|date('w')]['shifts'][room.__toString][key] is defined %}
                                {% for shift in shifts[date|date('w')]['shifts'][room.__toString][key] %}
                                    <div>
                                        {% for subject in shift %}
                                            {% if subject['subject'] == 'shift_leader' %}
                                                <div style=\"padding: 8px 8px 2px 8px; color: #ffffff; background-color: #337ab7;\">
                                                    <i class=\"fa fa-star\"></i> {{ \"Shift Leader: \" }}
                                                    {% for assignment in subject['assignments'] %}
                                                        {{ assignment.mentor }}
                                                    {% endfor %}
                                                </div>
                                            {% endif %}
                                        {% endfor %}
                                        {% for subject in shift %}
                                            {% if subject['subject'] != 'shift_leader' %}
                                                <div style=\"padding: 0px 8px 2px 8px; color: #ffffff; background-color: {{ subject['subject'].color }};\">
                                                    {#{{ subject['subject'] -}} {{- \": \" }}#}
                                                    {% for assignment in subject['assignments'] %}
                                                        {% if assignment.absence is not empty %}
                                                            {{ assignment.mentor }} {{ \"(absent)\" -}}
                                                        {% else %}
                                                            {{ assignment.mentor -}}
                                                        {% endif %}
                                                        {% if not loop.last %}
                                                            {{- \", \" }}
                                                        {% endif %}
                                                    {% endfor %}
                                                </div>
                                            {% endif %}
                                        {% endfor %}
                                    </div>
                                {% endfor %}
                            {% endif %}
                        </td>
                    {% endfor %}
                </tr>
            {% endfor %}
            </tbody>
        </table>
        <h3 class=\"content_title\">Sessions</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td>Time</td>
                <td>Room</td>
                <td>Topic</td>
                <td>Mentors</td>
                <td>Materials</td>
                <td>Details</td>
            </tr>
            </thead>
            <tbody>
            {% for timeslot in sessions[date|date('w')] %}
                <tr>
                    <td>{{ timeslot.startTime|date('g:i A') }}</td>
                    <td>{{ timeslot.location }}</td>
                    {% if timeslot.session.topic is defined%}
                        <td>{{ timeslot.session.topic }}</td>
                    {% else %}
                        <td></td>
                    {% endif %}
                    <td>
                     {% if timeslot.assignments is defined%}
                        {% for assignment in timeslot.assignments %}
                            {{ assignment.mentor }}
                            {% if not loop.last %}
                                {{ \", \" }}
                            {% endif %}
                        {% else %}
                        {% endfor %}
                      {% endif %}  
                    </td>
                    <td>
                    {% if timeslot.session.files is defined%}
                        {% for file in timeslot.session.files %}
                            <a href=\"{{ path('download', {'id': file.id}) }}\">{{ file.name }}</a>
                            {% if not loop.last %}
                                <br>
                            {% endif %}
                        {% endfor %}
                    {% endif %}     
                    </td>
                    <td>
                        <a class=\"btn btn-success\" href=\"{{ path('session_timeslot_view', {'tid': timeslot.id}) }}\">
                            View
                        </a>
                    </td>
                </tr>
            {% else %}
                <tr>
                    <td colspan=\"6\">No sessions!</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
        <h3>Quizzes</h3>
        <table class=\"table table-striped table-bordered\">
            <thead>
            <tr>
                <td>Topic</td>
                <td>Room</td>
                <td>Description</td>
                <td>Materials</td>
            </tr>
            </thead>
            <tbody>
            {% for quiz in quizzes[date|date('w')] %}
                <tr>
                    <td>{{ quiz.topic }}</td>
                    <td>{{ quiz.location }}</td>
                    <td>{{ quiz.description }}</td>
                    <td>
                        {% for file in quiz.files %}
                            <a href=\"{{ path('download', {'fid': file.id}) }}\">{{ file.name }}</a>
                            {% if not loop.last %}
                                <br>
                            {% endif %}
                        {% endfor %}
                    </td>
                </tr>
            {% else %}
                <tr>
                    <td colspan=\"4\">No quizzes!</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    {% endfor %}
{% endblock %}", "role/mentor/schedule/weekly.html.twig", "/code/templates/role/mentor/schedule/weekly.html.twig");
    }
}
