<?php

/* role/admin/schedule/listScheduleTimes.html.twig */
class __TwigTemplate_02988a5e368a3a46ae1e53fae785da97ee4043653e29bf7fc9539c69bb014b1c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/schedule/listScheduleTimes.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/listScheduleTimes.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/listScheduleTimes.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Schedule Time
                            <button type=\"button\" class=\"btn btn-primary\"
                                    onclick=\"location.href='./edit_schedule_time?id=";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["schedule_time"]) || array_key_exists("schedule_time", $context) ? $context["schedule_time"] : (function () { throw new Twig_Error_Runtime('Variable "schedule_time" does not exist.', 13, $this->source); })()), 0, [], "array"), "id", []), "html", null, true);
        echo "'\"
                                    style=\"float:right\">Edit
                            </button>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Calender Property</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["schedule_time"]) || array_key_exists("schedule_time", $context) ? $context["schedule_time"] : (function () { throw new Twig_Error_Runtime('Variable "schedule_time" does not exist.', 30, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["s"]) {
            // line 31
            echo "                        <tr>
                            <td>Start Date</td>
                            <td>";
            // line 33
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "startDate", []), "m/d/y"), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                            <td>EndDate</td>
                            <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "endDate", []), "m/d/y"), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                            <td>Start of Week</td>
                            <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "startOfWeek", []), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                            <td>Start Time</td>
                            <td>";
            // line 45
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "startTime", []), "g:i a"), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                            <td>End Time</td>
                            <td>";
            // line 49
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["s"], "EndTime", []), "g:i a"), "html", null, true);
            echo "</td>
                        </tr>
                        <tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['s'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/schedule/listScheduleTimes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 53,  121 => 49,  114 => 45,  107 => 41,  100 => 37,  93 => 33,  89 => 31,  85 => 30,  65 => 13,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Schedule Time
                            <button type=\"button\" class=\"btn btn-primary\"
                                    onclick=\"location.href='./edit_schedule_time?id={{ schedule_time[0].id }}'\"
                                    style=\"float:right\">Edit
                            </button>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Calender Property</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for s in schedule_time %}
                        <tr>
                            <td>Start Date</td>
                            <td>{{ s.startDate|date(\"m/d/y\") }}</td>
                        </tr>
                        <tr>
                            <td>EndDate</td>
                            <td>{{ s.endDate|date(\"m/d/y\") }}</td>
                        </tr>
                        <tr>
                            <td>Start of Week</td>
                            <td>{{ s.startOfWeek }}</td>
                        </tr>
                        <tr>
                            <td>Start Time</td>
                            <td>{{ s.startTime|date('g:i a') }}</td>
                        </tr>
                        <tr>
                            <td>End Time</td>
                            <td>{{ s.EndTime|date('g:i a') }}</td>
                        </tr>
                        <tr>
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "role/admin/schedule/listScheduleTimes.html.twig", "/code/templates/role/admin/schedule/listScheduleTimes.html.twig");
    }
}
