<?php

/* role/admin/announcement/list.html.twig */
class __TwigTemplate_28c80400833b1975fc1ef7e8470460faf7c29f9a032c698dc4d1dd21bbbd7227 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/announcement/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Announcements
                            <a type=\"button\" class=\"btn btn-primary\" href=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_announcement_create");
        echo "\"
                               style=\"float:right\">New Announcement
                            </a>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    ";
        // line 21
        echo "                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Active</th>
                            <th>Roles</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["announcements"]) || array_key_exists("announcements", $context) ? $context["announcements"] : (function () { throw new Twig_Error_Runtime('Variable "announcements" does not exist.', 34, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 35
            echo "                            <tr>
                                <td>";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "subject", []), "html", null, true);
            echo "</td>

                                <td>
                                    ";
            // line 39
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "message", [])) > 40)) {
                // line 40
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "message", []), 0, 40), "html", null, true);
                echo " ...
                                    ";
            } else {
                // line 42
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "message", []), "html", null, true);
                echo "
                                    ";
            }
            // line 44
            echo "                                </td>

                                <td>";
            // line 46
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "startDate", []), "m/d/y"), "html", null, true);
            echo "  </td>
                                <td>";
            // line 47
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["a"], "endDate", []), "m/d/y"), "html", null, true);
            echo " </td>
                                <td>
                                    ";
            // line 49
            echo (((twig_get_attribute($this->env, $this->source, $context["a"], "active", []) == 1)) ? ("Active") : ("Inactive"));
            echo "
                                </td>
                                <td>
                                    ";
            // line 52
            echo twig_escape_filter($this->env, twig_join_filter(twig_get_attribute($this->env, $this->source, $context["a"], "roles", []), ", "), "html", null, true);
            echo "
                                </td>
                                <td>
                                    ";
            // line 56
            echo "                                    ";
            // line 57
            echo "                                    ";
            // line 58
            echo "                                    Not yet implemented
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "                        </tbody>
                    </table>
                </div>

                ";
        // line 107
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 112
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 113
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            \$('#datatable').DataTable({
                ordering: true,
                order: [],
                paging: true,
                searching: true
            })
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/announcement/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 113,  176 => 112,  162 => 107,  156 => 62,  147 => 58,  145 => 57,  143 => 56,  137 => 52,  131 => 49,  126 => 47,  122 => 46,  118 => 44,  112 => 42,  106 => 40,  104 => 39,  98 => 36,  95 => 35,  91 => 34,  76 => 21,  63 => 10,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Announcements
                            <a type=\"button\" class=\"btn btn-primary\" href=\"{{ path('admin_announcement_create') }}\"
                               style=\"float:right\">New Announcement
                            </a>
                        </h3>
                    </div>
                </div>
                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    {#                  <p><b>Active Announcement</b></p>     #}
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Active</th>
                            <th>Roles</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for a in announcements %}
                            <tr>
                                <td>{{ a.subject }}</td>

                                <td>
                                    {% if a.message|length > 40 %}
                                        {{ a.message[0:40] }} ...
                                    {% else %}
                                        {{ a.message }}
                                    {% endif %}
                                </td>

                                <td>{{ a.startDate|date(\"m/d/y\") }}  </td>
                                <td>{{ a.endDate|date(\"m/d/y\") }} </td>
                                <td>
                                    {{ a.active == 1 ? \"Active\" : \"Inactive\" }}
                                </td>
                                <td>
                                    {{ a.roles|join(\", \") }}
                                </td>
                                <td>
                                    {#<button type=\"button\" class=\"btn btn-round btn-success\"#}
                                    {#onclick=\"location.href='/editAnnouncement?id={{ a.id }}'\">Edit#}
                                    {#</button>#}
                                    Not yet implemented
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>

                {#
                // Inactive Announcements are NOT REQUIRED in this project

                              {% if numInactive != 0 %}
                              <div class=\"x_content\">
                                  <p class=\"text-muted font-13 m-b-30\"></p>
                                  <p><b>Inactive Announcement</b></p>
                                  <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                      <thead>
                                      <tr>
                                          <th>Subject</th>

                                          <th>Message</th>

                                          <th>Start Date</th>
                                          <th>End Date</th>
                                          <th>Edit</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      {% for a in announcements  %}
                                          {% if a.active == 0 %}
                                              <tr>
                                                  <td>{{ a.subject }}</td>

                                                  <td>{{ a.message }}</td>

                                                  <td>{{ a.startDate|date(\"m/d/y\") }}  </td>
                                                  <td>{{ a.endDate|date(\"m/d/y\") }} </td>
                                                  <td>
                                                      <button type=\"button\" class=\"btn btn-round btn-success\"
                                                              onclick=\"location.href='/editAnnouncement?id={{a.id}}'\">Edit</button>
                                                  </td>
                                              </tr>
                                          {% endif %}
                                      {% endfor %}
                                      </tbody>
                                  </table>
                              </div>
                              {% endif %}
                #}

            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            \$('#datatable').DataTable({
                ordering: true,
                order: [],
                paging: true,
                searching: true
            })
        });
    </script>
{% endblock %}
", "role/admin/announcement/list.html.twig", "/code/templates/role/admin/announcement/list.html.twig");
    }
}
