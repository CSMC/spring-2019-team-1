<?php

/* role/admin/course/list.html.twig */
class __TwigTemplate_953842a44a1fc6c58a50a28f96b18846e18401085d7ee1c353c23aedbfcedddc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/course/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/course/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/course/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Courses
                            <button type=\"button\" class=\"btn btn-primary\"
                                    onclick=\"location.href='";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_create");
        echo "'\" style=\"float:right\">
                                New Course
                            </button>
                        </h3>
                    </div>
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <p><b>Supported Course</b></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Course Number</th>
                            <th>Course Name</th>
                            <th>Admin Notes</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new Twig_Error_Runtime('Variable "courses" does not exist.', 35, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 36
            echo "                            ";
            if ((twig_get_attribute($this->env, $this->source, $context["c"], "supported", []) == 1)) {
                // line 37
                echo "                                <tr>
                                    <td>";
                // line 38
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "department", []), "abbreviation", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "number", []), "html", null, true);
                echo "</td>
                                    <td>";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "name", []), "html", null, true);
                echo "</td>
                                    <td>
                                        ";
                // line 41
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", [])) > 20)) {
                    // line 42
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", []), 0, 20), "html", null, true);
                    echo " ...
                                        ";
                } else {
                    // line 44
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", []), "html", null, true);
                    echo "
                                        ";
                }
                // line 46
                echo "                                    </td>
                                    <td>
                                        <button type=\"button\" class=\"btn btn-round btn-success\"
                                                onclick=\"location.href='";
                // line 49
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["c"], "id", [])]), "html", null, true);
                echo "'\">
                                            Edit
                                        </button>
                                    </td>
                                </tr>
                            ";
            }
            // line 55
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                        </tbody>
                    </table>
                </div>

                ";
        // line 60
        if (((isset($context["numUnsupported"]) || array_key_exists("numUnsupported", $context) ? $context["numUnsupported"] : (function () { throw new Twig_Error_Runtime('Variable "numUnsupported" does not exist.', 60, $this->source); })()) != 0)) {
            // line 61
            echo "                    <div class=\"x_content\">
                        <p class=\"text-muted font-13 m-b-30\"></p>
                        <p><b>Unsupported Course</b></p>
                        <table id=\"datatable\" class=\"table table-striped table-bordered\">
                            <thead>
                            <tr>
                                <th>Course Number</th>
                                <th>Course Name</th>
                                <th>Admin Notes</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            ";
            // line 74
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new Twig_Error_Runtime('Variable "courses" does not exist.', 74, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 75
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["c"], "supported", []) == 0)) {
                    // line 76
                    echo "                                    <tr>
                                        <td>";
                    // line 77
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["c"], "department", []), "abbreviation", []), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "number", []), "html", null, true);
                    echo "</td>
                                        <td>";
                    // line 78
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "name", []), "html", null, true);
                    echo "</td>
                                        <td>
                                            ";
                    // line 80
                    if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", [])) > 20)) {
                        // line 81
                        echo "                                                ";
                        echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", []), 0, 20), "html", null, true);
                        echo " ...
                                            ";
                    } else {
                        // line 83
                        echo "                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "description", []), "html", null, true);
                        echo "
                                            ";
                    }
                    // line 85
                    echo "                                        </td>
                                        <td>
                                            <button type=\"button\" class=\"btn btn-round btn-success\"
                                                    onclick=\"location.href='";
                    // line 88
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["c"], "id", [])]), "html", null, true);
                    echo "'\">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                ";
                }
                // line 94
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 95
            echo "                            </tbody>
                        </table>
                    </div>
                ";
        }
        // line 99
        echo "            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 103
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 104
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/course/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 104,  243 => 103,  230 => 99,  224 => 95,  218 => 94,  209 => 88,  204 => 85,  198 => 83,  192 => 81,  190 => 80,  185 => 78,  179 => 77,  176 => 76,  173 => 75,  169 => 74,  154 => 61,  152 => 60,  146 => 56,  140 => 55,  131 => 49,  126 => 46,  120 => 44,  114 => 42,  112 => 41,  107 => 39,  101 => 38,  98 => 37,  95 => 36,  91 => 35,  66 => 13,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"page-title\">
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>Courses
                            <button type=\"button\" class=\"btn btn-primary\"
                                    onclick=\"location.href='{{ path('admin_course_create') }}'\" style=\"float:right\">
                                New Course
                            </button>
                        </h3>
                    </div>
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <p><b>Supported Course</b></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Course Number</th>
                            <th>Course Name</th>
                            <th>Admin Notes</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for c in courses %}
                            {% if c.supported == 1 %}
                                <tr>
                                    <td>{{ c.department.abbreviation }} {{ c.number }}</td>
                                    <td>{{ c.name }}</td>
                                    <td>
                                        {% if c.description|length > 20 %}
                                            {{ c.description[0:20] }} ...
                                        {% else %}
                                            {{ c.description }}
                                        {% endif %}
                                    </td>
                                    <td>
                                        <button type=\"button\" class=\"btn btn-round btn-success\"
                                                onclick=\"location.href='{{ path('admin_course_edit', {'id': c.id}) }}'\">
                                            Edit
                                        </button>
                                    </td>
                                </tr>
                            {% endif %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>

                {% if numUnsupported != 0 %}
                    <div class=\"x_content\">
                        <p class=\"text-muted font-13 m-b-30\"></p>
                        <p><b>Unsupported Course</b></p>
                        <table id=\"datatable\" class=\"table table-striped table-bordered\">
                            <thead>
                            <tr>
                                <th>Course Number</th>
                                <th>Course Name</th>
                                <th>Admin Notes</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for c in courses %}
                                {% if c.supported == 0 %}
                                    <tr>
                                        <td>{{ c.department.abbreviation }} {{ c.number }}</td>
                                        <td>{{ c.name }}</td>
                                        <td>
                                            {% if c.description|length > 20 %}
                                                {{ c.description[0:20] }} ...
                                            {% else %}
                                                {{ c.description }}
                                            {% endif %}
                                        </td>
                                        <td>
                                            <button type=\"button\" class=\"btn btn-round btn-success\"
                                                    onclick=\"location.href='{{ path('admin_course_edit', {'id': c.id}) }}'\">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                {% endif %}
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                {% endif %}
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
{% endblock %}", "role/admin/course/list.html.twig", "/code/templates/role/admin/course/list.html.twig");
    }
}
