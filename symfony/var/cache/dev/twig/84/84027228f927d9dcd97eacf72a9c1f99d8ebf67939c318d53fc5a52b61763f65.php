<?php

/* shared/home/hours.html.twig */
class __TwigTemplate_a9796e4173cedc1ad1b5ad98e10d954f802ae79da96bf2ec26de92679a8ff171 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/hours.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/hours.html.twig"));

        // line 1
        echo "<div class=\"content_head\">
    <h2 class=\"content_title\">Hours of Operation</h2>
</div>
<div class=\"content_list\">
    ";
        // line 6
        echo "    <div class=\"content_list_label\">Monday</div>
    <div class=\"content_list_info\">10:00 AM - 8:30 PM</div>
    <div class=\"content_list_label\">Tuesday</div>
    <div class=\"content_list_info\">10:00 AM - 10:00 PM</div>
    <div class=\"content_list_label\">Wednesday</div>
    <div class=\"content_list_info\">10:00 AM - 10:00 PM</div>
    <div class=\"content_list_label\">Thursday</div>
    <div class=\"content_list_info\">10:00 AM - 8:30 PM</div>
    <div class=\"content_list_label\">Friday</div>
    <div class=\"content_list_info\">10:00 AM - 6:00 PM</div>
    <div class=\"content_list_label\">Saturday</div>
    <div class=\"content_list_info\">12:00 PM - 6:00 PM</div>
    <div class=\"content_list_label\">Sunday</div>
    <div class=\"content_list_info\">12:00 PM - 6:00 PM</div>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/home/hours.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content_head\">
    <h2 class=\"content_title\">Hours of Operation</h2>
</div>
<div class=\"content_list\">
    {# TODO put this in db and make editable#}
    <div class=\"content_list_label\">Monday</div>
    <div class=\"content_list_info\">10:00 AM - 8:30 PM</div>
    <div class=\"content_list_label\">Tuesday</div>
    <div class=\"content_list_info\">10:00 AM - 10:00 PM</div>
    <div class=\"content_list_label\">Wednesday</div>
    <div class=\"content_list_info\">10:00 AM - 10:00 PM</div>
    <div class=\"content_list_label\">Thursday</div>
    <div class=\"content_list_info\">10:00 AM - 8:30 PM</div>
    <div class=\"content_list_label\">Friday</div>
    <div class=\"content_list_info\">10:00 AM - 6:00 PM</div>
    <div class=\"content_list_label\">Saturday</div>
    <div class=\"content_list_info\">12:00 PM - 6:00 PM</div>
    <div class=\"content_list_label\">Sunday</div>
    <div class=\"content_list_info\">12:00 PM - 6:00 PM</div>
</div>
", "shared/home/hours.html.twig", "/code/templates/shared/home/hours.html.twig");
    }
}
