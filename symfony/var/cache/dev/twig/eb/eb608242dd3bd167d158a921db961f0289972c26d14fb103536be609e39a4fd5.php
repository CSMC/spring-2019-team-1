<?php

/* role/admin/session/requests.html.twig */
class __TwigTemplate_cab9a3ffb337fc3019dc61ec0a5d88fac533261805fa7272523608c2065244bc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/session/requests.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/requests.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/requests.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <h4>New Requests</h4>
    <table id=\"new-requests\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Section(s)</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Time Requested</th>
            <th>Approve</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Pending</h4>
    <table id=\"pending\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Repeats</th>
            <th>Grades</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Completed</h4>
    <table id=\"completed\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Repeats</th>
            <th>Grades</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Denied Requests</h4>
    <table id=\"denied-requests\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Reason</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <div class=\"modal\"
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 78
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 79
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js\"></script>
    <script>
        \$(function () {
            var options = {
                paging: true,
                searching: true,
                lengthMenu: [10, 25, 50, 75, 100],
                ordering: true,
            };

            var render_sections = function (data, type, row) {
                if (data == null || data.length == 0) {
                    return 'No sections';
                }
                var courses = [];
                for (var index in data) {
                    var section = data[index];
                    var cstring = section.course.department.abbreviation + ' ' + section.course.number;
                    if (!courses[cstring]) {
                        courses[cstring] = []
                    }
                    courses[cstring].push(section.number);
                    // this is terribly inefficient if there are bunches of sections
                    // but most should be sorted already and size should be usually small
                    courses[cstring].sort();
                }
                courses.sort();

                var display = [];
                for (var cstring in courses) {
                    display.push(cstring + '.' + courses[cstring].join('/'));
                }

                return display.join(\", \");
            };
            var render_user = function (data, type, row) {
                return data.firstName + \" \" + data.lastName;
            };
            var render_instructors = function (data, type, row) {
                if (data == null || data.length == 0) {
                    return 'No instructors';
                }
                var instructors = [];
                for (var index in data) {
                    var section = data[index];
                    for (var i in section.instructors) {
                        var instr = section.instructors[i];
                        if (!instructors[instr.username]) {
                            instructors[instr.username] = instr.firstName + \" \" + instr.lastName;
                        }
                    }
                }

                var display = [];
                for (var i in instructors) {
                    display.push(instructors[i]);
                }

                if (display.length == 0) {
                    return 'No instructors';
                }
                return display.join(\", \");
            };
            var render_date_range = function (data, type, row) {
                return moment(row.startDate).format('MM/DD/YY') + ' - ' + moment(row.endDate).format('MM/DD/YY');
            };
            var render_created = function(data, type, row) {
                return moment(row.created).format('MM/DD/YY hh:mm a');
            };

            var render_approve = function (data, type, row) {
                return '<a href=\"/admin/session/create/' + row.id + '\" class=\"btn btn-default\">Approve</a>';
            };
            var render_edit = function (data, type, row) {
                if (row.status == 'new') {
                    return '<a href=\"/admin/session/request/edit/' + row.id + '\" class=\"btn btn-default\">Edit</a>';
                } else {
                    return '<a href=\"/admin/session/edit/' + row.session.id + '\" class=\"btn btn-default\">Edit</a>';
                }
            };
            var render_repeats = function (data, type, row) {
                if (row.session.repeats != null) {
                    return row.session.timeSlots.length + '/' + row.session.repeats;
                }
                return '1/1';
            };
            var render_grades = function (data, type, row) {
                return '<a href=\"/admin/session/grades/' + row.session.id + '\" class=\"btn btn-default\">Grades</a>';
            };

            var render_materials = function (data, type, row) {
                var s = ''
                for(var index in row.files) {
                    s += '<a href=\"/download/' + row.files[index].id + '\">' + row.files[index].name + '</a><br>';
                }
                return s;
            };

            var new_options = Object.assign({
                ajax: {
                    url: '";
        // line 180
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests_feed", ["status" => "new"]);
        echo "',
                    dataSrc: ''
                },
                columns: [
                    {data: 'topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'user',
                        render: render_user
                    },
                    {data: 'type.name'},
                    {
                        render: render_date_range
                    },
                    {render: render_materials},
                    {
                        render: render_created
                    },
                    {
                        render: render_approve
                    },
                    {
                        render: render_edit
                    }
                ]
            }, options);

            \$('#new-requests').DataTable(new_options);

            var pending_options = Object.assign({
                ajax: {
                    url: '";
        // line 214
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests_feed", ["status" => "pending"]);
        echo "',
                    dataSrc: ''
                },
                columns: [
                    {data: 'session.topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'sections',
                        render: render_instructors
                    },
                    {data: 'type.name'},
                    {render: render_date_range},
                    {render: render_materials},
                    {render: render_repeats},
                    {render: render_grades},
                    {render: render_edit}
                ]
            }, options);

            \$('#pending').DataTable(pending_options);

            var completed_options = Object.assign({
                ajax: {
                    url: '";
        // line 240
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests_feed", ["status" => "completed"]);
        echo "',
                    dataSrc: ''
                },
                columns: [
                    {data: 'session.topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'sections',
                        render: render_instructors
                    },
                    {data: 'type.name'},
                    {render: render_date_range},
                    {render: render_materials},
                    {render: render_repeats},
                    {render: render_grades},
                    {render: render_edit}
                ]
            }, options);
            \$('#completed').DataTable(completed_options);

            ";
        // line 264
        echo "            ";
        // line 265
        echo "            ";
        // line 266
        echo "            ";
        // line 267
        echo "            ";
        // line 268
        echo "            ";
        // line 269
        echo "            ";
        // line 270
        echo "            ";
        // line 271
        echo "            ";
        // line 272
        echo "            ";
        // line 273
        echo "            ";
        // line 274
        echo "            ";
        // line 275
        echo "            ";
        // line 276
        echo "            ";
        // line 277
        echo "            ";
        // line 278
        echo "            ";
        // line 279
        echo "            ";
        // line 280
        echo "            ";
        // line 281
        echo "            ";
        // line 282
        echo "            ";
        // line 283
        echo "            ";
        // line 284
        echo "            ";
        // line 285
        echo "
            ";
        // line 287
        echo "        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/session/requests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  389 => 287,  386 => 285,  384 => 284,  382 => 283,  380 => 282,  378 => 281,  376 => 280,  374 => 279,  372 => 278,  370 => 277,  368 => 276,  366 => 275,  364 => 274,  362 => 273,  360 => 272,  358 => 271,  356 => 270,  354 => 269,  352 => 268,  350 => 267,  348 => 266,  346 => 265,  344 => 264,  318 => 240,  289 => 214,  252 => 180,  147 => 79,  138 => 78,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/admin/base.html.twig' %}
{% block body %}
    <h4>New Requests</h4>
    <table id=\"new-requests\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Section(s)</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Time Requested</th>
            <th>Approve</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Pending</h4>
    <table id=\"pending\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Repeats</th>
            <th>Grades</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Completed</h4>
    <table id=\"completed\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Materials</th>
            <th>Repeats</th>
            <th>Grades</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr>
    <h4>Denied Requests</h4>
    <table id=\"denied-requests\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Sections</th>
            <th>Instructor(s)</th>
            <th>Type</th>
            <th>Dates</th>
            <th>Reason</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <div class=\"modal\"
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js\"></script>
    <script>
        \$(function () {
            var options = {
                paging: true,
                searching: true,
                lengthMenu: [10, 25, 50, 75, 100],
                ordering: true,
            };

            var render_sections = function (data, type, row) {
                if (data == null || data.length == 0) {
                    return 'No sections';
                }
                var courses = [];
                for (var index in data) {
                    var section = data[index];
                    var cstring = section.course.department.abbreviation + ' ' + section.course.number;
                    if (!courses[cstring]) {
                        courses[cstring] = []
                    }
                    courses[cstring].push(section.number);
                    // this is terribly inefficient if there are bunches of sections
                    // but most should be sorted already and size should be usually small
                    courses[cstring].sort();
                }
                courses.sort();

                var display = [];
                for (var cstring in courses) {
                    display.push(cstring + '.' + courses[cstring].join('/'));
                }

                return display.join(\", \");
            };
            var render_user = function (data, type, row) {
                return data.firstName + \" \" + data.lastName;
            };
            var render_instructors = function (data, type, row) {
                if (data == null || data.length == 0) {
                    return 'No instructors';
                }
                var instructors = [];
                for (var index in data) {
                    var section = data[index];
                    for (var i in section.instructors) {
                        var instr = section.instructors[i];
                        if (!instructors[instr.username]) {
                            instructors[instr.username] = instr.firstName + \" \" + instr.lastName;
                        }
                    }
                }

                var display = [];
                for (var i in instructors) {
                    display.push(instructors[i]);
                }

                if (display.length == 0) {
                    return 'No instructors';
                }
                return display.join(\", \");
            };
            var render_date_range = function (data, type, row) {
                return moment(row.startDate).format('MM/DD/YY') + ' - ' + moment(row.endDate).format('MM/DD/YY');
            };
            var render_created = function(data, type, row) {
                return moment(row.created).format('MM/DD/YY hh:mm a');
            };

            var render_approve = function (data, type, row) {
                return '<a href=\"/admin/session/create/' + row.id + '\" class=\"btn btn-default\">Approve</a>';
            };
            var render_edit = function (data, type, row) {
                if (row.status == 'new') {
                    return '<a href=\"/admin/session/request/edit/' + row.id + '\" class=\"btn btn-default\">Edit</a>';
                } else {
                    return '<a href=\"/admin/session/edit/' + row.session.id + '\" class=\"btn btn-default\">Edit</a>';
                }
            };
            var render_repeats = function (data, type, row) {
                if (row.session.repeats != null) {
                    return row.session.timeSlots.length + '/' + row.session.repeats;
                }
                return '1/1';
            };
            var render_grades = function (data, type, row) {
                return '<a href=\"/admin/session/grades/' + row.session.id + '\" class=\"btn btn-default\">Grades</a>';
            };

            var render_materials = function (data, type, row) {
                var s = ''
                for(var index in row.files) {
                    s += '<a href=\"/download/' + row.files[index].id + '\">' + row.files[index].name + '</a><br>';
                }
                return s;
            };

            var new_options = Object.assign({
                ajax: {
                    url: '{{ path('admin_session_requests_feed', {status: 'new'}) }}',
                    dataSrc: ''
                },
                columns: [
                    {data: 'topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'user',
                        render: render_user
                    },
                    {data: 'type.name'},
                    {
                        render: render_date_range
                    },
                    {render: render_materials},
                    {
                        render: render_created
                    },
                    {
                        render: render_approve
                    },
                    {
                        render: render_edit
                    }
                ]
            }, options);

            \$('#new-requests').DataTable(new_options);

            var pending_options = Object.assign({
                ajax: {
                    url: '{{ path('admin_session_requests_feed', {status: 'pending'}) }}',
                    dataSrc: ''
                },
                columns: [
                    {data: 'session.topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'sections',
                        render: render_instructors
                    },
                    {data: 'type.name'},
                    {render: render_date_range},
                    {render: render_materials},
                    {render: render_repeats},
                    {render: render_grades},
                    {render: render_edit}
                ]
            }, options);

            \$('#pending').DataTable(pending_options);

            var completed_options = Object.assign({
                ajax: {
                    url: '{{ path('admin_session_requests_feed', {status: 'completed'}) }}',
                    dataSrc: ''
                },
                columns: [
                    {data: 'session.topic'},
                    {
                        data: 'sections',
                        render: render_sections
                    },
                    {
                        data: 'sections',
                        render: render_instructors
                    },
                    {data: 'type.name'},
                    {render: render_date_range},
                    {render: render_materials},
                    {render: render_repeats},
                    {render: render_grades},
                    {render: render_edit}
                ]
            }, options);
            \$('#completed').DataTable(completed_options);

            {#var denied_options = Object.assign({#}
            {#ajax: {#}
            {#url: '{{ path('admin_session_requests_feed', {status: 'denied'}) }}',#}
            {#dataSrc: ''#}
            {#},#}
            {#columns: [#}
            {#{data: 'topic'},#}
            {#{#}
            {#data: 'sections',#}
            {#render: render_sections#}
            {#},#}
            {#{#}
            {#data: 'user',#}
            {#render: render_user#}
            {#},#}
            {#{data: 'type.name'},#}
            {#{#}
            {#render: render_date_range#}
            {#},#}
            {#{data: 'reason'}#}
            {#]#}
            {#});#}

            {#\$('#denied-requests').DataTable(denied_options);#}
        });
    </script>
{% endblock %}", "role/admin/session/requests.html.twig", "/code/templates/role/admin/session/requests.html.twig");
    }
}
