<?php

/* role/mentor/profile.html.twig */
class __TwigTemplate_1c4ca22ace29a3f480bcaafd4303d9b9f6ceb675c97e4f769898205bfbc126de extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/profile.html.twig", 1);
        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/profile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/profile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/global.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/mentor_profile.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.css\">

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "    <!--
Render example :
    <small>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 16, $this->source); })()), "username", []), "html", null, true);
        echo "</small>
-->

    <style>
        .occurences_view.modal {
            display: none;
            position: absolute;
            top: 200px;
            z-index: 1200;
            max-width: 900px;
            box-sizing: border-box;
            width: 90%;
            background-color: #f7fbfc;
            padding: 15px 30px;
            border-radius: 3px;
            box-shadow: 0 0 70px #444;
            text-align: left;
            transition: all .5s;
            line-height: 2em;
            right: initial;
            bottom: initial;
            overflow: initial;
            left: initial;
        }
    </style>
    <div class=\"\">
        <div class=\"container\">
            <div class=\"row pt\">
                <br>
                <br>
                <div class=\"col-md-6\">

                    <div class=\"row\">
                        <div class=\"col-md-12 info-board\">
                            <div class=\"main-name\">
                                <div class=\"row\">
                                    <div class=\"col-md-9 align-middle  \">
                                        <!--
                                            ***
                                            Todo: Render your user.\"property\" here
                                            ***
                                        -->
                                        <h2>";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 58, $this->source); })()), "firstName", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 58, $this->source); })()), "lastName", []), "html", null, true);
        echo "</h2>

                                    </div>
                                    <div class=\"col-md-3 main-btn\">
                                        <a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_profile", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 62, $this->source); })()), "username", [])]), "html", null, true);
        echo "\"
                                           class=\"btn btn-info btn-block btn-edit\" aria-label=\"Left Align\">
                                            <span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span> Edit
                                            Profile
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <br>
                            <div class=\"attr-field\">
                                <p>Your total point:</p>
                                <p>";
        // line 75
        echo twig_escape_filter($this->env, (isset($context["user_point"]) || array_key_exists("user_point", $context) ? $context["user_point"] : (function () { throw new Twig_Error_Runtime('Variable "user_point" does not exist.', 75, $this->source); })()), "html", null, true);
        echo "</p>
                                <a href=\"#\" id=\"view_history\">View History</a>
                            </div>
                            <!-- new code start-->
                            <div class=\"occurences_view modal fade\" aria-hidden=\"true\">
                                <div>
                                    <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
                                        <button type=\"submit\" class=\"searchButton\">
                                        <i class=\"fa fa-search\"></i>
                                        </button>
                                </div>
                                <div class=\"row table-outer\" style=\"padding-bottom:8px\">
                                    <table class=\"table table-hover\" id=\"closedTable\">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Date Occurred</th>
                                                <th>Points</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        ";
        // line 96
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["closedOccurrences"]) || array_key_exists("closedOccurrences", $context) ? $context["closedOccurrences"] : (function () { throw new Twig_Error_Runtime('Variable "closedOccurrences" does not exist.', 96, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["occurrence"]) {
            // line 97
            echo "                                            <tr>
                                                <td> ";
            // line 98
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\OccurrenceExtension']->type($context["occurrence"]), "html", null, true);
            echo " </td>
                                                <td> ";
            // line 99
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->extensions['App\Twig\OccurrenceExtension']->date($context["occurrence"]), "m/d/Y"), "html", null, true);
            echo " </td>
                                                <td> ";
            // line 100
            echo twig_escape_filter($this->env, $this->extensions['App\Twig\OccurrenceExtension']->points($context["occurrence"]), "html", null, true);
            echo " </td>
                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['occurrence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "                                        </tbody>
                                    </table>
                                </div>
                                 <div class=\"modal-footer\">
                                    <button id =\"done\"class=\"btn btn-success\">Done</button>
                                </div>
                            </div>
                            <!-- new code end-->
                            <div class=\"attr-field\">
                                <p>Preferred Name:</p>
                                <p>";
        // line 113
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 113, $this->source); })()), "preferredName", []), "html", null, true);
        echo "</p>
                            </div>

                            ";
        // line 117
        echo "                            ";
        $context["prefNameReq"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 117, $this->source); })()), "profile", []), "preferredNameModificationRequest", []);
        // line 118
        echo "                            ";
        if ((isset($context["prefNameReq"]) || array_key_exists("prefNameReq", $context) ? $context["prefNameReq"] : (function () { throw new Twig_Error_Runtime('Variable "prefNameReq" does not exist.', 118, $this->source); })())) {
            // line 119
            echo "                                <div class=\"panel panel-default change-req-panel\">
                                    <p class=\"change-req-label\">Pending Approval:</p>
                                    <div class=\"attr-field change-req-field\">
                                        <p>Preferred Name:</p>
                                        <p>";
            // line 123
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 123, $this->source); })()), "profile", []), "preferredNameModificationRequest", []), "value", []), "html", null, true);
            echo "</p>
                                    </div>
                                    ";
            // line 125
            if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 125, $this->source); })())) {
                // line 126
                echo "                                        <div class=\"change-req-btn-row\">
                                            <a href=\"";
                // line 127
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_reject_mod_request", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["prefNameReq"]) || array_key_exists("prefNameReq", $context) ? $context["prefNameReq"] : (function () { throw new Twig_Error_Runtime('Variable "prefNameReq" does not exist.', 127, $this->source); })()), "id", [])]), "html", null, true);
                echo "\"
                                               class=\"btn btn-default btn-reject\">
                                                <span class=\"glyphicon glyphicon-remove \"></span> Reject
                                            </a>
                                            <a href=\"";
                // line 131
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_mod_request", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["prefNameReq"]) || array_key_exists("prefNameReq", $context) ? $context["prefNameReq"] : (function () { throw new Twig_Error_Runtime('Variable "prefNameReq" does not exist.', 131, $this->source); })()), "id", [])]), "html", null, true);
                echo "\"
                                               class=\"btn btn-approve\">
                                                <span class=\"glyphicon glyphicon-ok\"></span> Approve
                                            </a>
                                        </div>
                                    ";
            }
            // line 137
            echo "                                </div>
                            ";
        }
        // line 139
        echo "
                            <div class=\"attr-field\">
                                <p>Birth date:</p>
                                <p>";
        // line 142
        echo twig_escape_filter($this->env, ((twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 142, $this->source); })()), "profile", []), "birthDate", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 142, $this->source); })()), "profile", []), "birthDate", []), "m/d/Y"))), "html", null, true);
        echo "</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Expected graduation date: </p>
                                <p>";
        // line 146
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 146, $this->source); })()), "profile", []), "expectedGraduationSemester", []), "html", null, true);
        echo "</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Phone number: </p>
                                <p>";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['App\Twig\AppExtension']->phoneFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 150, $this->source); })()), "profile", []), "phoneNumber", [])), "html", null, true);
        echo "</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Course Expertise Levels: </p>
                                <br>

                                <div class=\"level-list\">
                                    ";
        // line 157
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 157, $this->source); })()), "profile", []), "specialties", []));
        foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
            // line 158
            echo "                                        <div class=\"row\">
                                            <p class=\"progress-desc\">";
            // line 159
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "name", []), "html", null, true);
            echo "</p>
                                            <br>
                                            <div class=\"col-md-11 progress-container\">
                                                <div class=\"progress\">
                                                    <div class=\"progress-bar\" role=\"progressbar\"
                                                         style=\"width: ";
            // line 164
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["specialty"], "rating", []) * 20), "html", null, true);
            echo "%\"
                                                         aria-valuenow=\"";
            // line 165
            echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, $context["specialty"], "rating", []) * 20), "html", null, true);
            echo "\"
                                                         aria-valuemin=\"0\" aria-valuemax=\"100\">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=\"col-md-1\">
                                                <p>";
            // line 171
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["specialty"], "rating", []), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 175
        echo "                                </div>
                            </div>

                            ";
        // line 178
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 178, $this->source); })()), "profile", []), "dietaryRestrictions", [])) {
            // line 179
            echo "                                <div class=\"attr-field\">
                                    <p>Dietary Restrictions: </p>
                                    <br>
                                    <p class=\"dietary\">";
            // line 182
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 182, $this->source); })()), "profile", []), "dietaryRestrictions", []), "html", null, true);
            echo "</p>
                                </div>
                            ";
        }
        // line 185
        echo "
                            ";
        // line 186
        if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 186, $this->source); })())) {
            // line 187
            echo "                                <div class=\"attr-field\">

                                    <p>Admin Notes: </p>
                                    <br>
                                    <p class=\"dietary\">";
            // line 191
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 191, $this->source); })()), "profile", []), "adminNotes", []), "html", null, true);
            echo "</p>

                                </div>
                            ";
        }
        // line 195
        echo "
                            <h2>Notification Preferences:</h2>
                            ";
        // line 197
        $context["prefs"] = twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 197, $this->source); })()), "notificationPreferences", []);
        // line 198
        echo "
                            ";
        // line 199
        if (twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 199, $this->source); })()), "hasNotifications", [])) {
            // line 200
            echo "                                <div class=\"attr-field\">
                                    <p>Preferred Delivery Method: </p> <br>

                                    ";
            // line 203
            if (twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 203, $this->source); })()), "useEmail", [])) {
                // line 204
                echo "                                        <p>Email: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 204, $this->source); })()), "preferredEmail", []), "html", null, true);
                echo "</p> <br>
                                    ";
            }
            // line 206
            echo "
                                    ";
            // line 207
            if (twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 207, $this->source); })()), "usePhoneNumber", [])) {
                // line 208
                echo "                                        <p>Text: ";
                echo twig_escape_filter($this->env, $this->extensions['App\Twig\AppExtension']->phoneFilter(twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 208, $this->source); })()), "preferredPhoneNumber", [])), "html", null, true);
                echo ",
                                            ";
                // line 209
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 209, $this->source); })()), "preferredPhoneNumberCarrier", []), "html", null, true);
                echo "</p>
                                    ";
            }
            // line 211
            echo "                                </div>

                                <div class=\"attr-field\">
                                    <p>I would like to receive notifications:</p> <br>

                                    ";
            // line 216
            if (twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 216, $this->source); })()), "notifyWhenAssigned", [])) {
                // line 217
                echo "                                        <p>When I am assigned to a session</p> <br>
                                    ";
            }
            // line 219
            echo "
                                    ";
            // line 220
            if (twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 220, $this->source); })()), "notifyBeforeSession", [])) {
                // line 221
                echo "                                        ";
                $context["days"] = twig_get_attribute($this->env, $this->source, (isset($context["prefs"]) || array_key_exists("prefs", $context) ? $context["prefs"] : (function () { throw new Twig_Error_Runtime('Variable "prefs" does not exist.', 221, $this->source); })()), "sessionReminderAdvanceDays", []);
                // line 222
                echo "                                        ";
                $context["daysString"] = "day|days";
                // line 223
                echo "                                        <p>";
                echo twig_escape_filter($this->env, (isset($context["days"]) || array_key_exists("days", $context) ? $context["days"] : (function () { throw new Twig_Error_Runtime('Variable "days" does not exist.', 223, $this->source); })()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->transchoice((isset($context["daysString"]) || array_key_exists("daysString", $context) ? $context["daysString"] : (function () { throw new Twig_Error_Runtime('Variable "daysString" does not exist.', 223, $this->source); })()), (isset($context["days"]) || array_key_exists("days", $context) ? $context["days"] : (function () { throw new Twig_Error_Runtime('Variable "days" does not exist.', 223, $this->source); })())), "html", null, true);
                echo " before an upcoming
                                            session</p>
                                    ";
            }
            // line 226
            echo "                                </div>
                            ";
        } else {
            // line 228
            echo "                                <p>Notifications are disabled</p>
                            ";
        }
        // line 230
        echo "                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                        </div>
                    </div>

                </div>
                <div class=\"col-md-6\">
                    <div class=\"profile-picture-frame text-center\">
                        ";
        // line 240
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 240, $this->source); })()), "profilePicture", []))) {
            // line 241
            echo "                            <img class=\"profile-picture\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 241, $this->source); })()), "username", [])]), "html", null, true);
            echo "\">
                        ";
        } else {
            // line 243
            echo "                            ";
            // line 244
            echo "                            <img class=\"profile-picture\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/user.png"), "html", null, true);
            echo "\">
                        ";
        }
        // line 246
        echo "                    </div>

                    ";
        // line 249
        echo "                    ";
        $context["pictureRequest"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 249, $this->source); })()), "profile", []), "profilePictureModificationRequest", []);
        // line 250
        echo "                    ";
        if ( !(null === (isset($context["pictureRequest"]) || array_key_exists("pictureRequest", $context) ? $context["pictureRequest"] : (function () { throw new Twig_Error_Runtime('Variable "pictureRequest" does not exist.', 250, $this->source); })()))) {
            // line 251
            echo "                        <div class=\"panel panel-default change-req-panel\">
                            <p class=\"change-req-label\">Pending Approval:</p>
                            <img class=\"profile-picture\"
                                 src=\"";
            // line 254
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_requested_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 254, $this->source); })()), "username", [])]), "html", null, true);
            echo "\">
                            ";
            // line 255
            if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 255, $this->source); })())) {
                // line 256
                echo "                                <div class=\"change-req-btn-row\">
                                    <a href=\"";
                // line 257
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_reject_mod_request", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["pictureRequest"]) || array_key_exists("pictureRequest", $context) ? $context["pictureRequest"] : (function () { throw new Twig_Error_Runtime('Variable "pictureRequest" does not exist.', 257, $this->source); })()), "id", [])]), "html", null, true);
                echo "\"
                                       class=\"btn btn-default btn-reject\">
                                        <span class=\"glyphicon glyphicon-remove \"></span> Reject
                                    </a>
                                    <div id=\"crop_current_profile_picture\" class=\"btn btn-default btn-crop\">
                                        <span class=\"glyphicon glyphicon-scissors\"></span> Crop
                                    </div>
                                    <a href=\"";
                // line 264
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_approve_mod_request", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["pictureRequest"]) || array_key_exists("pictureRequest", $context) ? $context["pictureRequest"] : (function () { throw new Twig_Error_Runtime('Variable "pictureRequest" does not exist.', 264, $this->source); })()), "id", [])]), "html", null, true);
                echo "\"
                                       class=\"btn btn-approve\">
                                        <span class=\"glyphicon glyphicon-ok\"></span> Approve
                                    </a>
                                </div>
                            ";
            }
            // line 270
            echo "                        </div>
                    ";
        }
        // line 272
        echo "                </div>
            </div>
        </div>
    </div>

    ";
        // line 277
        if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 277, $this->source); })())) {
            // line 278
            echo "        <div class=\"panel panel-default pic-upload-panel\" style=\"display: none;\">
            <div class=\"panel-heading\">
            </div>
            <div class=\"panel-body\">
                <div id=\"";
            // line 282
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 282, $this->source); })()), "id", []), "html", null, true);
            echo "\" class=\"dropzone\"
                     style=\"width: 100%; height: 0; min-height: 0; padding-bottom: calc(100% - 24px)\"
                     data-crop
                     data-canvas data-image data-user=\"";
            // line 285
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 285, $this->source); })()), "id", []), "html", null, true);
            echo "\" data-username=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 285, $this->source); })()), "username", []), "html", null, true);
            echo "\"
                     data-has-image=\"";
            // line 286
            echo twig_escape_filter($this->env,  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 286, $this->source); })()), "profile", []), "image", [])), "html", null, true);
            echo "\">
                </div>
            </div>
            <div class=\"panel-footer\">
                <div class=\"row\">
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
            // line 292
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 292, $this->source); })()), "id", []), "html", null, true);
            echo "\">
                            <span class=\"glyphicon glyphicon-open\"></span>
                            <span class=\"sr-only\">Upload</span>
                        </button>
                    </div>
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
            // line 298
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 298, $this->source); })()), "id", []), "html", null, true);
            echo "\">
                            <span class=\"glyphicon glyphicon-trash\"></span>
                            <span class=\"sr-only\">Trash</span>
                        </button>
                    </div>
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
            // line 304
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 304, $this->source); })()), "id", []), "html", null, true);
            echo "\">
                            <span class=\"fas fa-compress\"></span>
                            <span class=\"sr-only\">Crop</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id=\"template-container\" hidden>
            ";
            // line 314
            echo "            <div class=\"dz-preview dz-file-preview\"
                 style=\"width: calc(100% - 32px); height: 0; min-height: 0; padding-bottom: calc(100% - 32px);\">
                <div class=\"dz-image\" style=\"width: 100%; height: 0; min-height: 0; padding-bottom: 100%;\">
                    <img data-dz-thumbnail style=\"width: 100%; border: 2px solid red; border-radius: 20px\"/>
                </div>
                <div class=\"dz-details\">
                    <div class=\"dz-size\">
                        <span data-dz-size></span>
                    </div>
                    ";
            // line 324
            echo "                    ";
            // line 325
            echo "                    ";
            // line 326
            echo "                </div>
                <div class=\"dz-error-message\">
                    <span data-dz-errormessage></span>
                </div>
            </div>
        </div>
        <div id=\"admin-modal\" class=\"modal fade\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">

                    </div>
                    <div class=\"modal-body\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <img id=\"admin-modal-image\" data-active>
                            </div>
                        </div>

                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button class=\"btn btn-success\">Accept</button>
                    </div>
                </div>
            </div>
        </div>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 355
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 356
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(document).ready(function () {

            let closedTable = \$('#closedTable').DataTable({
                language: {
                    \"paginate\": {
                        \"previous\": \"<<\",
                        \"next\": \">>\"
                    }
                },
                dom: 'rtp',
                // dom: 'rtp', //if want to show paginate,you should add p
                columnDefs: [
                    {\"visible\": true, targets: [0,1,2]}, // Hides ID column, even from inspection
                    {\"searchable\": true, targets: [0,1,2]}, // Makes filterType searchable
                    {'orderable': true, targets: [0,1,2]}, // Disallow ordering the columns that are just icons
                ],
            });
            
            \$(\".searchTerm\").on( 'keyup change', function () {
                closedTable.search( this.value ).draw();
                } );
            });
            \$('#view_history').on('click', function () {
                \$('.occurences_view').modal('show');
            });
            \$('#done').on('click',function(){
                \$('.occurences_view').modal('hide');
            })
        </script>
    ";
        // line 387
        if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 387, $this->source); })())) {
            // line 388
            echo "        <script type=\"text/javascript\"
                src=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js\"></script>
        <script type=\"text/javascript\"
                src=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.js\"></script>
        <script>
            \$('#crop_current_profile_picture').on('click', function () {
                var dropzone = \$('.dropzone').get(0).dropzone;
                let originalPictureURL = '";
            // line 395
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("origin_requested_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 395, $this->source); })()), "username", [])]), "html", null, true);
            echo "';
                var file = {name: \"file\", dataURL: originalPictureURL};

                if (dropzone.previewsContainer) {
                    file.previewElement = Dropzone.createElement(dropzone.options.previewTemplate.trim());

                    dropzone.previewsContainer.appendChild(file.previewElement);

                    if (dropzone.options.addRemoveLinks) {
                        file._removeLink = Dropzone.createElement(\"<a class=\\\"dz-remove\\\" href=\\\"javascript:undefined;\\\" data-dz-remove>\" + dropzone.options.dictRemoveFile + \"</a>\");
                        file.previewElement.appendChild(file._removeLink);
                    }
                }
                \$('#admin-modal-image').data('active', dropzone.element.id);
                \$('#admin-modal-image').cropper('replace', file.dataURL);
                \$('#admin-modal').modal('show');
            });

            var minWidth = window.innerWidth < 500 ? window.innerWidth * 0.8 : window.innerWidth * 0.6;
            var minHeight = window.innerHeight < 500 ? window.innerHeight * 0.8 : window.innerHeight * 0.6;

            \$('#admin-modal-image').cropper({
                aspectRatio: 1,
                viewMode: 2,
                minContainerWidth: minWidth,
                minContainerHeight: minHeight,
                responsive: true,
                ready: function () {
                    \$('.cropper-container').css('margin', 'auto');
                    \$('.modal-content').css('width', minWidth + 30);
                    \$('#admin-modal .btn-success').off('click').on('click', function (event) {
                        var active = \$('#admin-modal-image').data('active');

                        var crop = JSON.stringify(\$('#admin-modal-image').cropper('getCropBoxData'));
                        var canvas = JSON.stringify(\$('#admin-modal-image').cropper('getCanvasData'));
                        var image = JSON.stringify(\$('#admin-modal-image').cropper('getImageData'));
                        \$('#' + active).data('crop', crop);
                        \$('#' + active).data('canvas', canvas);
                        \$('#' + active).data('image', image);

                        \$('#admin-modal').modal('hide');

                        \$.ajax({
                            type: 'POST',
                            url: \"";
            // line 439
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("update_profile_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 439, $this->source); })()), "username", [])]), "html", null, true);
            echo "\",
                            data: {
                                'user': active,
                                'crop': crop,
                                'canvas': canvas,
                                'image': image
                            },
                            complete: function (response) {
                                if (response.responseText) {
                                    location.reload();
                                }
                            }
                        });
                    });
                }
            });
            \$('#admin-modal').on('show.bs.modal', function (event) {
                var active = \$('#admin-modal-image').data('active');

                var crop = \$('#' + active).data('crop');
                var canvas = \$('#' + active).data('canvas');
                var image = \$('#' + active).data('image');
                if (crop && canvas && image) {
                    \$('#admin-modal-image').cropper('setCropBoxData', JSON.parse(crop));
                    \$('#admin-modal-image').cropper('setCanvasData', JSON.parse(canvas));
                    \$('#admin-modal-image').cropper('setImageData', JSON.parse(image));
                }
            });
            \$('#admin-modal').on('hidden.bs.modal', function (event) {
                \$('#admin-modal').data('bs.modal', null);
            });
           
        </script>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  750 => 439,  703 => 395,  694 => 388,  692 => 387,  657 => 356,  648 => 355,  610 => 326,  608 => 325,  606 => 324,  595 => 314,  583 => 304,  574 => 298,  565 => 292,  556 => 286,  550 => 285,  544 => 282,  538 => 278,  536 => 277,  529 => 272,  525 => 270,  516 => 264,  506 => 257,  503 => 256,  501 => 255,  497 => 254,  492 => 251,  489 => 250,  486 => 249,  482 => 246,  476 => 244,  474 => 243,  468 => 241,  466 => 240,  454 => 230,  450 => 228,  446 => 226,  437 => 223,  434 => 222,  431 => 221,  429 => 220,  426 => 219,  422 => 217,  420 => 216,  413 => 211,  408 => 209,  403 => 208,  401 => 207,  398 => 206,  392 => 204,  390 => 203,  385 => 200,  383 => 199,  380 => 198,  378 => 197,  374 => 195,  367 => 191,  361 => 187,  359 => 186,  356 => 185,  350 => 182,  345 => 179,  343 => 178,  338 => 175,  328 => 171,  319 => 165,  315 => 164,  307 => 159,  304 => 158,  300 => 157,  290 => 150,  283 => 146,  276 => 142,  271 => 139,  267 => 137,  258 => 131,  251 => 127,  248 => 126,  246 => 125,  241 => 123,  235 => 119,  232 => 118,  229 => 117,  223 => 113,  211 => 103,  202 => 100,  198 => 99,  194 => 98,  191 => 97,  187 => 96,  163 => 75,  147 => 62,  138 => 58,  93 => 16,  89 => 14,  80 => 13,  64 => 6,  60 => 5,  55 => 4,  46 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/global.css') }}\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/mentor_profile.css') }}\"/>
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.css\">

{% endblock %}

{% block body %}
    <!--
Render example :
    <small>{{ user.username }}</small>
-->

    <style>
        .occurences_view.modal {
            display: none;
            position: absolute;
            top: 200px;
            z-index: 1200;
            max-width: 900px;
            box-sizing: border-box;
            width: 90%;
            background-color: #f7fbfc;
            padding: 15px 30px;
            border-radius: 3px;
            box-shadow: 0 0 70px #444;
            text-align: left;
            transition: all .5s;
            line-height: 2em;
            right: initial;
            bottom: initial;
            overflow: initial;
            left: initial;
        }
    </style>
    <div class=\"\">
        <div class=\"container\">
            <div class=\"row pt\">
                <br>
                <br>
                <div class=\"col-md-6\">

                    <div class=\"row\">
                        <div class=\"col-md-12 info-board\">
                            <div class=\"main-name\">
                                <div class=\"row\">
                                    <div class=\"col-md-9 align-middle  \">
                                        <!--
                                            ***
                                            Todo: Render your user.\"property\" here
                                            ***
                                        -->
                                        <h2>{{ user.firstName }} {{ user.lastName }}</h2>

                                    </div>
                                    <div class=\"col-md-3 main-btn\">
                                        <a href=\"{{ path(\"edit_profile\", {\"username\" : user.username}) }}\"
                                           class=\"btn btn-info btn-block btn-edit\" aria-label=\"Left Align\">
                                            <span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span> Edit
                                            Profile
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <br>
                            <div class=\"attr-field\">
                                <p>Your total point:</p>
                                <p>{{ user_point }}</p>
                                <a href=\"#\" id=\"view_history\">View History</a>
                            </div>
                            <!-- new code start-->
                            <div class=\"occurences_view modal fade\" aria-hidden=\"true\">
                                <div>
                                    <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
                                        <button type=\"submit\" class=\"searchButton\">
                                        <i class=\"fa fa-search\"></i>
                                        </button>
                                </div>
                                <div class=\"row table-outer\" style=\"padding-bottom:8px\">
                                    <table class=\"table table-hover\" id=\"closedTable\">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Date Occurred</th>
                                                <th>Points</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {% for occurrence in closedOccurrences %}
                                            <tr>
                                                <td> {{ occurrence | occurrenceType }} </td>
                                                <td> {{ occurrence | occurrenceDate | date(\"m/d/Y\") }} </td>
                                                <td> {{ occurrence | occurrencePoints }} </td>
                                            </tr>
                                        {% endfor %}
                                        </tbody>
                                    </table>
                                </div>
                                 <div class=\"modal-footer\">
                                    <button id =\"done\"class=\"btn btn-success\">Done</button>
                                </div>
                            </div>
                            <!-- new code end-->
                            <div class=\"attr-field\">
                                <p>Preferred Name:</p>
                                <p>{{ user.preferredName }}</p>
                            </div>

                            {#Change requested alert for preferred name#}
                            {% set prefNameReq = user.profile.preferredNameModificationRequest %}
                            {% if prefNameReq %}
                                <div class=\"panel panel-default change-req-panel\">
                                    <p class=\"change-req-label\">Pending Approval:</p>
                                    <div class=\"attr-field change-req-field\">
                                        <p>Preferred Name:</p>
                                        <p>{{ user.profile.preferredNameModificationRequest.value }}</p>
                                    </div>
                                    {% if isAdmin %}
                                        <div class=\"change-req-btn-row\">
                                            <a href=\"{{ path('admin_reject_mod_request', {'id': prefNameReq.id}) }}\"
                                               class=\"btn btn-default btn-reject\">
                                                <span class=\"glyphicon glyphicon-remove \"></span> Reject
                                            </a>
                                            <a href=\"{{ path('admin_approve_mod_request', {'id': prefNameReq.id}) }}\"
                                               class=\"btn btn-approve\">
                                                <span class=\"glyphicon glyphicon-ok\"></span> Approve
                                            </a>
                                        </div>
                                    {% endif %}
                                </div>
                            {% endif %}

                            <div class=\"attr-field\">
                                <p>Birth date:</p>
                                <p>{{ user.profile.birthDate is empty ? \"\" : user.profile.birthDate | date(\"m/d/Y\") }}</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Expected graduation date: </p>
                                <p>{{ user.profile.expectedGraduationSemester }}</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Phone number: </p>
                                <p>{{ user.profile.phoneNumber | phone }}</p>
                            </div>
                            <div class=\"attr-field\">
                                <p>Course Expertise Levels: </p>
                                <br>

                                <div class=\"level-list\">
                                    {% for specialty in user.profile.specialties %}
                                        <div class=\"row\">
                                            <p class=\"progress-desc\">{{ specialty.subject.name }}</p>
                                            <br>
                                            <div class=\"col-md-11 progress-container\">
                                                <div class=\"progress\">
                                                    <div class=\"progress-bar\" role=\"progressbar\"
                                                         style=\"width: {{ specialty.rating * 20 }}%\"
                                                         aria-valuenow=\"{{ specialty.rating * 20 }}\"
                                                         aria-valuemin=\"0\" aria-valuemax=\"100\">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=\"col-md-1\">
                                                <p>{{ specialty.rating }}</p>
                                            </div>
                                        </div>
                                    {% endfor %}
                                </div>
                            </div>

                            {% if user.profile.dietaryRestrictions %}
                                <div class=\"attr-field\">
                                    <p>Dietary Restrictions: </p>
                                    <br>
                                    <p class=\"dietary\">{{ user.profile.dietaryRestrictions }}</p>
                                </div>
                            {% endif %}

                            {% if isAdmin %}
                                <div class=\"attr-field\">

                                    <p>Admin Notes: </p>
                                    <br>
                                    <p class=\"dietary\">{{ user.profile.adminNotes }}</p>

                                </div>
                            {% endif %}

                            <h2>Notification Preferences:</h2>
                            {% set prefs = user.notificationPreferences %}

                            {% if prefs.hasNotifications %}
                                <div class=\"attr-field\">
                                    <p>Preferred Delivery Method: </p> <br>

                                    {% if prefs.useEmail %}
                                        <p>Email: {{ prefs.preferredEmail }}</p> <br>
                                    {% endif %}

                                    {% if prefs.usePhoneNumber %}
                                        <p>Text: {{ prefs.preferredPhoneNumber | phone }},
                                            {{ prefs.preferredPhoneNumberCarrier }}</p>
                                    {% endif %}
                                </div>

                                <div class=\"attr-field\">
                                    <p>I would like to receive notifications:</p> <br>

                                    {% if prefs.notifyWhenAssigned %}
                                        <p>When I am assigned to a session</p> <br>
                                    {% endif %}

                                    {% if prefs.notifyBeforeSession %}
                                        {% set days = prefs.sessionReminderAdvanceDays %}
                                        {% set daysString = 'day|days' %}
                                        <p>{{ days }} {{ daysString | transchoice(days) }} before an upcoming
                                            session</p>
                                    {% endif %}
                                </div>
                            {% else %}
                                <p>Notifications are disabled</p>
                            {% endif %}
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                        </div>
                    </div>

                </div>
                <div class=\"col-md-6\">
                    <div class=\"profile-picture-frame text-center\">
                        {% if user.profilePicture is not null %}
                            <img class=\"profile-picture\" src=\"{{ path('profile_image', {'username': user.username}) }}\">
                        {% else %}
                            {#Use the placeholder image#}
                            <img class=\"profile-picture\" src=\"{{ asset('build/images/user.png') }}\">
                        {% endif %}
                    </div>

                    {#Change requested alert for profile picture#}
                    {% set pictureRequest = user.profile.profilePictureModificationRequest %}
                    {% if pictureRequest is not null %}
                        <div class=\"panel panel-default change-req-panel\">
                            <p class=\"change-req-label\">Pending Approval:</p>
                            <img class=\"profile-picture\"
                                 src=\"{{ path('profile_requested_image', {'username': user.username}) }}\">
                            {% if isAdmin %}
                                <div class=\"change-req-btn-row\">
                                    <a href=\"{{ path('admin_reject_mod_request', {'id': pictureRequest.id}) }}\"
                                       class=\"btn btn-default btn-reject\">
                                        <span class=\"glyphicon glyphicon-remove \"></span> Reject
                                    </a>
                                    <div id=\"crop_current_profile_picture\" class=\"btn btn-default btn-crop\">
                                        <span class=\"glyphicon glyphicon-scissors\"></span> Crop
                                    </div>
                                    <a href=\"{{ path('admin_approve_mod_request', {'id': pictureRequest.id}) }}\"
                                       class=\"btn btn-approve\">
                                        <span class=\"glyphicon glyphicon-ok\"></span> Approve
                                    </a>
                                </div>
                            {% endif %}
                        </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>

    {% if isAdmin %}
        <div class=\"panel panel-default pic-upload-panel\" style=\"display: none;\">
            <div class=\"panel-heading\">
            </div>
            <div class=\"panel-body\">
                <div id=\"{{ user.id }}\" class=\"dropzone\"
                     style=\"width: 100%; height: 0; min-height: 0; padding-bottom: calc(100% - 24px)\"
                     data-crop
                     data-canvas data-image data-user=\"{{ user.id }}\" data-username=\"{{ user.username }}\"
                     data-has-image=\"{{ user.profile.image is not null }}\">
                </div>
            </div>
            <div class=\"panel-footer\">
                <div class=\"row\">
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                            <span class=\"glyphicon glyphicon-open\"></span>
                            <span class=\"sr-only\">Upload</span>
                        </button>
                    </div>
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                            <span class=\"glyphicon glyphicon-trash\"></span>
                            <span class=\"sr-only\">Trash</span>
                        </button>
                    </div>
                    <div class=\"col-xs-4\">
                        <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                            <span class=\"fas fa-compress\"></span>
                            <span class=\"sr-only\">Crop</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div id=\"template-container\" hidden>
            {# TODO add progress bar and indication if uploaded or not #}
            <div class=\"dz-preview dz-file-preview\"
                 style=\"width: calc(100% - 32px); height: 0; min-height: 0; padding-bottom: calc(100% - 32px);\">
                <div class=\"dz-image\" style=\"width: 100%; height: 0; min-height: 0; padding-bottom: 100%;\">
                    <img data-dz-thumbnail style=\"width: 100%; border: 2px solid red; border-radius: 20px\"/>
                </div>
                <div class=\"dz-details\">
                    <div class=\"dz-size\">
                        <span data-dz-size></span>
                    </div>
                    {#<div class=\"dz-filename\">#}
                    {#<span data-dz-name></span>#}
                    {#</div>#}
                </div>
                <div class=\"dz-error-message\">
                    <span data-dz-errormessage></span>
                </div>
            </div>
        </div>
        <div id=\"admin-modal\" class=\"modal fade\" aria-hidden=\"true\">
            <div class=\"modal-dialog\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">

                    </div>
                    <div class=\"modal-body\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <img id=\"admin-modal-image\" data-active>
                            </div>
                        </div>

                    </div>
                    <div class=\"modal-footer\">
                        <button class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button class=\"btn btn-success\">Accept</button>
                    </div>
                </div>
            </div>
        </div>
    {% endif %}
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(document).ready(function () {

            let closedTable = \$('#closedTable').DataTable({
                language: {
                    \"paginate\": {
                        \"previous\": \"<<\",
                        \"next\": \">>\"
                    }
                },
                dom: 'rtp',
                // dom: 'rtp', //if want to show paginate,you should add p
                columnDefs: [
                    {\"visible\": true, targets: [0,1,2]}, // Hides ID column, even from inspection
                    {\"searchable\": true, targets: [0,1,2]}, // Makes filterType searchable
                    {'orderable': true, targets: [0,1,2]}, // Disallow ordering the columns that are just icons
                ],
            });
            
            \$(\".searchTerm\").on( 'keyup change', function () {
                closedTable.search( this.value ).draw();
                } );
            });
            \$('#view_history').on('click', function () {
                \$('.occurences_view').modal('show');
            });
            \$('#done').on('click',function(){
                \$('.occurences_view').modal('hide');
            })
        </script>
    {% if isAdmin %}
        <script type=\"text/javascript\"
                src=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js\"></script>
        <script type=\"text/javascript\"
                src=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.js\"></script>
        <script>
            \$('#crop_current_profile_picture').on('click', function () {
                var dropzone = \$('.dropzone').get(0).dropzone;
                let originalPictureURL = '{{ path('origin_requested_image', {'username': user.username}) }}';
                var file = {name: \"file\", dataURL: originalPictureURL};

                if (dropzone.previewsContainer) {
                    file.previewElement = Dropzone.createElement(dropzone.options.previewTemplate.trim());

                    dropzone.previewsContainer.appendChild(file.previewElement);

                    if (dropzone.options.addRemoveLinks) {
                        file._removeLink = Dropzone.createElement(\"<a class=\\\"dz-remove\\\" href=\\\"javascript:undefined;\\\" data-dz-remove>\" + dropzone.options.dictRemoveFile + \"</a>\");
                        file.previewElement.appendChild(file._removeLink);
                    }
                }
                \$('#admin-modal-image').data('active', dropzone.element.id);
                \$('#admin-modal-image').cropper('replace', file.dataURL);
                \$('#admin-modal').modal('show');
            });

            var minWidth = window.innerWidth < 500 ? window.innerWidth * 0.8 : window.innerWidth * 0.6;
            var minHeight = window.innerHeight < 500 ? window.innerHeight * 0.8 : window.innerHeight * 0.6;

            \$('#admin-modal-image').cropper({
                aspectRatio: 1,
                viewMode: 2,
                minContainerWidth: minWidth,
                minContainerHeight: minHeight,
                responsive: true,
                ready: function () {
                    \$('.cropper-container').css('margin', 'auto');
                    \$('.modal-content').css('width', minWidth + 30);
                    \$('#admin-modal .btn-success').off('click').on('click', function (event) {
                        var active = \$('#admin-modal-image').data('active');

                        var crop = JSON.stringify(\$('#admin-modal-image').cropper('getCropBoxData'));
                        var canvas = JSON.stringify(\$('#admin-modal-image').cropper('getCanvasData'));
                        var image = JSON.stringify(\$('#admin-modal-image').cropper('getImageData'));
                        \$('#' + active).data('crop', crop);
                        \$('#' + active).data('canvas', canvas);
                        \$('#' + active).data('image', image);

                        \$('#admin-modal').modal('hide');

                        \$.ajax({
                            type: 'POST',
                            url: \"{{ path('update_profile_image', {'username': user.username}) }}\",
                            data: {
                                'user': active,
                                'crop': crop,
                                'canvas': canvas,
                                'image': image
                            },
                            complete: function (response) {
                                if (response.responseText) {
                                    location.reload();
                                }
                            }
                        });
                    });
                }
            });
            \$('#admin-modal').on('show.bs.modal', function (event) {
                var active = \$('#admin-modal-image').data('active');

                var crop = \$('#' + active).data('crop');
                var canvas = \$('#' + active).data('canvas');
                var image = \$('#' + active).data('image');
                if (crop && canvas && image) {
                    \$('#admin-modal-image').cropper('setCropBoxData', JSON.parse(crop));
                    \$('#admin-modal-image').cropper('setCanvasData', JSON.parse(canvas));
                    \$('#admin-modal-image').cropper('setImageData', JSON.parse(image));
                }
            });
            \$('#admin-modal').on('hidden.bs.modal', function (event) {
                \$('#admin-modal').data('bs.modal', null);
            });
           
        </script>
    {% endif %}
{% endblock %}
", "role/mentor/profile.html.twig", "/code/templates/role/mentor/profile.html.twig");
    }
}
