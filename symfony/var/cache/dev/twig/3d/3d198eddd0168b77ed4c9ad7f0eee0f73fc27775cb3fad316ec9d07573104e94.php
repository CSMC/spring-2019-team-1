<?php

/* shared/security/login.html.twig */
class __TwigTemplate_78d481b2e79f3678e6350c32306d90485fa458087ddba709921c4aef6243e64d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
";
        // line 11
        echo "
";
        // line 15
        echo "
";
        // line 18
        echo "
";
        // line 21
        echo "
";
        // line 25
        echo "<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/favicon.png"), "html", null, true);
        echo "\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/global.css"), "html", null, true);
        echo "\"/>
</head>
<body style=\"background: #FFFFFF url(";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/swipe-background.png"), "html", null, true);
        echo ") no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/logo-csmc.png"), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"row\">
                            <div class=\"col-xs-3\"></div>
                            <div class=\"col-xs-6\">
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h2>Login</h2>
                                    </div>
                                    <div class=\"panel-body\">

                                        <form class=\"form-horizontal form-label-left\" action=\"";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
        echo "\"
                                              method=\"post\">
                                            ";
        // line 70
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 70, $this->source); })())) {
            // line 71
            echo "                                                <div class=\"message red_message\">
                                                    ";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 72, $this->source); })()), "messageKey", []), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 72, $this->source); })()), "messageData", []), "security"), "html", null, true);
            echo "
                                                </div>
                                            ";
        }
        // line 75
        echo "
                                            <label class=\"control-label\" for=\"username\">Username:</label>
                                            <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\"
                                                   value=\"";
        // line 78
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 78, $this->source); })()), "html", null, true);
        echo "\" autocomplete=\"off\"/>

                                            <label class=\"control-label\" for=\"password\">Password:</label>
                                            <input class=\"form-control\" type=\"password\" id=\"password\"
                                                   name=\"_password\" autocomplete=\"off\"/>

                                            ";
        // line 85
        echo "                                            <input type=\"hidden\" name=\"_target_path\" value=\"/home\"/>

                                            <button class=\"btn btn-success\"
                                                    style=\"width: 100%; margin-bottom: 0;\" type=\"submit\">Login
                                            </button>
                                        </form>
                                        ";
        // line 92
        echo "                                        ";
        // line 93
        echo "                                        ";
        // line 94
        echo "                                        ";
        // line 95
        echo "                                        ";
        // line 96
        echo "                                        ";
        // line 97
        echo "                                        ";
        // line 98
        echo "                                        ";
        // line 99
        echo "                                        ";
        // line 100
        echo "                                        ";
        // line 101
        echo "                                        ";
        // line 102
        echo "                                        ";
        // line 103
        echo "                                        ";
        // line 104
        echo "                                        ";
        // line 105
        echo "                                        ";
        // line 106
        echo "                                        ";
        // line 107
        echo "                                        ";
        // line 108
        echo "                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-3\"></div>
                        </div>
                        <div class=\"col-xs-3\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 108,  170 => 107,  168 => 106,  166 => 105,  164 => 104,  162 => 103,  160 => 102,  158 => 101,  156 => 100,  154 => 99,  152 => 98,  150 => 97,  148 => 96,  146 => 95,  144 => 94,  142 => 93,  140 => 92,  132 => 85,  123 => 78,  118 => 75,  112 => 72,  109 => 71,  107 => 70,  102 => 68,  81 => 50,  70 => 42,  65 => 40,  52 => 30,  45 => 25,  42 => 21,  39 => 18,  36 => 15,  33 => 11,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
{#<div class=\"page\">#}
{#<form class=\"page_box\" action=\"{{ path('login') }}\"#}
{#method=\"post\">#}
{#{% if error %}#}
{#<div class=\"message red_message\">#}
{#{{ error.messageKey|trans(error.messageData, 'security') }}#}
{#</div>#}
{#{% endif %}#}

{#<label class=\"page_box_label\" for=\"username\">Username:</label>#}
{#<input class=\"page_box_input\" type=\"text\" id=\"username\" name=\"_username\"#}
{#value=\"{{ last_username }}\"/>#}

{#<label class=\"page_box_label\" for=\"password\">Password:</label>#}
{#<input class=\"page_box_input\" type=\"password\" id=\"password\" name=\"_password\"/>#}

{# If you want to control the URL the user is redirected to on success #}
{#<input type=\"hidden\" name=\"_target_path\" value=\"/home\"/>#}

{#<button class=\"button bottom green_button\" style=\"width: 100%; margin-bottom: 0;\" type=\"submit\">Login</button>#}
{#</form>#}
{#</div>#}
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('build/images/favicon.png') }}\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/global.css') }}\"/>
</head>
<body style=\"background: #FFFFFF url({{ asset('build/images/swipe-background.png') }}) no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <img src=\"{{ asset(\"build/images/logo-csmc.png\") }}\">
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"row\">
                            <div class=\"col-xs-3\"></div>
                            <div class=\"col-xs-6\">
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h2>Login</h2>
                                    </div>
                                    <div class=\"panel-body\">

                                        <form class=\"form-horizontal form-label-left\" action=\"{{ path('login') }}\"
                                              method=\"post\">
                                            {% if error %}
                                                <div class=\"message red_message\">
                                                    {{ error.messageKey|trans(error.messageData, 'security') }}
                                                </div>
                                            {% endif %}

                                            <label class=\"control-label\" for=\"username\">Username:</label>
                                            <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\"
                                                   value=\"{{ last_username }}\" autocomplete=\"off\"/>

                                            <label class=\"control-label\" for=\"password\">Password:</label>
                                            <input class=\"form-control\" type=\"password\" id=\"password\"
                                                   name=\"_password\" autocomplete=\"off\"/>

                                            {#If you want to control the URL the user is redirected to on success#}
                                            <input type=\"hidden\" name=\"_target_path\" value=\"/home\"/>

                                            <button class=\"btn btn-success\"
                                                    style=\"width: 100%; margin-bottom: 0;\" type=\"submit\">Login
                                            </button>
                                        </form>
                                        {##}
                                        {#{{ form_start(swipe_form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}#}
                                        {#{{ form_errors(swipe_form) }}#}
                                        {#<div id=\"success\" class=\"alert alert-success\" style=\"display: none;\"></div>#}
                                        {#<div class=\"form-group\">#}
                                        {#<div class=\"control-label\">#}
                                        {#{{ form_label(form.scancode) }}#}
                                        {#</div>#}
                                        {#{{ form_errors(swipe_form.scancode) }}#}
                                        {#<div>#}
                                        {#<div id=\"loader\" class=\"text-center\" style=\"display: none;\">#}
                                        {#<img src=\"{{ asset('build/images/ajax-loader.gif') }}\">#}
                                        {#</div>#}
                                        {#{{ form_widget(swipe_form.scancode, {'attr': {'class': 'form-control', 'placeholder': 'Please swipe your Comet Card'}}) }}#}
                                        {#</div>#}
                                        {#</div>#}
                                        {#{{ form_end(swipe_form) }}#}
                                    </div>
                                </div>
                            </div>
                            <div class=\"col-xs-3\"></div>
                        </div>
                        <div class=\"col-xs-3\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
</body>
</html>
", "shared/security/login.html.twig", "/code/templates/shared/security/login.html.twig");
    }
}
