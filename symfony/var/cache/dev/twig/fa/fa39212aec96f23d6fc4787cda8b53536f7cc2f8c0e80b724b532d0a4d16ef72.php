<?php

/* role/mentor/session/timeslot.html.twig */
class __TwigTemplate_f378b29eab7115b7cf704ced8d747a6cecca07f356dde6abe2b9504e53cde1d2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/session/timeslot.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/timeslot.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/timeslot.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 3, $this->source); })()), "session", []), "topic", []), "html", null, true);
        echo "</h2>
    <dl class=\"dl-horizontal\">
        <dt>Description</dt>
        <dd>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 6, $this->source); })()), "session", []), "description", []), "html", null, true);
        echo "</dd>
        <dt>Student Instructions</dt>
        <dd>";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 8, $this->source); })()), "session", []), "studentInstructions", []), "html", null, true);
        echo "</dd>
        <dt>Mentor Instructions</dt>
        <dd>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 10, $this->source); })()), "session", []), "mentorInstructions", []), "html", null, true);
        echo "</dd>
        <dt>Graded</dt>
        <dd>";
        // line 12
        echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 12, $this->source); })()), "session", []), "graded", [])) ? ("yes") : ("no"));
        echo "</dd>
        ";
        // line 13
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 13, $this->source); })()), "session", []), "graded", [])) {
            // line 14
            echo "            <dt>Grading Scheme</dt>
            <dd>";
            // line 15
            echo ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 15, $this->source); })()), "session", []), "numericGrade", [])) ? ("numeric") : ("pass/fail"));
            echo "</dd>
        ";
        }
        // line 17
        echo "        <dt>Materials</dt>
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 18, $this->source); })()), "session", []), "files", []));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 19
            echo "            ";
            if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "first", [])) {
                // line 20
                echo "                <dt></dt>
            ";
            }
            // line 22
            echo "            <dd>
                <a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["id" => twig_get_attribute($this->env, $this->source, $context["file"], "id", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["file"], "name", []), "html", null, true);
            echo "</a>
            </dd>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 26
            echo "            <dd></dd>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "        <dt>Date and Time</dt>
        <dd>
            ";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 30, $this->source); })()), "startTime", []), "m/d/Y g:i A"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 30, $this->source); })()), "endTime", []), "g:i A"), "html", null, true);
        echo "
        </dd>
        <dt>Room</dt>
        <dd>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 33, $this->source); })()), "location", []), "html", null, true);
        echo "</dd>
        <dt>Mentors</dt>
        ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 35, $this->source); })()), "assignments", []));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
            // line 36
            echo "            ";
            if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "first", [])) {
                // line 37
                echo "                <dt></dt>
            ";
            }
            // line 39
            echo "            <dd>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["assignment"], "mentor", []), "html", null, true);
            echo "</dd>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 41
            echo "            <dd></dd>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        <dt>Registrations</dt>
        <dd>";
        // line 44
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 44, $this->source); })()), "registeredStudents", [])), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 44, $this->source); })()), "capacity", []), "html", null, true);
        echo "</dd>
        <dt>Download Roster</dt>
        <dd>
            NYI";
        // line 48
        echo "        </dd>
    </dl>
    ";
        // line 50
        if ((twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 50, $this->source); })()), "started", []) && twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 50, $this->source); })()), "ended", []))) {
            // line 51
            echo "        <div class=\"btn btn-default\">
            Ended
        </div>
    ";
        } else {
            // line 55
            echo "        <a class=\"btn btn-success\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("swipe_session", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 55, $this->source); })()), "id", [])]), "html", null, true);
            echo "\">
            ";
            // line 56
            if ( !twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 56, $this->source); })()), "started", [])) {
                // line 57
                echo "                Start
            ";
            } else {
                // line 59
                echo "                Continue
            ";
            }
            // line 61
            echo "        </a>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/session/timeslot.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 61,  244 => 59,  240 => 57,  238 => 56,  233 => 55,  227 => 51,  225 => 50,  221 => 48,  213 => 44,  210 => 43,  203 => 41,  187 => 39,  183 => 37,  180 => 36,  162 => 35,  157 => 33,  149 => 30,  145 => 28,  138 => 26,  120 => 23,  117 => 22,  113 => 20,  110 => 19,  92 => 18,  89 => 17,  84 => 15,  81 => 14,  79 => 13,  75 => 12,  70 => 10,  65 => 8,  60 => 6,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <h2>{{ timeslot.session.topic }}</h2>
    <dl class=\"dl-horizontal\">
        <dt>Description</dt>
        <dd>{{ timeslot.session.description }}</dd>
        <dt>Student Instructions</dt>
        <dd>{{ timeslot.session.studentInstructions }}</dd>
        <dt>Mentor Instructions</dt>
        <dd>{{ timeslot.session.mentorInstructions }}</dd>
        <dt>Graded</dt>
        <dd>{{ timeslot.session.graded ? 'yes' : 'no' }}</dd>
        {% if timeslot.session.graded %}
            <dt>Grading Scheme</dt>
            <dd>{{ timeslot.session.numericGrade ? 'numeric' : 'pass/fail' }}</dd>
        {% endif %}
        <dt>Materials</dt>
        {% for file in timeslot.session.files %}
            {% if not loop.first %}
                <dt></dt>
            {% endif %}
            <dd>
                <a href=\"{{ path('download', {'id': file.id}) }}\">{{ file.name }}</a>
            </dd>
        {% else %}
            <dd></dd>
        {% endfor %}
        <dt>Date and Time</dt>
        <dd>
            {{ timeslot.startTime|date('m/d/Y g:i A') }} - {{ timeslot.endTime|date('g:i A') }}
        </dd>
        <dt>Room</dt>
        <dd>{{ timeslot.location }}</dd>
        <dt>Mentors</dt>
        {% for assignment in timeslot.assignments %}
            {% if not loop.first %}
                <dt></dt>
            {% endif %}
            <dd>{{ assignment.mentor }}</dd>
        {% else %}
            <dd></dd>
        {% endfor %}
        <dt>Registrations</dt>
        <dd>{{ timeslot.registeredStudents|length }}/{{ timeslot.capacity }}</dd>
        <dt>Download Roster</dt>
        <dd>
            NYI{#<a href=\"{{ path('session_timeslot_roster', {'tid': timeslot.id}) }}\">Download</a>#}
        </dd>
    </dl>
    {% if timeslot.started and timeslot.ended %}
        <div class=\"btn btn-default\">
            Ended
        </div>
    {% else %}
        <a class=\"btn btn-success\" href=\"{{ path('swipe_session', {'id': timeslot.id}) }}\">
            {% if not timeslot.started %}
                Start
            {% else %}
                Continue
            {% endif %}
        </a>
    {% endif %}
{% endblock %}", "role/mentor/session/timeslot.html.twig", "/code/templates/role/mentor/session/timeslot.html.twig");
    }
}
