<?php

/* role/admin/mentor_summary.html.twig */
class __TwigTemplate_18b4d24fec7eca970f85164948c01078c9a9ddc2f6ddc614bca7677d5255faa3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/mentor_summary.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/mentor_summary.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/mentor_summary.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<style>

</style>


<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/view_summary.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
<div class=\"container\">
    <div class=\"row title\">
        <div class=\"col-md-4\">
        <h1>Mentor Management Summary</h1>
        </div>
        <div class=\"col-md-8 text-right pr-3\">

        <div class=\"search-wrap\">
           <div class=\"search\">
              <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
              <button type=\"submit\" class=\"searchButton\">
                <i class=\"fa fa-search\"></i>
             </button>
           </div>
        </div>
        </div>
    </div>
    <div class=\"\">
      <div class=\"row\">
        <div id=\"summary\">
            <div class=\"row table-outer\">
                <table class=\"table table-hover\" id=\"summaryTable\" width=\"95%\" style=\"text-align: center;\">
                    <thead>
                        <tr>
<!--                         <th colspan=\"4\" class=\"thead-notes\"></th>
 -->                        <th class=\"col-md-1\" rowspan=\"2\" ></th>
                            <th rowspan=\"2\">Name</th>
                            <th rowspan=\"2\">Preferred Name</th>
                            <th class=\"thead-notes\" rowspan=\"2\">Notes</th>
                            <th colspan=\"5\">Points</th>
                        </tr>
                        <tr>
<!--                             <th class=\"col-md-2\">Pending Review</th>
                            <th>Name</th>
                            <th>Preferred Name</th>
                            <th class=\"thead-notes\">Notes</th> -->
                            <th>Att.</th>
                            <th>Beh.</th>
                            <th>Tot.</th>
                            <th>Suggested Action</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mentors"]) || array_key_exists("mentors", $context) ? $context["mentors"] : (function () { throw new Twig_Error_Runtime('Variable "mentors" does not exist.', 55, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
            // line 56
            echo "                        <tr>
                            <td class=\"align-middle\">
                                    <div class=\"col-md-1\"> 
                                    ";
            // line 59
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "profilePictureModificationRequest", [])) {
                // line 60
                echo "                                        <div class=\"pending-review pending-profile-picture\">
                                            <a href=\"";
                // line 61
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
                echo "\">
                                                <span class=\"glyphicon glyphicon-info-sign pending-profile-picture\"></span>
                                            </a>
                                        </div>
                                    ";
            }
            // line 66
            echo "                                    </div>
                                    <div class=\"col-md-11\"> 

                                    ";
            // line 69
            if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "profilePicture", []))) {
                // line 70
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_image", ["username" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
                echo "\"
                                             class=\"rounded float-left\" alt=\"...\">
                                    ";
            } else {
                // line 73
                echo "                                        <img src=\"https://lawyerscommittee.org/wp-content/uploads/2017/04/5a68d99cd467003c04b4ef64004c4313_download-this-image-as-profile-icon-clipart_600-557.png\"
                                             class=\"rounded float-left\" alt=\"...\">
                                    ";
            }
            // line 76
            echo "                                    </div>
                                
                            </td>
                            <td>
                                ";
            // line 80
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "firstName", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "lastName", []), "html", null, true);
            echo "
                            </td>
                            <td>
                                ";
            // line 83
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "preferredNameModificationRequest", [])) {
                // line 84
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
                echo "\">
                                        <span class=\"glyphicon glyphicon-info-sign pending-preferred-name\"></span>
                                    </a>
                                ";
            }
            // line 88
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
            echo "\"
                                   target=\"_blank\">";
            // line 89
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "preferredName", []), "html", null, true);
            echo "</a>
                            </td>
                            <td class=\"thead-notes\">";
            // line 91
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "adminNotes", []), "html", null, true);
            echo "</td>
                            <td>";
            // line 92
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "attendanceOccurrencePoints", []), "html", null, true);
            echo "</td>
                            <td>";
            // line 93
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "behaviorOccurrencePoints", []), "html", null, true);
            echo "</td>
                            <td>";
            // line 94
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "totalOccurrencePoints", []), "html", null, true);
            echo "</td>
                            <td>";
            // line 95
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["coursesOfAction"]) || array_key_exists("coursesOfAction", $context) ? $context["coursesOfAction"] : (function () { throw new Twig_Error_Runtime('Variable "coursesOfAction" does not exist.', 95, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["mentor"], "id", []), [], "array"), "html", null, true);
            echo "
                            </td>
                            <td class=\"tbody-details\">
                                <a href=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_view_occurrence_summary", ["user" => twig_get_attribute($this->env, $this->source, $context["mentor"], "username", [])]), "html", null, true);
            echo "\">
                                    ";
            // line 99
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["mentor"], "id", []), (isset($context["mentorIdsWithOccurrence"]) || array_key_exists("mentorIdsWithOccurrence", $context) ? $context["mentorIdsWithOccurrence"] : (function () { throw new Twig_Error_Runtime('Variable "mentorIdsWithOccurrence" does not exist.', 99, $this->source); })()))) {
                // line 100
                echo "                                        <span class=\"glyphicon glyphicon-info-sign detail-btn pending-occurrences\" data-toggle=\"tooltip\" title=\"See more info\"></span>
                                    ";
            } else {
                // line 102
                echo "                                        <span class=\"glyphicon glyphicon-info-sign detail-btn\" data-toggle=\"tooltip\" title=\"See more info\">
                                        </span>
                                    ";
            }
            // line 105
            echo "                                </a>
                                </a>
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "                    </tbody>
                </table>
            </div>
         </div>
      </div>

    </div>
</div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 122
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 123
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

    <script>
    \$(document).ready(function() {

        \$(\".searchTerm\").on( 'keyup change', function () {
            summaryTable.search( this.value ).draw();
        } );

        let summaryTable = \$('#summaryTable').DataTable({
            aaSorting: [],
            language: {
                \"paginate\": {
                    \"previous\": \"<<\",
                    \"next\": \">>\"
                }
            },
            dom: 'rtp'
        });

        // \$(\".summaryName\").click(function() {
        //     \$('#summaryUserInfoModal').modal('show')
        // });

        // let summaryData = [
        //     [\"TODO (This field will be get from backend)\",
        //     \"Mavis Francia\",
        //     \"Mavis\",
        //     'Admin notes will appear here, e.g. \"likes leading sessions,\" hates leading sessions, etc.',
        //     \"0\",
        //     \"0\",
        //     \"0\",
        //     \"\",
        //     \"TODO (This field will be get from backend)\"
        //     ],
        // ];
        //
        // \$('#summaryTable').DataTable( {
        //     data: summaryData,
        //     aoColumns: [
        //         { mdata: 'Pending Review' ,
        //           'className': 'align-middle',
        //         'render': function(data, type, row) {
        //             function renderString(){
        //                 // Filter data to DOM
        //                 // ===
        //                 // **TODO**
        //                 // ===
        //             }
        //
        //             return '<div class=\"col-md-8\">'+
        //             '                <div class=\"pending-review pending-profile-picture\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Profile Picture</p>'+
        //             '                </div>'+
        //             '                <div class=\"pending-review pending-preferred-name\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Preferred Name</p>'+
        //             '                </div>'+
        //             '                <div class=\"pending-review pending-occurrences\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Occurrences</p>'+
        //             '                </div>                                '+
        //             '            </div>'+
        //             '            <div class=\"col-md-4\">'+
        //             '                <img src=\"https://lawyerscommittee.org/wp-content/uploads/2017/04/5a68d99cd467003c04b4ef64004c4313_download-this-image-as-profile-icon-clipart_600-557.png\" class=\"rounded float-left\" alt=\"...\">'+
        //             '            </div>';
        //
        //         }},
        //         { mdata: 'Name' },
        //         { mdata: 'Preferred Name' ,
        //             'render': function(data, type, row) {
        //             return  ' <button type=\"button\" class=\"btn btn-link summaryName\">Mavis</button>';
        //         }
        //         },
        //         { mdata: 'Notes' ,
        //             'className': 'thead-notes'},
        //         { mdata: 'Att.' },
        //         { mdata: 'Beh.' },
        //         { mdata: 'Tot.' },
        //         { mdata: 'Course of Action' ,},
        //         { mdata: 'Details' ,
        //             'className': 'tbody-details',
        //             'render': function(data, type, row) {
        //             return  '<span class=\"glyphicon glyphicon-info-sign detail-btn\" data-toggle=\"tooltip\" title=\"See more info\"></span>';
        //         }
        //         }
        //     ],
        //
        //     \"ordering\": false,
        // } );
        //
        // \$(\".glyphicon.glyphicon-remove\").click(function(){
        //     alert(\"Cancel Event, Todo.\")
        // });
        // \$(\".glyphicon.glyphicon-ok\").click(function(){
        //     alert(\"Approve Event, Todo.\")
        // });
        // \$(\".btn-approve\").click(function(){
        //     alert(\"Send Approve Event, Todo.\")
        // });
        //
        // \$('[data-toggle=\"tooltip\"]').tooltip();
        //
        // \$(\"#summaryTable .tbody-details\").click(function() {
        //     \$('#summaryDetailsModal').modal('show')
        // });

    });
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/mentor_summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 123,  252 => 122,  231 => 110,  221 => 105,  216 => 102,  212 => 100,  210 => 99,  206 => 98,  200 => 95,  196 => 94,  192 => 93,  188 => 92,  184 => 91,  179 => 89,  174 => 88,  166 => 84,  164 => 83,  156 => 80,  150 => 76,  145 => 73,  138 => 70,  136 => 69,  131 => 66,  123 => 61,  120 => 60,  118 => 59,  113 => 56,  109 => 55,  61 => 10,  54 => 5,  45 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% extends 'role/admin/base.html.twig' %}

{% block body %}
<style>

</style>


<link href=\"{{ asset('build/css/view_summary.css') }}\" rel=\"stylesheet\" />
<div class=\"container\">
    <div class=\"row title\">
        <div class=\"col-md-4\">
        <h1>Mentor Management Summary</h1>
        </div>
        <div class=\"col-md-8 text-right pr-3\">

        <div class=\"search-wrap\">
           <div class=\"search\">
              <input type=\"text\" class=\"searchTerm\" placeholder=\"What are you looking for?\">
              <button type=\"submit\" class=\"searchButton\">
                <i class=\"fa fa-search\"></i>
             </button>
           </div>
        </div>
        </div>
    </div>
    <div class=\"\">
      <div class=\"row\">
        <div id=\"summary\">
            <div class=\"row table-outer\">
                <table class=\"table table-hover\" id=\"summaryTable\" width=\"95%\" style=\"text-align: center;\">
                    <thead>
                        <tr>
<!--                         <th colspan=\"4\" class=\"thead-notes\"></th>
 -->                        <th class=\"col-md-1\" rowspan=\"2\" ></th>
                            <th rowspan=\"2\">Name</th>
                            <th rowspan=\"2\">Preferred Name</th>
                            <th class=\"thead-notes\" rowspan=\"2\">Notes</th>
                            <th colspan=\"5\">Points</th>
                        </tr>
                        <tr>
<!--                             <th class=\"col-md-2\">Pending Review</th>
                            <th>Name</th>
                            <th>Preferred Name</th>
                            <th class=\"thead-notes\">Notes</th> -->
                            <th>Att.</th>
                            <th>Beh.</th>
                            <th>Tot.</th>
                            <th>Suggested Action</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for mentor in mentors %}
                        <tr>
                            <td class=\"align-middle\">
                                    <div class=\"col-md-1\"> 
                                    {% if mentor.profile.profilePictureModificationRequest %}
                                        <div class=\"pending-review pending-profile-picture\">
                                            <a href=\"{{ path('profile', {'username': mentor.username}) }}\">
                                                <span class=\"glyphicon glyphicon-info-sign pending-profile-picture\"></span>
                                            </a>
                                        </div>
                                    {% endif %}
                                    </div>
                                    <div class=\"col-md-11\"> 

                                    {% if mentor.profile.profilePicture is not null %}
                                        <img src=\"{{ path('profile_image', {'username': mentor.username }) }}\"
                                             class=\"rounded float-left\" alt=\"...\">
                                    {% else %}
                                        <img src=\"https://lawyerscommittee.org/wp-content/uploads/2017/04/5a68d99cd467003c04b4ef64004c4313_download-this-image-as-profile-icon-clipart_600-557.png\"
                                             class=\"rounded float-left\" alt=\"...\">
                                    {% endif %}
                                    </div>
                                
                            </td>
                            <td>
                                {{ mentor.firstName }} {{ mentor.lastName }}
                            </td>
                            <td>
                                {% if mentor.profile.preferredNameModificationRequest %}
                                    <a href=\"{{ path('profile', {'username': mentor.username}) }}\">
                                        <span class=\"glyphicon glyphicon-info-sign pending-preferred-name\"></span>
                                    </a>
                                {% endif %}
                                <a href=\"{{ path('profile', {'username': mentor.username}) }}\"
                                   target=\"_blank\">{{ mentor.preferredName }}</a>
                            </td>
                            <td class=\"thead-notes\">{{ mentor.profile.adminNotes }}</td>
                            <td>{{ mentor.attendanceOccurrencePoints }}</td>
                            <td>{{ mentor.behaviorOccurrencePoints }}</td>
                            <td>{{ mentor.totalOccurrencePoints }}</td>
                            <td>{{ coursesOfAction[mentor.id] }}
                            </td>
                            <td class=\"tbody-details\">
                                <a href=\"{{ path('admin_view_occurrence_summary', {'user': mentor.username}) }}\">
                                    {% if mentor.id in mentorIdsWithOccurrence %}
                                        <span class=\"glyphicon glyphicon-info-sign detail-btn pending-occurrences\" data-toggle=\"tooltip\" title=\"See more info\"></span>
                                    {% else %}
                                        <span class=\"glyphicon glyphicon-info-sign detail-btn\" data-toggle=\"tooltip\" title=\"See more info\">
                                        </span>
                                    {% endif %}
                                </a>
                                </a>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
         </div>
      </div>

    </div>
</div>



{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\" charset=\"utf8\" src=\"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js\"></script>

    <script>
    \$(document).ready(function() {

        \$(\".searchTerm\").on( 'keyup change', function () {
            summaryTable.search( this.value ).draw();
        } );

        let summaryTable = \$('#summaryTable').DataTable({
            aaSorting: [],
            language: {
                \"paginate\": {
                    \"previous\": \"<<\",
                    \"next\": \">>\"
                }
            },
            dom: 'rtp'
        });

        // \$(\".summaryName\").click(function() {
        //     \$('#summaryUserInfoModal').modal('show')
        // });

        // let summaryData = [
        //     [\"TODO (This field will be get from backend)\",
        //     \"Mavis Francia\",
        //     \"Mavis\",
        //     'Admin notes will appear here, e.g. \"likes leading sessions,\" hates leading sessions, etc.',
        //     \"0\",
        //     \"0\",
        //     \"0\",
        //     \"\",
        //     \"TODO (This field will be get from backend)\"
        //     ],
        // ];
        //
        // \$('#summaryTable').DataTable( {
        //     data: summaryData,
        //     aoColumns: [
        //         { mdata: 'Pending Review' ,
        //           'className': 'align-middle',
        //         'render': function(data, type, row) {
        //             function renderString(){
        //                 // Filter data to DOM
        //                 // ===
        //                 // **TODO**
        //                 // ===
        //             }
        //
        //             return '<div class=\"col-md-8\">'+
        //             '                <div class=\"pending-review pending-profile-picture\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Profile Picture</p>'+
        //             '                </div>'+
        //             '                <div class=\"pending-review pending-preferred-name\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Preferred Name</p>'+
        //             '                </div>'+
        //             '                <div class=\"pending-review pending-occurrences\">'+
        //             '                    <span class=\"glyphicon glyphicon-info-sign\"></span><p>Occurrences</p>'+
        //             '                </div>                                '+
        //             '            </div>'+
        //             '            <div class=\"col-md-4\">'+
        //             '                <img src=\"https://lawyerscommittee.org/wp-content/uploads/2017/04/5a68d99cd467003c04b4ef64004c4313_download-this-image-as-profile-icon-clipart_600-557.png\" class=\"rounded float-left\" alt=\"...\">'+
        //             '            </div>';
        //
        //         }},
        //         { mdata: 'Name' },
        //         { mdata: 'Preferred Name' ,
        //             'render': function(data, type, row) {
        //             return  ' <button type=\"button\" class=\"btn btn-link summaryName\">Mavis</button>';
        //         }
        //         },
        //         { mdata: 'Notes' ,
        //             'className': 'thead-notes'},
        //         { mdata: 'Att.' },
        //         { mdata: 'Beh.' },
        //         { mdata: 'Tot.' },
        //         { mdata: 'Course of Action' ,},
        //         { mdata: 'Details' ,
        //             'className': 'tbody-details',
        //             'render': function(data, type, row) {
        //             return  '<span class=\"glyphicon glyphicon-info-sign detail-btn\" data-toggle=\"tooltip\" title=\"See more info\"></span>';
        //         }
        //         }
        //     ],
        //
        //     \"ordering\": false,
        // } );
        //
        // \$(\".glyphicon.glyphicon-remove\").click(function(){
        //     alert(\"Cancel Event, Todo.\")
        // });
        // \$(\".glyphicon.glyphicon-ok\").click(function(){
        //     alert(\"Approve Event, Todo.\")
        // });
        // \$(\".btn-approve\").click(function(){
        //     alert(\"Send Approve Event, Todo.\")
        // });
        //
        // \$('[data-toggle=\"tooltip\"]').tooltip();
        //
        // \$(\"#summaryTable .tbody-details\").click(function() {
        //     \$('#summaryDetailsModal').modal('show')
        // });

    });
    </script>

{% endblock %}", "role/admin/mentor_summary.html.twig", "/code/templates/role/admin/mentor_summary.html.twig");
    }
}
