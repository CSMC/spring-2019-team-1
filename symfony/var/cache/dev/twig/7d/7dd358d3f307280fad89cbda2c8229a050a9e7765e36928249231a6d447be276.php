<?php

/* shared/home/announcements.html.twig */
class __TwigTemplate_e01650a29a071e898edb88b60efec7708613177e28f353340973f62a372414dc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/announcements.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/announcements.html.twig"));

        // line 1
        echo "<div class=\"content_head\">
    <h2 class=\"content_title\">Announcements</h2>
</div>
<div class=\"content_list\">
    ";
        // line 5
        if ( !twig_test_empty((isset($context["announcements"]) || array_key_exists("announcements", $context) ? $context["announcements"] : (function () { throw new Twig_Error_Runtime('Variable "announcements" does not exist.', 5, $this->source); })()))) {
            // line 6
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["announcements"]) || array_key_exists("announcements", $context) ? $context["announcements"] : (function () { throw new Twig_Error_Runtime('Variable "announcements" does not exist.', 6, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["announcement"]) {
                // line 7
                echo "            <div class=\"content_list_label\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["announcement"], "postDate", []), "m/d/Y"), "html", null, true);
                echo "</div>
            <div class=\"content_list_info\">";
                // line 8
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["announcement"], "message", []), "html", null, true);
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['announcement'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "    ";
        } else {
            // line 11
            echo "        <div class=\"content_list_label\"></div>
        <div class=\"content_list_info\">No recent announcements</div>
    ";
        }
        // line 14
        echo "</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/home/announcements.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 14,  58 => 11,  55 => 10,  47 => 8,  42 => 7,  37 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content_head\">
    <h2 class=\"content_title\">Announcements</h2>
</div>
<div class=\"content_list\">
    {% if announcements is not empty %}
        {% for announcement in announcements %}
            <div class=\"content_list_label\">{{ announcement.postDate|date('m/d/Y') }}</div>
            <div class=\"content_list_info\">{{ announcement.message }}</div>
        {% endfor %}
    {% else %}
        <div class=\"content_list_label\"></div>
        <div class=\"content_list_info\">No recent announcements</div>
    {% endif %}
</div>
", "shared/home/announcements.html.twig", "/code/templates/shared/home/announcements.html.twig");
    }
}
