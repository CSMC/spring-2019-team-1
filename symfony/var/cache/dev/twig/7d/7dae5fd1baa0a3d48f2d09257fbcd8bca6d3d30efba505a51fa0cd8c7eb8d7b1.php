<?php

/* role/instructor/session/grades/grades.html.twig */
class __TwigTemplate_bd5e2886ff263bcd3ae01f221ca24f95eab2605f6313f2295bb16feef65a77b1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/grades/grades.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/grades/grades.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/grades/grades.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        // line 4
        echo "    ";
        $this->loadTemplate("shared/component/flash_messages.html.twig", "role/instructor/session/grades/grades.html.twig", 4)->display($context);
        // line 5
        echo "    <div class=\"content_head\">
        <h2 class=\"content_title\">";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 6, $this->source); })()), "topic", []), "html", null, true);
        echo "</h2>
        ";
        // line 8
        echo "            ";
        // line 9
        echo "                ";
        // line 10
        echo "            ";
        // line 11
        echo "        ";
        // line 12
        echo "    </div>
    ";
        // line 13
        if ((isset($context["attendees"]) || array_key_exists("attendees", $context))) {
            // line 14
            echo "        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Name</th>
                <th class=\"content_table_cell\">NetID</th>
                ";
            // line 18
            if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 18, $this->source); })()), "graded", [])) {
                // line 19
                echo "                    <th class=\"content_table_cell\">Grade</th>
                ";
            }
            // line 21
            echo "            </tr>
            ";
            // line 22
            if (twig_test_empty((isset($context["attendees"]) || array_key_exists("attendees", $context) ? $context["attendees"] : (function () { throw new Twig_Error_Runtime('Variable "attendees" does not exist.', 22, $this->source); })()))) {
                // line 23
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"3\">No attendees.</td>
                </tr>
            ";
            } else {
                // line 27
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["attendees"]) || array_key_exists("attendees", $context) ? $context["attendees"] : (function () { throw new Twig_Error_Runtime('Variable "attendees" does not exist.', 27, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                    // line 28
                    echo "                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\">";
                    // line 29
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "html", null, true);
                    echo "</td>
                        <td class=\"content_table_cell\">";
                    // line 30
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                    echo "</td>

                        ";
                    // line 32
                    if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 32, $this->source); })()), "graded", [])) {
                        // line 33
                        echo "                            <td class=\"content_table_cell\">
                                ";
                        // line 34
                        $context["grade"] = twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []);
                        // line 35
                        echo "                                ";
                        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 35, $this->source); })()), "numericGrade", [])) {
                            // line 36
                            echo "                                    ";
                            echo twig_escape_filter($this->env, (isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 36, $this->source); })()), "html", null, true);
                            echo "
                                ";
                        } else {
                            // line 38
                            echo "                                    ";
                            echo (((isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 38, $this->source); })())) ? (((((isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 38, $this->source); })()) == 1)) ? ("Pass") : ("Fail"))) : (""));
                            echo "
                                ";
                        }
                        // line 40
                        echo "                            </td>
                        ";
                    }
                    // line 42
                    echo "                    </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 44
                echo "            ";
            }
            // line 45
            echo "        </table>
    ";
        } elseif (        // line 46
(isset($context["timeslots"]) || array_key_exists("timeslots", $context))) {
            // line 47
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["timeslots"]) || array_key_exists("timeslots", $context) ? $context["timeslots"] : (function () { throw new Twig_Error_Runtime('Variable "timeslots" does not exist.', 47, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
                // line 48
                echo "            <div class=\"content_head\">
                <h3 class=\"content_title\">
                    ";
                // line 50
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "m/d g:i A"), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "endTime", []), "g:i A"), "html", null, true);
                echo "
                </h3>
            </div>
            ";
                // line 53
                $context["attendances"] = twig_get_attribute($this->env, $this->source, $context["timeslot"], "attendances", []);
                // line 54
                echo "            <table class=\"content_table\">
                <tr class=\"content_table_head\">
                    <th class=\"content_table_cell\">Name</th>
                    <th class=\"content_table_cell\">NetID</th>
                    ";
                // line 58
                if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 58, $this->source); })()), "graded", [])) {
                    // line 59
                    echo "                        <th class=\"content_table_cell\">Grade</th>
                    ";
                }
                // line 61
                echo "                </tr>
                ";
                // line 62
                if (twig_test_empty((isset($context["attendances"]) || array_key_exists("attendances", $context) ? $context["attendances"] : (function () { throw new Twig_Error_Runtime('Variable "attendances" does not exist.', 62, $this->source); })()))) {
                    // line 63
                    echo "                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\" colspan=\"3\">No attendees.</td>
                    </tr>
                ";
                } else {
                    // line 67
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["attendances"]) || array_key_exists("attendances", $context) ? $context["attendances"] : (function () { throw new Twig_Error_Runtime('Variable "attendances" does not exist.', 67, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                        // line 68
                        echo "                        <tr class=\"content_table_data\">
                            <td class=\"content_table_cell\">";
                        // line 69
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "firstName", []), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "lastName", []), "html", null, true);
                        echo "</td>
                            <td class=\"content_table_cell\">";
                        // line 70
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                        echo "</td>
                            ";
                        // line 71
                        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 71, $this->source); })()), "graded", [])) {
                            // line 72
                            echo "                                <td class=\"content_table_cell\">
                                    ";
                            // line 73
                            $context["grade"] = twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []);
                            // line 74
                            echo "                                    ";
                            if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 74, $this->source); })()), "numericGrade", [])) {
                                // line 75
                                echo "                                        ";
                                echo twig_escape_filter($this->env, (isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 75, $this->source); })()), "html", null, true);
                                echo "
                                    ";
                            } else {
                                // line 77
                                echo "                                        ";
                                echo (((isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 77, $this->source); })())) ? (((((isset($context["grade"]) || array_key_exists("grade", $context) ? $context["grade"] : (function () { throw new Twig_Error_Runtime('Variable "grade" does not exist.', 77, $this->source); })()) == 1)) ? ("Pass") : ("Fail"))) : (""));
                                echo "
                                    ";
                            }
                            // line 79
                            echo "                                </td>
                            ";
                        }
                        // line 81
                        echo "                        </tr>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 83
                    echo "                ";
                }
                // line 84
                echo "            </table>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/grades/grades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  262 => 86,  255 => 84,  252 => 83,  245 => 81,  241 => 79,  235 => 77,  229 => 75,  226 => 74,  224 => 73,  221 => 72,  219 => 71,  215 => 70,  209 => 69,  206 => 68,  201 => 67,  195 => 63,  193 => 62,  190 => 61,  186 => 59,  184 => 58,  178 => 54,  176 => 53,  168 => 50,  164 => 48,  159 => 47,  157 => 46,  154 => 45,  151 => 44,  144 => 42,  140 => 40,  134 => 38,  128 => 36,  125 => 35,  123 => 34,  120 => 33,  118 => 32,  113 => 30,  109 => 29,  106 => 28,  101 => 27,  95 => 23,  93 => 22,  90 => 21,  86 => 19,  84 => 18,  78 => 14,  76 => 13,  73 => 12,  71 => 11,  69 => 10,  67 => 9,  65 => 8,  61 => 6,  58 => 5,  55 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {# TODO add download link for instructor/admin #}
    {% include('shared/component/flash_messages.html.twig') %}
    <div class=\"content_head\">
        <h2 class=\"content_title\">{{ session.topic }}</h2>
        {#<div class=\"content_options\">#}
            {#<a href=\"{{ path('session_grades_download', {'id': session.id}) }}\">#}
                {#<div class=\"content_option button green_button\">Download</div>#}
            {#</a>#}
        {#</div>#}
    </div>
    {% if attendees is defined %}
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Name</th>
                <th class=\"content_table_cell\">NetID</th>
                {% if session.graded %}
                    <th class=\"content_table_cell\">Grade</th>
                {% endif %}
            </tr>
            {% if attendees is empty %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"3\">No attendees.</td>
                </tr>
            {% else %}
                {% for attendance in attendees %}
                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\">{{ attendance.user }}</td>
                        <td class=\"content_table_cell\">{{ attendance.user.username }}</td>

                        {% if session.graded %}
                            <td class=\"content_table_cell\">
                                {% set grade = attendance.grade %}
                                {% if session.numericGrade %}
                                    {{ grade }}
                                {% else %}
                                    {{ grade ? (grade == 1 ? 'Pass' : 'Fail') }}
                                {% endif %}
                            </td>
                        {% endif %}
                    </tr>
                {% endfor %}
            {% endif %}
        </table>
    {% elseif timeslots is defined %}
        {% for timeslot in timeslots %}
            <div class=\"content_head\">
                <h3 class=\"content_title\">
                    {{ timeslot.startTime|date('m/d g:i A') }} - {{ timeslot.endTime|date('g:i A') }}
                </h3>
            </div>
            {% set attendances = timeslot.attendances %}
            <table class=\"content_table\">
                <tr class=\"content_table_head\">
                    <th class=\"content_table_cell\">Name</th>
                    <th class=\"content_table_cell\">NetID</th>
                    {% if session.graded %}
                        <th class=\"content_table_cell\">Grade</th>
                    {% endif %}
                </tr>
                {% if attendances is empty %}
                    <tr class=\"content_table_data\">
                        <td class=\"content_table_cell\" colspan=\"3\">No attendees.</td>
                    </tr>
                {% else %}
                    {% for attendance in attendances %}
                        <tr class=\"content_table_data\">
                            <td class=\"content_table_cell\">{{ attendance.user.firstName }} {{ attendance.user.lastName }}</td>
                            <td class=\"content_table_cell\">{{ attendance.user.username }}</td>
                            {% if session.graded %}
                                <td class=\"content_table_cell\">
                                    {% set grade = attendance.grade %}
                                    {% if session.numericGrade %}
                                        {{ grade }}
                                    {% else %}
                                        {{ grade ? (grade == 1 ? 'Pass' : 'Fail') }}
                                    {% endif %}
                                </td>
                            {% endif %}
                        </tr>
                    {% endfor %}
                {% endif %}
            </table>
        {% endfor %}
    {% endif %}
{% endblock %}", "role/instructor/session/grades/grades.html.twig", "/code/templates/role/instructor/session/grades/grades.html.twig");
    }
}
