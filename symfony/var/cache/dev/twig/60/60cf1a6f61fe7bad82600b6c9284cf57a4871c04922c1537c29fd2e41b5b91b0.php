<?php

/* role/instructor/session/request/requests.html.twig */
class __TwigTemplate_1412ee198593e0ae3348d137fa953757486dc285859b083ff2e5587b49f142dc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/request/requests.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/requests.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/requests.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    <h2>Requests
        <a class=\"btn btn-success\" href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_request_create");
        echo "\" style=\"float:right\">
            Request a Session
        </a>
    </h2>
    <table id=\"datatable\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Type</th>
            <th>Course</th>
            <th>Section(s)</th>
            <th>Days</th>
            <th>Status</th>
            <th>View</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["requests"]) || array_key_exists("requests", $context) ? $context["requests"] : (function () { throw new Twig_Error_Runtime('Variable "requests" does not exist.', 23, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["request"]) {
            // line 24
            echo "            <tr>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["request"], "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["request"], "type", []), "name", []), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["request"], "sections", []), 0, [], "array"), "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["request"], "sections", []), 0, [], "array"), "course", []), "number", []), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 31
            $context["count"] = twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["request"], "sections", []));
            // line 32
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["request"], "sections", []));
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 33
                echo "                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["section"], "number", []), "html", null, true);
                // line 34
                if (((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 34, $this->source); })()) > 1)) {
                    // line 35
                    echo ", ";
                    echo "
                            ";
                    // line 36
                    $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 36, $this->source); })()) - 1);
                    // line 37
                    echo "                        ";
                }
                // line 38
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                </td>
                <td>
                    ";
            // line 41
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["request"], "startDate", []), "m/d/y"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["request"], "endDate", []), "m/d/y"), "html", null, true);
            echo "
                </td>
                <td>
                    ";
            // line 44
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["request"], "status", []))) {
                // line 45
                echo "                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["request"], "status", []), "html", null, true);
                echo "
                    ";
            }
            // line 47
            echo "                </td>
                <td>
                    <a class=\"btn btn-success\" href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_request_view", ["id" => twig_get_attribute($this->env, $this->source, $context["request"], "id", [])]), "html", null, true);
            echo "\">View</a>
                </td>
                <td>
                    <a class=\"btn btn-warning\" href=\"";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_request_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["request"], "id", [])]), "html", null, true);
            echo "\">Edit</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['request'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 59
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 60
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: false,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/request/requests.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 60,  184 => 59,  172 => 56,  162 => 52,  156 => 49,  152 => 47,  146 => 45,  144 => 44,  136 => 41,  132 => 39,  126 => 38,  123 => 37,  121 => 36,  117 => 35,  115 => 34,  112 => 33,  107 => 32,  105 => 31,  97 => 28,  92 => 26,  88 => 25,  85 => 24,  81 => 23,  60 => 5,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    <h2>Requests
        <a class=\"btn btn-success\" href=\"{{ path('session_request_create') }}\" style=\"float:right\">
            Request a Session
        </a>
    </h2>
    <table id=\"datatable\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Type</th>
            <th>Course</th>
            <th>Section(s)</th>
            <th>Days</th>
            <th>Status</th>
            <th>View</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        {% for request in requests %}
            <tr>
                <td>{{ request.topic }}</td>
                <td>{{ request.type.name }}</td>
                <td>
                    {{ request.sections[0].course.department.abbreviation }} {{ request.sections[0].course.number }}
                </td>
                <td>
                    {% set count = request.sections|length %}
                    {% for section in request.sections %}
                        {{ section.number -}}
                        {% if count > 1 %}
                            {{- \", \" }}
                            {% set count = count - 1 %}
                        {% endif %}
                    {% endfor %}
                </td>
                <td>
                    {{ request.startDate|date('m/d/y') }} - {{ request.endDate|date('m/d/y') }}
                </td>
                <td>
                    {% if request.status is not empty %}
                        {{ request.status }}
                    {% endif %}
                </td>
                <td>
                    <a class=\"btn btn-success\" href=\"{{ path('session_request_view', {\"id\": request.id}) }}\">View</a>
                </td>
                <td>
                    <a class=\"btn btn-warning\" href=\"{{ path('session_request_edit', {\"id\": request.id}) }}\">Edit</a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: false,
            });
        });
    </script>
{% endblock %}", "role/instructor/session/request/requests.html.twig", "/code/templates/role/instructor/session/request/requests.html.twig");
    }
}
