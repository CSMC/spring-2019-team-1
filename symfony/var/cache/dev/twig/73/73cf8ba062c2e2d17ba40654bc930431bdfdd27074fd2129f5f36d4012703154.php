<?php

/* role/admin/session/form.html.twig */
class __TwigTemplate_f7b596ee0ebf0c3842ffc944bf8451a1a2c7bb1c07f5d408e6e2fa2ebed5c6d3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/session/form.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 3, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 6, $this->source); })()), "session", []), "topic", []), 'label');
        echo "
        </div>
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 8, $this->source); })()), "session", []), "topic", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 10, $this->source); })()), "session", []), "topic", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 16, $this->source); })()), "session", []), "type", []), 'label');
        echo "
        </div>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 18, $this->source); })()), "session", []), "type", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 20, $this->source); })()), "session", []), "type", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 26, $this->source); })()), "session", []), "description", []), 'label');
        echo "
        </div>
        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 28, $this->source); })()), "session", []), "description", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 30, $this->source); })()), "session", []), "description", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 36, $this->source); })()), "session", []), "studentInstructions", []), 'label');
        echo "
        </div>
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 38, $this->source); })()), "session", []), "studentInstructions", []), 'errors');
        echo "
        ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 39, $this->source); })()), "session", []), "studentInstructions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 43, $this->source); })()), "session", []), "mentorInstructions", []), 'label');
        echo "
        </div>
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 45, $this->source); })()), "session", []), "mentorInstructions", []), 'errors');
        echo "
        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 46, $this->source); })()), "session", []), "mentorInstructions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 50, $this->source); })()), "session", []), "sections", []), 'label');
        echo "
        </div>
        ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 52, $this->source); })()), "session", []), "sections", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 54, $this->source); })()), "session", []), "sections", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 60, $this->source); })()), "session", []), "graded", []), 'label');
        echo "
        </div>
        ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 62, $this->source); })()), "session", []), "graded", []), 'errors');
        echo "
        ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 63, $this->source); })()), "session", []), "graded", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 67, $this->source); })()), "session", []), "numericGrade", []), 'label');
        echo "
        </div>
        ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 69, $this->source); })()), "session", []), "numericGrade", []), 'errors');
        echo "
        ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 70, $this->source); })()), "session", []), "numericGrade", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 74, $this->source); })()), "session", []), "files", []), 'label');
        echo "
        </div>
        ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 76, $this->source); })()), "session", []), "files", []), 'errors');
        echo "
        ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 77, $this->source); })()), "session", []), "files", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 81, $this->source); })()), "session", []), "uploadedFiles", []), 'label');
        echo "
        </div>
        ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 83, $this->source); })()), "session", []), "uploadedFiles", []), 'errors');
        echo "
        ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 84, $this->source); })()), "session", []), "uploadedFiles", []), 'widget');
        echo "
        <div>
            ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 86, $this->source); })()), "session", []), "uploadedFiles", []));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 87
            echo "                <div class=\"checkbox\">
                    <label>
                        ";
            // line 89
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["r"], 'widget', ["attr" => ["checked" => "checked"]]);
            echo "
                        ";
            // line 90
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "vars", []), "label", []), "html", null, true);
            echo "
                    </label>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 98, $this->source); })()), "session", []), "color", []), 'label');
        echo "
        </div>
        ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 100, $this->source); })()), "session", []), "color", []), 'errors');
        echo "
        ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 101, $this->source); })()), "session", []), "color", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 105, $this->source); })()), "repeats", []), 'label');
        echo "
        </div>
        ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 107, $this->source); })()), "repeats", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 109, $this->source); })()), "repeats", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 115, $this->source); })()), "defaultLocation", []), 'label');
        echo "
        </div>
        ";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 117, $this->source); })()), "defaultLocation", []), 'errors');
        echo "
        ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 118, $this->source); })()), "defaultLocation", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 122
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 122, $this->source); })()), "defaultCapacity", []), 'label');
        echo "
        </div>
        ";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 124, $this->source); })()), "defaultCapacity", []), 'errors');
        echo "
        ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 125, $this->source); })()), "defaultCapacity", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 129, $this->source); })()), "defaultDuration", []), 'label');
        echo "
        </div>
        ";
        // line 131
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 131, $this->source); })()), "defaultDuration", []), 'errors');
        echo "
        ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 132, $this->source); })()), "defaultDuration", []), 'widget');
        echo "
    </div>
    ";
        // line 134
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 134, $this->source); })()), 'rest');
        echo "
    ";
        // line 135
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["session_form"]) || array_key_exists("session_form", $context) ? $context["session_form"] : (function () { throw new Twig_Error_Runtime('Variable "session_form" does not exist.', 135, $this->source); })()), 'form_end');
        echo "

    ";
        // line 137
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 137, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 140
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 140, $this->source); })()), "session", []), "topic", []), 'label');
        echo "
        </div>
        ";
        // line 142
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 142, $this->source); })()), "session", []), "topic", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 144
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 144, $this->source); })()), "session", []), "topic", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 150, $this->source); })()), "session", []), "type", []), 'label');
        echo "
        </div>
        ";
        // line 152
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 152, $this->source); })()), "session", []), "type", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 154
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 154, $this->source); })()), "session", []), "type", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 160
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 160, $this->source); })()), "session", []), "description", []), 'label');
        echo "
        </div>
        ";
        // line 162
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 162, $this->source); })()), "session", []), "description", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 164
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 164, $this->source); })()), "session", []), "description", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 170
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 170, $this->source); })()), "session", []), "studentInstructions", []), 'label');
        echo "
        </div>
        ";
        // line 172
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 172, $this->source); })()), "session", []), "studentInstructions", []), 'errors');
        echo "
        ";
        // line 173
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 173, $this->source); })()), "session", []), "studentInstructions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 177
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 177, $this->source); })()), "session", []), "mentorInstructions", []), 'label');
        echo "
        </div>
        ";
        // line 179
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 179, $this->source); })()), "session", []), "mentorInstructions", []), 'errors');
        echo "
        ";
        // line 180
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 180, $this->source); })()), "session", []), "mentorInstructions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 184
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 184, $this->source); })()), "session", []), "sections", []), 'label');
        echo "
        </div>
        ";
        // line 186
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 186, $this->source); })()), "session", []), "sections", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 188
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 188, $this->source); })()), "session", []), "sections", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 194
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 194, $this->source); })()), "session", []), "graded", []), 'label');
        echo "
        </div>
        ";
        // line 196
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 196, $this->source); })()), "session", []), "graded", []), 'errors');
        echo "
        ";
        // line 197
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 197, $this->source); })()), "session", []), "graded", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 201
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 201, $this->source); })()), "session", []), "numericGrade", []), 'label');
        echo "
        </div>
        ";
        // line 203
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 203, $this->source); })()), "session", []), "numericGrade", []), 'errors');
        echo "
        ";
        // line 204
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 204, $this->source); })()), "session", []), "numericGrade", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 208
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 208, $this->source); })()), "session", []), "files", []), 'label');
        echo "
        </div>
        ";
        // line 210
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 210, $this->source); })()), "session", []), "files", []), 'errors');
        echo "
        ";
        // line 211
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 211, $this->source); })()), "session", []), "files", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 215
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 215, $this->source); })()), "session", []), "uploadedFiles", []), 'label');
        echo "
        </div>
        ";
        // line 217
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 217, $this->source); })()), "session", []), "uploadedFiles", []), 'errors');
        echo "
        ";
        // line 218
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 218, $this->source); })()), "session", []), "uploadedFiles", []), 'widget');
        echo "
        <div>
            ";
        // line 220
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 220, $this->source); })()), "session", []), "uploadedFiles", []));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 221
            echo "                <div class=\"checkbox\">
                    <label>
                        ";
            // line 223
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["r"], 'widget', ["attr" => ["checked" => "checked"]]);
            echo "
                        ";
            // line 224
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["r"], "vars", []), "label", []), "html", null, true);
            echo "
                    </label>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 228
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 232
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 232, $this->source); })()), "session", []), "color", []), 'label');
        echo "
        </div>
        ";
        // line 234
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 234, $this->source); })()), "session", []), "color", []), 'errors');
        echo "
        ";
        // line 235
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 235, $this->source); })()), "session", []), "color", []), 'widget');
        echo "
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 239
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 239, $this->source); })()), "room", []), 'label');
        echo "
        </div>
        ";
        // line 241
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 241, $this->source); })()), "room", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 243
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 243, $this->source); })()), "room", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 249
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 249, $this->source); })()), "startDate", []), 'label');
        echo "
        </div>
        ";
        // line 251
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 251, $this->source); })()), "startDate", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 253
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 253, $this->source); })()), "startDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 259
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 259, $this->source); })()), "endDate", []), 'label');
        echo "
        </div>
        ";
        // line 261
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 261, $this->source); })()), "endDate", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 263
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 263, $this->source); })()), "endDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    ";
        // line 267
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 267, $this->source); })()), 'rest');
        echo "
    ";
        // line 268
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["quiz_form"]) || array_key_exists("quiz_form", $context) ? $context["quiz_form"] : (function () { throw new Twig_Error_Runtime('Variable "quiz_form" does not exist.', 268, $this->source); })()), 'form_end');
        echo "
    </div>
    <div class=\"modal-footer\">
        <a class=\"btn btn-default\" href=\"";
        // line 271
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests");
        echo "\">Close</a>
        <button id=\"session-submit\" type=\"button\" class=\"btn btn-success\" data-active=\"scheduled_session\">
            Submit
        </button>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 277
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 278
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {

            var text = \$('#scheduled_session_session_type option:selected').text();
            if (text == 'quiz') {
                \$('#session-submit').data('active', 'quiz');

                \$('[name=\"scheduled_session\"]').hide();
                \$('[name=\"quiz\"]').show();
            } else {
                \$('#session-submit').data('active', 'scheduled_session');

                \$('[name=\"quiz\"]').hide();
                \$('[name=\"scheduled_session\"]').show();
            }

            \$('#scheduled_session_session_type').on('change', function () {
                var text = \$('option:selected', \$(this)).text();
                if (text == 'quiz') {
                    \$('#session-submit').data('active', 'quiz');

                    \$('[name=\"scheduled_session\"]').hide();
                    \$('[name=\"quiz\"]').show();

                    sync_data('#scheduled_session', '#quiz');
                }
            });

            \$('#quiz_session_type').on('change', function () {
                var text = \$('option:selected', \$(this)).text();
                if (text == 'review' || text == 'rework') {
                    \$('#session-submit').data('active', 'scheduled_session');

                    \$('[name=\"quiz\"]').hide();
                    \$('[name=\"scheduled_session\"]').show();

                    sync_data('#quiz', '#scheduled_session');
                }
            });

            var sync_data = function (one, two) {
                var topic = \$(one + '_session_topic').val();
                \$(two + '_session_topic').val(topic);

                var type = \$(one + '_session_type').val();
                \$(two + '_session_type').val(type);

                var description = \$(one + '_session_description').val();
                \$(two + '_session_description').val(description);

                var sInstr = \$(one + '_session_studentInstructions').val();
                \$(two + '_session_studentInstructions').val(sInstr);

                var mInstr = \$(one + '_session_mentorInstructions').val();
                \$(two + '_session_mentorInstructions').val(mInstr);

                // sections
                var sections = [];
                \$.each(\$(one + '_session_sections option:selected'), function () {
                    sections.push(\$(this).val());
                });
                \$(two + '_session_sections').val(sections);
                \$(two + '_session_sections').trigger('change');

                var graded = \$(one + '_session_graded').is(':checked');
                \$(two + '_session_graded').prop('checked', graded);

                var numeric = \$(one + '_session_numericGrade').is(':checked');
                \$(two + '_session_numericGrade').prop('checked', numeric);

                // files

                var color = \$(one + '_session_color').val();
                \$(two + '_session_color').val(color);
            };

            \$('#session-submit').on('click', function () {
                \$('[name=\"' + \$(this).data('active') + '\"]').submit();
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/session/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  659 => 278,  650 => 277,  634 => 271,  628 => 268,  624 => 267,  617 => 263,  612 => 261,  607 => 259,  598 => 253,  593 => 251,  588 => 249,  579 => 243,  574 => 241,  569 => 239,  562 => 235,  558 => 234,  553 => 232,  547 => 228,  537 => 224,  533 => 223,  529 => 221,  525 => 220,  520 => 218,  516 => 217,  511 => 215,  504 => 211,  500 => 210,  495 => 208,  488 => 204,  484 => 203,  479 => 201,  472 => 197,  468 => 196,  463 => 194,  454 => 188,  449 => 186,  444 => 184,  437 => 180,  433 => 179,  428 => 177,  421 => 173,  417 => 172,  412 => 170,  403 => 164,  398 => 162,  393 => 160,  384 => 154,  379 => 152,  374 => 150,  365 => 144,  360 => 142,  355 => 140,  349 => 137,  344 => 135,  340 => 134,  335 => 132,  331 => 131,  326 => 129,  319 => 125,  315 => 124,  310 => 122,  303 => 118,  299 => 117,  294 => 115,  285 => 109,  280 => 107,  275 => 105,  268 => 101,  264 => 100,  259 => 98,  253 => 94,  243 => 90,  239 => 89,  235 => 87,  231 => 86,  226 => 84,  222 => 83,  217 => 81,  210 => 77,  206 => 76,  201 => 74,  194 => 70,  190 => 69,  185 => 67,  178 => 63,  174 => 62,  169 => 60,  160 => 54,  155 => 52,  150 => 50,  143 => 46,  139 => 45,  134 => 43,  127 => 39,  123 => 38,  118 => 36,  109 => 30,  104 => 28,  99 => 26,  90 => 20,  85 => 18,  80 => 16,  71 => 10,  66 => 8,  61 => 6,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/admin/base.html.twig' %}
{% block body %}
    {{ form_start(session_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.topic) }}
        </div>
        {{ form_errors(session_form.session.topic) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(session_form.session.topic, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.type) }}
        </div>
        {{ form_errors(session_form.session.type) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(session_form.session.type, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.description) }}
        </div>
        {{ form_errors(session_form.session.description) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(session_form.session.description, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.studentInstructions) }}
        </div>
        {{ form_errors(session_form.session.studentInstructions) }}
        {{ form_widget(session_form.session.studentInstructions, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.mentorInstructions) }}
        </div>
        {{ form_errors(session_form.session.mentorInstructions) }}
        {{ form_widget(session_form.session.mentorInstructions, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.sections) }}
        </div>
        {{ form_errors(session_form.session.sections) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(session_form.session.sections, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.graded) }}
        </div>
        {{ form_errors(session_form.session.graded) }}
        {{ form_widget(session_form.session.graded) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.numericGrade) }}
        </div>
        {{ form_errors(session_form.session.numericGrade) }}
        {{ form_widget(session_form.session.numericGrade) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.files) }}
        </div>
        {{ form_errors(session_form.session.files) }}
        {{ form_widget(session_form.session.files) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.uploadedFiles) }}
        </div>
        {{ form_errors(session_form.session.uploadedFiles) }}
        {{ form_widget(session_form.session.uploadedFiles) }}
        <div>
            {% for r in session_form.session.uploadedFiles %}
                <div class=\"checkbox\">
                    <label>
                        {{ form_widget(r, {'attr': {'checked': 'checked'}}) }}
                        {{ r.vars.label }}
                    </label>
                </div>
            {% endfor %}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.session.color) }}
        </div>
        {{ form_errors(session_form.session.color) }}
        {{ form_widget(session_form.session.color, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.repeats) }}
        </div>
        {{ form_errors(session_form.repeats) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(session_form.repeats, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.defaultLocation) }}
        </div>
        {{ form_errors(session_form.defaultLocation) }}
        {{ form_widget(session_form.defaultLocation, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.defaultCapacity) }}
        </div>
        {{ form_errors(session_form.defaultCapacity) }}
        {{ form_widget(session_form.defaultCapacity, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(session_form.defaultDuration) }}
        </div>
        {{ form_errors(session_form.defaultDuration) }}
        {{ form_widget(session_form.defaultDuration) }}
    </div>
    {{ form_rest(session_form) }}
    {{ form_end(session_form) }}

    {{ form_start(quiz_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.topic) }}
        </div>
        {{ form_errors(quiz_form.session.topic) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.session.topic, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.type) }}
        </div>
        {{ form_errors(quiz_form.session.type) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.session.type, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.description) }}
        </div>
        {{ form_errors(quiz_form.session.description) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.session.description, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.studentInstructions) }}
        </div>
        {{ form_errors(quiz_form.session.studentInstructions) }}
        {{ form_widget(quiz_form.session.studentInstructions, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.mentorInstructions) }}
        </div>
        {{ form_errors(quiz_form.session.mentorInstructions) }}
        {{ form_widget(quiz_form.session.mentorInstructions, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.sections) }}
        </div>
        {{ form_errors(quiz_form.session.sections) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.session.sections, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.graded) }}
        </div>
        {{ form_errors(quiz_form.session.graded) }}
        {{ form_widget(quiz_form.session.graded) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.numericGrade) }}
        </div>
        {{ form_errors(quiz_form.session.numericGrade) }}
        {{ form_widget(quiz_form.session.numericGrade) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.files) }}
        </div>
        {{ form_errors(quiz_form.session.files) }}
        {{ form_widget(quiz_form.session.files) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.uploadedFiles) }}
        </div>
        {{ form_errors(quiz_form.session.uploadedFiles) }}
        {{ form_widget(quiz_form.session.uploadedFiles) }}
        <div>
            {% for r in quiz_form.session.uploadedFiles %}
                <div class=\"checkbox\">
                    <label>
                        {{ form_widget(r, {'attr': {'checked': 'checked'}}) }}
                        {{ r.vars.label }}
                    </label>
                </div>
            {% endfor %}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.session.color) }}
        </div>
        {{ form_errors(quiz_form.session.color) }}
        {{ form_widget(quiz_form.session.color) }}
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.room) }}
        </div>
        {{ form_errors(quiz_form.room) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.room, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.startDate) }}
        </div>
        {{ form_errors(quiz_form.startDate) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.startDate, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(quiz_form.endDate) }}
        </div>
        {{ form_errors(quiz_form.endDate) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(quiz_form.endDate, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    {{ form_rest(quiz_form) }}
    {{ form_end(quiz_form) }}
    </div>
    <div class=\"modal-footer\">
        <a class=\"btn btn-default\" href=\"{{ path('admin_session_requests') }}\">Close</a>
        <button id=\"session-submit\" type=\"button\" class=\"btn btn-success\" data-active=\"scheduled_session\">
            Submit
        </button>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {

            var text = \$('#scheduled_session_session_type option:selected').text();
            if (text == 'quiz') {
                \$('#session-submit').data('active', 'quiz');

                \$('[name=\"scheduled_session\"]').hide();
                \$('[name=\"quiz\"]').show();
            } else {
                \$('#session-submit').data('active', 'scheduled_session');

                \$('[name=\"quiz\"]').hide();
                \$('[name=\"scheduled_session\"]').show();
            }

            \$('#scheduled_session_session_type').on('change', function () {
                var text = \$('option:selected', \$(this)).text();
                if (text == 'quiz') {
                    \$('#session-submit').data('active', 'quiz');

                    \$('[name=\"scheduled_session\"]').hide();
                    \$('[name=\"quiz\"]').show();

                    sync_data('#scheduled_session', '#quiz');
                }
            });

            \$('#quiz_session_type').on('change', function () {
                var text = \$('option:selected', \$(this)).text();
                if (text == 'review' || text == 'rework') {
                    \$('#session-submit').data('active', 'scheduled_session');

                    \$('[name=\"quiz\"]').hide();
                    \$('[name=\"scheduled_session\"]').show();

                    sync_data('#quiz', '#scheduled_session');
                }
            });

            var sync_data = function (one, two) {
                var topic = \$(one + '_session_topic').val();
                \$(two + '_session_topic').val(topic);

                var type = \$(one + '_session_type').val();
                \$(two + '_session_type').val(type);

                var description = \$(one + '_session_description').val();
                \$(two + '_session_description').val(description);

                var sInstr = \$(one + '_session_studentInstructions').val();
                \$(two + '_session_studentInstructions').val(sInstr);

                var mInstr = \$(one + '_session_mentorInstructions').val();
                \$(two + '_session_mentorInstructions').val(mInstr);

                // sections
                var sections = [];
                \$.each(\$(one + '_session_sections option:selected'), function () {
                    sections.push(\$(this).val());
                });
                \$(two + '_session_sections').val(sections);
                \$(two + '_session_sections').trigger('change');

                var graded = \$(one + '_session_graded').is(':checked');
                \$(two + '_session_graded').prop('checked', graded);

                var numeric = \$(one + '_session_numericGrade').is(':checked');
                \$(two + '_session_numericGrade').prop('checked', numeric);

                // files

                var color = \$(one + '_session_color').val();
                \$(two + '_session_color').val(color);
            };

            \$('#session-submit').on('click', function () {
                \$('[name=\"' + \$(this).data('active') + '\"]').submit();
            });
        });
    </script>
{% endblock %}
", "role/admin/session/form.html.twig", "/code/templates/role/admin/session/form.html.twig");
    }
}
