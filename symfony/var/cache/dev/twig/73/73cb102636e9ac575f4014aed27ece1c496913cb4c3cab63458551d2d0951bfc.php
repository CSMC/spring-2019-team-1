<?php

/* role/instructor/session/schedule_by_course.html.twig */
class __TwigTemplate_64cffe5d0014f5a93442078156bbda01142e1a23b3acaff21b164f21ed208f63 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/schedule_by_course.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/schedule_by_course.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/schedule_by_course.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new Twig_Error_Runtime('Variable "courses" does not exist.', 5, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
            // line 6
            echo "        <div class=\"content_head\">
            <h2 class=\"content_title\">
                ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "number", []), "html", null, true);
            echo "
                : ";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "name", []), "html", null, true);
            echo "
            </h2>
        </div>
        <div class=\"content_head\">
            <h3 class=\"content_title\">Quizzes</h3>
        </div>
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Topic</th>
                <th class=\"content_table_cell\">Dates</th>
                <th class=\"content_table_cell\">Room</th>
                <th class=\"content_table_cell\">Description</th>
            </tr>
            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "quizzes", [], "array"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
                // line 23
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 25
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "startDate", []), "m/d/y"), "html", null, true);
                echo "
                        - ";
                // line 26
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "endDate", []), "m/d/y"), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "room", []), "html", null, true);
                echo "</td>
                    <td class=\"content_table_cell\">";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
                echo "</td>
                </tr>
            ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 31
                echo "                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"4\">No upcoming quizzes.</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "        </table>
        <br>
        <div class=\"content_head\">
            <h2 class=\"content_title\">Sessions</h2>
        </div>
        ";
            // line 40
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "sessions", [], "array"));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
                // line 41
                echo "            <div class=\"content_head\">
                <h3 class=\"content_title\">";
                // line 42
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
                echo "</h3>
                <div class=\"content_options\">
                    <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_view", ["id" => twig_get_attribute($this->env, $this->source, $context["session"], "id", [])]), "html", null, true);
                echo "\">
                        <div class=\"button green_button\">
                            View Details
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"content_list\">
                <div class=\"content_list_label\">Description</div>
                <div class=\"content_list_info\">";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "description", []), "html", null, true);
                echo "</div>
                <div class=\"content_list_label\">Time Slots</div>
                ";
                // line 55
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["session"], "timeslots", []));
                $context['_iterated'] = false;
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
                    // line 56
                    echo "                    <div class=\"content_list_info\">
                        ";
                    // line 57
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "m/d/Y g:i A"), "html", null, true);
                    echo "
                        - ";
                    // line 58
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "endTime", []), "g:i A"), "html", null, true);
                    echo "
                    </div>
                    ";
                    // line 60
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                        // line 61
                        echo "                        <div class=\"content_list_label\"></div>
                    ";
                    }
                    // line 63
                    echo "                ";
                    $context['_iterated'] = true;
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                if (!$context['_iterated']) {
                    // line 64
                    echo "                    <div class=\"content_list_info\">None</div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 66
                echo "            </div>
        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 68
                echo "            <div class=\"content_list\">
                <div class=\"content_list_label\"></div>
                <div class=\"content_list_info\">No sessions currently scheduled.</div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "        <br>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 75
            echo "        <div class=\"message red_message\">You are not currently assigned to any courses. If this is an error please
            contact a CSMC Admin
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/schedule_by_course.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 75,  239 => 73,  229 => 68,  223 => 66,  216 => 64,  203 => 63,  199 => 61,  197 => 60,  192 => 58,  188 => 57,  185 => 56,  167 => 55,  162 => 53,  150 => 44,  145 => 42,  142 => 41,  137 => 40,  130 => 35,  121 => 31,  113 => 28,  109 => 27,  105 => 26,  101 => 25,  97 => 24,  94 => 23,  89 => 22,  73 => 9,  67 => 8,  63 => 6,  58 => 5,  53 => 4,  44 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# For use with instructors/students #}
{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    {% for course in courses %}
        <div class=\"content_head\">
            <h2 class=\"content_title\">
                {{ course['section'].course.department.abbreviation }} {{ course['section'].course.number }}
                : {{ course['section'].course.name }}
            </h2>
        </div>
        <div class=\"content_head\">
            <h3 class=\"content_title\">Quizzes</h3>
        </div>
        <table class=\"content_table\">
            <tr class=\"content_table_head\">
                <th class=\"content_table_cell\">Topic</th>
                <th class=\"content_table_cell\">Dates</th>
                <th class=\"content_table_cell\">Room</th>
                <th class=\"content_table_cell\">Description</th>
            </tr>
            {% for quiz in course['quizzes'] %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\">{{ quiz.topic }}</td>
                    <td class=\"content_table_cell\">{{ quiz.startDate|date('m/d/y') }}
                        - {{ quiz.endDate|date('m/d/y') }}</td>
                    <td class=\"content_table_cell\">{{ quiz.room }}</td>
                    <td class=\"content_table_cell\">{{ quiz.description }}</td>
                </tr>
            {% else %}
                <tr class=\"content_table_data\">
                    <td class=\"content_table_cell\" colspan=\"4\">No upcoming quizzes.</td>
                </tr>
            {% endfor %}
        </table>
        <br>
        <div class=\"content_head\">
            <h2 class=\"content_title\">Sessions</h2>
        </div>
        {% for session in course['sessions'] %}
            <div class=\"content_head\">
                <h3 class=\"content_title\">{{ session.topic }}</h3>
                <div class=\"content_options\">
                    <a href=\"{{ path('session_view', {'id': session.id}) }}\">
                        <div class=\"button green_button\">
                            View Details
                        </div>
                    </a>
                </div>
            </div>
            <div class=\"content_list\">
                <div class=\"content_list_label\">Description</div>
                <div class=\"content_list_info\">{{ session.description }}</div>
                <div class=\"content_list_label\">Time Slots</div>
                {% for timeslot in session.timeslots %}
                    <div class=\"content_list_info\">
                        {{ timeslot.startTime|date('m/d/Y g:i A') }}
                        - {{ timeslot.endTime|date('g:i A') }}
                    </div>
                    {% if not loop.last %}
                        <div class=\"content_list_label\"></div>
                    {% endif %}
                {% else %}
                    <div class=\"content_list_info\">None</div>
                {% endfor %}
            </div>
        {% else %}
            <div class=\"content_list\">
                <div class=\"content_list_label\"></div>
                <div class=\"content_list_info\">No sessions currently scheduled.</div>
            </div>
        {% endfor %}
        <br>
    {% else %}
        <div class=\"message red_message\">You are not currently assigned to any courses. If this is an error please
            contact a CSMC Admin
        </div>
    {% endfor %}
{% endblock %}", "role/instructor/session/schedule_by_course.html.twig", "/code/templates/role/instructor/session/schedule_by_course.html.twig");
    }
}
