<?php

/* role/admin/section/edit.html.twig */
class __TwigTemplate_19fe859f705b941c1b35f8579c9798ab39335ab8acc12a6704d299a829f200ab extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/section/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/section/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/section/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Section?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    ";
        // line 42
        echo "                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='/deleteSection?id=";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["deleteId"]) || array_key_exists("deleteId", $context) ? $context["deleteId"] : (function () { throw new Twig_Error_Runtime('Variable "deleteId" does not exist.', 43, $this->source); })()), "html", null, true);
        echo "'\">Delete
                    </button>

                </div>
            </div>
        </div>
    </div>


    <div class=\"center_col\" role=\"main\">

        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Section (";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["sectionNumber"]) || array_key_exists("sectionNumber", $context) ? $context["sectionNumber"] : (function () { throw new Twig_Error_Runtime('Variable "sectionNumber" does not exist.', 58, $this->source); })()), "html", null, true);
        echo ") </h2>
                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">
                        <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                    </span>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">
                        ";
        // line 68
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 68, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 69
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 71
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 72
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
                        ";
        // line 79
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 79, $this->source); })()), "vars", []), "valid", [])) {
            // line 80
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 81
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 81, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 84
        echo "                        <br/>

                        ";
        // line 86
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 86, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                        ";
        // line 87
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 87, $this->source); })()), 'errors');
        echo "

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 91, $this->source); })()), "course", []), 'label');
        echo "
                            </div>
                            ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 93, $this->source); })()), "course", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 95, $this->source); })()), "course", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 102, $this->source); })()), "number", []), 'label');
        echo "
                            </div>
                            ";
        // line 104
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 104, $this->source); })()), "number", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 106, $this->source); })()), "number", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 113
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 113, $this->source); })()), "semester", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 115, $this->source); })()), "semester", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 117, $this->source); })()), "semester", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>


                        <!-- SHOULD be selected from selection box -->
                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 125
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 125, $this->source); })()), "instructors", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 127
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 127, $this->source); })()), "instructors", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 129, $this->source); })()), "instructors", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 135
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 135, $this->source); })()), "teaching_assistants", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 137
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 137, $this->source); })()), "teaching_assistants", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 139, $this->source); })()), "teaching_assistants", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "size" => "7"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 145
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 145, $this->source); })()), "description", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 147
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 147, $this->source); })()), "description", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 149
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 149, $this->source); })()), "description", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "rows" => "7"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">

                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                Upload Roster
                            </div>

                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                <b>[[File Upload Control ...]]</b>
                            </div>
                        </div>


                        <div class=\"ln_solid\"></div>

                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <a class=\"btn btn-secondary\" href=\"";
        // line 169
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_list");
        echo "\">
                                    Cancel
                                </a>
                                ";
        // line 172
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 172, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "
                            </div>
                        </div>

                        ";
        // line 176
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 176, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>


    ";
        // line 184
        $this->displayBlock('javascripts', $context, $blocks);
        // line 206
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 184
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 185
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();


            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();


            });


        </script>


    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/section/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 185,  361 => 184,  348 => 206,  346 => 184,  335 => 176,  328 => 172,  322 => 169,  299 => 149,  294 => 147,  289 => 145,  280 => 139,  275 => 137,  270 => 135,  261 => 129,  256 => 127,  251 => 125,  240 => 117,  235 => 115,  230 => 113,  220 => 106,  215 => 104,  210 => 102,  200 => 95,  195 => 93,  190 => 91,  183 => 87,  179 => 86,  175 => 84,  169 => 81,  166 => 80,  163 => 79,  160 => 77,  153 => 75,  144 => 72,  139 => 71,  135 => 70,  132 => 69,  127 => 68,  115 => 58,  97 => 43,  94 => 42,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Section?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    {# Change the delete URL #}
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='/deleteSection?id={{ deleteId }}'\">Delete
                    </button>

                </div>
            </div>
        </div>
    </div>


    <div class=\"center_col\" role=\"main\">

        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Section ({{ sectionNumber }}) </h2>
                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">
                        <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>
                    </span>
                        <div class=\"clearfix\"></div>
                    </div>

                    <div class=\"x_content\">
                        {# Displaying Flash message for foreign key constraint violation#}
                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}

                        {# Checking for form errors and displaying them#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>

                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}
                        {{ form_errors(form) }}

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.course) }}
                            </div>
                            {{ form_errors(form.course) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.course, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.number) }}
                            </div>
                            {{ form_errors(form.number) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.number, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                            </div>
                        </div>


                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.semester, null, {'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.semester) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.semester, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>


                        <!-- SHOULD be selected from selection box -->
                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.instructors, null, { 'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.instructors) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.instructors, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.teaching_assistants, null, { 'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.teaching_assistants) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.teaching_assistants, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'size': '7' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.description, null, { 'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.description) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.description, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'rows': '7' }}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">

                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                Upload Roster
                            </div>

                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                <b>[[File Upload Control ...]]</b>
                            </div>
                        </div>


                        <div class=\"ln_solid\"></div>

                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <a class=\"btn btn-secondary\" href=\"{{ path('admin_section_list') }}\">
                                    Cancel
                                </a>
                                {{ form_widget(form.save, {'attr': {'class': 'btn btn-success', 'data-toggle':'modal','data-target':'#myModalEdit' }}) }}
                            </div>
                        </div>

                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();


            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();


            });


        </script>


    {% endblock %}



{% endblock %}
", "role/admin/section/edit.html.twig", "/code/templates/role/admin/section/edit.html.twig");
    }
}
