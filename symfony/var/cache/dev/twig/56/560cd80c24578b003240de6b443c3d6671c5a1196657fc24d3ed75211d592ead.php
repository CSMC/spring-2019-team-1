<?php

/* role/admin/ip/add.html.twig */
class __TwigTemplate_d9f63f83eed660f018823d73a2b732c6f86aaffbba348fca47aa0674cf366ad5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/ip/add.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/ip/add.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/ip/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"center_col\" role=\"main\">



    <div class=\"row\">
    <div class=\"col-md-12 col-sm-12 col-xs-12\">
    <div class=\"x_panel\">
    <div class=\"x_title\">

        <h2>Create IP Address</h2>

        <div class=\"clearfix\"></div>
    </div>
    <div class=\"x_content\">


        <br/>
        <div class=\"x_content\">
            <br/>
            ";
        // line 24
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 24, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left input_mask"]]);
        echo "

            ";
        // line 26
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 26, $this->source); })()), "vars", []), "valid", [])) {
            // line 27
            echo "                <div class=\"alert alert-danger\" id=\"errorDiv\">
                    ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->source); })()), 'errors');
            echo "
                </div>
            ";
        }
        // line 31
        echo "
            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->source); })()), "address", []), 'label');
        echo "
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->source); })()), "address", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                       for=\"first-name\">";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 42, $this->source); })()), "room", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 45, $this->source); })()), "room", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                       for=\"first-name\">";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 51, $this->source); })()), "blocked", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 54, $this->source); })()), "blocked", []), 'widget');
        echo "
                </div>
            </div>


            <div class=\"ln_solid\"></div>
            <div class=\"form-group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <button class=\"btn btn-secondary\" type=\"button\" onclick=\"location.href='./addressShowAll'\">Cancel
                    </button>
                    ";
        // line 64
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 64, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "
                </div>
            </div>

            ";
        // line 68
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 68, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/ip/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 68,  145 => 64,  132 => 54,  126 => 51,  117 => 45,  111 => 42,  102 => 36,  96 => 33,  92 => 31,  86 => 28,  83 => 27,  81 => 26,  76 => 24,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"center_col\" role=\"main\">



    <div class=\"row\">
    <div class=\"col-md-12 col-sm-12 col-xs-12\">
    <div class=\"x_panel\">
    <div class=\"x_title\">

        <h2>Create IP Address</h2>

        <div class=\"clearfix\"></div>
    </div>
    <div class=\"x_content\">


        <br/>
        <div class=\"x_content\">
            <br/>
            {{ form_start(form,{'attr':{'class':'form-horizontal form-label-left input_mask'}}) }}

            {% if not form.vars.valid %}
                <div class=\"alert alert-danger\" id=\"errorDiv\">
                    {{ form_errors(form) }}
                </div>
            {% endif %}

            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">{{ form_label(form.address) }}
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    {{ form_widget(form.address, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                       for=\"first-name\">{{ form_label(form.room,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    {{ form_widget(form.room, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                </div>
            </div>

            <div class=\"form-group\">
                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                       for=\"first-name\">{{ form_label(form.blocked,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                </label>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    {{ form_widget(form.blocked) }}
                </div>
            </div>


            <div class=\"ln_solid\"></div>
            <div class=\"form-group\">
                <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                    <button class=\"btn btn-secondary\" type=\"button\" onclick=\"location.href='./addressShowAll'\">Cancel
                    </button>
                    {{ form_widget(form.submit, {'attr': {'class': 'btn btn-success'}}) }}
                </div>
            </div>

            {{ form_end(form) }}
        </div>
    </div>


{% endblock %}", "role/admin/ip/add.html.twig", "/code/templates/role/admin/ip/add.html.twig");
    }
}
