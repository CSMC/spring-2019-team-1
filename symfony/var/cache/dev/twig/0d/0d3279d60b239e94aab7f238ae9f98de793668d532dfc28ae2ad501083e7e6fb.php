<?php

/* role/student/session/register/register.html.twig */
class __TwigTemplate_621fc9cf0c62280f9d2564b0d8e2026b5f7d194f46c470e83c9dea5b29b08a3a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/student/session/register/register.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/register/register.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/register/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    <h2>";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 4, $this->source); })()), "topic", []), "html", null, true);
        echo "</h2>
    <dl class=\"dl-horizontal\">
        <dt>Date</dt>
        <dd>";
        // line 7
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 7, $this->source); })()), "startTime", []), "m/d/Y"), "html", null, true);
        echo "</dd>
        <dt>Time</dt>
        <dd>";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 9, $this->source); })()), "startTime", []), "g:i A"), "html", null, true);
        echo "</dd>
        <dt>Room</dt>
        <dd>";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 11, $this->source); })()), "location", []), "html", null, true);
        echo "</dd>
        <dt>Descriptions</dt>
        <dd>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 13, $this->source); })()), "description", []), "html", null, true);
        echo "</dd>
        <dt>Instructions</dt>
        <dd>";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 15, $this->source); })()), "studentInstructions", []), "html", null, true);
        echo "</dd>
        <dt>Remaining Seats</dt>
        <dd>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 17, $this->source); })()), "remainingSeats", []), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timeslot"]) || array_key_exists("timeslot", $context) ? $context["timeslot"] : (function () { throw new Twig_Error_Runtime('Variable "timeslot" does not exist.', 17, $this->source); })()), "capacity", []), "html", null, true);
        echo "</dd>
    </dl>
    ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 19, $this->source); })()), 'form_start', ["attr" => ["class" => "content_form"]]);
        echo "
    ";
        // line 20
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "policyCheck", [], "any", true, true)) {
            // line 21
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->source); })()), "policyCheck", []), 'widget', ["attr" => ["class" => "content_form_policy_agreement_checkbox"]]);
            echo "
        ";
            // line 23
            echo "        <div class=\"content_form_policy_agreement_label\">
            I have read and agree to the CSMC
            <a target=\"_blank\" href=\"";
            // line 25
            if ( !twig_test_empty((isset($context["policy"]) || array_key_exists("policy", $context) ? $context["policy"] : (function () { throw new Twig_Error_Runtime('Variable "policy" does not exist.', 25, $this->source); })()))) {
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["fid" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["policy"]) || array_key_exists("policy", $context) ? $context["policy"] : (function () { throw new Twig_Error_Runtime('Variable "policy" does not exist.', 25, $this->source); })()), "file", []), "id", [])]), "html", null, true);
            }
            echo "\">
                Rules and Policies
            </a>.
        </div>
    ";
        }
        // line 30
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "button green_button content_form_button"]]);
        echo "
    ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 31, $this->source); })()), 'rest');
        echo "
    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 32, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/student/session/register/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 32,  126 => 31,  121 => 30,  111 => 25,  107 => 23,  102 => 21,  100 => 20,  96 => 19,  89 => 17,  84 => 15,  79 => 13,  74 => 11,  69 => 9,  64 => 7,  58 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    <h2>{{ session.topic }}</h2>
    <dl class=\"dl-horizontal\">
        <dt>Date</dt>
        <dd>{{ timeslot.startTime|date('m/d/Y') }}</dd>
        <dt>Time</dt>
        <dd>{{ timeslot.startTime|date('g:i A') }}</dd>
        <dt>Room</dt>
        <dd>{{ timeslot.location }}</dd>
        <dt>Descriptions</dt>
        <dd>{{ session.description }}</dd>
        <dt>Instructions</dt>
        <dd>{{ session.studentInstructions }}</dd>
        <dt>Remaining Seats</dt>
        <dd>{{ timeslot.remainingSeats }}/{{ timeslot.capacity }}</dd>
    </dl>
    {{ form_start(form, {'attr': {'class': 'content_form'} }) }}
    {% if form.policyCheck is defined %}
        {{ form_widget(form.policyCheck, {'attr': {'class': 'content_form_policy_agreement_checkbox'} }) }}
        {# TODO reconsider below check on policy existence #}
        <div class=\"content_form_policy_agreement_label\">
            I have read and agree to the CSMC
            <a target=\"_blank\" href=\"{% if policy is not empty %}{{ path('download', {'fid': policy.file.id}) }}{% endif %}\">
                Rules and Policies
            </a>.
        </div>
    {% endif %}
    {{ form_widget(form.submit, {'attr': {'class': 'button green_button content_form_button'} }) }}
    {{ form_rest(form) }}
    {{ form_end(form) }}
{% endblock %}", "role/student/session/register/register.html.twig", "/code/templates/role/student/session/register/register.html.twig");
    }
}
