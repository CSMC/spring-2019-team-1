<?php

/* shared/component/nav.html.twig */
class __TwigTemplate_0c61fafa3f6eddf77de6269e910108135469d3fd46c676527c0a2d0f87ee7ad9 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/component/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/component/nav.html.twig"));

        // line 1
        echo "<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a class=\"site_title\">";
        // line 4
        echo " <span>CSMC</span></a>
        </div>

        <div class=\"clearfix\"></div>

        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                <h3>General</h3>
                <ul class=\"nav side-menu\">
                    <li>
                        <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><i class=\"fa fa-home\"></i> Home</a>
                    </li>
                    ";
        // line 17
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("admin") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("developer"))) {
            // line 18
            echo "                        <li><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_home");
            echo "\"><i class=\"fas fa-clipboard\"></i> Admin Dashboard</a></li>
                    ";
        }
        // line 20
        echo "                    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("instructor")) {
            // line 21
            echo "                        <li><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("section");
            echo "\">Courses</a></li>
                        <li><a>Reports</a></li>
                        <li><a href=\"";
            // line 23
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_schedule");
            echo "\">Session Schedule</a></li>
                        <li><a href=\"";
            // line 24
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_request");
            echo "\">Request a Session</a></li>
                        <li><a href=\"";
            // line 25
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_history");
            echo "\">Session Grades</a></li>
                    ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("mentor")) {
            // line 27
            echo "                        <li><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_schedule");
            echo "\"><i class=\"fa fa-calendar\"></i> Calendar</a></li>
                        <li><a href=\"";
            // line 28
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_history");
            echo "\"><i class=\"fa fa-check\"></i> Grades</a></li>
                        <li><a href=\"";
            // line 29
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule");
            echo "\"><i class=\"fa fa-users\"></i> Staff Schedule</a></li>
                        <li><a href=\"";
            // line 30
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("schedule_timesheet");
            echo "\"><i class=\"fa fa-clock\"></i> Time Sheet Report</a></li>
                        <li><a href=\"";
            // line 31
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("absence");
            echo "\"><i class=\"fa fa-exclamation-circle\"></i> Absences</a></li>
                        <li><a href=\"";
            // line 32
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("behavior_occurrence_submission");
            echo "\"><i class=\"fa fa-comment\"></i>Submit Occurrence</a></li>
                        <li><a href=\"";
            // line 33
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("swipe_walk_in");
            echo "\"><i class=\"fa fa-credit-card\"></i> Swipe Screen</a></li>
                    ";
        } elseif ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("student")) {
            // line 35
            echo "                        {<li><a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("section");
            echo "\">Courses</a></li>}
                        {<li><a href=\"";
            // line 36
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_schedule");
            echo "\">Register</a></li>}
                        {<li><a href=\"";
            // line 37
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_history");
            echo "\">Grades</a></li>}
                    ";
        }
        // line 39
        echo "                    ";
        // line 40
        echo "                    ";
        // line 41
        echo "                    ";
        // line 42
        echo "                    ";
        // line 43
        echo "                    ";
        // line 44
        echo "                    ";
        // line 45
        echo "                    ";
        // line 46
        echo "                    ";
        // line 47
        echo "                    ";
        // line 48
        echo "                    ";
        // line 49
        echo "                    ";
        // line 50
        echo "                    ";
        // line 51
        echo "
                    ";
        // line 53
        echo "                    ";
        // line 54
        echo "                    ";
        // line 55
        echo "                    ";
        // line 56
        echo "                    ";
        // line 57
        echo "                    ";
        // line 58
        echo "                    ";
        // line 59
        echo "
                    ";
        // line 61
        echo "                    ";
        // line 62
        echo "
                    ";
        // line 64
        echo "                    ";
        // line 65
        echo "                    ";
        // line 66
        echo "                    ";
        // line 67
        echo "
                    ";
        // line 69
        echo "                    ";
        // line 70
        echo "                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            ";
        // line 78
        echo "            ";
        // line 79
        echo "            ";
        // line 80
        echo "            ";
        // line 81
        echo "            ";
        // line 82
        echo "            ";
        // line 83
        echo "            ";
        // line 84
        echo "            ";
        // line 85
        echo "            ";
        // line 86
        echo "            ";
        // line 87
        echo "            ";
        // line 88
        echo "            ";
        // line 89
        echo "        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                ";
        // line 103
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 104
            echo "                    <li class=\"\">
                        <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                           aria-expanded=\"false\">
                            ";
            // line 107
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
                // line 108
                echo "                                <span class=\"fa fa-user-secret\"></span>
                            ";
            }
            // line 110
            echo "                            <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_image", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 110, $this->source); })()), "user", []), "username", [])]), "html", null, true);
            echo "\" alt=\"\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 110, $this->source); })()), "user", []), "preferredName", []), "html", null, true);
            echo "
                            <span class=\" fa fa-angle-down\"></span>
                        </a>
                        <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                            ";
            // line 114
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_PREVIOUS_ADMIN")) {
                // line 115
                echo "                                <li><a href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home", ["_switch_user" => "_exit"]);
                echo "\">Exit Impersonation</a></li>
                            ";
            }
            // line 117
            echo "                            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("developer")) {
                // line 118
                echo "                                <li><a ";
                echo ">(NYI) Impersonate User: <input type=\"text\"></a></li> ";
                // line 119
                echo "                            ";
            }
            // line 120
            echo "                            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("mentor")) {
                // line 121
                echo "                                <li><a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 121, $this->source); })()), "user", []), "username", [])]), "html", null, true);
                echo "\">Profile</a></li>
                            ";
            }
            // line 123
            echo "                            ";
            // line 124
            echo "                            ";
            // line 125
            echo "                            ";
            // line 126
            echo "                            ";
            // line 127
            echo "                            ";
            // line 128
            echo "                            ";
            // line 129
            echo "                            ";
            // line 130
            echo "                            <li><a href=\"javascript:;\">Help</a></li>
                            <li><a href=\"";
            // line 131
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_logout");
            echo "\"><i class=\"fa fa-sign-out pull-right\"></i> Log
                                    Out</a></li>
                        </ul>
                    </li>
                ";
        } else {
            // line 136
            echo "                    <li class=\"\">
                        <a href=\"";
            // line 137
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
            echo "\">Log In</a>
                    </li>
                ";
        }
        // line 140
        echo "            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/component/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 140,  308 => 137,  305 => 136,  297 => 131,  294 => 130,  292 => 129,  290 => 128,  288 => 127,  286 => 126,  284 => 125,  282 => 124,  280 => 123,  274 => 121,  271 => 120,  268 => 119,  265 => 118,  262 => 117,  256 => 115,  254 => 114,  244 => 110,  240 => 108,  238 => 107,  233 => 104,  231 => 103,  215 => 89,  213 => 88,  211 => 87,  209 => 86,  207 => 85,  205 => 84,  203 => 83,  201 => 82,  199 => 81,  197 => 80,  195 => 79,  193 => 78,  184 => 70,  182 => 69,  179 => 67,  177 => 66,  175 => 65,  173 => 64,  170 => 62,  168 => 61,  165 => 59,  163 => 58,  161 => 57,  159 => 56,  157 => 55,  155 => 54,  153 => 53,  150 => 51,  148 => 50,  146 => 49,  144 => 48,  142 => 47,  140 => 46,  138 => 45,  136 => 44,  134 => 43,  132 => 42,  130 => 41,  128 => 40,  126 => 39,  121 => 37,  117 => 36,  112 => 35,  107 => 33,  103 => 32,  99 => 31,  95 => 30,  91 => 29,  87 => 28,  82 => 27,  77 => 25,  73 => 24,  69 => 23,  63 => 21,  60 => 20,  54 => 18,  52 => 17,  47 => 15,  34 => 4,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a class=\"site_title\">{#<i class=\"fa fa-paw\"></i>#} <span>CSMC</span></a>
        </div>

        <div class=\"clearfix\"></div>

        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                <h3>General</h3>
                <ul class=\"nav side-menu\">
                    <li>
                        <a href=\"{{ path('home') }}\"><i class=\"fa fa-home\"></i> Home</a>
                    </li>
                    {% if is_granted('admin') or is_granted('developer') %}
                        <li><a href=\"{{ path('admin_home') }}\"><i class=\"fas fa-clipboard\"></i> Admin Dashboard</a></li>
                    {% endif %}
                    {% if is_granted('instructor') %}
                        <li><a href=\"{{ path('section') }}\">Courses</a></li>
                        <li><a>Reports</a></li>
                        <li><a href=\"{{ path('session_schedule') }}\">Session Schedule</a></li>
                        <li><a href=\"{{ path('session_request') }}\">Request a Session</a></li>
                        <li><a href=\"{{ path('session_history') }}\">Session Grades</a></li>
                    {% elseif is_granted('mentor') %}
                        <li><a href=\"{{ path('session_schedule') }}\"><i class=\"fa fa-calendar\"></i> Calendar</a></li>
                        <li><a href=\"{{ path('session_history') }}\"><i class=\"fa fa-check\"></i> Grades</a></li>
                        <li><a href=\"{{ path('schedule') }}\"><i class=\"fa fa-users\"></i> Staff Schedule</a></li>
                        <li><a href=\"{{ path('schedule_timesheet') }}\"><i class=\"fa fa-clock\"></i> Time Sheet Report</a></li>
                        <li><a href=\"{{ path('absence') }}\"><i class=\"fa fa-exclamation-circle\"></i> Absences</a></li>
                        <li><a href=\"{{ path('behavior_occurrence_submission') }}\"><i class=\"fa fa-comment\"></i>Submit Occurrence</a></li>
                        <li><a href=\"{{ path('swipe_walk_in') }}\"><i class=\"fa fa-credit-card\"></i> Swipe Screen</a></li>
                    {% elseif is_granted('student') %}
                        {<li><a href=\"{{ path('section') }}\">Courses</a></li>}
                        {<li><a href=\"{{ path('session_schedule') }}\">Register</a></li>}
                        {<li><a href=\"{{ path('session_history') }}\">Grades</a></li>}
                    {% endif %}
                    {#<li><a><i class=\"fa fa-edit\"></i> System Set Up <span class=\"fa fa-chevron-down\"></span></a>#}
                    {#<ul class=\"nav child_menu\">#}
                    {#<li><a href=\"addressShowAll\">IP</a></li>#}
                    {#<li><a href='roomShowAll'>Rooms</a></li>#}
                    {#<li><a href=\"semesterShowAll\">Semesters</a></li>#}
                    {#<li><a href=\"list_departments\">Departments</a></li>#}
                    {#<li><a href=\"listCourses\">Courses</a></li>#}
                    {#<li><a href=\"listSections\">Sections</a></li>#}
                    {#<li><a href=\"list_operation_hours\">Operation Hours</a></li>#}
                    {#<li><a href=\"list_schedule_time\">Schedule time</a></li>#}
                    {#</ul>#}
                    {#</li>#}

                    {#<li><a><i class=\"fa fa-edit\"></i> Users <span class=\"fa fa-chevron-down\"></span></a>#}
                    {#<ul class=\"nav child_menu\">#}
                    {#<li><a href=\"userShowAll\">User</a></li>#}
                    {#<li><a href=\"userGroupShowAll\">User Groups</a></li>#}
                    {#<li><a href=\"roleShowAll\">Roles</a></li>#}
                    {#</ul>#}
                    {#</li>#}

                    {#<li><a href=\"listAnnouncements\"><i class=\"fa fa-edit\"></i> Announcements</a>#}
                    {#</li>#}

                    {#<li><a><i class=\"fa fa-edit\"></i> Session Schedule <span class=\"badge bg-green\">6</span><span#}
                    {#class=\"fa fa-chevron-down\"></span></a>#}
                    {#<ul class=\"nav child_menu\">#}
                    {#<li><a href=\"list_holidays\">Holidays</a></li>#}

                    {#</ul>#}
                    {#</li>#}
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            {#<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">#}
            {#<span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>#}
            {#</a>#}
            {#<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">#}
            {#<span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>#}
            {#</a>#}
            {#<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">#}
            {#<span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>#}
            {#</a>#}
            {#<a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">#}
            {#<span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>#}
            {#</a>#}
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                    <li class=\"\">
                        <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                           aria-expanded=\"false\">
                            {% if is_granted('ROLE_PREVIOUS_ADMIN') %}
                                <span class=\"fa fa-user-secret\"></span>
                            {% endif %}
                            <img src=\"{{ path('profile_image', {'username': app.user.username}) }}\" alt=\"\">{{ app.user.preferredName }}
                            <span class=\" fa fa-angle-down\"></span>
                        </a>
                        <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                            {% if is_granted('ROLE_PREVIOUS_ADMIN') %}
                                <li><a href=\"{{ path('home', {'_switch_user':'_exit'}) }}\">Exit Impersonation</a></li>
                            {% endif %}
                            {% if is_granted('developer') %}
                                <li><a {#href=\"{{ path('home', {'_switch_user':'username'}) }}\"#}>(NYI) Impersonate User: <input type=\"text\"></a></li> {# TODO #}
                            {% endif %}
                            {% if is_granted('mentor') %}
                                <li><a href=\"{{ path('profile', {'username': app.user.username}) }}\">Profile</a></li>
                            {% endif %}
                            {#<li><a href=\"javascript:;\"> Profile</a></li>#}
                            {#<li>#}
                            {#<a href=\"javascript:;\">#}
                            {#<span class=\"badge bg-red pull-right\">50%</span>#}
                            {#<span>Settings</span>#}
                            {#</a>#}
                            {#</li>#}
                            <li><a href=\"javascript:;\">Help</a></li>
                            <li><a href=\"{{ path('user_logout') }}\"><i class=\"fa fa-sign-out pull-right\"></i> Log
                                    Out</a></li>
                        </ul>
                    </li>
                {% else %}
                    <li class=\"\">
                        <a href=\"{{ path('login') }}\">Log In</a>
                    </li>
                {% endif %}
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
", "shared/component/nav.html.twig", "/code/templates/shared/component/nav.html.twig");
    }
}
