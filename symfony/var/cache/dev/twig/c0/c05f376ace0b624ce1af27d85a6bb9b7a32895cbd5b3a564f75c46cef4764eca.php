<?php

/* role/instructor/session/request/form.html.twig */
class __TwigTemplate_69af6e204b32e8fb2c26d5846cf678569884a31b91f6a00b69933f6364e6cfd1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/instructor/session/request/form.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/instructor/session/request/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->source); })()), "topic", []), 'label');
        echo "
        </div>
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->source); })()), "topic", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->source); })()), "topic", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->source); })()), "type", []), 'label');
        echo "
        </div>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->source); })()), "type", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), "type", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 26, $this->source); })()), "studentInstructions", []), 'label');
        echo "
        </div>
        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->source); })()), "studentInstructions", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "studentInstructions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->source); })()), "startDate", []), 'label');
        echo "
        </div>
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->source); })()), "startDate", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 40, $this->source); })()), "startDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 46, $this->source); })()), "endDate", []), 'label');
        echo "
        </div>
        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 48, $this->source); })()), "endDate", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 50, $this->source); })()), "endDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 56, $this->source); })()), "sections", []), 'label');
        echo "
        </div>
        ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 58, $this->source); })()), "sections", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 60, $this->source); })()), "sections", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 66
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 66, $this->source); })()), "files", []), 'label');
        echo "
        </div>
        ";
        // line 68
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 68, $this->source); })()), "files", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 70, $this->source); })()), "files", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 72
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-md-6 col-sm-6 col-xs-12\">
            <a class=\"btn btn-secondary\" type=\"button\" href=\"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_request");
        echo "\">
                Cancel
            </a>
            ";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 79, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "
        </div>
    </div>
    ";
        // line 82
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 82, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/instructor/session/request/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 82,  200 => 79,  194 => 76,  188 => 72,  184 => 70,  179 => 68,  174 => 66,  165 => 60,  160 => 58,  155 => 56,  146 => 50,  141 => 48,  136 => 46,  127 => 40,  122 => 38,  117 => 36,  108 => 30,  103 => 28,  98 => 26,  89 => 20,  84 => 18,  79 => 16,  70 => 10,  65 => 8,  60 => 6,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ form_start(form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.topic) }}
        </div>
        {{ form_errors(form.topic) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.topic, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.type) }}
        </div>
        {{ form_errors(form.type) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.type, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.studentInstructions) }}
        </div>
        {{ form_errors(form.studentInstructions) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.studentInstructions, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.startDate) }}
        </div>
        {{ form_errors(form.startDate) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.startDate, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.endDate) }}
        </div>
        {{ form_errors(form.endDate) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.endDate, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.sections) }}
        </div>
        {{ form_errors(form.sections) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.sections, {'attr': {'class': 'form-control'}}) }}
            <span class=\"bt-flabels__error-desc\">Required</span>
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.files) }}
        </div>
        {{ form_errors(form.files) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.files, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-md-6 col-sm-6 col-xs-12\">
            <a class=\"btn btn-secondary\" type=\"button\" href=\"{{ path('session_request') }}\">
                Cancel
            </a>
            {{ form_widget(form.submit, {'attr': {'class': 'btn btn-success'}}) }}
        </div>
    </div>
    {{ form_end(form) }}
{% endblock %}", "role/instructor/session/request/form.html.twig", "/code/templates/role/instructor/session/request/form.html.twig");
    }
}
