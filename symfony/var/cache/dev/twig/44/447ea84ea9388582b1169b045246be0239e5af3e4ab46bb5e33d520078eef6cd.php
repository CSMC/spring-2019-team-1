<?php

/* role/mentor/session/session.html.twig */
class __TwigTemplate_917c937de8c500293693d673be125fefdcf2f292e4562dd12292ce7061945c89 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/session/session.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/session.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/session.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"content_head\">
        <h2 class=\"content_title\">";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 4, $this->source); })()), "topic", []), "html", null, true);
        echo "</h2>
    </div>
    <div class=\"content_list\">
        <div class=\"content_list_label\">Description</div>
        <div class=\"content_list_info\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 8, $this->source); })()), "description", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Student Instructions</div>
        <div class=\"content_list_info\">";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 10, $this->source); })()), "studentInstructions", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Mentor Instructions</div>
        <div class=\"content_list_info\">";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 12, $this->source); })()), "mentorInstructions", []), "html", null, true);
        echo "</div>
        <div class=\"content_list_label\">Graded</div>
        <div class=\"content_list_info\">";
        // line 14
        echo ((twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 14, $this->source); })()), "graded", [])) ? ("yes") : ("no"));
        echo "</div>
        ";
        // line 15
        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 15, $this->source); })()), "graded", [])) {
            // line 16
            echo "            <div class=\"content_list_label\">Grading Scheme</div>
            <div class=\"content_list_info\">";
            // line 17
            echo ((twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 17, $this->source); })()), "numericGrade", [])) ? ("numeric") : ("pass/fail"));
            echo "</div>
        ";
        }
        // line 19
        echo "        <div class=\"content_list_label\">Materials</div>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 20, $this->source); })()), "files", []));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["file"]) {
            // line 21
            echo "            ";
            if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "first", [])) {
                // line 22
                echo "                <div class=\"content_list_label\"></div>
            ";
            }
            // line 24
            echo "            <div class=\"content_list_info\">
                <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("download", ["fid" => twig_get_attribute($this->env, $this->source, $context["file"], "id", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["file"], "name", []), "html", null, true);
            echo "</a>
            </div>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 28
            echo "            <div class=\"content_list_info\"></div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['file'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        <div class=\"content_list_label\">Dates</div>
        <div class=\"content_list_info\">
            ";
        // line 32
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 32, $this->source); })()), "startDate", []), "m/d/Y"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 32, $this->source); })()), "endDate", []), "m/d/y"), "html", null, true);
        echo "
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/session/session.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 32,  148 => 30,  141 => 28,  123 => 25,  120 => 24,  116 => 22,  113 => 21,  95 => 20,  92 => 19,  87 => 17,  84 => 16,  82 => 15,  78 => 14,  73 => 12,  68 => 10,  63 => 8,  56 => 4,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <div class=\"content_head\">
        <h2 class=\"content_title\">{{ session.topic }}</h2>
    </div>
    <div class=\"content_list\">
        <div class=\"content_list_label\">Description</div>
        <div class=\"content_list_info\">{{ session.description }}</div>
        <div class=\"content_list_label\">Student Instructions</div>
        <div class=\"content_list_info\">{{ session.studentInstructions }}</div>
        <div class=\"content_list_label\">Mentor Instructions</div>
        <div class=\"content_list_info\">{{ session.mentorInstructions }}</div>
        <div class=\"content_list_label\">Graded</div>
        <div class=\"content_list_info\">{{ session.graded ? 'yes' : 'no' }}</div>
        {% if session.graded %}
            <div class=\"content_list_label\">Grading Scheme</div>
            <div class=\"content_list_info\">{{ session.numericGrade ? 'numeric' : 'pass/fail' }}</div>
        {% endif %}
        <div class=\"content_list_label\">Materials</div>
        {% for file in session.files %}
            {% if not loop.first %}
                <div class=\"content_list_label\"></div>
            {% endif %}
            <div class=\"content_list_info\">
                <a href=\"{{ path('download', {'fid': file.id}) }}\">{{ file.name }}</a>
            </div>
        {% else %}
            <div class=\"content_list_info\"></div>
        {% endfor %}
        <div class=\"content_list_label\">Dates</div>
        <div class=\"content_list_info\">
            {{ session.startDate|date('m/d/Y') }} - {{ session.endDate|date('m/d/y') }}
        </div>
    </div>
{% endblock %}", "role/mentor/session/session.html.twig", "/code/templates/role/mentor/session/session.html.twig");
    }
}
