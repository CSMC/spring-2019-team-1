<?php

/* role/admin/room/edit.html.twig */
class __TwigTemplate_e1f5727e1ca6c3dda1534381c56edca4cda65cd369c7e8ab66e9778d98faa094 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/room/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/room/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/room/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>

                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Room?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./roomDelete?id=";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["deleteId"]) || array_key_exists("deleteId", $context) ? $context["deleteId"] : (function () { throw new Twig_Error_Runtime('Variable "deleteId" does not exist.', 42, $this->source); })()), "html", null, true);
        echo "'\">Delete
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <div class=\"title_left\">
                            <h2>Edit Room (";
        // line 58
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 58, $this->source); })()), "vars", []), "value", []), "building", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 58, $this->source); })()), "vars", []), "value", []), "floor", []), "html", null, true);
        echo "
                                .";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 59, $this->source); })()), "vars", []), "value", []), "number", []), "html", null, true);
        echo ")</h2>
                            <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                    data-target=\"#myModalDelete\" style=\"float: right\">Delete
                            </button>
                        </div>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        ";
        // line 68
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 68, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 69
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 71
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 72
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                        ";
        // line 78
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 78, $this->source); })()), "vars", []), "valid", [])) {
            // line 79
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 80
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 80, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 83
        echo "                        <br/>
                        ";
        // line 84
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left input_mask"]]);
        echo "

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 88, $this->source); })()), "building", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 91, $this->source); })()), "building", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 97, $this->source); })()), "floor", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 100, $this->source); })()), "floor", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 106, $this->source); })()), "number", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 109, $this->source); })()), "number", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 115, $this->source); })()), "description", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 118, $this->source); })()), "description", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 124
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 124, $this->source); })()), "capacity", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 127
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 127, $this->source); })()), "capacity", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 133
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 133, $this->source); })()), "active", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 136
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 136, $this->source); })()), "active", []), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\" onclick=\"location.href='./roomShowAll'\">
                                Cancel
                            </button>
                            ";
        // line 144
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 144, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "


                        </div>
                        ";
        // line 148
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 148, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 155
        $this->displayBlock('javascripts', $context, $blocks);
        // line 171
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 155
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 156
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });
        </script>


    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/room/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  319 => 156,  310 => 155,  299 => 171,  297 => 155,  287 => 148,  280 => 144,  269 => 136,  263 => 133,  254 => 127,  248 => 124,  239 => 118,  233 => 115,  224 => 109,  218 => 106,  209 => 100,  203 => 97,  194 => 91,  188 => 88,  181 => 84,  178 => 83,  172 => 80,  169 => 79,  166 => 78,  164 => 77,  157 => 75,  148 => 72,  143 => 71,  139 => 70,  136 => 69,  131 => 68,  120 => 59,  114 => 58,  95 => 42,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>

                </div>
            </div>
        </div>
    </div>

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Room?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./roomDelete?id={{ deleteId }}'\">Delete
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <div class=\"title_left\">
                            <h2>Edit Room ({{ form.vars.value.building }} {{ form.vars.value.floor }}
                                .{{ form.vars.value.number }})</h2>
                            <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                    data-target=\"#myModalDelete\" style=\"float: right\">Delete
                            </button>
                        </div>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        {# Displaying Flash message for foreign key constraint violation#}
                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}
                        {# Displaying form errors#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>
                        {{ form_start(form,{'attr':{'class':'form-horizontal form-label-left input_mask'}}) }}

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.building) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.building, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.floor) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.floor, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.number) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.number, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.description) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.description, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.capacity) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.capacity, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.active,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.active) }}
                            </div>
                        </div>

                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\" onclick=\"location.href='./roomShowAll'\">
                                Cancel
                            </button>
                            {{ form_widget(form.submit,{'attr':{'class':'btn btn-success','data-toggle':'modal','data-target':'#myModalEdit'}}) }}


                        </div>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });
        </script>


    {% endblock %}

{% endblock %}", "role/admin/room/edit.html.twig", "/code/templates/role/admin/room/edit.html.twig");
    }
}
