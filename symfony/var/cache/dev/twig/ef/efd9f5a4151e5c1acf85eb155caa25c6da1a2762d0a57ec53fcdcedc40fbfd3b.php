<?php

/* role/admin/user/userShowMentor.html.twig */
class __TwigTemplate_4949594b3fb6f53b030409346de1faad5620f1653c78bb1e9bc215677c9803bf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user/userShowMentor.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/userShowMentor.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/userShowMentor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"page-title\">
        <div class=\"title_left\">
            <h3>Student </h3>
        </div>
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowAll'\">All
                                Users
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowInstructor'\">
                                Instructor
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowMentor'\">
                                Mentor
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowAdmin'\">Admin
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./addNewRoleStudent'\">
                                Student
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-right panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./addNewRole'\">New
                                User
                            </button>
                        </li>
                        <li><a class=\"collapse-link\"><i class=\"fas fa-chevron-up\"></i></a>
                        </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Netid</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Roles</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 82, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 83
            echo "                            <tr>
                                <td>";
            // line 84
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 85
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "firstName", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 86
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "lastName", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 87
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["u"], "getRoles", [], "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 88
                echo "                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "name", []), "html", null, true);
                echo "
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./userEdit?id=";
            // line 92
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "                    </table>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user/userShowMentor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 97,  171 => 92,  166 => 89,  157 => 88,  153 => 87,  149 => 86,  145 => 85,  141 => 84,  138 => 83,  134 => 82,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"page-title\">
        <div class=\"title_left\">
            <h3>Student </h3>
        </div>
    </div>
    <div class=\"clearfix\"></div>
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowAll'\">All
                                Users
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowInstructor'\">
                                Instructor
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowMentor'\">
                                Mentor
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./userShowAdmin'\">Admin
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-left panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-dark\" onclick=\"location.href='./addNewRoleStudent'\">
                                Student
                            </button>
                        </li>
                        <li><a><i></i></a>
                        </li>
                    </ul>
                    <ul class=\"nav navbar-right panel_toolbox\">
                        <li>
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./addNewRole'\">New
                                User
                            </button>
                        </li>
                        <li><a class=\"collapse-link\"><i class=\"fas fa-chevron-up\"></i></a>
                        </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                </div>
                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\">
                    </p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>Netid</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Roles</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for u in users %}
                            <tr>
                                <td>{{ u.username }}</td>
                                <td>{{ u.firstName }}</td>
                                <td>{{ u.lastName }}</td>
                                <td>{% for r in u.getRoles() %}
                                        {{ r.name }}
                                    {% endfor %}</td>
                                <td>
                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./userEdit?id={{ u.id }}'\">Edit
                                    </button>
                                </td>
                            </tr>
                        {% endfor %}
                    </table>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
     
     
     
", "role/admin/user/userShowMentor.html.twig", "/code/templates/role/admin/user/userShowMentor.html.twig");
    }
}
