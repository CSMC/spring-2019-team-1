<?php

/* role/admin/announcement/add.html.twig */
class __TwigTemplate_471c7246019868b0c635ef5b75144769df62361bf9d889a465c6b8289cec54e8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/announcement/add.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/add.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/announcement/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h2>Create Announcement</h2>
                    <div class=\"clearfix\"></div>
                </div>

                <div class=\"x_content\">
                    ";
        // line 13
        echo "                    ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->source); })()), "vars", []), "valid", [])) {
            // line 14
            echo "                        <div class=\"alert alert-danger\" id=\"errorDiv\">
                            ";
            // line 15
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->source); })()), 'errors');
            echo "
                        </div>
                    ";
        }
        // line 18
        echo "                    <br/>

                    ";
        // line 20
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                    ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->source); })()), 'errors');
        echo "

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 25, $this->source); })()), "subject", []), 'label');
        echo "
                        </div>
                        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 27, $this->source); })()), "subject", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 29, $this->source); })()), "subject", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->source); })()), "message", []), 'label');
        echo "
                        </div>
                        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->source); })()), "message", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 40, $this->source); })()), "message", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 47, $this->source); })()), "startDate", []), 'label');
        echo "
                        </div>
                        ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 49, $this->source); })()), "startDate", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 51, $this->source); })()), "startDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 58, $this->source); })()), "endDate", []), 'label');
        echo "
                        </div>
                        ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 60, $this->source); })()), "endDate", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 62, $this->source); })()), "endDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 69, $this->source); })()), "roles", []), 'label');
        echo "
                        </div>
                        ";
        // line 71
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 71, $this->source); })()), "roles", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 73, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                        </div>
                    </div>

                    ";
        // line 78
        echo "                        ";
        // line 79
        echo "                            ";
        // line 80
        echo "                        ";
        // line 81
        echo "                        ";
        // line 82
        echo "                        ";
        // line 83
        echo "                            ";
        // line 84
        echo "                        ";
        // line 85
        echo "                    ";
        // line 86
        echo "
                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 89, $this->source); })()), "active", []), 'label');
        echo "
                        </div>
                        ";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 91, $this->source); })()), "active", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 93, $this->source); })()), "active", []), 'widget', ["attr" => ["class" => "checkbox"]]);
        echo "
                        </div>
                    </div>

                    ";
        // line 98
        echo "                    ";
        // line 99
        echo "                    ";
        // line 100
        echo "                    ";
        // line 101
        echo "                    ";
        // line 102
        echo "
                    ";
        // line 104
        echo "                    ";
        // line 105
        echo "                    ";
        // line 106
        echo "                    ";
        // line 107
        echo "                    ";
        // line 108
        echo "                    ";
        // line 109
        echo "                    ";
        // line 110
        echo "                    ";
        // line 111
        echo "                    ";
        // line 112
        echo "                    ";
        // line 113
        echo "                    ";
        // line 114
        echo "                    ";
        // line 115
        echo "                    ";
        // line 116
        echo "                    ";
        // line 117
        echo "                    ";
        // line 118
        echo "                    ";
        // line 119
        echo "                    ";
        // line 120
        echo "                    ";
        // line 121
        echo "                    ";
        // line 122
        echo "                    ";
        // line 123
        echo "                    ";
        // line 124
        echo "                    ";
        // line 125
        echo "                    ";
        // line 126
        echo "                    ";
        // line 127
        echo "                    ";
        // line 128
        echo "                    ";
        // line 129
        echo "                    ";
        // line 130
        echo "                    ";
        // line 131
        echo "                    ";
        // line 132
        echo "                    ";
        // line 133
        echo "                    ";
        // line 134
        echo "                    ";
        // line 135
        echo "                    ";
        // line 136
        echo "                    ";
        // line 137
        echo "                    ";
        // line 138
        echo "                    ";
        // line 139
        echo "                    ";
        // line 140
        echo "                    ";
        // line 141
        echo "                    ";
        // line 142
        echo "                    ";
        // line 143
        echo "                    ";
        // line 144
        echo "                    ";
        // line 145
        echo "                    ";
        // line 146
        echo "                    ";
        // line 147
        echo "                    ";
        // line 148
        echo "                    ";
        // line 149
        echo "                    ";
        // line 150
        echo "                    ";
        // line 151
        echo "                    ";
        // line 152
        echo "                    ";
        // line 153
        echo "                    ";
        // line 154
        echo "                    ";
        // line 155
        echo "                    ";
        // line 156
        echo "                    ";
        // line 157
        echo "                    ";
        // line 158
        echo "                    ";
        // line 159
        echo "                    ";
        // line 160
        echo "                    ";
        // line 161
        echo "                    ";
        // line 162
        echo "                    ";
        // line 163
        echo "                    ";
        // line 164
        echo "                    ";
        // line 165
        echo "                    ";
        // line 166
        echo "                    ";
        // line 167
        echo "                    ";
        // line 168
        echo "                    ";
        // line 169
        echo "                    ";
        // line 170
        echo "                    ";
        // line 171
        echo "                    ";
        // line 172
        echo "                    ";
        // line 173
        echo "                    ";
        // line 174
        echo "                    ";
        // line 175
        echo "                    ";
        // line 176
        echo "                    ";
        // line 177
        echo "                    ";
        // line 178
        echo "                    ";
        // line 179
        echo "                    ";
        // line 180
        echo "                    ";
        // line 181
        echo "                    ";
        // line 182
        echo "                    ";
        // line 183
        echo "                    ";
        // line 184
        echo "                    ";
        // line 185
        echo "
                    ";
        // line 187
        echo "                    ";
        // line 188
        echo "                    ";
        // line 189
        echo "                    ";
        // line 190
        echo "                    ";
        // line 191
        echo "
                    ";
        // line 193
        echo "                    ";
        // line 194
        echo "                    ";
        // line 195
        echo "                    ";
        // line 196
        echo "                    ";
        // line 197
        echo "                    ";
        // line 198
        echo "                    ";
        // line 199
        echo "                    ";
        // line 200
        echo "                    ";
        // line 201
        echo "                    ";
        // line 202
        echo "                    ";
        // line 203
        echo "                    ";
        // line 204
        echo "                    ";
        // line 205
        echo "
                    ";
        // line 207
        echo "                    ";
        // line 208
        echo "                    ";
        // line 209
        echo "                    ";
        // line 210
        echo "                    ";
        // line 211
        echo "                    ";
        // line 212
        echo "                    ";
        // line 213
        echo "                    ";
        // line 214
        echo "                    ";
        // line 215
        echo "
                    ";
        // line 217
        echo "                    ";
        // line 218
        echo "                    ";
        // line 219
        echo "                    ";
        // line 220
        echo "                    ";
        // line 221
        echo "                    ";
        // line 222
        echo "                    ";
        // line 223
        echo "                    ";
        // line 224
        echo "                    ";
        // line 225
        echo "
                    ";
        // line 227
        echo "                    ";
        // line 228
        echo "                    ";
        // line 229
        echo "                    ";
        // line 230
        echo "                    ";
        // line 231
        echo "                    ";
        // line 232
        echo "                    ";
        // line 233
        echo "                    ";
        // line 234
        echo "                    ";
        // line 235
        echo "
                    ";
        // line 237
        echo "
                    <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                            <a class=\"btn btn-secondary\" type=\"button\"
                               href=\"";
        // line 241
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_announcement_list");
        echo "\">Cancel
                            </a>
                            ";
        // line 243
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 243, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "
                        </div>
                    </div>
                    ";
        // line 246
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 246, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>
        </div>
    </div>
    </div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 256
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 257
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 288
        echo "    ";
        // line 299
        echo "    <script type='text/javascript'>
        \$(function () {
            \$('#editor-one').wysiwyg();

            \$('#form_save').click(function () {
                document.getElementById(\"form_message\").value = \$('#editor-one').html();
            });
        });
    </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/announcement/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  551 => 299,  549 => 288,  544 => 257,  535 => 256,  515 => 246,  509 => 243,  504 => 241,  498 => 237,  495 => 235,  493 => 234,  491 => 233,  489 => 232,  487 => 231,  485 => 230,  483 => 229,  481 => 228,  479 => 227,  476 => 225,  474 => 224,  472 => 223,  470 => 222,  468 => 221,  466 => 220,  464 => 219,  462 => 218,  460 => 217,  457 => 215,  455 => 214,  453 => 213,  451 => 212,  449 => 211,  447 => 210,  445 => 209,  443 => 208,  441 => 207,  438 => 205,  436 => 204,  434 => 203,  432 => 202,  430 => 201,  428 => 200,  426 => 199,  424 => 198,  422 => 197,  420 => 196,  418 => 195,  416 => 194,  414 => 193,  411 => 191,  409 => 190,  407 => 189,  405 => 188,  403 => 187,  400 => 185,  398 => 184,  396 => 183,  394 => 182,  392 => 181,  390 => 180,  388 => 179,  386 => 178,  384 => 177,  382 => 176,  380 => 175,  378 => 174,  376 => 173,  374 => 172,  372 => 171,  370 => 170,  368 => 169,  366 => 168,  364 => 167,  362 => 166,  360 => 165,  358 => 164,  356 => 163,  354 => 162,  352 => 161,  350 => 160,  348 => 159,  346 => 158,  344 => 157,  342 => 156,  340 => 155,  338 => 154,  336 => 153,  334 => 152,  332 => 151,  330 => 150,  328 => 149,  326 => 148,  324 => 147,  322 => 146,  320 => 145,  318 => 144,  316 => 143,  314 => 142,  312 => 141,  310 => 140,  308 => 139,  306 => 138,  304 => 137,  302 => 136,  300 => 135,  298 => 134,  296 => 133,  294 => 132,  292 => 131,  290 => 130,  288 => 129,  286 => 128,  284 => 127,  282 => 126,  280 => 125,  278 => 124,  276 => 123,  274 => 122,  272 => 121,  270 => 120,  268 => 119,  266 => 118,  264 => 117,  262 => 116,  260 => 115,  258 => 114,  256 => 113,  254 => 112,  252 => 111,  250 => 110,  248 => 109,  246 => 108,  244 => 107,  242 => 106,  240 => 105,  238 => 104,  235 => 102,  233 => 101,  231 => 100,  229 => 99,  227 => 98,  220 => 93,  215 => 91,  210 => 89,  205 => 86,  203 => 85,  201 => 84,  199 => 83,  197 => 82,  195 => 81,  193 => 80,  191 => 79,  189 => 78,  182 => 73,  177 => 71,  172 => 69,  162 => 62,  157 => 60,  152 => 58,  142 => 51,  137 => 49,  132 => 47,  122 => 40,  117 => 38,  112 => 36,  102 => 29,  97 => 27,  92 => 25,  85 => 21,  81 => 20,  77 => 18,  71 => 15,  68 => 14,  65 => 13,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
                <div class=\"x_title\">
                    <h2>Create Announcement</h2>
                    <div class=\"clearfix\"></div>
                </div>

                <div class=\"x_content\">
                    {# Checking for form errors and displaying them#}
                    {% if not form.vars.valid %}
                        <div class=\"alert alert-danger\" id=\"errorDiv\">
                            {{ form_errors(form) }}
                        </div>
                    {% endif %}
                    <br/>

                    {{ form_start(form, {'attr': {'class': 'bt-flabels js-flabels' }}) }}
                    {{ form_errors(form) }}

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.subject) }}
                        </div>
                        {{ form_errors(form.subject) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.subject, {'attr': {'class': 'form-control' }}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.message) }}
                        </div>
                        {{ form_errors(form.message) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.message, {'attr': {'class': 'form-control' }}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.startDate) }}
                        </div>
                        {{ form_errors(form.startDate) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.startDate, {'attr': {'class': 'form-control' }}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.endDate) }}
                        </div>
                        {{ form_errors(form.endDate) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.endDate, {'attr': {'class': 'form-control' }}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.roles) }}
                        </div>
                        {{ form_errors(form.roles) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.roles, {'attr': {'class': 'form-control' }}) }}
                        </div>
                    </div>

                    {#<div class=\"form-group\">#}
                        {#<div class=\"control-label\">#}
                            {#{{ form_label(form.userGroups) }}#}
                        {#</div>#}
                        {#{{ form_errors(form.userGroups) }}#}
                        {#<div class=\"bt-flabels__wrapper\">#}
                            {#{{ form_widget(form.userGroups, {'attr': {'class': 'form-control' }}) }}#}
                        {#</div>#}
                    {#</div>#}

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(form.active) }}
                        </div>
                        {{ form_errors(form.active) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(form.active, {'attr': {'class': 'checkbox' }}) }}
                        </div>
                    </div>

                    {#<div class=\"form-group\">#}
                    {#<div class=\"control-label col-md-3 col-sm-3 col-xs-12\">#}
                    {#{{ form_label(form.message) }}#}
                    {#</div>#}
                    {#{{ form_errors(form.message) }}#}

                    {#<div class=\"col-md-6 col-sm-6 col-xs-12\">#}
                    {#<div class=\"btn-toolbar editor\" data-role=\"editor-toolbar\" data-target=\"#editor-one\">#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font\"><i#}
                    {#class=\"fas fa-font\"></i><b class=\"caret\"></b></a>#}
                    {#<ul class=\"dropdown-menu\">#}
                    {#</ul>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Font Size\"><i#}
                    {#class=\"fas fa-text-height\"></i>&nbsp;<b class=\"caret\"></b></a>#}
                    {#<ul class=\"dropdown-menu\">#}
                    {#<li>#}
                    {#<a data-edit=\"fontSize 5\">#}
                    {#<p style=\"font-size:17px\">Huge</p>#}
                    {#</a>#}
                    {#</li>#}
                    {#<li>#}
                    {#<a data-edit=\"fontSize 3\">#}
                    {#<p style=\"font-size:14px\">Normal</p>#}
                    {#</a>#}
                    {#</li>#}
                    {#<li>#}
                    {#<a data-edit=\"fontSize 1\">#}
                    {#<p style=\"font-size:11px\">Small</p>#}
                    {#</a>#}
                    {#</li>#}
                    {#</ul>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn\" data-edit=\"bold\" title=\"Bold (Ctrl/Cmd+B)\"><i#}
                    {#class=\"fas fa-bold\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"italic\" title=\"Italic (Ctrl/Cmd+I)\"><i#}
                    {#class=\"fas fa-italic\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"strikethrough\" title=\"Strikethrough\"><i#}
                    {#class=\"fas fa-strikethrough\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"underline\" title=\"Underline (Ctrl/Cmd+U)\"><i#}
                    {#class=\"fas fa-underline\"></i></a>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn\" data-edit=\"insertunorderedlist\" title=\"Bullet list\"><i#}
                    {#class=\"fas fa-list-ul\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"insertorderedlist\" title=\"Number list\"><i#}
                    {#class=\"fas fa-list-ol\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"outdent\" title=\"Reduce indent (Shift+Tab)\"><i#}
                    {#class=\"fas fa-dedent\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"indent\" title=\"Indent (Tab)\"><i#}
                    {#class=\"fas fa-indent\"></i></a>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn\" data-edit=\"justifyleft\" title=\"Align Left (Ctrl/Cmd+L)\"><i#}
                    {#class=\"fas fa-align-left\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"justifycenter\" title=\"Center (Ctrl/Cmd+E)\"><i#}
                    {#class=\"fas fa-align-center\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"justifyright\" title=\"Align Right (Ctrl/Cmd+R)\"><i#}
                    {#class=\"fas fa-align-right\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"justifyfull\" title=\"Justify (Ctrl/Cmd+J)\"><i#}
                    {#class=\"fas fa-align-justify\"></i></a>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" title=\"Hyperlink\"><i#}
                    {#class=\"fas fa-link\"></i></a>#}
                    {#<div class=\"dropdown-menu input-append\">#}
                    {#<input class=\"span2\" placeholder=\"URL\" type=\"text\" data-edit=\"createLink\"/>#}
                    {#<button class=\"btn\" type=\"button\">Add</button>#}
                    {#</div>#}
                    {#<a class=\"btn\" data-edit=\"unlink\" title=\"Remove Hyperlink\"><i#}
                    {#class=\"fas fa-cut\"></i></a>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn\" title=\"Insert picture (or just drag & drop)\" id=\"pictureBtn\"><i#}
                    {#class=\"fas fa-picture-o\"></i></a>#}
                    {#<input type=\"file\" data-role=\"magic-overlay\" data-target=\"#pictureBtn\"#}
                    {#data-edit=\"insertImage\"/>#}
                    {#</div>#}
                    {#<div class=\"btn-group\">#}
                    {#<a class=\"btn\" data-edit=\"undo\" title=\"Undo (Ctrl/Cmd+Z)\"><i#}
                    {#class=\"fas fa-undo\"></i></a>#}
                    {#<a class=\"btn\" data-edit=\"redo\" title=\"Redo (Ctrl/Cmd+Y)\"><i#}
                    {#class=\"fas fa-repeat\"></i></a>#}
                    {#</div>#}
                    {#</div>#}

                    {# TODO: HOW DO We Pass the JavaScript Value to the Textarea??? #}
                    {#<div id=\"editor-one\" class=\"editor-wrapper\"></div>#}
                    {#{{ form_widget(form.message, {'attr': {'style': 'display:none;', 'class': 'form-control col-md-7 col-xs-12' }}) }}#}
                    {#</div>#}
                    {#</div>#}

                    {#<div class=\"form-group\">#}
                    {#<div class=\"control-label col-md-3 col-sm-3 col-xs-12\">#}
                    {#{{ form_label(form.roles) }}#}
                    {#</div>#}
                    {#{{ form_errors(form.roles) }}#}
                    {#<div class=\"col-md-6 col-sm-6 col-xs-12\">#}
                    {##}
                    {#<option value=\"\" selected disabled hidden>Choose here</option>#}
                    {##}
                    {#{{ form_widget(form.roles, {#}
                    {#'attr': {'class': 'form-control col-md-7 col-xs-12', 'size': '7'}}) }}#}
                    {#</div>#}
                    {#</div>#}

                    {#<div class=\"form-group\">#}
                    {#<div class=\"control-label col-md-3 col-sm-3 col-xs-12\">#}
                    {#{{ form_label(form.userGroups) }}#}
                    {#</div>#}
                    {#{{ form_errors(form.userGroups) }}#}
                    {#<div class=\"col-md-6 col-sm-6 col-xs-12\">#}
                    {#{{ form_widget(form.userGroups, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'size': '7' }}) }}#}
                    {#</div>#}
                    {#</div>#}

                    {#<div class=\"form-group\">#}
                    {#<div class=\"control-label col-md-3 col-sm-3 col-xs-12\">#}
                    {#{{ form_label(form.startDate) }}#}
                    {#</div>#}
                    {#{{ form_errors(form.startDate) }}#}
                    {#<div class=\"col-md-6 col-sm-6 col-xs-12\">#}
                    {#{{ form_widget(form.startDate, {'attr': {'class': 'form-control col-md-7 col-xs-12' }}) }}#}
                    {#</div>#}
                    {#</div>#}

                    {#<div class=\"form-group\">#}
                    {#<div class=\"control-label col-md-3 col-sm-3 col-xs-12\">#}
                    {#{{ form_label(form.endDate) }}#}
                    {#</div>#}
                    {#{{ form_errors(form.endDate) }}#}
                    {#<div class=\"col-md-6 col-sm-6 col-xs-12\">#}
                    {#{{ form_widget(form.endDate, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}#}
                    {#</div>#}
                    {#</div>#}

                    {#<div class=\"ln_solid\"></div>#}

                    <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                            <a class=\"btn btn-secondary\" type=\"button\"
                               href=\"{{ path('admin_announcement_list') }}\">Cancel
                            </a>
                            {{ form_widget(form.submit, {'attr': {'class': 'btn btn-success' }}) }}
                        </div>
                    </div>
                    {{ form_end(form) }}
                </div>
            </div>
        </div>
    </div>
    </div>



{% endblock %}
{% block javascripts %}
    {{ parent() }}
    {#
            <script>

                \$('#editor-one').wysiwyg();
                \$(document).ready(function() {
                    \$('.btn btn-success').click(function() {

                        document.form.form[message].value = \$('#editor-one').html();
                        alert(document.form.form[message].value);



                        var html = \$('#editor-one').val();
                        // Put this in the hidden field
                        \$(\"#html\").val(html);

                        \$.ajax({
                            url: 'update.php',
                            type: 'POST',
                            data: {
                                content: content
                            }
                        });


                    });
                });

            </script>
        #}
    {#
        <script type='text/javascript'>

            \$('#editor-one').wysiwyg();

            \$('#form_save').click(function(){
                alert(\$('#editor-one').val());
            });

        </script>
    #}
    <script type='text/javascript'>
        \$(function () {
            \$('#editor-one').wysiwyg();

            \$('#form_save').click(function () {
                document.getElementById(\"form_message\").value = \$('#editor-one').html();
            });
        });
    </script>
    {#
            // Another way to do :::
            <script type=\"text/javascript\">

                // Below Function Executes On Form Submit
                function copyMessageOnJS() {

                    \$('#editor-one').wysiwyg();

                    // alert('Start');
                    // alert('Sungsoo Ahn');
                    // var jsValue = \$('#editor-one').html();
                    // alert(jsValue);

                    document.getElementById(\"form_message\").value = \$('#editor-one').html();

                    // alert('By-');

                    return true;   // Returns Value
                }
            </script>
    #}
{% endblock %}
", "role/admin/announcement/add.html.twig", "/code/templates/role/admin/announcement/add.html.twig");
    }
}
