<?php

/* role/admin/user/list.html.twig */
class __TwigTemplate_1c8629e3969a4af122c8ff0e436a548b4c0f0fecd42214a51db34e5201527464 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"page-title\">
                        <div class=\"title_left\">
                            <h3>Users</h3>
                        </div>
                    </div>

                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list");
        echo "\">All Users
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list", ["role" => "admin"]);
        echo "\">Admin
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list", ["role" => "instructor"]);
        echo "\">Instructor
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list", ["role" => "mentor"]);
        echo "\">Mentor
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>

                                    </ul>


                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list", ["role" => "student"]);
        echo "\">Student
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-right panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-primary\"
                                               href=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_create");
        echo "\">New User
                                            </a>
                                        </li>
                                        <li><a class=\"collapse-link\"><i class=\"fas fa-chevron-up\"></i></a>
                                        </li>
                                    </ul>


                                    <div class=\"clearfix\"></div>
                                </div>


                                <div class=\"x_content\">
                                    <p class=\"text-muted font-13 m-b-30\">
                                    </p>
                                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                        <thead>
                                        <tr>
                                            <th>Netid</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Roles</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 102, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
            // line 103
            echo "                                            <tr>
                                                <td>";
            // line 104
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "username", []), "html", null, true);
            echo "</td>
                                                <td>";
            // line 105
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "firstName", []), "html", null, true);
            echo "</td>
                                                <td>";
            // line 106
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "lastName", []), "html", null, true);
            echo "</td>

                                                <td>
                                                    ";
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["u"], "getRoles", [], "method"));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 110
                echo "                                                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "name", []), "html", null, true);
                echo "
                                                        ";
                // line 111
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 112
                    echo "                                                            ";
                    echo " || ";
                    echo "
                                                        ";
                }
                // line 114
                echo "                                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</td>
                                                <td>
                                                    <a class=\"btn btn-success\"
                                                       href=\"";
            // line 117
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", [])]), "html", null, true);
            echo "\">
                                                        Edit
                                                    </a>
                                                </td>
                                            </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 133
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 134
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 134,  273 => 133,  254 => 123,  242 => 117,  224 => 114,  218 => 112,  216 => 111,  211 => 110,  194 => 109,  188 => 106,  184 => 105,  180 => 104,  177 => 103,  173 => 102,  142 => 74,  129 => 64,  114 => 52,  101 => 42,  88 => 32,  75 => 22,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"page-title\">
                        <div class=\"title_left\">
                            <h3>Users</h3>
                        </div>
                    </div>

                    <div class=\"clearfix\"></div>

                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <div class=\"x_panel\">
                                <div class=\"x_title\">
                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"{{ path('admin_user_list') }}\">All Users
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"{{ path('admin_user_list', {\"role\":\"admin\"}) }}\">Admin
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"{{ path('admin_user_list', {\"role\":\"instructor\"}) }}\">Instructor
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"{{ path('admin_user_list', {\"role\":\"mentor\"}) }}\">Mentor
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>

                                    </ul>


                                    <ul class=\"nav navbar-left panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-dark\"
                                               href=\"{{ path('admin_user_list', {\"role\":\"student\"}) }}\">Student
                                            </a>
                                        </li>
                                        <li><a><i></i></a>
                                        </li>
                                    </ul>

                                    <ul class=\"nav navbar-right panel_toolbox\">
                                        <li>
                                            <a class=\"btn btn-primary\"
                                               href=\"{{ path('admin_user_create') }}\">New User
                                            </a>
                                        </li>
                                        <li><a class=\"collapse-link\"><i class=\"fas fa-chevron-up\"></i></a>
                                        </li>
                                    </ul>


                                    <div class=\"clearfix\"></div>
                                </div>


                                <div class=\"x_content\">
                                    <p class=\"text-muted font-13 m-b-30\">
                                    </p>
                                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                        <thead>
                                        <tr>
                                            <th>Netid</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Roles</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        {% for u in users %}
                                            <tr>
                                                <td>{{ u.username }}</td>
                                                <td>{{ u.firstName }}</td>
                                                <td>{{ u.lastName }}</td>

                                                <td>
                                                    {% for r in u.getRoles() %}
                                                        {{ r.name }}
                                                        {% if not loop.last %}
                                                            {{ ' || ' }}
                                                        {% endif %}
                                                    {% endfor %}</td>
                                                <td>
                                                    <a class=\"btn btn-success\"
                                                       href=\"{{ path('admin_user_edit', {\"id\": u.id}) }}\">
                                                        Edit
                                                    </a>
                                                </td>
                                            </tr>
                                        {% endfor %}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var table = \$('#datatable').DataTable({
                searching: true,
                ordering: true,
                paging: true,
            });
        });
    </script>
{% endblock %}
     
     
", "role/admin/user/list.html.twig", "/code/templates/role/admin/user/list.html.twig");
    }
}
