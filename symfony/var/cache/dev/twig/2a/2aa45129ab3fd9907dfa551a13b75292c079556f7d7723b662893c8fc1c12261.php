<?php

/* role/student/session/register/timeslots.html.twig */
class __TwigTemplate_5a95c56c9a5f32919c3fc93a582cfc46c2dd293df467c110e6257ff8ef914736 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/student/session/register/timeslots.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/register/timeslots.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/register/timeslots.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "        <h2 class=\"content_title\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 3, $this->source); })()), "topic", []), "html", null, true);
        echo "</h2>
    <dl class=\"dl-horizontal\">
        <dt>Description</dt>
        <dd>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 6, $this->source); })()), "description", []), "html", null, true);
        echo "</dd>
        <dt>Instructions</dt>
        <dd>";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 8, $this->source); })()), "studentInstructions", []), "html", null, true);
        echo "</dd>
    </dl>
    <table id=\"table\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <td>Date</td>
            <td>Time</td>
            <td>Room</td>
            <td>Seats Remaining</td>
            <td>Register</td>
        </tr>
        </thead>
        ";
        // line 20
        $context["registered_for_session"] = twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 20, $this->source); })()), "isRegistered", [0 => twig_get_attribute($this->env, $this->source, (isset($context["user_service"]) || array_key_exists("user_service", $context) ? $context["user_service"] : (function () { throw new Twig_Error_Runtime('Variable "user_service" does not exist.', 20, $this->source); })()), "user", [])], "method");
        // line 21
        echo "        ";
        $context["attended_session"] = twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 21, $this->source); })()), "hasAttended", [0 => twig_get_attribute($this->env, $this->source, (isset($context["user_service"]) || array_key_exists("user_service", $context) ? $context["user_service"] : (function () { throw new Twig_Error_Runtime('Variable "user_service" does not exist.', 21, $this->source); })()), "user", [])], "method");
        // line 22
        echo "        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["timeslots"]) || array_key_exists("timeslots", $context) ? $context["timeslots"] : (function () { throw new Twig_Error_Runtime('Variable "timeslots" does not exist.', 23, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
            // line 24
            echo "            <tr>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "m/d/Y"), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "g:i A"), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "location", []), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "remainingSeats", []), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "capacity", []), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 30
            if ((isset($context["registered_for_session"]) || array_key_exists("registered_for_session", $context) ? $context["registered_for_session"] : (function () { throw new Twig_Error_Runtime('Variable "registered_for_session" does not exist.', 30, $this->source); })())) {
                // line 31
                echo "                        ";
                if (twig_get_attribute($this->env, $this->source, $context["timeslot"], "isRegistered", [0 => twig_get_attribute($this->env, $this->source, (isset($context["user_service"]) || array_key_exists("user_service", $context) ? $context["user_service"] : (function () { throw new Twig_Error_Runtime('Variable "user_service" does not exist.', 31, $this->source); })()), "user", [])], "method")) {
                    // line 32
                    echo "                            ";
                    if ((twig_date_format_filter($this->env, "now", "Y/m/d H:i") > twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "Y/m/d H:i"))) {
                        // line 33
                        echo "                                <div class=\"btn btn-danger\">Registration Time Expired</div>
                            ";
                    } else {
                        // line 35
                        echo "                                <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_register_form", ["tid" => twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", [])]), "html", null, true);
                        echo "\">
                                    <div class=\"btn btn-warning\">Unregister</div>
                                </a>
                            ";
                    }
                    // line 39
                    echo "                        ";
                } else {
                    // line 40
                    echo "                            <div class=\"btn btn-default\">Registered for Other</div>
                        ";
                }
                // line 42
                echo "                    ";
            } elseif ((isset($context["attended_session"]) || array_key_exists("attended_session", $context) ? $context["attended_session"] : (function () { throw new Twig_Error_Runtime('Variable "attended_session" does not exist.', 42, $this->source); })())) {
                // line 43
                echo "                        ";
                if (twig_get_attribute($this->env, $this->source, $context["timeslot"], "hasAttended", [0 => twig_get_attribute($this->env, $this->source, (isset($context["user_service"]) || array_key_exists("user_service", $context) ? $context["user_service"] : (function () { throw new Twig_Error_Runtime('Variable "user_service" does not exist.', 43, $this->source); })()), "user", [])], "method")) {
                    // line 44
                    echo "                            <div class=\"btn btn-default\">Already Attended</div>
                        ";
                } else {
                    // line 46
                    echo "                            <div class=\"btn btn-default\">Attended Other</div>
                        ";
                }
                // line 48
                echo "                    ";
            } else {
                // line 49
                echo "                        ";
                if ((twig_date_format_filter($this->env, "now", "Y/m/d H:i") > twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "Y/m/d H:i"))) {
                    // line 50
                    echo "                            <div class=\"btn btn-default\">Registration Time Expired</div>
                        ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 51
$context["timeslot"], "remainingSeats", []) <= 0)) {
                    // line 52
                    echo "                            <div class=\"btn btn-default\">No Seats Remaining</div>
                        ";
                } else {
                    // line 54
                    echo "                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_register_form", ["tid" => twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", [])]), "html", null, true);
                    echo "\">
                                <div class=\"btn btn-success\">Register</div>
                            </a>
                        ";
                }
                // line 58
                echo "                    ";
            }
            // line 59
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 65
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 66
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            \$('#table').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/student/session/register/timeslots.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 66,  202 => 65,  190 => 62,  182 => 59,  179 => 58,  171 => 54,  167 => 52,  165 => 51,  162 => 50,  159 => 49,  156 => 48,  152 => 46,  148 => 44,  145 => 43,  142 => 42,  138 => 40,  135 => 39,  127 => 35,  123 => 33,  120 => 32,  117 => 31,  115 => 30,  108 => 28,  104 => 27,  100 => 26,  96 => 25,  93 => 24,  89 => 23,  86 => 22,  83 => 21,  81 => 20,  66 => 8,  61 => 6,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
        <h2 class=\"content_title\">{{ session.topic }}</h2>
    <dl class=\"dl-horizontal\">
        <dt>Description</dt>
        <dd>{{ session.description }}</dd>
        <dt>Instructions</dt>
        <dd>{{ session.studentInstructions }}</dd>
    </dl>
    <table id=\"table\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <td>Date</td>
            <td>Time</td>
            <td>Room</td>
            <td>Seats Remaining</td>
            <td>Register</td>
        </tr>
        </thead>
        {% set registered_for_session = session.isRegistered(user_service.user) %}
        {% set attended_session = session.hasAttended(user_service.user) %}
        <tbody>
        {% for timeslot in timeslots %}
            <tr>
                <td>{{ timeslot.startTime|date('m/d/Y') }}</td>
                <td>{{ timeslot.startTime|date('g:i A') }}</td>
                <td>{{ timeslot.location }}</td>
                <td>{{ timeslot.remainingSeats }}/{{ timeslot.capacity }}</td>
                <td>
                    {% if registered_for_session %}
                        {% if timeslot.isRegistered(user_service.user) %}
                            {% if \"now\"|date('Y/m/d H:i') > timeslot.startTime|date('Y/m/d H:i') %}
                                <div class=\"btn btn-danger\">Registration Time Expired</div>
                            {% else %}
                                <a href=\"{{ path('session_register_form', {'tid': timeslot.id}) }}\">
                                    <div class=\"btn btn-warning\">Unregister</div>
                                </a>
                            {% endif %}
                        {% else %}
                            <div class=\"btn btn-default\">Registered for Other</div>
                        {% endif %}
                    {% elseif attended_session %}
                        {% if timeslot.hasAttended(user_service.user) %}
                            <div class=\"btn btn-default\">Already Attended</div>
                        {% else %}
                            <div class=\"btn btn-default\">Attended Other</div>
                        {% endif %}
                    {% else %}
                        {% if \"now\"|date('Y/m/d H:i') > timeslot.startTime|date('Y/m/d H:i') %}
                            <div class=\"btn btn-default\">Registration Time Expired</div>
                        {% elseif timeslot.remainingSeats <= 0 %}
                            <div class=\"btn btn-default\">No Seats Remaining</div>
                        {% else %}
                            <a href=\"{{ path('session_register_form', {'tid': timeslot.id}) }}\">
                                <div class=\"btn btn-success\">Register</div>
                            </a>
                        {% endif %}
                    {% endif %}
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            \$('#table').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
        });
    </script>
{% endblock %}", "role/student/session/register/timeslots.html.twig", "/code/templates/role/student/session/register/timeslots.html.twig");
    }
}
