<?php

/* role/mentor/schedule/absence_market.html.twig */
class __TwigTemplate_e9fbe16cab0934430e156cb92ece694ccd0956f1bb3b2a4401eeeac1392f3db4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/schedule/absence_market.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/absence_market.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/schedule/absence_market.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    <h2>My Absences
        <a class=\"pull-right btn btn-success\" href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("absence_create");
        echo "\">
            Submit Absence
        </a>
    </h2>
    <table id=\"my\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Date</th>
            <th>Shift</th>
            <th>Reason</th>
            <th>Substitute</th>
            <th>Update</th>
            <th>Cancel</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["your_absences"]) || array_key_exists("your_absences", $context) ? $context["your_absences"] : (function () { throw new Twig_Error_Runtime('Variable "your_absences" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["absence"]) {
            // line 22
            echo "        <tr id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "id", []), "html", null, true);
            echo "\">
            <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "date", []), "m/d/Y"), "html", null, true);
            echo "</td>
            <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "shift", []), "startTime", []), "g:i A"), "html", null, true);
            echo "</td>
            <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "reason", []), "html", null, true);
            echo "</td>
            <td>
                ";
            // line 27
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "substitute", [], "any", false, true), "mentor", [], "any", true, true)) {
                // line 28
                echo "                    ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "substitute", []), "mentor", []), "html", null, true);
                echo "
                    ";
                // line 29
                $context["sub"] = true;
                // line 30
                echo "                ";
            } else {
                // line 31
                echo "                    ";
                $context["sub"] = false;
                // line 32
                echo "                ";
            }
            // line 33
            echo "            </td>

            ";
            // line 35
            $context["f_date"] = (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "date", []), "Y-m-d") > twig_date_format_filter($this->env, "now", "Y-m-d"));
            // line 36
            echo "            ";
            $context["c_date"] = (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "date", []), "Y-m-d") == twig_date_format_filter($this->env, "now", "Y-m-d"));
            // line 37
            echo "            ";
            $context["f_time"] = (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "shift", []), "startTime", []), "H:i") > twig_date_format_filter($this->env, "now", "H:i"));
            // line 38
            echo "            ";
            if (((isset($context["f_date"]) || array_key_exists("f_date", $context) ? $context["f_date"] : (function () { throw new Twig_Error_Runtime('Variable "f_date" does not exist.', 38, $this->source); })()) || ((isset($context["c_date"]) || array_key_exists("c_date", $context) ? $context["c_date"] : (function () { throw new Twig_Error_Runtime('Variable "c_date" does not exist.', 38, $this->source); })()) && (isset($context["f_time"]) || array_key_exists("f_time", $context) ? $context["f_time"] : (function () { throw new Twig_Error_Runtime('Variable "f_time" does not exist.', 38, $this->source); })())))) {
                // line 39
                echo "                ";
                $context["future"] = true;
                // line 40
                echo "            ";
            } else {
                // line 41
                echo "                ";
                $context["future"] = false;
                // line 42
                echo "            ";
            }
            // line 43
            echo "            <td>
                ";
            // line 44
            if (( !(isset($context["sub"]) || array_key_exists("sub", $context) ? $context["sub"] : (function () { throw new Twig_Error_Runtime('Variable "sub" does not exist.', 44, $this->source); })()) && (isset($context["future"]) || array_key_exists("future", $context) ? $context["future"] : (function () { throw new Twig_Error_Runtime('Variable "future" does not exist.', 44, $this->source); })()))) {
                // line 45
                echo "                    <a class=\"btn btn-success\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("absence_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["absence"], "id", [])]), "html", null, true);
                echo "\">
                        Update
                    </a>
                ";
            } else {
                // line 49
                echo "                    ";
                // line 50
                echo "                ";
            }
            // line 51
            echo "            </td>
            <td>
                ";
            // line 53
            if (( !(isset($context["sub"]) || array_key_exists("sub", $context) ? $context["sub"] : (function () { throw new Twig_Error_Runtime('Variable "sub" does not exist.', 53, $this->source); })()) && (isset($context["future"]) || array_key_exists("future", $context) ? $context["future"] : (function () { throw new Twig_Error_Runtime('Variable "future" does not exist.', 53, $this->source); })()))) {
                // line 54
                echo "                    <button id=\"cancel\" class=\"btn btn-warning\" data-absence=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "id", []), "html", null, true);
                echo "\">
                        Cancel
                    </button>
                ";
            } else {
                // line 58
                echo "                    ";
                // line 59
                echo "                ";
            }
            // line 60
            echo "            </td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['absence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "        </tbody>
    </table>
    <h2>Absences</h2>
    <div class=\"alert alert-danger\">Reminder: Make sure you do NOT work more than 19 hours in a week!</div>
    <table id=\"other\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Mentor</th>
            <th>Date</th>
            <th>Shift</th>
            <th>Session</th>
            <th>Substitute Mentor</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["absences"]) || array_key_exists("absences", $context) ? $context["absences"] : (function () { throw new Twig_Error_Runtime('Variable "absences" does not exist.', 78, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["absence"]) {
            // line 79
            echo "        <tr>
            <td>";
            // line 80
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "mentor", []), "html", null, true);
            echo "</td>
            <td>";
            // line 81
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "date", []), "m/d/Y"), "html", null, true);
            echo "</td>
            <td>";
            // line 82
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "scheduledShift", []), "shift", []), "startTime", []), "g:i A"), "html", null, true);
            echo "</td>
            <td>
                ";
            // line 84
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", [], "any", false, true), "session", [], "any", false, true), "session", [], "any", true, true)) {
                // line 85
                echo "                    ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "assignment", []), "session", []), "session", []), "topic", []), "html", null, true);
                echo "
                ";
            }
            // line 87
            echo "            </td>
            <td id=\"";
            // line 88
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "id", []), "html", null, true);
            echo "\">
                ";
            // line 89
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "substitute", [], "any", false, true), "mentor", [], "any", true, true)) {
                // line 90
                echo "                    ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["absence"], "substitute", []), "mentor", []), "html", null, true);
                echo "
                ";
            } else {
                // line 92
                echo "                    <button id=\"claim-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "id", []), "html", null, true);
                echo "\" class=\"btn btn-success\"
                            data-absence=\"";
                // line 93
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["absence"], "id", []), "html", null, true);
                echo "\">
                        Claim Shift
                    </button>
                ";
            }
            // line 97
            echo "            </td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['absence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 103
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 104
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"application/javascript\">
        \$(function () {
            var my = \$('#my').DataTable({
                'paging': false,
                'ordering': true,
                'searching': true
            });

            var other = \$('#other').DataTable({
                'paging': false,
                'ordering': true,
                'searching': true
            });

            \$('#cancel').on('click', function () {
                var absence = \$(this).data('absence');

                \$.ajax({
                    type: 'POST',
                    url: '";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("absence_cancel_ajax");
        echo "',
                    data: {'absence': absence}
                }).done(function (data) {
                    // success
                    my.row(\$('#' + absence)).remove().draw();
                }).fail(function (data) {
                    // error
                    ";
        // line 132
        echo "                });
            });

            \$('[id^=\"claim\"]').on('click', function () {
                var absence = \$(this).data('absence');

                console.log('hello');

                \$.ajax({
                    type: 'POST',
                    url: '";
        // line 142
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("absence_market_ajax");
        echo "',
                    data: {'absence': absence}
                }).done(function (data) {
                    // success
                    \$('#' + absence).text('";
        // line 146
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 146, $this->source); })()), "user", []), "html", null, true);
        echo "');
                }).fail(function (data) {
                    // error
                    ";
        // line 150
        echo "                });
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/schedule/absence_market.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 150,  345 => 146,  338 => 142,  326 => 132,  316 => 124,  292 => 104,  283 => 103,  271 => 100,  263 => 97,  256 => 93,  251 => 92,  245 => 90,  243 => 89,  239 => 88,  236 => 87,  230 => 85,  228 => 84,  223 => 82,  219 => 81,  215 => 80,  212 => 79,  208 => 78,  191 => 63,  183 => 60,  180 => 59,  178 => 58,  170 => 54,  168 => 53,  164 => 51,  161 => 50,  159 => 49,  151 => 45,  149 => 44,  146 => 43,  143 => 42,  140 => 41,  137 => 40,  134 => 39,  131 => 38,  128 => 37,  125 => 36,  123 => 35,  119 => 33,  116 => 32,  113 => 31,  110 => 30,  108 => 29,  103 => 28,  101 => 27,  96 => 25,  92 => 24,  88 => 23,  83 => 22,  79 => 21,  60 => 5,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    <h2>My Absences
        <a class=\"pull-right btn btn-success\" href=\"{{ path('absence_create') }}\">
            Submit Absence
        </a>
    </h2>
    <table id=\"my\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Date</th>
            <th>Shift</th>
            <th>Reason</th>
            <th>Substitute</th>
            <th>Update</th>
            <th>Cancel</th>
        </tr>
        </thead>
        <tbody>
        {% for absence in your_absences %}
        <tr id=\"{{ absence.id }}\">
            <td>{{ absence.assignment.scheduledShift.date|date('m/d/Y') }}</td>
            <td>{{ absence.assignment.scheduledShift.shift.startTime|date('g:i A') }}</td>
            <td>{{ absence.reason }}</td>
            <td>
                {% if absence.substitute.mentor is defined %}
                    {{ absence.substitute.mentor }}
                    {% set sub = true %}
                {% else %}
                    {% set sub = false %}
                {% endif %}
            </td>

            {% set f_date = absence.assignment.scheduledShift.date|date('Y-m-d') > \"now\"|date('Y-m-d') %}
            {% set c_date = absence.assignment.scheduledShift.date|date('Y-m-d') == \"now\"|date('Y-m-d') %}
            {% set f_time = absence.assignment.scheduledShift.shift.startTime|date('H:i') > \"now\"|date('H:i') %}
            {% if f_date or ( c_date and f_time ) %}
                {% set future = true %}
            {% else %}
                {% set future = false %}
            {% endif %}
            <td>
                {% if not sub and future %}
                    <a class=\"btn btn-success\" href=\"{{ path('absence_edit', {'id': absence.id}) }}\">
                        Update
                    </a>
                {% else %}
                    {#<div class=\"btn btn-default\">Update</div>#}
                {% endif %}
            </td>
            <td>
                {% if not sub and future %}
                    <button id=\"cancel\" class=\"btn btn-warning\" data-absence=\"{{ absence.id }}\">
                        Cancel
                    </button>
                {% else %}
                    {#<div class=\"button grey_button\">Cancel</div>#}
                {% endif %}
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
    <h2>Absences</h2>
    <div class=\"alert alert-danger\">Reminder: Make sure you do NOT work more than 19 hours in a week!</div>
    <table id=\"other\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Mentor</th>
            <th>Date</th>
            <th>Shift</th>
            <th>Session</th>
            <th>Substitute Mentor</th>
        </tr>
        </thead>
        <tbody>
        {% for absence in absences %}
        <tr>
            <td>{{ absence.assignment.mentor }}</td>
            <td>{{ absence.assignment.scheduledShift.date|date('m/d/Y') }}</td>
            <td>{{ absence.assignment.scheduledShift.shift.startTime|date('g:i A') }}</td>
            <td>
                {% if absence.assignment.session.session is defined %}
                    {{ absence.assignment.session.session.topic }}
                {% endif %}
            </td>
            <td id=\"{{ absence.id }}\">
                {% if absence.substitute.mentor is defined %}
                    {{ absence.substitute.mentor }}
                {% else %}
                    <button id=\"claim-{{ absence.id }}\" class=\"btn btn-success\"
                            data-absence=\"{{ absence.id }}\">
                        Claim Shift
                    </button>
                {% endif %}
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script type=\"application/javascript\">
        \$(function () {
            var my = \$('#my').DataTable({
                'paging': false,
                'ordering': true,
                'searching': true
            });

            var other = \$('#other').DataTable({
                'paging': false,
                'ordering': true,
                'searching': true
            });

            \$('#cancel').on('click', function () {
                var absence = \$(this).data('absence');

                \$.ajax({
                    type: 'POST',
                    url: '{{ path('absence_cancel_ajax') }}',
                    data: {'absence': absence}
                }).done(function (data) {
                    // success
                    my.row(\$('#' + absence)).remove().draw();
                }).fail(function (data) {
                    // error
                    {# TODO implement fail #}
                });
            });

            \$('[id^=\"claim\"]').on('click', function () {
                var absence = \$(this).data('absence');

                console.log('hello');

                \$.ajax({
                    type: 'POST',
                    url: '{{ path('absence_market_ajax') }}',
                    data: {'absence': absence}
                }).done(function (data) {
                    // success
                    \$('#' + absence).text('{{ app.user }}');
                }).fail(function (data) {
                    // error
                    {# TODO implement fail #}
                });
            });
        });
    </script>
{% endblock %}", "role/mentor/schedule/absence_market.html.twig", "/code/templates/role/mentor/schedule/absence_market.html.twig");
    }
}
