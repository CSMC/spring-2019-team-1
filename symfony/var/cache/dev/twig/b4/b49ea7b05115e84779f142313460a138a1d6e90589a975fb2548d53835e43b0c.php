<?php

/* shared/display.html.twig */
class __TwigTemplate_08e621eb59e8730844e75758911a548263c25c35a2ccce3d717ca157a63fcbc7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/display.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/display.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <title>CSMC Display</title>

    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css\">
 
    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/display.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
</head>
<body>
    <div id=\"mentor_image\" data-path=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/user.png"), "html", null, true);
        echo "\"></div>

    <div id=\"general-content\" class=\"panel panel-default\">
        <div class=\"middle-header\">
            <h2><b>We will be with you shortly</b></h2>
            <h3 id=\"timer\"></h3><br>
        </div>
        <div class=\"clock\"></div>

        <div class=\"container\">
            <table id=\"wait-list\" class=\"table table-striped table-bordered\">
                <thead class=\"table-header\">
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Activity</th>
                    <th>Time In</th>
                </tr>
                </thead>
                <tbody id=\"wait-list-body\"></tbody>
            </table>
        </div>

        <div class=\"pagination-container\">
            <ul id=\"wait-list-pagination\" class=\"pagination\"></ul>
        </div>
    </div>

    <div id=\"mentors-container-header\" class=\"panel panel-default\">
        <p><b>Mentors on duty</b></p>
    </div>

    <div id=\"mentors-container\">
        <div id=\"mentors-content\" class=\"panel panel-default\"></div>
    </div>

    <div id=\"sessions-container\" class=\"panel panel-default\">

        <div id=\"sessions-title\" class=\"ui-widget-header\">
            <p><b>Sessions</b></p>
        </div>

        <div id=\"sessions-content\"></div>

        <div id=\"quiz-bar\" class=\"panel panel-default\">
            <!--
            <div style=\"width: 100%; height: 50%; text-align: center;\">
                <p style=\"font-size: 25px; margin: 0px;\"><b>3305 Morales Quiz 6</b></p>
            </div>
            <div style=\"width: 100%; height: 40%;\">
                <p style=\"font-size: 20px; margin: 0px; float: left;\"><b>Date:</b> May 22 - May 26</p>
                <p style=\"font-size: 20px; margin: 0px; float: right;\"><b>Location:</b> ECSS 4.415</p>
            </div>-->
        </div>

    </div>
    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js\"></script>
    <script type=\"text/javascript\">

        var buildQuizzes = function (response) {
            \$(\"#quiz-bar\").empty();

            for (i in response) {
                var quiz = response[i];

                var topicDiv = \$(\"<div style='width: 100%; height: 50%; text-align: center;'></div>\");
                topicDiv.append(\$(\"<p style='font-size: 25px; margin: 0px;'><b>\"+quiz.topic+\"</b></p>\"));

                var infoDiv = \$(\"<div style='width: 100%; height: 40%;'>\");
                infoDiv.append(\$(\"<p style='font-size: 20px; margin: 0px; float: left;'><b>Date:</b> \"+quiz.date+\"</p>\"));
                infoDiv.append(\$(\"<p style='font-size: 20px; margin: 0px; float: right;'><b>Location:</b> \"+quiz.location+\"</p>\"));

                \$(\"#quiz-bar\").append(topicDiv);
                \$(\"#quiz-bar\").append(infoDiv);
            }
        }

        var buildMentors = function (response) {

            \$(\"#mentors-content\").empty();

            if (response.length == 0) {
                \$(\"#mentors-content\").append(\$(\"<h3 style='text-align: center;'>Are you a <b>mentor</b>?</h3><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h4 style='text-align: center;'>Please <b>sign-in</b> if you do not see yourself on this list.</h4><br><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h3 style='text-align: center;'>Are you a <b>student</b>?</h3><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h4 style='text-align: center;'>No one here to help you? Please contact: <b>csmc@utdallas.edu</b></h4><br>\"));
            }

            var adaptContainer = false;

            if (response.length > 4) {
                adaptContainer = true;

                \$.keyframe.define({
                    name: 'slidementor',
                    from: {
                        'transform': 'translate3d(0,0,0)'
                    },
                    to: {
                        'transform': 'translate3d(0,-' + (136.04 * response.length) + 'px,0)' /* if over 4 mentors, transform -136.04(mentor-size + margin-top) for each mentor */
                    }
                });
            }

            for (i in response) {
                var mentor = response[i];

                var mentorName = mentor.name;

                if (mentorName.length > 17) {
                    mentorName = mentor.name.substring(0, 16).trim() + '.';
                }

                var mentorImageDiv = \$(\"<div class='mentor-image-div'></div>\");

                if(mentor.isShiftLeader) {
                    mentorImageDiv.append(\$(\"<p style='position: absolute; font-size: 17.5px; font-weight: bold; background-color: goldenrod;'>SHIFT LEADER</p>\"));
                }

                mentorImageDiv.append(\$(\"<span class='helper'></span>\" +
                    \"<img id='image_\" + mentor.username + \"' alt='\" + mentorName + \"' src='/profile/\" + mentor.username + \"/image\" + \"' class='mentor-image' onerror='this.onerror=null; this.src=\\\"\" + \$('#mentor_image').data('path') + \"\\\"'>\"));



                var mentorNameContainer = \$(\"<h1>\" + mentorName + \"</h1>\");
                var badgesContainer = \$(\"<h3></h3>\");

                mentor.specialties.forEach(function (specialty) {
                    badgesContainer.append(\"<span class='label label-default label-badge' style='background-color: \" + specialty.topic.color + \"!important'>\" + specialty.topic.abbreviation + \"</span>\");
                });

                var mentorDiv = \$(\"<div class='ui-widget-content ui-corner-tr mentor'></div>\");

                mentorDiv.append(mentorImageDiv);
                mentorDiv.append(mentorNameContainer);
                mentorDiv.append(badgesContainer);

                if (adaptContainer)
                    mentorDiv.css('animation', 'slidementor ' + (5 * response.length) + 's linear infinite');

                \$(\"#mentors-content\").append(mentorDiv);
            }

            if ((\$(\"#mentors-content div.mentor\").length) > 4) {
                var mentorAux = \$(\"#mentors-content div.mentor\")[0].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[1].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[2].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[3].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
            }

            \$(\"#mentors-content\").append(\"<br>\");

            // mentorPagination();
        };

        var buildSessions = function (response) {
            var cardsPerPage = 4;
            var numberOfPages = Math.ceil(response.length / cardsPerPage);

            var sessionContents = [];
            for (var pageNumber = 0; pageNumber < numberOfPages; pageNumber++) {
                sessionContents[pageNumber] = new Array();
            }

            var currentPage = 0;
            var currentCard = 0;
            for (i in response) {
                var session = response[i];

                currentPage = Math.ceil(++currentCard / cardsPerPage);

                var sessionTitle = \$(\"<div class='session-header'><p>\" + session.topic + \"</p></div>\");
                var sessionSubtitle = \$(\"<div class='session-sub-header'></div>\");
                var sessionTime = \$(\"<div class='session-field'></div>\");

                var scheduledDate = new Date(session.full_date);
                var currentDate = new Date();

                //Check card's subtitle
                if (currentDate > scheduledDate) {
                    //Started sessions (or at least they were supposed to be started
                    sessionSubtitle.append(\$(\"<p><i class='fa fa-circle' style='font-size:10px;color: red;vertical-align: middle;'></i> Today - Started</p>\"));
                    sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                } else {
                    var twoHoursRange = new Date(scheduledDate);
                    twoHoursRange.setHours(twoHoursRange.getHours() - 2);

                    if (currentDate > twoHoursRange) {
                        //Starts Soon! sessions
                        sessionSubtitle.append(\$(\"<p> Today - Starts Soon!</p>\"));
                        sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                    } else {
                        if (currentDate.getDay() == scheduledDate.getDay()) {
                            //later today
                            sessionSubtitle.append(\$(\"<p>Later Today</p>\"));
                            sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                        } else {
                            //upcoming
                            sessionSubtitle.append(\$(\"<p>\" + session.weekday + \"</p>\"));
                            sessionTime.append(\"<p><b>Time:</b> \" + session.time + \" (\" + session.date + \")</p>\");
                        }
                    }
                }

                var sessionLocation = \$(\"<div class='session-field'><p><b>Location:</b> \" + session.room + \"</p></div>\");
                var sessionAvailability = \$(\"<div class='session-field'><p><b>Availability:</b> \" + session.capacity + \"</p></div>\");

                //Build mentors string
                var mentors = '';
                for(var j=0; j < session.mentors.length; j++) {
                    mentors = mentors + session.mentors[j] + ', ';
                }
                mentors = mentors.substring(0, mentors.length-2);

                var sessionMentors = \$(\"<div class='session-field'><p><b>Mentors: </b>\"+mentors+\"</p></div>\");


                //Create new session card and add all info
                var sessionDiv = \$(\"<div class='session panel panel-default'></div>\");
                sessionDiv.append(sessionTitle);
                sessionDiv.append(sessionSubtitle);
                sessionDiv.append(sessionTime);
                sessionDiv.append(sessionLocation);
                sessionDiv.append(sessionAvailability);
                sessionDiv.append(sessionMentors);

                sessionContents[(currentPage - 1)].push(sessionDiv);
            }

            var firstSessions = sessionContents[0];
            for (i in firstSessions) {
                var session = firstSessions[i];

                \$(\"#sessions-content\").append(session);
            }

            \$(\"#sessions-title\").empty();
            \$(\"#sessions-title\").append(\"<p><b>Sessions (Page 1 of \" + numberOfPages + \")</b></p>\")

            var showPage = 1;
            setInterval(function () {
                if (showPage == numberOfPages) {
                    showPage = 1;
                } else {
                    showPage++;
                }

                \$(\"#sessions-content\").hide(\"fade\", {}, 800, function () {
                    \$(\"#sessions-content\").empty();

                    for (i in sessionContents[(showPage - 1)]) {
                        var session = sessionContents[(showPage - 1)][i];
                        \$(\"#sessions-content\").append(session);
                    }

                    \$(\"#sessions-title\").empty();
                    \$(\"#sessions-title\").append(\"<p><b>Sessions (Page \" + showPage + \" of \" + numberOfPages + \")</b></p>\")

                    \$(\"#sessions-content\").show(\"fade\", {}, 800);
                });
            }, 8000);

        };

        var updatePagination = function (currentPage) {
            var number = 1;
            \$(\"#wait-list-pagination li\").each(function (id, li) {
                var pageItem = \$(li);

                if (number == currentPage)
                    pageItem.addClass(\"active\");
                else
                    pageItem.removeClass(\"active\");

                number++;
            });
        };

        var createStudentTable = function (currentPage, studentsPerPage, students) {
            var start = (currentPage * studentsPerPage) - studentsPerPage;

            \$(\"#wait-list-body\").empty();
            for (var i = start; i < start + studentsPerPage; i++) {
                if (i < students.length) {
                    \$('#wait-list-body').append('<tr>'
                        + '<td>' + students[i]['number'] + '</td>'
                        + '<td>' + students[i]['name'] + '</td>'
                        + '<td>' + students[i]['course'] + '</td>'
                        + '<td>' + students[i]['activity'] + '</td>'
                        + '<td>' + students[i]['time'] + '</td>'
                        + '</tr>'
                    );
                } else {
                    \$('#wait-list-body').append('<tr style=\"\"><td> </td><td> </td><td> </td><td> </td><td> </td></tr>');
                }
            }


        };

        if (!String.format) {
            String.format = function (format) {
                var args = Array.prototype.slice.call(arguments, 1);
                return format.replace(/{(\\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
    <script type=\"text/javascript\">
        \$(function () {
            var refreshTime = 1000 * 60 * 2.5;

            setTimeout(function () {
                location.reload(true);
            }, refreshTime);
            var refreshMessage = \"Refreshing in {0}:{1}\";
            \$('#timer').append(String.format(refreshMessage, Math.floor(refreshTime / 60000), (\"00\" + Math.ceil((refreshTime / 1000) % 60)).substr(-2, 2)));

            var t = refreshTime;
            setInterval(function () {
                t -= 1000;
                \$('#timer').empty();
                \$('#timer').append(String.format(refreshMessage, Math.floor(t / 60000), (\"00\" + Math.ceil((t / 1000) % 60)).substr(-2, 2)));
            }, 1000);

            var numStudents = ";
        // line 364
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["students"]) || array_key_exists("students", $context) ? $context["students"] : (function () { throw new Twig_Error_Runtime('Variable "students" does not exist.', 364, $this->source); })())), "html", null, true);
        echo ";

            if (numStudents == 0) {
                // put in a message saying no students
                var emptyMessage = \"<h2>No Students Currently Signed In</h2><br><h3>If you signed in and see this message, don't panic, it will refresh in about {0} {1}</h3>\"

                var minutes = Math.ceil(refreshTime / 60000);
                \$(\"#general-content\").empty();
                \$(\"#general-content\").append(String.format(emptyMessage, minutes, \"minutes\"));

                var time = refreshTime;

                setInterval(function () {
                    time = time - 1000;
                    if (time >= 60000) {
                        if (time % 60000 == 0) {
                            var minutes = Math.ceil(time / 60000);
                            \$(\"#general-content\").empty();
                            \$(\"#general-content\").append(String.format(emptyMessage, minutes, \"minutes\"));
                        }
                    } else {
                        var seconds = Math.ceil(time / 1000);
                        \$(\"#general-content\").empty();
                        \$(\"#general-content\").append(String.format(emptyMessage, seconds, \"seconds\"));
                    }


                }, 1000);
            } else {
                var students = [];
                ";
        // line 394
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["students"]) || array_key_exists("students", $context) ? $context["students"] : (function () { throw new Twig_Error_Runtime('Variable "students" does not exist.', 394, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
            // line 395
            echo "                    var index = ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index0", []), "html", null, true);
            echo ";
                    students[index] = {
                        'number': index + 1,
                        'name': '";
            // line 398
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "user", []), "firstName", []), "html", null, true);
            echo "',
                        'course': '";
            // line 399
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "course", []), "number", []), "html", null, true);
            echo "',
                        'activity': '";
            // line 400
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["student"], "activity", []), "name", []), "html", null, true);
            echo "',
                        'time': '";
            // line 401
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["student"], "timeIn", []), "g:i A"), "html", null, true);
            echo "'
                    };
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 404
        echo "
                var studentsPerPage = 6;
                var currentPage = 1;
                var numberOfPages = Math.ceil(numStudents / studentsPerPage);

                if (numberOfPages > 1) {
                    ";
        // line 411
        echo "                    \$(\"#wait-list-pagination\").append(\"<li class=\\\"active\\\"><a href=\\\"#\\\">1</a></li>\");
                    ";
        // line 413
        echo "                    for (var i = 1; i < numberOfPages; i++) {
                        \$(\"#wait-list-pagination\").append(\"<li><a href=\\\"#\\\">\" + (i + 1) + \"</a></li>\");
                    }
                }
                createStudentTable(currentPage, studentsPerPage, students);

                if (numberOfPages > 1) {
                    setInterval(function () {
                        ";
        // line 422
        echo "                        if (currentPage == numberOfPages) {
                            currentPage = 1;
                        } else {
                            currentPage++;
                        }

                        \$(\".container\").hide(\"drop\", {direction: \"left\"}, 2000, function () {
                            \$(\".container\").show(\"drop\", {direction: \"right\"}, 2000);
                            updatePagination(currentPage);
                            createStudentTable(currentPage, studentsPerPage, students);
                        })
                    }, 14000);
                }
            }

            var mentors = [];

            ";
        // line 439
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mentors"]) || array_key_exists("mentors", $context) ? $context["mentors"] : (function () { throw new Twig_Error_Runtime('Variable "mentors" does not exist.', 439, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
            // line 440
            echo "                ";
            $context["i"] = twig_get_attribute($this->env, $this->source, $context["loop"], "index0", []);
            // line 441
            echo "
                mentors[";
            // line 442
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 442, $this->source); })()), "html", null, true);
            echo "] = [];
                mentors[";
            // line 443
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 443, $this->source); })()), "html", null, true);
            echo "]['name'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "preferredName", []), "html", null, true);
            echo "';
                mentors[";
            // line 444
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 444, $this->source); })()), "html", null, true);
            echo "]['username'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "userName", []), "html", null, true);
            echo "';
                mentors[";
            // line 445
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 445, $this->source); })()), "html", null, true);
            echo "]['specialties'] = [];

                ";
            // line 447
            if ((twig_get_attribute($this->env, $this->source, (isset($context["shift_leader"]) || array_key_exists("shift_leader", $context) ? $context["shift_leader"] : (function () { throw new Twig_Error_Runtime('Variable "shift_leader" does not exist.', 447, $this->source); })()), "id", []) == twig_get_attribute($this->env, $this->source, $context["mentor"], "id", []))) {
                // line 448
                echo "                    mentors[";
                echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 448, $this->source); })()), "html", null, true);
                echo "]['isShiftLeader'] = true;
                ";
            } else {
                // line 450
                echo "                    mentors[";
                echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 450, $this->source); })()), "html", null, true);
                echo "]['isShiftLeader'] = false;
                ";
            }
            // line 452
            echo "
                ";
            // line 453
            $context["j"] = 0;
            // line 454
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["mentor"], "profile", []), "specialties", []));
            foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
                // line 455
                echo "                    ";
                if (((twig_get_attribute($this->env, $this->source, $context["specialty"], "rating", []) >= 3) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "showOnCalendar", []))) {
                    // line 456
                    echo "                    mentors[";
                    echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 456, $this->source); })()), "html", null, true);
                    echo "]['specialties'][";
                    echo twig_escape_filter($this->env, (isset($context["j"]) || array_key_exists("j", $context) ? $context["j"] : (function () { throw new Twig_Error_Runtime('Variable "j" does not exist.', 456, $this->source); })()), "html", null, true);
                    echo "] = {
                        'topic': {
                            'name': '";
                    // line 458
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "name", []), "html", null, true);
                    echo "',
                            'color': '";
                    // line 459
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "color", []), "html", null, true);
                    echo "',
                            'abbreviation': '";
                    // line 460
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "subject", []), "abbreviation", []), "html", null, true);
                    echo "'
                        },
                    };
                    ";
                    // line 463
                    $context["j"] = ((isset($context["j"]) || array_key_exists("j", $context) ? $context["j"] : (function () { throw new Twig_Error_Runtime('Variable "j" does not exist.', 463, $this->source); })()) + 1);
                    // line 464
                    echo "                    ";
                }
                // line 465
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 466
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 468
        echo "
            buildMentors(mentors);


            var sessions = [];

            ";
        // line 474
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 474, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
            // line 475
            echo "                ";
            $context["i"] = twig_get_attribute($this->env, $this->source, $context["loop"], "index0", []);
            // line 476
            echo "                sessions[";
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 476, $this->source); })()), "html", null, true);
            echo "] = [];
                sessions[";
            // line 477
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 477, $this->source); })()), "html", null, true);
            echo "]['topic'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "name", []), "html", null, true);
            echo "';
                sessions[";
            // line 478
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 478, $this->source); })()), "html", null, true);
            echo "]['time'] = '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "h:i A"), "html", null, true);
            echo "';
                sessions[";
            // line 479
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 479, $this->source); })()), "html", null, true);
            echo "]['date'] = '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "F j"), "html", null, true);
            echo "';
                sessions[";
            // line 480
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 480, $this->source); })()), "html", null, true);
            echo "]['room'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "location", []), "html", null, true);
            echo "';
                sessions[";
            // line 481
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 481, $this->source); })()), "html", null, true);
            echo "]['capacity'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "capacity", []), "html", null, true);
            echo "';

                sessions[";
            // line 483
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 483, $this->source); })()), "html", null, true);
            echo "]['start_time'] = '";
            echo twig_escape_filter($this->env, (((null === twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []))) ? ("") : (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "H:i A"))), "html", null, true);
            echo "';

                sessions[";
            // line 485
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 485, $this->source); })()), "html", null, true);
            echo "]['full_date'] = '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "F d, Y G:i:s"), "html", null, true);
            echo "';

                sessions[";
            // line 487
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 487, $this->source); })()), "html", null, true);
            echo "]['weekday'] = '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", [], "method"), "l"), "html", null, true);
            echo "';

                sessions[";
            // line 489
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 489, $this->source); })()), "html", null, true);
            echo "]['mentors'] = [];
                ";
            // line 490
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["timeslot"], "getMentors", [], "method"));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
                // line 491
                echo "                    ";
                $context["j"] = twig_get_attribute($this->env, $this->source, $context["loop"], "index0", []);
                // line 492
                echo "                    sessions[";
                echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 492, $this->source); })()), "html", null, true);
                echo "]['mentors'][";
                echo twig_escape_filter($this->env, (isset($context["j"]) || array_key_exists("j", $context) ? $context["j"] : (function () { throw new Twig_Error_Runtime('Variable "j" does not exist.', 492, $this->source); })()), "html", null, true);
                echo "] = '";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "preferredName", []), "html", null, true);
                echo "';
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 494
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 496
        echo "
            buildSessions(sessions);

            var quizzes = [];

            console.log()

            ";
        // line 503
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 503, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
            // line 504
            echo "                ";
            $context["i"] = twig_get_attribute($this->env, $this->source, $context["loop"], "index0", []);
            // line 505
            echo "
                quizzes[";
            // line 506
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 506, $this->source); })()), "html", null, true);
            echo "] = [];
                quizzes[";
            // line 507
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 507, $this->source); })()), "html", null, true);
            echo "]['topic'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
            echo "';
                quizzes[";
            // line 508
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 508, $this->source); })()), "html", null, true);
            echo "]['location'] = '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "location", []), "html", null, true);
            echo "';
                quizzes[";
            // line 509
            echo twig_escape_filter($this->env, (isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new Twig_Error_Runtime('Variable "i" does not exist.', 509, $this->source); })()), "html", null, true);
            echo "]['date'] = '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "startDate", []), "F j"), "html", null, true);
            echo "' + ' - ' + '";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "endDate", []), "F j"), "html", null, true);
            echo "';

                console.log('";
            // line 511
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
            echo "');
                console.log('";
            // line 512
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "location", []), "html", null, true);
            echo "');
                ";
            // line 514
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 516
        echo "
            buildQuizzes(quizzes);


            ";
        // line 521
        echo "            var clock = \$('.clock').FlipClock({
                clockFace: 'TwelveHourClock',
                showSeconds: false
            });
        });
    </script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/display.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  878 => 521,  872 => 516,  857 => 514,  853 => 512,  849 => 511,  840 => 509,  834 => 508,  828 => 507,  824 => 506,  821 => 505,  818 => 504,  801 => 503,  792 => 496,  777 => 494,  756 => 492,  753 => 491,  736 => 490,  732 => 489,  725 => 487,  718 => 485,  711 => 483,  704 => 481,  698 => 480,  692 => 479,  686 => 478,  680 => 477,  675 => 476,  672 => 475,  655 => 474,  647 => 468,  632 => 466,  626 => 465,  623 => 464,  621 => 463,  615 => 460,  611 => 459,  607 => 458,  599 => 456,  596 => 455,  591 => 454,  589 => 453,  586 => 452,  580 => 450,  574 => 448,  572 => 447,  567 => 445,  561 => 444,  555 => 443,  551 => 442,  548 => 441,  545 => 440,  528 => 439,  509 => 422,  499 => 413,  496 => 411,  488 => 404,  471 => 401,  467 => 400,  461 => 399,  457 => 398,  450 => 395,  433 => 394,  400 => 364,  53 => 20,  47 => 17,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"en\">
<head>
    <title>CSMC Display</title>

    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.css\">
 
    <link href=\"{{ asset('build/css/display.css') }}\" rel=\"stylesheet\" />
</head>
<body>
    <div id=\"mentor_image\" data-path=\"{{ asset('build/images/user.png') }}\"></div>

    <div id=\"general-content\" class=\"panel panel-default\">
        <div class=\"middle-header\">
            <h2><b>We will be with you shortly</b></h2>
            <h3 id=\"timer\"></h3><br>
        </div>
        <div class=\"clock\"></div>

        <div class=\"container\">
            <table id=\"wait-list\" class=\"table table-striped table-bordered\">
                <thead class=\"table-header\">
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Activity</th>
                    <th>Time In</th>
                </tr>
                </thead>
                <tbody id=\"wait-list-body\"></tbody>
            </table>
        </div>

        <div class=\"pagination-container\">
            <ul id=\"wait-list-pagination\" class=\"pagination\"></ul>
        </div>
    </div>

    <div id=\"mentors-container-header\" class=\"panel panel-default\">
        <p><b>Mentors on duty</b></p>
    </div>

    <div id=\"mentors-container\">
        <div id=\"mentors-content\" class=\"panel panel-default\"></div>
    </div>

    <div id=\"sessions-container\" class=\"panel panel-default\">

        <div id=\"sessions-title\" class=\"ui-widget-header\">
            <p><b>Sessions</b></p>
        </div>

        <div id=\"sessions-content\"></div>

        <div id=\"quiz-bar\" class=\"panel panel-default\">
            <!--
            <div style=\"width: 100%; height: 50%; text-align: center;\">
                <p style=\"font-size: 25px; margin: 0px;\"><b>3305 Morales Quiz 6</b></p>
            </div>
            <div style=\"width: 100%; height: 40%;\">
                <p style=\"font-size: 20px; margin: 0px; float: left;\"><b>Date:</b> May 22 - May 26</p>
                <p style=\"font-size: 20px; margin: 0px; float: right;\"><b>Location:</b> ECSS 4.415</p>
            </div>-->
        </div>

    </div>
    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquerykeyframes/0.0.9/jquery.keyframes.min.js\"></script>
    <script type=\"text/javascript\">

        var buildQuizzes = function (response) {
            \$(\"#quiz-bar\").empty();

            for (i in response) {
                var quiz = response[i];

                var topicDiv = \$(\"<div style='width: 100%; height: 50%; text-align: center;'></div>\");
                topicDiv.append(\$(\"<p style='font-size: 25px; margin: 0px;'><b>\"+quiz.topic+\"</b></p>\"));

                var infoDiv = \$(\"<div style='width: 100%; height: 40%;'>\");
                infoDiv.append(\$(\"<p style='font-size: 20px; margin: 0px; float: left;'><b>Date:</b> \"+quiz.date+\"</p>\"));
                infoDiv.append(\$(\"<p style='font-size: 20px; margin: 0px; float: right;'><b>Location:</b> \"+quiz.location+\"</p>\"));

                \$(\"#quiz-bar\").append(topicDiv);
                \$(\"#quiz-bar\").append(infoDiv);
            }
        }

        var buildMentors = function (response) {

            \$(\"#mentors-content\").empty();

            if (response.length == 0) {
                \$(\"#mentors-content\").append(\$(\"<h3 style='text-align: center;'>Are you a <b>mentor</b>?</h3><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h4 style='text-align: center;'>Please <b>sign-in</b> if you do not see yourself on this list.</h4><br><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h3 style='text-align: center;'>Are you a <b>student</b>?</h3><br>\"));
                \$(\"#mentors-content\").append(\$(\"<h4 style='text-align: center;'>No one here to help you? Please contact: <b>csmc@utdallas.edu</b></h4><br>\"));
            }

            var adaptContainer = false;

            if (response.length > 4) {
                adaptContainer = true;

                \$.keyframe.define({
                    name: 'slidementor',
                    from: {
                        'transform': 'translate3d(0,0,0)'
                    },
                    to: {
                        'transform': 'translate3d(0,-' + (136.04 * response.length) + 'px,0)' /* if over 4 mentors, transform -136.04(mentor-size + margin-top) for each mentor */
                    }
                });
            }

            for (i in response) {
                var mentor = response[i];

                var mentorName = mentor.name;

                if (mentorName.length > 17) {
                    mentorName = mentor.name.substring(0, 16).trim() + '.';
                }

                var mentorImageDiv = \$(\"<div class='mentor-image-div'></div>\");

                if(mentor.isShiftLeader) {
                    mentorImageDiv.append(\$(\"<p style='position: absolute; font-size: 17.5px; font-weight: bold; background-color: goldenrod;'>SHIFT LEADER</p>\"));
                }

                mentorImageDiv.append(\$(\"<span class='helper'></span>\" +
                    \"<img id='image_\" + mentor.username + \"' alt='\" + mentorName + \"' src='/profile/\" + mentor.username + \"/image\" + \"' class='mentor-image' onerror='this.onerror=null; this.src=\\\"\" + \$('#mentor_image').data('path') + \"\\\"'>\"));



                var mentorNameContainer = \$(\"<h1>\" + mentorName + \"</h1>\");
                var badgesContainer = \$(\"<h3></h3>\");

                mentor.specialties.forEach(function (specialty) {
                    badgesContainer.append(\"<span class='label label-default label-badge' style='background-color: \" + specialty.topic.color + \"!important'>\" + specialty.topic.abbreviation + \"</span>\");
                });

                var mentorDiv = \$(\"<div class='ui-widget-content ui-corner-tr mentor'></div>\");

                mentorDiv.append(mentorImageDiv);
                mentorDiv.append(mentorNameContainer);
                mentorDiv.append(badgesContainer);

                if (adaptContainer)
                    mentorDiv.css('animation', 'slidementor ' + (5 * response.length) + 's linear infinite');

                \$(\"#mentors-content\").append(mentorDiv);
            }

            if ((\$(\"#mentors-content div.mentor\").length) > 4) {
                var mentorAux = \$(\"#mentors-content div.mentor\")[0].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[1].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[2].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
                var mentorAux = \$(\"#mentors-content div.mentor\")[3].outerHTML;
                \$(\"#mentors-content\").append(mentorAux);
            }

            \$(\"#mentors-content\").append(\"<br>\");

            // mentorPagination();
        };

        var buildSessions = function (response) {
            var cardsPerPage = 4;
            var numberOfPages = Math.ceil(response.length / cardsPerPage);

            var sessionContents = [];
            for (var pageNumber = 0; pageNumber < numberOfPages; pageNumber++) {
                sessionContents[pageNumber] = new Array();
            }

            var currentPage = 0;
            var currentCard = 0;
            for (i in response) {
                var session = response[i];

                currentPage = Math.ceil(++currentCard / cardsPerPage);

                var sessionTitle = \$(\"<div class='session-header'><p>\" + session.topic + \"</p></div>\");
                var sessionSubtitle = \$(\"<div class='session-sub-header'></div>\");
                var sessionTime = \$(\"<div class='session-field'></div>\");

                var scheduledDate = new Date(session.full_date);
                var currentDate = new Date();

                //Check card's subtitle
                if (currentDate > scheduledDate) {
                    //Started sessions (or at least they were supposed to be started
                    sessionSubtitle.append(\$(\"<p><i class='fa fa-circle' style='font-size:10px;color: red;vertical-align: middle;'></i> Today - Started</p>\"));
                    sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                } else {
                    var twoHoursRange = new Date(scheduledDate);
                    twoHoursRange.setHours(twoHoursRange.getHours() - 2);

                    if (currentDate > twoHoursRange) {
                        //Starts Soon! sessions
                        sessionSubtitle.append(\$(\"<p> Today - Starts Soon!</p>\"));
                        sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                    } else {
                        if (currentDate.getDay() == scheduledDate.getDay()) {
                            //later today
                            sessionSubtitle.append(\$(\"<p>Later Today</p>\"));
                            sessionTime.append(\"<p><b>Time:</b> \" + session.time + \"</p>\");
                        } else {
                            //upcoming
                            sessionSubtitle.append(\$(\"<p>\" + session.weekday + \"</p>\"));
                            sessionTime.append(\"<p><b>Time:</b> \" + session.time + \" (\" + session.date + \")</p>\");
                        }
                    }
                }

                var sessionLocation = \$(\"<div class='session-field'><p><b>Location:</b> \" + session.room + \"</p></div>\");
                var sessionAvailability = \$(\"<div class='session-field'><p><b>Availability:</b> \" + session.capacity + \"</p></div>\");

                //Build mentors string
                var mentors = '';
                for(var j=0; j < session.mentors.length; j++) {
                    mentors = mentors + session.mentors[j] + ', ';
                }
                mentors = mentors.substring(0, mentors.length-2);

                var sessionMentors = \$(\"<div class='session-field'><p><b>Mentors: </b>\"+mentors+\"</p></div>\");


                //Create new session card and add all info
                var sessionDiv = \$(\"<div class='session panel panel-default'></div>\");
                sessionDiv.append(sessionTitle);
                sessionDiv.append(sessionSubtitle);
                sessionDiv.append(sessionTime);
                sessionDiv.append(sessionLocation);
                sessionDiv.append(sessionAvailability);
                sessionDiv.append(sessionMentors);

                sessionContents[(currentPage - 1)].push(sessionDiv);
            }

            var firstSessions = sessionContents[0];
            for (i in firstSessions) {
                var session = firstSessions[i];

                \$(\"#sessions-content\").append(session);
            }

            \$(\"#sessions-title\").empty();
            \$(\"#sessions-title\").append(\"<p><b>Sessions (Page 1 of \" + numberOfPages + \")</b></p>\")

            var showPage = 1;
            setInterval(function () {
                if (showPage == numberOfPages) {
                    showPage = 1;
                } else {
                    showPage++;
                }

                \$(\"#sessions-content\").hide(\"fade\", {}, 800, function () {
                    \$(\"#sessions-content\").empty();

                    for (i in sessionContents[(showPage - 1)]) {
                        var session = sessionContents[(showPage - 1)][i];
                        \$(\"#sessions-content\").append(session);
                    }

                    \$(\"#sessions-title\").empty();
                    \$(\"#sessions-title\").append(\"<p><b>Sessions (Page \" + showPage + \" of \" + numberOfPages + \")</b></p>\")

                    \$(\"#sessions-content\").show(\"fade\", {}, 800);
                });
            }, 8000);

        };

        var updatePagination = function (currentPage) {
            var number = 1;
            \$(\"#wait-list-pagination li\").each(function (id, li) {
                var pageItem = \$(li);

                if (number == currentPage)
                    pageItem.addClass(\"active\");
                else
                    pageItem.removeClass(\"active\");

                number++;
            });
        };

        var createStudentTable = function (currentPage, studentsPerPage, students) {
            var start = (currentPage * studentsPerPage) - studentsPerPage;

            \$(\"#wait-list-body\").empty();
            for (var i = start; i < start + studentsPerPage; i++) {
                if (i < students.length) {
                    \$('#wait-list-body').append('<tr>'
                        + '<td>' + students[i]['number'] + '</td>'
                        + '<td>' + students[i]['name'] + '</td>'
                        + '<td>' + students[i]['course'] + '</td>'
                        + '<td>' + students[i]['activity'] + '</td>'
                        + '<td>' + students[i]['time'] + '</td>'
                        + '</tr>'
                    );
                } else {
                    \$('#wait-list-body').append('<tr style=\"\"><td> </td><td> </td><td> </td><td> </td><td> </td></tr>');
                }
            }


        };

        if (!String.format) {
            String.format = function (format) {
                var args = Array.prototype.slice.call(arguments, 1);
                return format.replace(/{(\\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
    <script type=\"text/javascript\">
        \$(function () {
            var refreshTime = 1000 * 60 * 2.5;

            setTimeout(function () {
                location.reload(true);
            }, refreshTime);
            var refreshMessage = \"Refreshing in {0}:{1}\";
            \$('#timer').append(String.format(refreshMessage, Math.floor(refreshTime / 60000), (\"00\" + Math.ceil((refreshTime / 1000) % 60)).substr(-2, 2)));

            var t = refreshTime;
            setInterval(function () {
                t -= 1000;
                \$('#timer').empty();
                \$('#timer').append(String.format(refreshMessage, Math.floor(t / 60000), (\"00\" + Math.ceil((t / 1000) % 60)).substr(-2, 2)));
            }, 1000);

            var numStudents = {{ students|length }};

            if (numStudents == 0) {
                // put in a message saying no students
                var emptyMessage = \"<h2>No Students Currently Signed In</h2><br><h3>If you signed in and see this message, don't panic, it will refresh in about {0} {1}</h3>\"

                var minutes = Math.ceil(refreshTime / 60000);
                \$(\"#general-content\").empty();
                \$(\"#general-content\").append(String.format(emptyMessage, minutes, \"minutes\"));

                var time = refreshTime;

                setInterval(function () {
                    time = time - 1000;
                    if (time >= 60000) {
                        if (time % 60000 == 0) {
                            var minutes = Math.ceil(time / 60000);
                            \$(\"#general-content\").empty();
                            \$(\"#general-content\").append(String.format(emptyMessage, minutes, \"minutes\"));
                        }
                    } else {
                        var seconds = Math.ceil(time / 1000);
                        \$(\"#general-content\").empty();
                        \$(\"#general-content\").append(String.format(emptyMessage, seconds, \"seconds\"));
                    }


                }, 1000);
            } else {
                var students = [];
                {% for student in students %}
                    var index = {{ loop.index0 }};
                    students[index] = {
                        'number': index + 1,
                        'name': '{{ student.user.firstName }}',
                        'course': '{{ student.course.department.abbreviation }} {{ student.course.number }}',
                        'activity': '{{ student.activity.name }}',
                        'time': '{{ student.timeIn|date(\"g:i A\") }}'
                    };
                {% endfor %}

                var studentsPerPage = 6;
                var currentPage = 1;
                var numberOfPages = Math.ceil(numStudents / studentsPerPage);

                if (numberOfPages > 1) {
                    {# first page #}
                    \$(\"#wait-list-pagination\").append(\"<li class=\\\"active\\\"><a href=\\\"#\\\">1</a></li>\");
                    {# following pages #}
                    for (var i = 1; i < numberOfPages; i++) {
                        \$(\"#wait-list-pagination\").append(\"<li><a href=\\\"#\\\">\" + (i + 1) + \"</a></li>\");
                    }
                }
                createStudentTable(currentPage, studentsPerPage, students);

                if (numberOfPages > 1) {
                    setInterval(function () {
                        {# check if it is the last page #}
                        if (currentPage == numberOfPages) {
                            currentPage = 1;
                        } else {
                            currentPage++;
                        }

                        \$(\".container\").hide(\"drop\", {direction: \"left\"}, 2000, function () {
                            \$(\".container\").show(\"drop\", {direction: \"right\"}, 2000);
                            updatePagination(currentPage);
                            createStudentTable(currentPage, studentsPerPage, students);
                        })
                    }, 14000);
                }
            }

            var mentors = [];

            {% for mentor in mentors %}
                {% set i = loop.index0 %}

                mentors[{{ i }}] = [];
                mentors[{{ i }}]['name'] = '{{ mentor.preferredName }}';
                mentors[{{ i }}]['username'] = '{{ mentor.userName }}';
                mentors[{{ i }}]['specialties'] = [];

                {% if shift_leader.id == mentor.id %}
                    mentors[{{ i }}]['isShiftLeader'] = true;
                {% else %}
                    mentors[{{ i }}]['isShiftLeader'] = false;
                {% endif %}

                {% set j = 0 %}
                {% for specialty in mentor.profile.specialties %}
                    {% if specialty.rating >= 3 and specialty.subject.showOnCalendar %}
                    mentors[{{ i }}]['specialties'][{{ j }}] = {
                        'topic': {
                            'name': '{{ specialty.subject.name }}',
                            'color': '{{ specialty.subject.color }}',
                            'abbreviation': '{{ specialty.subject.abbreviation }}'
                        },
                    };
                    {%  set j = j + 1 %}
                    {% endif %}
                {% endfor %}

            {% endfor %}

            buildMentors(mentors);


            var sessions = [];

            {% for timeslot in sessions %}
                {% set i = loop.index0 %}
                sessions[{{ i }}] = [];
                sessions[{{ i }}]['topic'] = '{{ timeslot.name }}';
                sessions[{{ i }}]['time'] = '{{ timeslot.startTime|date('h:i A') }}';
                sessions[{{ i }}]['date'] = '{{ timeslot.startTime|date('F j') }}';
                sessions[{{ i }}]['room'] = '{{ timeslot.location }}';
                sessions[{{ i }}]['capacity'] = '{{ timeslot.capacity }}';

                sessions[{{ i }}]['start_time'] = '{{ timeslot.startTime is null ? '' : timeslot.startTime|date('H:i A') }}';

                sessions[{{ i }}]['full_date'] = '{{ timeslot.startTime|date('F d, Y G:i:s') }}';

                sessions[{{ i }}]['weekday'] = '{{ timeslot.startTime()|date('l') }}';

                sessions[{{ i }}]['mentors'] = [];
                {% for mentor in timeslot.getMentors() %}
                    {% set j = loop.index0 %}
                    sessions[{{ i }}]['mentors'][{{ j }}] = '{{ mentor.preferredName }}';
                {% endfor %}

            {% endfor %}

            buildSessions(sessions);

            var quizzes = [];

            console.log()

            {% for quiz in quizzes %}
                {% set i = loop.index0 %}

                quizzes[{{ i }}] = [];
                quizzes[{{ i }}]['topic'] = '{{ quiz.topic }}';
                quizzes[{{ i }}]['location'] = '{{ quiz.location }}';
                quizzes[{{ i }}]['date'] = '{{ quiz.startDate|date('F j') }}' + ' - ' + '{{ quiz.endDate|date('F j') }}';

                console.log('{{ quiz.topic }}');
                console.log('{{ quiz.location }}');
                {#console.log('{{ quiz.timeSlot.startTime|date('F j') }}' + ' - ' + '{{ quiz.timeSlot.endTime|date('F j') }}');#}

            {% endfor %}

            buildQuizzes(quizzes);


            {# CLOCK #}
            var clock = \$('.clock').FlipClock({
                clockFace: 'TwelveHourClock',
                showSeconds: false
            });
        });
    </script>
</body>
</html>
", "shared/display.html.twig", "/code/templates/shared/display.html.twig");
    }
}
