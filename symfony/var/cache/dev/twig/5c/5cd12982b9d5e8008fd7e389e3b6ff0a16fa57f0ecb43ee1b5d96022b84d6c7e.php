<?php

/* role/admin/schedule/addNewHoliday.html.twig */
class __TwigTemplate_a3d5f6b1f8271431cb764d9e3149588ecc73be76d1bc82a7eeff692d8c7acb5e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/schedule/addNewHoliday.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/addNewHoliday.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/addNewHoliday.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Create Holiday</h2>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        ";
        // line 13
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->source); })()), "vars", []), "valid", [])) {
            // line 14
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 15
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 15, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 18
        echo "                        <br/>
                        ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 19, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left input_mask"]]);
        echo "
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 22, $this->source); })()), "holidayDate", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 25, $this->source); })()), "holidayDate", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "StartTime", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 33, $this->source); })()), "StartTime", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->source); })()), "EndTime", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 41, $this->source); })()), "EndTime", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 46, $this->source); })()), "closed", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 49, $this->source); })()), "closed", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 54, $this->source); })()), "description", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 57, $this->source); })()), "description", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\" onclick=\"location.href='./list_holidays'\">
                                Cancel
                            </button>
                            ";
        // line 64
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 64, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success"]]);
        echo "
                        </div>
                        ";
        // line 66
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 66, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 73
        $this->displayBlock('javascripts', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 74
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">

            \$(\"#form_closed\").change(function () {
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if (\$(this).is(\":checked\")) // \"this\" refers to the element that fired the event
                {
                    \$(\"#form_EndTime\").attr('readonly', true);
                    \$(\"#form_StartTime\").attr('readonly', true);
                    \$(\"#form_EndTime\").val(\"12:00\");
                    \$(\"#form_StartTime\").val(\"00:00\");
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
                else {
                    \$(\"#form_EndTime\").attr('readonly', false);
                    \$(\"#form_StartTime\").attr('readonly', false);
                }
            });
            \$(\"#form_StartTime\").change(function () {

                \$start_time = \"00:00\";
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if ((\$(this).val()) != \$start_time) // \"this\" refers to the element that fired the event
                {
                    \$('#form_closed').prop('checked', false);
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
            });
            \$(\"#form_EndTime\").change(function () {

                \$end_time = \"12:00\";
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if ((\$(this).val()) != \$end_time) // \"this\" refers to the element that fired the event
                {
                    \$('#form_closed').prop('checked', false);
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
            });

        </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/schedule/addNewHoliday.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 74,  173 => 73,  163 => 66,  158 => 64,  148 => 57,  142 => 54,  134 => 49,  128 => 46,  120 => 41,  114 => 38,  106 => 33,  100 => 30,  92 => 25,  86 => 22,  80 => 19,  77 => 18,  71 => 15,  68 => 14,  65 => 13,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Create Holiday</h2>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        {# Checking for form errors and displaying them#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>
                        {{ form_start(form,{'attr':{'class':'form-horizontal form-label-left input_mask'}}) }}
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.holidayDate) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.holidayDate, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.StartTime) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.StartTime, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.EndTime) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.EndTime, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.closed) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.closed, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.description,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.description, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>
                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\" onclick=\"location.href='./list_holidays'\">
                                Cancel
                            </button>
                            {{ form_widget(form.submit,{'attr':{'class':'btn btn-success'}}) }}
                        </div>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">

            \$(\"#form_closed\").change(function () {
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if (\$(this).is(\":checked\")) // \"this\" refers to the element that fired the event
                {
                    \$(\"#form_EndTime\").attr('readonly', true);
                    \$(\"#form_StartTime\").attr('readonly', true);
                    \$(\"#form_EndTime\").val(\"12:00\");
                    \$(\"#form_StartTime\").val(\"00:00\");
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
                else {
                    \$(\"#form_EndTime\").attr('readonly', false);
                    \$(\"#form_StartTime\").attr('readonly', false);
                }
            });
            \$(\"#form_StartTime\").change(function () {

                \$start_time = \"00:00\";
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if ((\$(this).val()) != \$start_time) // \"this\" refers to the element that fired the event
                {
                    \$('#form_closed').prop('checked', false);
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
            });
            \$(\"#form_EndTime\").change(function () {

                \$end_time = \"12:00\";
                // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
                if ((\$(this).val()) != \$end_time) // \"this\" refers to the element that fired the event
                {
                    \$('#form_closed').prop('checked', false);
                    // \$(\"#form_EndTime\").val(display.toUTCString());
                }
            });

        </script>
    {% endblock %}
{% endblock %}
", "role/admin/schedule/addNewHoliday.html.twig", "/code/templates/role/admin/schedule/addNewHoliday.html.twig");
    }
}
