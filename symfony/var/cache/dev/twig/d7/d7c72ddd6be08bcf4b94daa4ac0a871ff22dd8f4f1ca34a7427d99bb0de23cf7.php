<?php

/* role/developer/component/nav.html.twig */
class __TwigTemplate_ccd9bd917a2a7d62c131a4f4d87a3319df77de48142fe428a649bd9de0fa1a0f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/component/nav.html.twig", "role/developer/component/nav.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'links' => [$this, 'block_links'],
            'top_nav' => [$this, 'block_top_nav'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/component/nav.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/developer/component/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/developer/component/nav.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo "    <i class=\"fas fa-desktop\"></i> <span>CSMC Admin!</span>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_links($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "links"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "links"));

        // line 6
        echo "    <li>
        <a href=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><i class=\"fas fa-home\"></i> Home</a>
    </li>

    <li>
        <a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_home");
        echo "\"><i class=\"fas fa-clipboard\"></i> Admin Dashboard</a>
    </li>

    <li>
        <a href=\"";
        // line 15
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("dev_files");
        echo "\"><i class=\"fas fa-file-archive\"></i> Files</a>
    </li>

    <li>
        <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("dev_swipes");
        echo "\"><i class=\"fas fa-credit-card\"></i> Swipes</a>
    </li>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 22
    public function block_top_nav($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "top_nav"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "top_nav"));

        // line 23
        echo "    <li class=\"\">
        <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
           aria-expanded=\"false\">
            <img src=\"images/img.jpg\" alt=\"\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 26, $this->source); })()), "user", []), "preferredName", []), "html", null, true);
        echo "
            <span class=\" fas fa-angle-down\"></span>
        </a>
        <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
            <li><a href=\"javascript:;\">Help</a></li>
            <li>
                <a href=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_logout");
        echo "\"><i class=\"fas fa-sign-out pull-right\"></i> Log Out</a>
            </li>
        </ul>
    </li>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 37
    public function block_footer($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 38
        echo "    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
        <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
        <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
        <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
        <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
    </a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/developer/component/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 38,  151 => 37,  136 => 32,  127 => 26,  122 => 23,  113 => 22,  100 => 19,  93 => 15,  86 => 11,  79 => 7,  76 => 6,  67 => 5,  56 => 3,  47 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/component/nav.html.twig' %}
{% block title %}
    <i class=\"fas fa-desktop\"></i> <span>CSMC Admin!</span>
{% endblock %}
{% block links %}
    <li>
        <a href=\"{{ path('home') }}\"><i class=\"fas fa-home\"></i> Home</a>
    </li>

    <li>
        <a href=\"{{ path('admin_home') }}\"><i class=\"fas fa-clipboard\"></i> Admin Dashboard</a>
    </li>

    <li>
        <a href=\"{{ path('dev_files') }}\"><i class=\"fas fa-file-archive\"></i> Files</a>
    </li>

    <li>
        <a href=\"{{ path('dev_swipes') }}\"><i class=\"fas fa-credit-card\"></i> Swipes</a>
    </li>
{% endblock %}
{% block top_nav %}
    <li class=\"\">
        <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
           aria-expanded=\"false\">
            <img src=\"images/img.jpg\" alt=\"\">{{ app.user.preferredName }}
            <span class=\" fas fa-angle-down\"></span>
        </a>
        <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
            <li><a href=\"javascript:;\">Help</a></li>
            <li>
                <a href=\"{{ path('user_logout') }}\"><i class=\"fas fa-sign-out pull-right\"></i> Log Out</a>
            </li>
        </ul>
    </li>
{% endblock %}
{% block footer %}
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
        <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
        <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
        <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
    </a>
    <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
        <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
    </a>
{% endblock %}", "role/developer/component/nav.html.twig", "/code/templates/role/developer/component/nav.html.twig");
    }
}
