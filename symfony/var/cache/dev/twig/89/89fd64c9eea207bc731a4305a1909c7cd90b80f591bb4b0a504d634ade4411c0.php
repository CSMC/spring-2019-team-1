<?php

/* shared/home/home.html.twig */
class __TwigTemplate_b16ab65aa376520b064c4f4389158b24342ea512d9051051077837850fcc3d38 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("shared/base.html.twig", "shared/home/home.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        if (((isset($context["successMessage"]) || array_key_exists("successMessage", $context)) &&  !(null === (isset($context["successMessage"]) || array_key_exists("successMessage", $context) ? $context["successMessage"] : (function () { throw new Twig_Error_Runtime('Variable "successMessage" does not exist.', 4, $this->source); })())))) {
            // line 5
            echo "        <div class=\"flash-message alert alert-success\" role=\"alert\">
            ";
            // line 6
            echo twig_escape_filter($this->env, (isset($context["successMessage"]) || array_key_exists("successMessage", $context) ? $context["successMessage"] : (function () { throw new Twig_Error_Runtime('Variable "successMessage" does not exist.', 6, $this->source); })()), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 9
        echo "
    <div class=\"content_head\">
        <h1>Home</h1>
        <div class=\"content_options\">
            <h2>";
        // line 13
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "F j, Y"), "html", null, true);
        echo "</h2>
        </div>
    </div>
    <hr/>
    ";
        // line 18
        echo "
    ";
        // line 20
        echo "    ";
        echo twig_include($this->env, $context, "shared/home/announcements.html.twig");
        echo "

    ";
        // line 23
        echo "    ";
        echo twig_include($this->env, $context, "shared/home/hours.html.twig");
        echo "

    ";
        // line 26
        echo "    ";
        echo twig_include($this->env, $context, "shared/home/locations.html.twig");
        echo "

    ";
        // line 29
        echo "    <h2 class=\"content_title\">Contact Information</h2>
    <div class=\"content_list\">
        <div class=\"content_list_label\">Email</div>
        <div class=\"content_list_info\">
            <a href=\"mailto:csmc@utdallas.edu\" target=\"_top\">csmc@utdallas.edu</a>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "shared/home/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 29,  93 => 26,  87 => 23,  81 => 20,  78 => 18,  71 => 13,  65 => 9,  59 => 6,  56 => 5,  53 => 4,  44 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Data Needed: User Announcements Hours Rooms #}
{% extends 'shared/base.html.twig' %}
{% block body %}
    {% if successMessage is defined and successMessage is not null %}
        <div class=\"flash-message alert alert-success\" role=\"alert\">
            {{ successMessage }}
        </div>
    {% endif %}

    <div class=\"content_head\">
        <h1>Home</h1>
        <div class=\"content_options\">
            <h2>{{ \"now\"|date('F j, Y') }}</h2>
        </div>
    </div>
    <hr/>
    {#{{ include('shared/component/flash_messages.html.twig') }}#}

    {# Announcements #}
    {{ include('shared/home/announcements.html.twig') }}

    {# Opperation Hours #}
    {{ include('shared/home/hours.html.twig') }}

    {# Locations #}
    {{ include('shared/home/locations.html.twig') }}

    {# Contact #}
    <h2 class=\"content_title\">Contact Information</h2>
    <div class=\"content_list\">
        <div class=\"content_list_label\">Email</div>
        <div class=\"content_list_info\">
            <a href=\"mailto:csmc@utdallas.edu\" target=\"_top\">csmc@utdallas.edu</a>
        </div>
    </div>
{% endblock %}
", "shared/home/home.html.twig", "/code/templates/shared/home/home.html.twig");
    }
}
