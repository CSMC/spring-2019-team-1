<?php

/* role/admin/user/edit.html.twig */
class __TwigTemplate_42895e263d3fb0cf15eecd68342edd1189c54a5dc41ba413422158de8da5d4d3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "

    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete this User?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    ";
        // line 43
        echo "                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteUser?id=";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["deleteId"]) || array_key_exists("deleteId", $context) ? $context["deleteId"] : (function () { throw new Twig_Error_Runtime('Variable "deleteId" does not exist.', 44, $this->source); })()), "html", null, true);
        echo "'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>





    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Users (";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["userName"]) || array_key_exists("userName", $context) ? $context["userName"] : (function () { throw new Twig_Error_Runtime('Variable "userName" does not exist.', 62, $this->source); })()), "html", null, true);
        echo ")</h2>

                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">

                       <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                               data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>

                    </span>

                        <div class=\"clearfix\"></div>
                    </div>


                    <div class=\"x_content\">
                        ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 76, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 77
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 79
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 80
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "

                        ";
        // line 87
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 87, $this->source); })()), "vars", []), "valid", [])) {
            // line 88
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 89
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 89, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 92
        echo "
                        <br/>
                        ";
        // line 94
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 94, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 97, $this->source); })()), "username", []), 'label');
        echo "
                            </div>
                            ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 99, $this->source); })()), "username", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 101, $this->source); })()), "username", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 107, $this->source); })()), "firstName", []), 'label');
        echo "
                            </div>
                            ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 109, $this->source); })()), "firstName", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 111
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 111, $this->source); })()), "firstName", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 117, $this->source); })()), "lastName", []), 'label');
        echo "
                            </div>
                            ";
        // line 119
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 119, $this->source); })()), "lastName", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 121
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 121, $this->source); })()), "lastName", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 127
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 127, $this->source); })()), "cardId", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 129, $this->source); })()), "cardId", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 131
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 131, $this->source); })()), "cardId", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 137
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 137, $this->source); })()), "scancode", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </div>
                            ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 139, $this->source); })()), "scancode", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 141
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 141, $this->source); })()), "scancode", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                ";
        // line 147
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 147, $this->source); })()), "roles", []), 'label');
        echo "
                            </div>
                            ";
        // line 149
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 149, $this->source); })()), "roles", []), 'errors');
        echo "
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 151
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 151, $this->source); })()), "roles", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12", "required" => "required", "size" => "15"]]);
        echo "
                            </div>
                        </div>

                        <div class=\"ln_solid\"></div>
                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <a class=\"btn btn-default\" href=\"";
        // line 158
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list");
        echo "\">
                                    Cancel
                                </a>
                                ";
        // line 161
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 161, $this->source); })()), "save", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "

                            </div>
                        </div>
                        ";
        // line 165
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 165, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 172
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 173
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        \$('#form_submit').on('click', function (event) {
            event.preventDefault();

        });
        \$('#saveChanges').on('click', function () {
            \$('form').submit();
        });
        \$('#deleteBtn').on('click', function (event) {
            event.preventDefault();

        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  347 => 173,  338 => 172,  321 => 165,  314 => 161,  308 => 158,  298 => 151,  293 => 149,  288 => 147,  279 => 141,  274 => 139,  269 => 137,  260 => 131,  255 => 129,  250 => 127,  241 => 121,  236 => 119,  231 => 117,  222 => 111,  217 => 109,  212 => 107,  203 => 101,  198 => 99,  193 => 97,  187 => 94,  183 => 92,  177 => 89,  174 => 88,  172 => 87,  168 => 85,  161 => 83,  152 => 80,  147 => 79,  143 => 78,  140 => 77,  136 => 76,  119 => 62,  98 => 44,  95 => 43,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}


    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete this User?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    {# Change the delete URL #}
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteUser?id={{ deleteId }}'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>





    <div class=\"center_col\" role=\"main\">


        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <h2> Edit Users ({{ userName }})</h2>

                        <span class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\" align=\"right\">

                       <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                               data-target=\"#myModalDelete\" style=\"float: right\">Delete</button>

                    </span>

                        <div class=\"clearfix\"></div>
                    </div>


                    <div class=\"x_content\">
                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}


                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}

                        <br/>
                        {{ form_start(form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}
                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.username) }}
                            </div>
                            {{ form_errors(form.username) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.username, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.firstName) }}
                            </div>
                            {{ form_errors(form.firstName) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.firstName, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.lastName) }}
                            </div>
                            {{ form_errors(form.lastName) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.lastName, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.cardId,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.cardId) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.cardId, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.scancode,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                            </div>
                            {{ form_errors(form.scancode) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.scancode, {'attr': {'class': 'form-control col-md-7 col-xs-12'}}) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <div class=\"control-label col-md-3 col-sm-3 col-xs-12\">
                                {{ form_label(form.roles) }}
                            </div>
                            {{ form_errors(form.roles) }}
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.roles, {'attr': {'class': 'form-control col-md-7 col-xs-12', 'required': 'required',  'size':'15'}}) }}
                            </div>
                        </div>

                        <div class=\"ln_solid\"></div>
                        <div class=\"form-group\">
                            <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                                <a class=\"btn btn-default\" href=\"{{ path('admin_user_list') }}\">
                                    Cancel
                                </a>
                                {{ form_widget(form.save, {'attr': {'class': 'btn btn-success', 'data-toggle':'modal','data-target':'#myModalEdit' }}) }}

                            </div>
                        </div>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\">
        \$('#form_submit').on('click', function (event) {
            event.preventDefault();

        });
        \$('#saveChanges').on('click', function () {
            \$('form').submit();
        });
        \$('#deleteBtn').on('click', function (event) {
            event.preventDefault();

        });
    </script>
{% endblock %}", "role/admin/user/edit.html.twig", "/code/templates/role/admin/user/edit.html.twig");
    }
}
