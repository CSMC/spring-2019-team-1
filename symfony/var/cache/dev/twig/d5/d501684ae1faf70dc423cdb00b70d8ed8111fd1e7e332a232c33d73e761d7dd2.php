<?php

/* role/mentor/session/grades/grades.html.twig */
class __TwigTemplate_18aa3e7ed9bed797a9d1f820eaf727929719fcaab0a0a90770e5273f2477e9ac extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/session/grades/grades.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/grades/grades.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/grades/grades.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        // line 4
        echo "    ";
        $this->loadTemplate("shared/component/flash_messages.html.twig", "role/mentor/session/grades/grades.html.twig", 4)->display($context);
        // line 5
        echo "    <h2>
        ";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 6, $this->source); })()), "topic", []), "html", null, true);
        echo "
        <a class=\"pull-right btn btn-success\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_attend", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 7, $this->source); })()), "id", [])]), "html", null, true);
        echo "\">Mark Student as Attended</a>
    </h2>
    ";
        // line 9
        if ((isset($context["attendees"]) || array_key_exists("attendees", $context))) {
            // line 10
            echo "        <table id=\"grades\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Name</th>
                <th>NetID</th>
                ";
            // line 15
            if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 15, $this->source); })()), "graded", [])) {
                // line 16
                echo "                    <th>Grade</th>
                ";
            }
            // line 18
            echo "            </tr>
            </thead>
            <tbody>
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attendees"]) || array_key_exists("attendees", $context) ? $context["attendees"] : (function () { throw new Twig_Error_Runtime('Variable "attendees" does not exist.', 21, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                // line 22
                echo "                <tr>
                    <td>";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                echo "</td>
                    ";
                // line 25
                if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 25, $this->source); })()), "graded", [])) {
                    // line 26
                    echo "                        <td>
                            ";
                    // line 27
                    if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 27, $this->source); })()), "numericGrade", [])) {
                        // line 28
                        echo "                                <input value=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []), "html", null, true);
                        echo "\"
                                       onchange=\"updateGrade(this, '";
                        // line 29
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                        echo "')\"
                                       type=\"number\"
                                       min=\"0\"
                                       max=\"100\">
                            ";
                    } else {
                        // line 34
                        echo "                                <select onchange=\"updateGrade(this, '";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                        echo "')\">
                                    <option ";
                        // line 35
                        if ((null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []))) {
                            echo "selected";
                        }
                        echo "></option>
                                    <option value=\"1\" ";
                        // line 36
                        if ((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 1)) {
                            echo "selected";
                        }
                        echo ">Pass
                                    </option>
                                    <option value=\"0\"
                                            ";
                        // line 39
                        if (((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 0) &&  !(null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", [])))) {
                            echo "selected";
                        }
                        echo ">
                                        Fail
                                    </option>
                                </select>
                            ";
                    }
                    // line 44
                    echo "                        </td>
                    ";
                }
                // line 46
                echo "                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "            </tbody>
        </table>
    ";
        } elseif (        // line 50
(isset($context["timeslots"]) || array_key_exists("timeslots", $context))) {
            // line 51
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["timeslots"]) || array_key_exists("timeslots", $context) ? $context["timeslots"] : (function () { throw new Twig_Error_Runtime('Variable "timeslots" does not exist.', 51, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
                // line 52
                echo "            <h3>
                ";
                // line 53
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "m/d g:i A"), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "endTime", []), "g:i A"), "html", null, true);
                echo "
            </h3>
            ";
                // line 55
                $context["attendances"] = twig_get_attribute($this->env, $this->source, $context["timeslot"], "attendances", []);
                // line 56
                echo "            <table id=\"grades-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", []), "html", null, true);
                echo "\" class=\"table table-bordered table-striped\">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>NetID</th>
                    ";
                // line 61
                if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 61, $this->source); })()), "graded", [])) {
                    // line 62
                    echo "                        <th>Grade</th>
                    ";
                }
                // line 64
                echo "                </tr>
                </thead>
                <tbody>
                ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["attendances"]) || array_key_exists("attendances", $context) ? $context["attendances"] : (function () { throw new Twig_Error_Runtime('Variable "attendances" does not exist.', 67, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                    // line 68
                    echo "                    <tr>
                        <td>";
                    // line 69
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "firstName", []), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "lastName", []), "html", null, true);
                    echo "</td>
                        <td>";
                    // line 70
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                    echo "</td>
                        ";
                    // line 71
                    if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 71, $this->source); })()), "graded", [])) {
                        // line 72
                        echo "                            <td>
                                ";
                        // line 73
                        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 73, $this->source); })()), "numericGrade", [])) {
                            // line 74
                            echo "                                    <input value=\"";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []), "html", null, true);
                            echo "\"
                                           onchange=\"updateGrade(this, '";
                            // line 75
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                            echo "')\"
                                           type=\"number\"
                                           min=\"0\"
                                           max=\"100\">
                                ";
                        } else {
                            // line 80
                            echo "                                    <select onchange=\"updateGrade(this, '";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "id", []), "html", null, true);
                            echo "')\">
                                        <option ";
                            // line 81
                            if ((null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []))) {
                                echo "selected";
                            }
                            echo "></option>
                                        <option value=\"1\" ";
                            // line 82
                            if ((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 1)) {
                                echo "selected";
                            }
                            echo ">Pass
                                        </option>
                                        <option value=\"0\"
                                                ";
                            // line 85
                            if (((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []) == 0) &&  !(null === twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", [])))) {
                                echo "selected";
                            }
                            echo ">
                                            Fail
                                        </option>
                                    </select>
                                ";
                        }
                        // line 90
                        echo "                            </td>
                        ";
                    }
                    // line 92
                    echo "                    </tr>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                </tbody>
            </table>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 100
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 101
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            \$('[id^=grades]').DataTable({
                ordering: true,
                searching: true,
                paging: false
            })
        });

        function updateGrade(obj, attendance) {
            ";
        // line 112
        if (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 112, $this->source); })()), "numericGrade", [])) {
            // line 113
            echo "            var grade = obj.value;
            ";
        } else {
            // line 115
            echo "            var grade = obj.options[obj.selectedIndex].value;
            ";
        }
        // line 117
        echo "            \$.ajax({
                type: 'POST',
                url: '";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades_edit_ajax");
        echo "',
                data: {'grade': grade, 'attendance': attendance}
            }).done(function (data) {
                //success message
            }).fail(function (data) {
                //error message
            });
        }
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/session/grades/grades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  343 => 119,  339 => 117,  335 => 115,  331 => 113,  329 => 112,  314 => 101,  305 => 100,  294 => 97,  286 => 94,  279 => 92,  275 => 90,  265 => 85,  257 => 82,  251 => 81,  246 => 80,  238 => 75,  233 => 74,  231 => 73,  228 => 72,  226 => 71,  222 => 70,  216 => 69,  213 => 68,  209 => 67,  204 => 64,  200 => 62,  198 => 61,  189 => 56,  187 => 55,  180 => 53,  177 => 52,  172 => 51,  170 => 50,  166 => 48,  159 => 46,  155 => 44,  145 => 39,  137 => 36,  131 => 35,  126 => 34,  118 => 29,  113 => 28,  111 => 27,  108 => 26,  106 => 25,  102 => 24,  98 => 23,  95 => 22,  91 => 21,  86 => 18,  82 => 16,  80 => 15,  73 => 10,  71 => 9,  66 => 7,  62 => 6,  59 => 5,  56 => 4,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    {# TODO add download link for instructor/admin #}
    {% include('shared/component/flash_messages.html.twig') %}
    <h2>
        {{ session.topic }}
        <a class=\"pull-right btn btn-success\" href=\"{{ path('session_attend', {'id': session.id}) }}\">Mark Student as Attended</a>
    </h2>
    {% if attendees is defined %}
        <table id=\"grades\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Name</th>
                <th>NetID</th>
                {% if session.graded %}
                    <th>Grade</th>
                {% endif %}
            </tr>
            </thead>
            <tbody>
            {% for attendance in attendees %}
                <tr>
                    <td>{{ attendance.user }}</td>
                    <td>{{ attendance.user.username }}</td>
                    {% if session.graded %}
                        <td>
                            {% if session.numericGrade %}
                                <input value=\"{{ attendance.grade }}\"
                                       onchange=\"updateGrade(this, '{{ attendance.id }}')\"
                                       type=\"number\"
                                       min=\"0\"
                                       max=\"100\">
                            {% else %}
                                <select onchange=\"updateGrade(this, '{{ attendance.id }}')\">
                                    <option {% if attendance.grade is null %}selected{% endif %}></option>
                                    <option value=\"1\" {% if attendance.grade == 1 %}selected{% endif %}>Pass
                                    </option>
                                    <option value=\"0\"
                                            {% if attendance.grade == 0 and attendance.grade is not null %}selected{% endif %}>
                                        Fail
                                    </option>
                                </select>
                            {% endif %}
                        </td>
                    {% endif %}
                </tr>
            {% endfor %}
            </tbody>
        </table>
    {% elseif timeslots is defined %}
        {% for timeslot in timeslots %}
            <h3>
                {{ timeslot.startTime|date('m/d g:i A') }} - {{ timeslot.endTime|date('g:i A') }}
            </h3>
            {% set attendances = timeslot.attendances %}
            <table id=\"grades-{{ timeslot.id }}\" class=\"table table-bordered table-striped\">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>NetID</th>
                    {% if session.graded %}
                        <th>Grade</th>
                    {% endif %}
                </tr>
                </thead>
                <tbody>
                {% for attendance in attendances %}
                    <tr>
                        <td>{{ attendance.user.firstName }} {{ attendance.user.lastName }}</td>
                        <td>{{ attendance.user.username }}</td>
                        {% if session.graded %}
                            <td>
                                {% if session.numericGrade %}
                                    <input value=\"{{ attendance.grade }}\"
                                           onchange=\"updateGrade(this, '{{ attendance.id }}')\"
                                           type=\"number\"
                                           min=\"0\"
                                           max=\"100\">
                                {% else %}
                                    <select onchange=\"updateGrade(this, '{{ attendance.id }}')\">
                                        <option {% if attendance.grade is null %}selected{% endif %}></option>
                                        <option value=\"1\" {% if attendance.grade == 1 %}selected{% endif %}>Pass
                                        </option>
                                        <option value=\"0\"
                                                {% if attendance.grade == 0 and attendance.grade is not null %}selected{% endif %}>
                                            Fail
                                        </option>
                                    </select>
                                {% endif %}
                            </td>
                        {% endif %}
                    </tr>
                {% endfor %}
                </tbody>
            </table>
        {% endfor %}
    {% endif %}
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            \$('[id^=grades]').DataTable({
                ordering: true,
                searching: true,
                paging: false
            })
        });

        function updateGrade(obj, attendance) {
            {% if session.numericGrade %}
            var grade = obj.value;
            {% else %}
            var grade = obj.options[obj.selectedIndex].value;
            {% endif %}
            \$.ajax({
                type: 'POST',
                url: '{{ path('session_grades_edit_ajax') }}',
                data: {'grade': grade, 'attendance': attendance}
            }).done(function (data) {
                //success message
            }).fail(function (data) {
                //error message
            });
        }
    </script>
{% endblock %}", "role/mentor/session/grades/grades.html.twig", "/code/templates/role/mentor/session/grades/grades.html.twig");
    }
}
