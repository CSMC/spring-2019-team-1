<?php

/* role/admin/schedule/timesheets.html.twig */
class __TwigTemplate_3fd339fa0149665d80df7118e26594e9e628878f77090792fa254c2f78fc439b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/schedule/timesheets.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/timesheets.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/schedule/timesheets.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->source); })()), "mentor", []), 'label');
        echo "
        </div>
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->source); })()), "mentor", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 10, $this->source); })()), "mentor", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 12
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 16, $this->source); })()), "start", []), 'label');
        echo "
        </div>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->source); })()), "start", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 20, $this->source); })()), "start", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 22
        echo "        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 26, $this->source); })()), "end", []), 'label');
        echo "
        </div>
        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->source); })()), "end", []), 'errors');
        echo "
        <div class=\"bt-flabels__wrapper\">
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "end", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
            ";
        // line 32
        echo "        </div>
    </div>
    ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 34, $this->source); })()), "submit", []), 'widget');
        echo "
    ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 35, $this->source); })()), 'form_end');
        echo "
    ";
        // line 36
        if ( !(null === (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 36, $this->source); })()))) {
            // line 37
            echo "        <h3>Times for ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 37, $this->source); })()), "mentor", [], "array"), "preferredName", []), "html", null, true);
            echo " for ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 37, $this->source); })()), "start", [], "array"), "m/d/Y"), "html", null, true);
            echo "
            - ";
            // line 38
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 38, $this->source); })()), "end", [], "array"), "m/d/Y"), "html", null, true);
            echo "</h3>
        <table id=\"datatable\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <td>Time In</td>
                <td>Time Out</td>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 50
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 51
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            var data = [
                ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["timesheet"]) || array_key_exists("timesheet", $context) ? $context["timesheet"] : (function () { throw new Twig_Error_Runtime('Variable "timesheet" does not exist.', 55, $this->source); })()), "times", [], "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["time"]) {
            // line 56
            echo "                [
                    \"";
            // line 57
            echo twig_escape_filter($this->env, (( !(null === twig_get_attribute($this->env, $this->source, $context["time"], "timeIn", []))) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "timeIn", []), "m/d g:i A")) : ("")), "html", null, true);
            echo "\",
                    \"";
            // line 58
            echo twig_escape_filter($this->env, (( !(null === twig_get_attribute($this->env, $this->source, $context["time"], "timeOut", []))) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["time"], "timeOut", []), "g:i A")) : ("")), "html", null, true);
            echo "\",
                ],
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['time'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "            ];

            \$('#datatable').DataTable({
                paging: false,
                sorting: true,
                ordering: true,
                data: data
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/schedule/timesheets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 61,  186 => 58,  182 => 57,  179 => 56,  175 => 55,  167 => 51,  158 => 50,  136 => 38,  129 => 37,  127 => 36,  123 => 35,  119 => 34,  115 => 32,  111 => 30,  106 => 28,  101 => 26,  95 => 22,  91 => 20,  86 => 18,  81 => 16,  75 => 12,  71 => 10,  66 => 8,  61 => 6,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/admin/base.html.twig' %}
{% block body %}
    {{ form_start(form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.mentor) }}
        </div>
        {{ form_errors(form.mentor) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.mentor, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.start) }}
        </div>
        {{ form_errors(form.start) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.start, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"control-label\">
            {{ form_label(form.end) }}
        </div>
        {{ form_errors(form.end) }}
        <div class=\"bt-flabels__wrapper\">
            {{ form_widget(form.end, {'attr': {'class': 'form-control'}}) }}
            {#<span class=\"bt-flabels__error-desc\">Required</span>#}
        </div>
    </div>
    {{ form_widget(form.submit) }}
    {{ form_end(form) }}
    {% if timesheet is not null %}
        <h3>Times for {{ timesheet['mentor'].preferredName }} for {{ timesheet['start']|date('m/d/Y') }}
            - {{ timesheet['end']|date('m/d/Y') }}</h3>
        <table id=\"datatable\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <td>Time In</td>
                <td>Time Out</td>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    {% endif %}
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            var data = [
                {% for time in timesheet['times'] %}
                [
                    \"{{ time.timeIn is not null ? time.timeIn|date('m/d g:i A') : '' }}\",
                    \"{{ time.timeOut is not null ? time.timeOut|date('g:i A') : '' }}\",
                ],
                {% endfor %}
            ];

            \$('#datatable').DataTable({
                paging: false,
                sorting: true,
                ordering: true,
                data: data
            });
        });
    </script>
{% endblock %}", "role/admin/schedule/timesheets.html.twig", "/code/templates/role/admin/schedule/timesheets.html.twig");
    }
}
