<?php

/* shared/home/locations.html.twig */
class __TwigTemplate_df383b4ef9b4890ec173fbe4280bd663dd513b78ce5f209000e090e0fd5cc879 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/locations.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/home/locations.html.twig"));

        // line 1
        echo "<div class=\"content_head\">
    <h2 class=\"content_title\">Locations</h2>
</div>
<div class=\"content_list\">
    ";
        // line 5
        if ((twig_length_filter($this->env, (isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new Twig_Error_Runtime('Variable "rooms" does not exist.', 5, $this->source); })())) > 0)) {
            // line 6
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rooms"]) || array_key_exists("rooms", $context) ? $context["rooms"] : (function () { throw new Twig_Error_Runtime('Variable "rooms" does not exist.', 6, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["room"]) {
                // line 7
                echo "            <div class=\"content_list_label\">
                <a class=\"room_link\" target=\"_blank\"
                   href=\"http://www.utdallas.edu/locator/";
                // line 9
                echo twig_escape_filter($this->env, ((((twig_get_attribute($this->env, $this->source, $context["room"], "building", []) . "_") . twig_get_attribute($this->env, $this->source, $context["room"], "floor", [])) . ".") . twig_get_attribute($this->env, $this->source, $context["room"], "number", [])), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, ((((twig_get_attribute($this->env, $this->source, $context["room"], "building", []) . " ") . twig_get_attribute($this->env, $this->source, $context["room"], "floor", [])) . ".") . twig_get_attribute($this->env, $this->source, $context["room"], "number", [])), "html", null, true);
                echo "</a>
            </div>
            <div class=\"content_list_info\">";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["room"], "description", []), "html", null, true);
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['room'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "    ";
        } else {
            // line 14
            echo "    ";
        }
        // line 15
        echo "</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/home/locations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 15,  64 => 14,  61 => 13,  53 => 11,  46 => 9,  42 => 7,  37 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content_head\">
    <h2 class=\"content_title\">Locations</h2>
</div>
<div class=\"content_list\">
    {% if rooms|length > 0 %}
        {% for room in rooms %}
            <div class=\"content_list_label\">
                <a class=\"room_link\" target=\"_blank\"
                   href=\"http://www.utdallas.edu/locator/{{ room.building ~ '_' ~ room.floor ~ '.' ~ room.number }}\">{{ room.building ~ ' ' ~ room.floor ~ '.' ~ room.number }}</a>
            </div>
            <div class=\"content_list_info\">{{ room.description }}</div>
        {% endfor %}
    {% else %}
    {% endif %}
</div>
", "shared/home/locations.html.twig", "/code/templates/shared/home/locations.html.twig");
    }
}
