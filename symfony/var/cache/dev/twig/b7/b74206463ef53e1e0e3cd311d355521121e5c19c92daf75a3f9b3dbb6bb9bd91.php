<?php

/* role/student/session/history_by_time.html.twig */
class __TwigTemplate_733d000c6eaf630416d0beadd0fe5c49218489588cb390c2fb7cf977556fb6f2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/student/session/history_by_time.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/history_by_time.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/history_by_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <h2>Scheduled Sessions</h2>
    <table id=\"sessions\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
            <th>Grade</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 15, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
            // line 16
            echo "            <tr>
                <td>
                    ";
            // line 19
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "sections", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 20
                echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                // line 21
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 22
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                </td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []), "m/d/y g:i A"), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", [])) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []), "m/d/y g:i A")) : ("")), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 30
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "graded", [])) {
                // line 31
                echo "                        ";
                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []))) {
                    // line 32
                    echo "                            ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "numericGrade", [])) {
                        // line 33
                        echo "                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []), "html", null, true);
                        echo "
                            ";
                    } else {
                        // line 35
                        echo "                                ";
                        echo ((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", [])) ? ("Pass") : ("Fail"));
                        echo "
                            ";
                    }
                    // line 37
                    echo "                        ";
                } else {
                    // line 38
                    echo "                            Pending
                        ";
                }
                // line 40
                echo "                    ";
            } else {
                // line 41
                echo "                        N/A
                    ";
            }
            // line 43
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </tbody>
    </table>
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
            <th>Grade</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 60, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
            // line 61
            echo "            <tr>
                <td>
                    ";
            // line 64
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "session", []), "sections", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 65
                echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                // line 66
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 67
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                </td>
                <td>";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "session", []), "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 72
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []), "m/d/y g:i A"), "html", null, true);
            echo "</td>
                <td>";
            // line 73
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", [])) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []), "m/d/y g:i A")) : ("")), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 75
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "graded", [])) {
                // line 76
                echo "                        ";
                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []))) {
                    // line 77
                    echo "                            ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeSlot", []), "session", []), "numericGrade", [])) {
                        // line 78
                        echo "                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", []), "html", null, true);
                        echo "
                            ";
                    } else {
                        // line 80
                        echo "                                ";
                        echo ((twig_get_attribute($this->env, $this->source, $context["attendance"], "grade", [])) ? ("Pass") : ("Fail"));
                        echo "
                            ";
                    }
                    // line 82
                    echo "                        ";
                } else {
                    // line 83
                    echo "                            Pending
                        ";
                }
                // line 85
                echo "                    ";
            } else {
                // line 86
                echo "                        N/A
                    ";
            }
            // line 88
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "        </tbody>
    </table>
    <h2>Walk-In Sessions</h2>
    <table id=\"walkins\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Course</th>
            <th>Topic</th>
            <th>Activity</th>
            <th>Mentors</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 106
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["walkins"]) || array_key_exists("walkins", $context) ? $context["walkins"] : (function () { throw new Twig_Error_Runtime('Variable "walkins" does not exist.', 106, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
            // line 107
            echo "            <tr>
                <td>
                    ";
            // line 109
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "course", []), "number", []), "html", null, true);
            echo "
                </td>
                <td>";
            // line 111
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 112
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "activity", []), "name", []), "html", null, true);
            echo "</td>
                <td>";
            // line 113
            echo twig_escape_filter($this->env, twig_join_filter(twig_get_attribute($this->env, $this->source, $context["attendance"], "mentors", []), ", "), "html", null, true);
            echo "</td>
                <td>";
            // line 114
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []), "m/d/y g:i A"), "html", null, true);
            echo "</td>
                <td>";
            // line 115
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", [])) ? (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []), "m/d/y g:i A")) : ("")), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 121
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 122
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            \$('#sessions').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            \$('#quizzes').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            \$('#walkins').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/student/session/history_by_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  368 => 122,  359 => 121,  347 => 118,  338 => 115,  334 => 114,  330 => 113,  326 => 112,  322 => 111,  315 => 109,  311 => 107,  307 => 106,  290 => 91,  282 => 88,  278 => 86,  275 => 85,  271 => 83,  268 => 82,  262 => 80,  256 => 78,  253 => 77,  250 => 76,  248 => 75,  243 => 73,  239 => 72,  235 => 71,  232 => 70,  217 => 67,  215 => 66,  213 => 65,  195 => 64,  191 => 61,  187 => 60,  171 => 46,  163 => 43,  159 => 41,  156 => 40,  152 => 38,  149 => 37,  143 => 35,  137 => 33,  134 => 32,  131 => 31,  129 => 30,  124 => 28,  120 => 27,  116 => 26,  113 => 25,  98 => 22,  96 => 21,  94 => 20,  76 => 19,  72 => 16,  68 => 15,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <h2>Scheduled Sessions</h2>
    <table id=\"sessions\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
            <th>Grade</th>
        </tr>
        </thead>
        <tbody>
        {% for attendance in sessions %}
            <tr>
                <td>
                    {# TODO change to only student's section #}
                    {% for section in attendance.timeSlot.session.sections %}
                        {{- section -}}
                        {%- if not loop.last -%}
                            {{- \", \" -}}
                        {%- endif -%}
                    {% endfor %}
                </td>
                <td>{{ attendance.timeSlot.session.topic }}</td>
                <td>{{ attendance.timeIn|date('m/d/y g:i A') }}</td>
                <td>{{ attendance.timeOut ? attendance.timeOut|date('m/d/y g:i A') }}</td>
                <td>
                    {% if attendance.timeSlot.session.graded %}
                        {% if attendance.grade is not empty %}
                            {% if attendance.timeSlot.session.numericGrade %}
                                {{ attendance.grade }}
                            {% else %}
                                {{ attendance.grade ? 'Pass' : 'Fail' }}
                            {% endif %}
                        {% else %}
                            Pending
                        {% endif %}
                    {% else %}
                        N/A
                    {% endif %}
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
            <th>Grade</th>
        </tr>
        </thead>
        <tbody>
        {% for attendance in quizzes %}
            <tr>
                <td>
                    {# TODO change to only student's section #}
                    {% for section in attendance.session.sections %}
                        {{- section -}}
                        {%- if not loop.last -%}
                            {{- \", \" -}}
                        {%- endif -%}
                    {% endfor %}
                </td>
                <td>{{ attendance.session.topic }}</td>
                <td>{{ attendance.timeIn|date('m/d/y g:i A') }}</td>
                <td>{{ attendance.timeOut ? attendance.timeOut|date('m/d/y g:i A') }}</td>
                <td>
                    {% if attendance.timeSlot.session.graded %}
                        {% if attendance.grade is not empty %}
                            {% if attendance.timeSlot.session.numericGrade %}
                                {{ attendance.grade }}
                            {% else %}
                                {{ attendance.grade ? 'Pass' : 'Fail' }}
                            {% endif %}
                        {% else %}
                            Pending
                        {% endif %}
                    {% else %}
                        N/A
                    {% endif %}
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <h2>Walk-In Sessions</h2>
    <table id=\"walkins\" class=\"table table-striped table-bordered\">
        <thead>
        <tr>
            <th>Course</th>
            <th>Topic</th>
            <th>Activity</th>
            <th>Mentors</th>
            <th>Check In Time</th>
            <th>Check Out Time</th>
        </tr>
        </thead>
        <tbody>
        {% for attendance in walkins %}
            <tr>
                <td>
                    {{ attendance.course.department.abbreviation }} {{ attendance.course.number }}
                </td>
                <td>{{ attendance.topic }}</td>
                <td>{{ attendance.activity.name }}</td>
                <td>{{ attendance.mentors|join(', ') }}</td>
                <td>{{ attendance.timeIn|date('m/d/y g:i A') }}</td>
                <td>{{ attendance.timeOut ? attendance.timeOut|date('m/d/y g:i A') }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            \$('#sessions').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            \$('#quizzes').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            \$('#walkins').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
        });
    </script>
{% endblock %}", "role/student/session/history_by_time.html.twig", "/code/templates/role/student/session/history_by_time.html.twig");
    }
}
