<?php

/* role/mentor/occurrence_submission.twig */
class __TwigTemplate_014fa17e1312d36b457490428bbcd1e199f6482bd8065d154b062da031b14933 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/occurrence_submission.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/occurrence_submission.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/occurrence_submission.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <style>
        .form-control.textarea {
            resize: none;
            height: 200px;
        }
        .attr-field.required .field-label:after {
            content: \"*\";
            color: red;
        }
    </style>
    <h1>Occurrence Submission</h1>
    <hr/>
    <div class=\"\">
        <div class=\"container\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 18, $this->source); })()), 'form_start', ["attr" => ["class" => "form-group"]]);
        echo "
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->source); })()), "subject", []), 'label');
        echo ":
                        </span>
                        ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 23, $this->source); })()), "subject", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>
                <br/>
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 28, $this->source); })()), "type", []), 'label');
        echo ":
                        </span>
                        ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "type", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                </div>
                <br/>
                <div class=\"attr-field\">
                        <span class=\"field-label\">
                            ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 35, $this->source); })()), "details", []), 'label');
        echo ":
                        </span>
                        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 37, $this->source); })()), "details", []), 'widget', ["attr" => ["class" => "form-control textarea"]]);
        echo "
                        <div align=\"right\">
                            <div id=\"words\">200 words</div>
                        </div>
                </div>
                <br/>
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            <label for=\"form_dateOfOccurrence\">Time and Date of Occurrence:</label>
                        </span>
                        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 47, $this->source); })()), "dateOfOccurrence", []), 'widget', ["attr" => ["class" => "form-control js-datepicker"]]);
        echo "
                </div>
                <br/>
                <div class=\"attr-field\">
                        <span class=\"field-label\">
                            <label id=\"submitting-as\" for=\"form_anonymous\">Submitting as: Anonymous</label>
                        </span>
                        <p>
                            ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 55, $this->source); })()), "anonymous", []), 'widget', ["attr" => ["class" => "checkbox-inline", "label" => "ddd", "checked" => "checked"]]);
        echo "
                            I choose to submit anonymously.
                        </p>
                </div>
                <br/>
                <div class=\"attr-field container\">
                    ";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 61, $this->source); })()), "submit", []), 'label');
        echo "
                    ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 62, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-info btn-ok"]]);
        echo "
                    <a href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"
                        class=\"btn btn-default\">
                        Dismiss
                    </a>
                </div>
            ";
        // line 68
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 68, $this->source); })()), 'form_end');
        echo "
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 72
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 73
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(document).ready(function () {
            var textarea = document.getElementById(\"form_details\");
            textarea.addEventListener(\"input\", function(){
                var wom = this.value.match(/\\S+/g);
                var wordsCount = wom ? wom.length : 0;
                var rem = 200 - wordsCount;
                var remaining = rem < 0 ? 0 : rem;
                \$(\"#words\").text(remaining + \" words\");
                \$(\"#words\").css(\"color\", \"black\");
                var handle = \$(\"#words\").data('handle');
                if (handle) {
                    clearInterval(handle);
                    \$(\"#words\").data('handle', null);
                    \$(\"#words\").css('visibility', 'inherit');
                }
                if (remaining == 0 || this.value.length >= 1000) {
                    \$(\"#form_submit\").attr(\"disabled\", true);
                    \$(\"#words\").text(\"Too long text!\");
                    \$(\"#words\").css(\"color\", \"red\");
                    var handle = setInterval(function() {
                        if (\$(\"#words\").css(\"visibility\") === \"visible\") {
                            \$(\"#words\").css('visibility', 'hidden');
                        } else {
                            \$(\"#words\").css('visibility', 'visible');
                        }
                    }, 400);
                    \$(\"#words\").data('handle', handle);
                } else {
                    \$(\"#form_submit\").attr(\"disabled\", false);
                }
            });
            jQuery.datetimepicker.setLocale('en');
            var dt = new Date();
            var currtime = dt.getHours() + \":\" + (dt.getMinutes() >= 30 ? 30 : 0);
            \$(\"#form_dateOfOccurrence\").datetimepicker({
                roundTime:'floor',
                defaultTime:currtime,
                allowTimes:[
                    '06:00',
                    '06:30',
                    '07:00',
                    '07:30',
                    '08:00',
                    '08:30',
                    '09:00',
                    '09:30',
                    '10:00',
                    '10:30',
                    '11:00',
                    '11:30',
                    '12:00',
                    '12:30',
                    '13:00',
                    '13:30',
                    '14:00',
                    '14:30',
                    '15:00',
                    '15:30',
                    '16:00',
                    '16:30',
                    '17:00',
                    '17:30',
                    '18:00',
                    '18:30',
                    '19:00',
                    '19:30',
                    '20:00',
                    '20:30',
                    '21:00',
                    '21:30',
                    '22:00'
                ]
            });
            var currentdate = new Date(); 
            var datetime = (currentdate.getMonth() + 1) + \"/\"
                + currentdate.getDate() + \"/\" 
                + currentdate.getFullYear() + \" - \"  
                + currentdate.getHours() + \":\"  
                + currentdate.getMinutes();
            \$(\"#date\").text(datetime);
            \$(\"#form_anonymous\").on('change', function() {
                if (\$(this).is(\":checked\")) {
                    \$(\"#submitting-as\").text(\"Submitting as: Anonymous\");
                } else {
                    \$(\"#submitting-as\").text(\"Submitting as: ";
        // line 159
        echo twig_escape_filter($this->env, (isset($context["displayName"]) || array_key_exists("displayName", $context) ? $context["displayName"] : (function () { throw new Twig_Error_Runtime('Variable "displayName" does not exist.', 159, $this->source); })()), "html", null, true);
        echo "\");
                }
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/occurrence_submission.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 159,  179 => 73,  170 => 72,  156 => 68,  148 => 63,  144 => 62,  140 => 61,  131 => 55,  120 => 47,  107 => 37,  102 => 35,  94 => 30,  89 => 28,  81 => 23,  76 => 21,  70 => 18,  54 => 4,  45 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"shared/base.html.twig\" %}

{% block body %}
    <style>
        .form-control.textarea {
            resize: none;
            height: 200px;
        }
        .attr-field.required .field-label:after {
            content: \"*\";
            color: red;
        }
    </style>
    <h1>Occurrence Submission</h1>
    <hr/>
    <div class=\"\">
        <div class=\"container\">
            {{ form_start(form, {\"attr\": {\"class\": \"form-group\"}}) }}
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            {{ form_label(form.subject) }}:
                        </span>
                        {{ form_widget(form.subject, {\"attr\": {\"class\": \"form-control\"}}) }}
                </div>
                <br/>
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            {{ form_label(form.type) }}:
                        </span>
                        {{ form_widget(form.type, {\"attr\": {\"class\": \"form-control\"}}) }}
                </div>
                <br/>
                <div class=\"attr-field\">
                        <span class=\"field-label\">
                            {{ form_label(form.details) }}:
                        </span>
                        {{ form_widget(form.details, {\"attr\": {\"class\": \"form-control textarea\"}}) }}
                        <div align=\"right\">
                            <div id=\"words\">200 words</div>
                        </div>
                </div>
                <br/>
                <div class=\"attr-field required\">
                        <span class=\"field-label\">
                            <label for=\"form_dateOfOccurrence\">Time and Date of Occurrence:</label>
                        </span>
                        {{ form_widget(form.dateOfOccurrence, {\"attr\": {\"class\": \"form-control js-datepicker\"}}) }}
                </div>
                <br/>
                <div class=\"attr-field\">
                        <span class=\"field-label\">
                            <label id=\"submitting-as\" for=\"form_anonymous\">Submitting as: Anonymous</label>
                        </span>
                        <p>
                            {{ form_widget(form.anonymous, {\"attr\": {\"class\": \"checkbox-inline\", \"label\" : \"ddd\", \"checked\":\"checked\"}}) }}
                            I choose to submit anonymously.
                        </p>
                </div>
                <br/>
                <div class=\"attr-field container\">
                    {{ form_label(form.submit) }}
                    {{ form_widget(form.submit, {\"attr\": {\"class\": \"btn btn-info btn-ok\"}}) }}
                    <a href=\"{{ path(\"home\") }}\"
                        class=\"btn btn-default\">
                        Dismiss
                    </a>
                </div>
            {{ form_end(form) }}
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(document).ready(function () {
            var textarea = document.getElementById(\"form_details\");
            textarea.addEventListener(\"input\", function(){
                var wom = this.value.match(/\\S+/g);
                var wordsCount = wom ? wom.length : 0;
                var rem = 200 - wordsCount;
                var remaining = rem < 0 ? 0 : rem;
                \$(\"#words\").text(remaining + \" words\");
                \$(\"#words\").css(\"color\", \"black\");
                var handle = \$(\"#words\").data('handle');
                if (handle) {
                    clearInterval(handle);
                    \$(\"#words\").data('handle', null);
                    \$(\"#words\").css('visibility', 'inherit');
                }
                if (remaining == 0 || this.value.length >= 1000) {
                    \$(\"#form_submit\").attr(\"disabled\", true);
                    \$(\"#words\").text(\"Too long text!\");
                    \$(\"#words\").css(\"color\", \"red\");
                    var handle = setInterval(function() {
                        if (\$(\"#words\").css(\"visibility\") === \"visible\") {
                            \$(\"#words\").css('visibility', 'hidden');
                        } else {
                            \$(\"#words\").css('visibility', 'visible');
                        }
                    }, 400);
                    \$(\"#words\").data('handle', handle);
                } else {
                    \$(\"#form_submit\").attr(\"disabled\", false);
                }
            });
            jQuery.datetimepicker.setLocale('en');
            var dt = new Date();
            var currtime = dt.getHours() + \":\" + (dt.getMinutes() >= 30 ? 30 : 0);
            \$(\"#form_dateOfOccurrence\").datetimepicker({
                roundTime:'floor',
                defaultTime:currtime,
                allowTimes:[
                    '06:00',
                    '06:30',
                    '07:00',
                    '07:30',
                    '08:00',
                    '08:30',
                    '09:00',
                    '09:30',
                    '10:00',
                    '10:30',
                    '11:00',
                    '11:30',
                    '12:00',
                    '12:30',
                    '13:00',
                    '13:30',
                    '14:00',
                    '14:30',
                    '15:00',
                    '15:30',
                    '16:00',
                    '16:30',
                    '17:00',
                    '17:30',
                    '18:00',
                    '18:30',
                    '19:00',
                    '19:30',
                    '20:00',
                    '20:30',
                    '21:00',
                    '21:30',
                    '22:00'
                ]
            });
            var currentdate = new Date(); 
            var datetime = (currentdate.getMonth() + 1) + \"/\"
                + currentdate.getDate() + \"/\" 
                + currentdate.getFullYear() + \" - \"  
                + currentdate.getHours() + \":\"  
                + currentdate.getMinutes();
            \$(\"#date\").text(datetime);
            \$(\"#form_anonymous\").on('change', function() {
                if (\$(this).is(\":checked\")) {
                    \$(\"#submitting-as\").text(\"Submitting as: Anonymous\");
                } else {
                    \$(\"#submitting-as\").text(\"Submitting as: {{ displayName }}\");
                }
            });
        });
    </script>
{% endblock %}", "role/mentor/occurrence_submission.twig", "/code/templates/role/mentor/occurrence_submission.twig");
    }
}
