<?php

/* role/admin/component/nav-old.html.twig */
class __TwigTemplate_f8998268ace085599771f1c4973a3297379695fd9c6b1212aaac388bc4d40c61 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/component/nav-old.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/component/nav-old.html.twig"));

        // line 1
        echo "<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a href=\"index.html\" class=\"site_title\"><i class=\"fas fa-desktop\"></i> <span>CSMC Admin!</span></a>
        </div>

        <div class=\"clearfix\"></div>

        ";
        // line 10
        echo "        ";
        // line 11
        echo "        ";
        // line 12
        echo "        ";
        // line 13
        echo "        ";
        // line 14
        echo "        ";
        // line 15
        echo "        ";
        // line 16
        echo "        ";
        // line 17
        echo "        ";
        // line 18
        echo "        ";
        // line 19
        echo "        ";
        // line 20
        echo "        ";
        // line 21
        echo "
        ";
        // line 23
        echo "
        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                ";
        // line 28
        echo "                <ul class=\"nav side-menu\">
                    <li>
                        <a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><i class=\"fas fa-home\"></i> Home</a>
                    </li>

                    <li>
                        <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_announcement_list");
        echo "\"><i class=\"fas fa-bullhorn\"></i> Announcements</a>
                    </li>

                    <li><a><i class=\"fas fa-edit\"></i> System Set Up <span class=\"fas fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_ip_list");
        echo "\">IPs</a></li>
                            <li><a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_list");
        echo "\">Rooms</a></li>
                            <li><a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_semester_list");
        echo "\">Semesters</a></li>
                            <li><a href=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_department_list");
        echo "\">Departments</a></li>
                            <li><a href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list");
        echo "\">Courses</a></li>
                            <li><a href=\"";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_list");
        echo "\">Sections</a></li>
                            <li><a href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_hours_list");
        echo "\">Operation Hours</a></li>
                            ";
        // line 47
        echo "                        </ul>
                    </li>

                    <li><a><i class=\"fas fa-users\"></i> Users <span class=\"fas fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list");
        echo "\">User</a></li>
                            <li><a href=\"";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_group_list");
        echo "\">User Groups</a></li>
                            <li><a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_role_list");
        echo "\">Roles</a></li>
                        </ul>
                    </li>

                    <li><a>
                            <i class=\"fas fa-calendar-alt\"></i> Sessions ";
        // line 60
        echo "                            <span class=\"fas fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_calendar");
        echo "\">Calendar</a></li>
                            <li><a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests");
        echo "\">Requests</a></li>
                            <li><a href=\"";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_holiday_list");
        echo "\">Holidays</a></li>
                        </ul>
                    </li>

                    <li>
                        <a>
                            <i class=\"fas fa-user-clock\"></i> Staffing <span class=\"fas fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_schedule_calendar");
        echo "\">Schedule</a></li>
                            <li><a href=\"";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_schedule_timesheets");
        echo "\">Timesheets</a></li>
                            <li><a href=\"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_schedule_absences");
        echo "\">Absences</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fas fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                    <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                       aria-expanded=\"false\">
                        <img src=\"images/img.jpg\" alt=\"\">CSMC Admin
                        <span class=\" fas fa-angle-down\"></span>
                    </a>
                    <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                        <li><a href=\"javascript:;\"> Profile</a></li>
                        <li>
                            <a href=\"javascript:;\">
                                <span class=\"badge bg-red pull-right\">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href=\"javascript:;\">Help</a></li>
                        <li><a href=\"login.html\"><i class=\"fas fa-sign-out pull-right\"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/component/nav-old.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 76,  169 => 75,  165 => 74,  153 => 65,  149 => 64,  145 => 63,  140 => 60,  132 => 54,  128 => 53,  124 => 52,  117 => 47,  113 => 45,  109 => 44,  105 => 43,  101 => 42,  97 => 41,  93 => 40,  89 => 39,  81 => 34,  74 => 30,  70 => 28,  64 => 23,  61 => 21,  59 => 20,  57 => 19,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  47 => 14,  45 => 13,  43 => 12,  41 => 11,  39 => 10,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a href=\"index.html\" class=\"site_title\"><i class=\"fas fa-desktop\"></i> <span>CSMC Admin!</span></a>
        </div>

        <div class=\"clearfix\"></div>

        {#<!-- menu profile quick info -->#}
        {#<div class=\"profile clearfix\">#}
        {#<div class=\"profile_pic\">#}
        {#<img src=\"images/img.jpg\" alt=\"...\" class=\"img-circle profile_img\">#}
        {#</div>#}
        {#<div class=\"profile_info\">#}
        {#<span>Welcome,</span>#}
        {#<h2>John Doe</h2>#}
        {#</div>#}
        {#<div class=\"clearfix\"></div>#}
        {#</div>#}
        {#<!-- /menu profile quick info -->#}

        {#<br />#}

        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                {#<h3>General</h3>#}
                <ul class=\"nav side-menu\">
                    <li>
                        <a href=\"{{ path('home') }}\"><i class=\"fas fa-home\"></i> Home</a>
                    </li>

                    <li>
                        <a href=\"{{ path('admin_announcement_list') }}\"><i class=\"fas fa-bullhorn\"></i> Announcements</a>
                    </li>

                    <li><a><i class=\"fas fa-edit\"></i> System Set Up <span class=\"fas fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_ip_list') }}\">IPs</a></li>
                            <li><a href=\"{{ path('admin_room_list') }}\">Rooms</a></li>
                            <li><a href=\"{{ path('admin_semester_list') }}\">Semesters</a></li>
                            <li><a href=\"{{ path('admin_department_list') }}\">Departments</a></li>
                            <li><a href=\"{{ path('admin_course_list') }}\">Courses</a></li>
                            <li><a href=\"{{ path('admin_section_list') }}\">Sections</a></li>
                            <li><a href=\"{{ path('admin_hours_list') }}\">Operation Hours</a></li>
                            {#<li><a href=\"{{ path('admin_schedule_list') }}\">Schedule Time</a></li>#}
                        </ul>
                    </li>

                    <li><a><i class=\"fas fa-users\"></i> Users <span class=\"fas fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_user_list') }}\">User</a></li>
                            <li><a href=\"{{ path('admin_user_group_list') }}\">User Groups</a></li>
                            <li><a href=\"{{ path('admin_role_list') }}\">Roles</a></li>
                        </ul>
                    </li>

                    <li><a>
                            <i class=\"fas fa-calendar-alt\"></i> Sessions {#<span class=\"badge bg-green\">6</span>#}
                            <span class=\"fas fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_session_calendar') }}\">Calendar</a></li>
                            <li><a href=\"{{ path('admin_session_requests') }}\">Requests</a></li>
                            <li><a href=\"{{ path('admin_holiday_list') }}\">Holidays</a></li>
                        </ul>
                    </li>

                    <li>
                        <a>
                            <i class=\"fas fa-user-clock\"></i> Staffing <span class=\"fas fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_schedule_calendar') }}\">Schedule</a></li>
                            <li><a href=\"{{ path('admin_schedule_timesheets') }}\">Timesheets</a></li>
                            <li><a href=\"{{ path('admin_schedule_absences') }}\">Absences</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fas fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                    <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                       aria-expanded=\"false\">
                        <img src=\"images/img.jpg\" alt=\"\">CSMC Admin
                        <span class=\" fas fa-angle-down\"></span>
                    </a>
                    <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                        <li><a href=\"javascript:;\"> Profile</a></li>
                        <li>
                            <a href=\"javascript:;\">
                                <span class=\"badge bg-red pull-right\">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href=\"javascript:;\">Help</a></li>
                        <li><a href=\"login.html\"><i class=\"fas fa-sign-out pull-right\"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->", "role/admin/component/nav-old.html.twig", "/code/templates/role/admin/component/nav-old.html.twig");
    }
}
