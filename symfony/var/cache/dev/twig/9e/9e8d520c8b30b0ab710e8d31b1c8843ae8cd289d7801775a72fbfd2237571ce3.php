<?php

/* role/admin/user_group/list.html.twig */
class __TwigTemplate_afa70ad95a88cfa129e3fb96f8333dee04e045330a6d9c0bb8642baf23899e05 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/user_group/list.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/user_group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "


    <div class=\"clearfix\"></div>


    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">

                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>User Groups
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./userGroupAdd'\"
                                    style=\"float:right\">New User Group
                            </button>
                        </h3>
                    </div>
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>User Group Name</th>
                            <th>Description</th>
                            <th>Edit</th>

                        </tr>
                        </thead>


                        <tbody>
                        ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userGroups"]) || array_key_exists("userGroups", $context) ? $context["userGroups"] : (function () { throw new Twig_Error_Runtime('Variable "userGroups" does not exist.', 39, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 40
            echo "                            <tr>
                                <td>";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "name", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "description", []), "html", null, true);
            echo "</td>
                                <td>

                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./userGroupEdit?id=";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["r"], "id", []), "html", null, true);
            echo "'\">Edit
                                    </button>
                                </td>


                            </tr>

                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "
                    </table>

                </div>

            </div>
        </div>
    </div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/user_group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 54,  109 => 46,  102 => 42,  98 => 41,  95 => 40,  91 => 39,  53 => 3,  44 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}



    <div class=\"clearfix\"></div>


    <div class=\"row\">
        <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">

                <div class=\"x_title\">
                    <div class=\"title_left\">
                        <h3>User Groups
                            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='./userGroupAdd'\"
                                    style=\"float:right\">New User Group
                            </button>
                        </h3>
                    </div>
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"x_content\">
                    <p class=\"text-muted font-13 m-b-30\"></p>
                    <table id=\"datatable\" class=\"table table-striped table-bordered\">
                        <thead>
                        <tr>
                            <th>User Group Name</th>
                            <th>Description</th>
                            <th>Edit</th>

                        </tr>
                        </thead>


                        <tbody>
                        {% for r in userGroups %}
                            <tr>
                                <td>{{ r.name }}</td>
                                <td>{{ r.description }}</td>
                                <td>

                                    <button type=\"button\" class=\"btn btn-round btn-success\"
                                            onclick=\"location.href='./userGroupEdit?id={{ r.id }}'\">Edit
                                    </button>
                                </td>


                            </tr>

                        {% endfor %}

                    </table>

                </div>

            </div>
        </div>
    </div>



{% endblock %}
     
     
     
", "role/admin/user_group/list.html.twig", "/code/templates/role/admin/user_group/list.html.twig");
    }
}
