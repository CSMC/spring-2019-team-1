<?php

/* role/admin/session/calendar.html.twig */
class __TwigTemplate_ee09ee1e7f13d9fb9b97ec6125b9e51ed7421a020a1a913cb18d9802fe3e57a4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/session/calendar.html.twig", 1);
        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/calendar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/session/calendar.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.3/scheduler.min.css\">

    <style>
        /*#row-main {*/
        /*overflow-x: hidden;*/
        /*}*/

        #content {
            transition: width 0.3s ease;
        }

        #sidebar {
            transition: margin 0.3s ease;
        }

        .collapsed {
            display: none;
        }

        .no-overflow {
            /*white-space: nowrap;*/
            /*overflow: hidden;*/
            /*text-overflow: ellipsis;*/
        }

        /*@media (min-width: 992px) {*/
        /*.collapsed {*/
        /*display: block;*/
        /*!* same width as sidebar *!*/
        /*margin-right: -25%;*/
        /*}*/
        /*}*/

        .row-eq-height {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 48
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 49
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.3/scheduler.min.js\"></script>
    <script type=\"text/javascript\">
        \$(function () {
            \$('#sidebar-tdocker-compose downoggle').on('click', function () {
                \$(\"#sidebar\").toggleClass(\"collapsed\");
                \$(\"#content\").toggleClass(\"col-md-12 col-md-10\");
            });

            \$('#create-session').on('click', function () {
                \$('#session-modal').modal('show');
            });

            \$('#external-events .fc-event').each(function () {
                // store data so the calendar knows to render an event upon drop
                // \$(this).data('event', {
                //     title: \$.trim(\$(this).text()),
                //     stick: true
                // });

                // make the event draggable using jQuery UI
                \$(this).draggable({
                    zIndex: 999,
                    revert: true,
                    revertDuration: 0
                });
            });

            // TODO implement
            var resolveColor = function (event) {
                return '';
            }

            // TODO separate out quiz
            var transformEvent = function (event) {
                // console.log(event);
                var e = {};
                if (event.hasOwnProperty('quiz')) {
                    e.allDay = true;
                    e.title = event.quiz.topic;
                } else if (event.hasOwnProperty('session')) {
                    e.allDay = false;
                    e.title = event.session.topic;
                    e.registrations = event.registrations.length;
                    e.capacity = event.capacity;
                    e.assignments = event.assignments;
                    for (var i in event.assignments) {
                        if (event.assignments[i].absence) {
                            e.borderColor = '#ff0000';
                        }
                    }

                    e.id = event.id;
                } else {
                    return e;
                }

                e.start = event.startTime;
                e.end = event.endTime;
                e.location = {};
                e.location.id = event.location.id;
                e.location.room = event.location.room;

                e.color = resolveColor(event);

                return e;
            };

            var \$cal = \$('#calendar');
            \$cal.fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                height: 'auto',
                defaultView: 'agendaWeek',
                minTime: '09:00:00',
                maxTime: '23:00:00',
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth next',
                },
                footer: false,
                slotDuration: '00:15:00',
                editable: true,
                droppable: true,
                dragRevertDuration: 0,
                drop: function (date) {
                    var count = \$(this).data('count') - 1;

                    var event = \$(this).data('event');
                    \$('#' + event.counter).text(count);

                    \$(this).data('count', count);
                    if (count == 0) {
                        \$(this).hide();
                        \$('#' + \$(this).data('counter')).hide();
                    }
                },
                eventReceive: function (event) {
                    \$.ajax({
                        url: \"";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_create_time_slot");
        echo "\",
                        data: {
                            \"time_slot\": {
                                \"start\": moment(event.start).format('YYYY-MM-DD HH:mm'),
                                \"end\": moment(event.end).format('YYYY-MM-DD HH:mm'),
                                \"location\": event.location.id,
                                \"session\": event.session,
                                \"capacity\": event.capacity,
                            }
                        },
                        type: 'POST'
                    }).done(function (data) {
                        event.id = data;
                        \$('#calendar').fullCalendar('updateEvent', event);
                    }).fail(function () {

                    });
                },
                eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                    \$.ajax({
                        url: \"";
        // line 170
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_edit_time_slot");
        echo "\",
                        data: {
                            \"time_slot\": {
                                \"id\": event.id,
                                \"start\": moment(event.start).format('YYYY-MM-DD HH:mm'),
                                \"end\": moment(event.end).format('YYYY-MM-DD HH:mm'),
                                \"location\": event.location.id,
                                \"session\": event.session,
                                \"capacity\": event.capacity,
                            }
                        },
                        type: 'POST'
                    }).done(function () {

                    }).fail(function () {
                        revertFunc();
                    })
                },
                // TODO eventDragStart/Stop disable popover
                eventDragStop: function (event, jsEvent, ui, view) {
                    // if (isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
                    //     \$('#calendar').fullCalendar('removeEvents', event._id);
                    // }
                },
                eventClick: function (event, jsEvent, view) {
                    \$('#timeslot-modal').modal();

                    \$('#timeslot-modal').data('event', event.id);

                    \$('#time_slot_date').val(moment(event.start).format('YYYY-MM-DD'));
                    \$('#time_slot_startTime').val(moment(event.start).format('HH:mm'));
                    \$('#time_slot_endTime').val(moment(event.end).format('HH:mm'));
                    \$('#time_slot_location').val(event.location.id).change();
                    \$('#time_slot_capacity').val(event.capacity);

                    \$('#time_slot_session').val(event.session);
                    \$('#time_slot_start').val(moment(event.start).format('YYYY-MM-DD HH:mm'));
                    \$('#time_slot_end').val(moment(event.end).format('YYYY-MM-DD HH:mm'));

                    \$('#time_slot_assignments').empty();

                    \$.ajax({
                        url: \"";
        // line 212
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_assignments");
        echo "\",
                        data: {
                            \"date\": moment(event.start).format('YYYY-MM-DD'),
                            \"time\": moment(event.start).format('HH:mm')
                        },
                        type: \"GET\",
                    }).done(function (data) {
                        for (var i in data) {
                            \$('#time_slot_assignments').append(\$('<option>', {value: data[i].id}).text(data[i].mentor));
                        }

                        for (var i in event.assignments) {
                            \$('#time_slot_assignments option[value=\"' + event.assignments[i].id + '\"]').prop('selected', true);
                            console.log(event.assignments[i]);
                        }
                    });
                },
                eventLimit: true,
                themeSystem: 'bootstrap3',
                bootstrapGlypicons: {
                    close: 'glyphicon-remove',
                    prev: 'glyphicon-chevron-left',
                    next: 'glyphicon-chevron-right',
                    prevYear: 'glyphicon-backward',
                    nextYear: 'glyphicon-forward'
                },
                businessHours: [
                    ";
        // line 239
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["hours"]) || array_key_exists("hours", $context) ? $context["hours"] : (function () { throw new Twig_Error_Runtime('Variable "hours" does not exist.', 239, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["day"]) {
            // line 240
            echo "                    {
                        dow: ['";
            // line 241
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["day"], "day", []), "html", null, true);
            echo "'],
                        start: '";
            // line 242
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["day"], "startTime", []), "H:i"), "html", null, true);
            echo "',
                        end: '";
            // line 243
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["day"], "endTime", []), "H:i"), "html", null, true);
            echo "'
                    },
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['day'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 246
        echo "                ],
                events: {
                    url: '";
        // line 248
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("events");
        echo "',
                    type: 'POST',
                    data: {
                        // put parameters such as filters here
                    },
                    success: function (events) {
                        // console.log(events);
                        // transform raw event data into something fullcalendar can use
                        for (var event in events) {
                            events[event] = transformEvent(events[event]);
                        }
                    }
                },
                eventRender: function (event, element) {
                    var view = \$('#calendar').fullCalendar('getView').type;
                    console.log(view);
                    if (view == 'listMonth') {
                        // console.log(element);
                        var descr = '<td class=\"fc-list-item-title\">'
                            + \"<strong>Location:</strong> \" + event.location.room
                            + '</td>';
                        element.find('.fc-list-item-title').append(descr);
                    } else if(view == 'month') {
                        element.find('.fc-title').remove();
                        element.find('.fc-time').remove();
                        var descr =
                            '<div class=\"no-overflow\"><strong>Title:</strong> ' + event.title + '</div>';

                        element.append(descr);
                    } else {
                        element.find('.fc-title').remove();
                        element.find('.fc-time').remove();
                        var descr =
                            '<div class=\"no-overflow\"><strong>Title:</strong> ' + event.title + '</div>'
                            + \"<strong>Location:</strong> \" + event.location.room + '<br>';
                        if (event.registrations != null) {
                            descr += \"<strong>Capacity:</strong> \" + (event.registrations ? event.registrations : 0) + '/' + event.capacity + '<br>';
                        }
                        // + \"<strong>Time:</strong> \"
                        // + moment(event.start).format(\"HH:mm\") + '-'
                        // + moment(event.end).format(\"HH:mm\") + '<br>';
                        if (event.hasOwnProperty('assignments')) {
                            var names = [];
                            for (var key in event.assignments) {
                                names[key] = event.assignments[key].mentor.firstName;
                            }

                            descr += \"<strong>Mentors:</strong> \" + names.join(\", \");
                        }
                        element.append(descr);

                        // element.popover({
                        //     title: event.title,
                        //     trigger: 'hover',
                        //     placement: 'top',
                        //     container: 'body'
                        // });
                    }
                }
            });

            \$('#timeslot-submit').on('click', function () {
                \$('[name=\"time_slot\"]').submit();
            });

            \$('[name=\"time_slot\"]').on('submit', function (e) {
                e.preventDefault();

                var form = \$('[name=\"time_slot\"]');

                var data = new FormData(this);
                var event = \$('#timeslot-modal').data(\"event\");

                data.append(\"id\", event);
                \$.ajax({
                    url: \$(this).prop('action'),
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    beforeSend: function () {
                        form.hide();
                        \$('#timeslot-loader').show();
                    }
                }).always(function () {
                    \$('#timeslot-loader').hide();
                    form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#timeslot-modal').modal('hide');
                    // need to update the event
                }).fail(function () {

                });
            });

            var updateTimeSlotForm = function() {
                \$.ajax({
                    url: \"";
        // line 345
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_assignments");
        echo "\",
                    data: {
                        \"date\": \$('#time_slot_date').val(),
                        \"time\": \$('#time_slot_startTime').val()
                    },
                    type: \"GET\",
                    beforeSend: function() {
                        \$('#time_slot_assignments').empty();
                    }
                }).done(function (data) {
                    for (var i in data) {
                        \$('#time_slot_assignments').append(\$('<option>', {value: data[i].id}).text(data[i].mentor));
                    }

                    for (var i in event.assignments) {
                        \$('#time_slot_assignments option[value=\"' + event.assignments[i].id + '\"]').prop('selected', true);
                        console.log(event.assignments[i]);
                    }
                });
            };
            \$('#time_slot_date').on('change', updateTimeSlotForm);
            \$('#time_slot_startTime').on('change', updateTimeSlotForm);
            \$('#time_slot_endTime').on('change', updateTimeSlotForm);
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 371
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 372
        echo "    <div class=\"row\">
        <div class=\"col-xs-7\">
            <h3>Calendar</h3>
        </div>
        <div class=\"col-xs-3\">
            <div class=\"btn-group\" role=\"group\">
                ";
        // line 379
        echo "                <a class=\"btn btn-default\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_create");
        echo "\">Create Session</a>
                <a id=\"view-requests\" href=\"";
        // line 380
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests");
        echo "\" class=\"btn btn-default\">View
                    Requests</a>
            </div>
        </div>
        <div class=\"col-xs-2\">
            <div class=\"input-group\">
                <button id=\"sidebar-toggle\" class=\"btn btn-default\" type=\"button\">Toggle Sidebar</button>
            </div>
        </div>
    </div>

    <div class=\"clearfix\"></div>

    <div class=\"container\">
        <div id=\"row-main\" class=\"row row-eq-height\">
            <div id=\"content\" class=\"col-md-10\">
                <div class=\"panel\">
                    <div class=\"panel-body\">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
            <div id=\"sidebar\" class=\"col-md-2\">
                <div class=\"affix\">
                    <div class=\"panel\">
                        <div class=\"panel-body\">
                            <h3>
                                <small>Unscheduled Sessions</small>
                            </h3>
                            <div id=\"external-events\">
                                ";
        // line 410
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 410, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
            // line 411
            echo "                                    ";
            $context["count"] = (twig_get_attribute($this->env, $this->source, $context["session"], "repeats", []) - twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "timeSlots", [])));
            // line 412
            echo "                                    <div id=\"external-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "id", []), "html", null, true);
            echo "\" class=\"fc-event label label-default\"
                                         style=\"background-color: ";
            // line 413
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "color", []), "html", null, true);
            echo ";
                                                 display: inline-block;
                                                 /*overflow:hidden;*/
                                                 /*text-overflow: ellipsis;*/
                                                 max-width: 120px\"
                                         data-event='{
                                                \"id\":\"temp-";
            // line 419
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "id", []), "html", null, true);
            echo "\",
                                                \"title\":\"";
            // line 420
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
            echo "\",
                                                \"color\":\"";
            // line 421
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "color", []), "html", null, true);
            echo "\",
                                                \"stick\":false,
                                                \"duration\":\"";
            // line 423
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "defaultDuration", []), "%H:%I"), "html", null, true);
            echo "\",
                                                \"capacity\":\"";
            // line 424
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "defaultCapacity", []), "html", null, true);
            echo "\",
                                                \"location\":{
                                                    \"id\":\"";
            // line 426
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["session"], "defaultLocation", []), "id", []), "html", null, true);
            echo "\",
                                                    \"room\":\"";
            // line 427
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "defaultLocation", []), "html", null, true);
            echo "\"
                                                    },
                                                \"session\":\"";
            // line 429
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "id", []), "html", null, true);
            echo "\"
                                                }'
                                         data-counter=\"counter-";
            // line 431
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "id", []), "html", null, true);
            echo "\"
                                         data-count=\"";
            // line 432
            echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 432, $this->source); })()), "html", null, true);
            echo "\">
                                        ";
            // line 433
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
            echo "
                                    </div>
                                    <span id=\"counter-";
            // line 435
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "id", []), "html", null, true);
            echo "\" class=\"badge\">";
            echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 435, $this->source); })()), "html", null, true);
            echo "</span>
                                    <br>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 438
        echo "                            </div>
                            ";
        // line 440
        echo "                            ";
        // line 441
        echo "                            ";
        // line 442
        echo "                            ";
        // line 443
        echo "                            ";
        // line 444
        echo "                            ";
        // line 445
        echo "                            ";
        // line 446
        echo "                            ";
        // line 447
        echo "                            ";
        // line 448
        echo "                            ";
        // line 449
        echo "                            ";
        // line 450
        echo "                            ";
        // line 451
        echo "                            ";
        // line 452
        echo "
                            ";
        // line 454
        echo "                            ";
        // line 455
        echo "                            ";
        // line 456
        echo "                            ";
        // line 457
        echo "                            ";
        // line 458
        echo "
                            ";
        // line 460
        echo "                            ";
        // line 461
        echo "                            ";
        // line 462
        echo "                            ";
        // line 463
        echo "                            ";
        // line 464
        echo "
                            ";
        // line 466
        echo "                            ";
        // line 467
        echo "                            ";
        // line 468
        echo "                            ";
        // line 469
        echo "                            ";
        // line 470
        echo "                            ";
        // line 471
        echo "                        </div>
                        ";
        // line 473
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id=\"timeslot-modal\" class=\"modal fade\" data-event>
        <div class=\"modal-dialog modal-lg\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span> <span
                                class=\"sr-only\">close</span></button>
                    <h4 class=\"modal-title\"></h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"timeslot-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"";
        // line 488
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                    </div>

                    ";
        // line 491
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 491, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 495
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 495, $this->source); })()), "date", []), 'label');
        echo "
                        </div>
                        ";
        // line 497
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 497, $this->source); })()), "date", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 499
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 499, $this->source); })()), "date", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 506
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 506, $this->source); })()), "startTime", []), 'label');
        echo "
                        </div>
                        ";
        // line 508
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 508, $this->source); })()), "startTime", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 510
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 510, $this->source); })()), "startTime", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 517
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 517, $this->source); })()), "endTime", []), 'label');
        echo "
                        </div>
                        ";
        // line 519
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 519, $this->source); })()), "endTime", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 521
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 521, $this->source); })()), "endTime", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 528
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 528, $this->source); })()), "location", []), 'label');
        echo "
                        </div>
                        ";
        // line 530
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 530, $this->source); })()), "location", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 532
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 532, $this->source); })()), "location", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 539
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 539, $this->source); })()), "capacity", []), 'label');
        echo "
                        </div>
                        ";
        // line 541
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 541, $this->source); })()), "capacity", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 543
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 543, $this->source); })()), "capacity", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            ";
        // line 550
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 550, $this->source); })()), "assignments", []), 'label');
        echo "
                        </div>
                        ";
        // line 552
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 552, $this->source); })()), "assignments", []), 'errors');
        echo "
                        <div class=\"bt-flabels__wrapper\">
                            ";
        // line 554
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 554, $this->source); })()), "assignments", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    ";
        // line 559
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 559, $this->source); })()), 'rest');
        echo "
                    ";
        // line 560
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["time_slot_form"]) || array_key_exists("time_slot_form", $context) ? $context["time_slot_form"] : (function () { throw new Twig_Error_Runtime('Variable "time_slot_form" does not exist.', 560, $this->source); })()), 'form_end');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button id=\"timeslot-submit\" type=\"button\" class=\"btn btn-success\">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/session/calendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  840 => 560,  836 => 559,  828 => 554,  823 => 552,  818 => 550,  808 => 543,  803 => 541,  798 => 539,  788 => 532,  783 => 530,  778 => 528,  768 => 521,  763 => 519,  758 => 517,  748 => 510,  743 => 508,  738 => 506,  728 => 499,  723 => 497,  718 => 495,  711 => 491,  705 => 488,  688 => 473,  685 => 471,  683 => 470,  681 => 469,  679 => 468,  677 => 467,  675 => 466,  672 => 464,  670 => 463,  668 => 462,  666 => 461,  664 => 460,  661 => 458,  659 => 457,  657 => 456,  655 => 455,  653 => 454,  650 => 452,  648 => 451,  646 => 450,  644 => 449,  642 => 448,  640 => 447,  638 => 446,  636 => 445,  634 => 444,  632 => 443,  630 => 442,  628 => 441,  626 => 440,  623 => 438,  612 => 435,  607 => 433,  603 => 432,  599 => 431,  594 => 429,  589 => 427,  585 => 426,  580 => 424,  576 => 423,  571 => 421,  567 => 420,  563 => 419,  554 => 413,  549 => 412,  546 => 411,  542 => 410,  509 => 380,  504 => 379,  496 => 372,  487 => 371,  451 => 345,  351 => 248,  347 => 246,  338 => 243,  334 => 242,  330 => 241,  327 => 240,  323 => 239,  293 => 212,  248 => 170,  225 => 150,  120 => 49,  111 => 48,  55 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'role/admin/base.html.twig' %}
{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.3/scheduler.min.css\">

    <style>
        /*#row-main {*/
        /*overflow-x: hidden;*/
        /*}*/

        #content {
            transition: width 0.3s ease;
        }

        #sidebar {
            transition: margin 0.3s ease;
        }

        .collapsed {
            display: none;
        }

        .no-overflow {
            /*white-space: nowrap;*/
            /*overflow: hidden;*/
            /*text-overflow: ellipsis;*/
        }

        /*@media (min-width: 992px) {*/
        /*.collapsed {*/
        /*display: block;*/
        /*!* same width as sidebar *!*/
        /*margin-right: -25%;*/
        /*}*/
        /*}*/

        .row-eq-height {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
        }
    </style>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.3/scheduler.min.js\"></script>
    <script type=\"text/javascript\">
        \$(function () {
            \$('#sidebar-tdocker-compose downoggle').on('click', function () {
                \$(\"#sidebar\").toggleClass(\"collapsed\");
                \$(\"#content\").toggleClass(\"col-md-12 col-md-10\");
            });

            \$('#create-session').on('click', function () {
                \$('#session-modal').modal('show');
            });

            \$('#external-events .fc-event').each(function () {
                // store data so the calendar knows to render an event upon drop
                // \$(this).data('event', {
                //     title: \$.trim(\$(this).text()),
                //     stick: true
                // });

                // make the event draggable using jQuery UI
                \$(this).draggable({
                    zIndex: 999,
                    revert: true,
                    revertDuration: 0
                });
            });

            // TODO implement
            var resolveColor = function (event) {
                return '';
            }

            // TODO separate out quiz
            var transformEvent = function (event) {
                // console.log(event);
                var e = {};
                if (event.hasOwnProperty('quiz')) {
                    e.allDay = true;
                    e.title = event.quiz.topic;
                } else if (event.hasOwnProperty('session')) {
                    e.allDay = false;
                    e.title = event.session.topic;
                    e.registrations = event.registrations.length;
                    e.capacity = event.capacity;
                    e.assignments = event.assignments;
                    for (var i in event.assignments) {
                        if (event.assignments[i].absence) {
                            e.borderColor = '#ff0000';
                        }
                    }

                    e.id = event.id;
                } else {
                    return e;
                }

                e.start = event.startTime;
                e.end = event.endTime;
                e.location = {};
                e.location.id = event.location.id;
                e.location.room = event.location.room;

                e.color = resolveColor(event);

                return e;
            };

            var \$cal = \$('#calendar');
            \$cal.fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                height: 'auto',
                defaultView: 'agendaWeek',
                minTime: '09:00:00',
                maxTime: '23:00:00',
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth next',
                },
                footer: false,
                slotDuration: '00:15:00',
                editable: true,
                droppable: true,
                dragRevertDuration: 0,
                drop: function (date) {
                    var count = \$(this).data('count') - 1;

                    var event = \$(this).data('event');
                    \$('#' + event.counter).text(count);

                    \$(this).data('count', count);
                    if (count == 0) {
                        \$(this).hide();
                        \$('#' + \$(this).data('counter')).hide();
                    }
                },
                eventReceive: function (event) {
                    \$.ajax({
                        url: \"{{ path('admin_session_create_time_slot') }}\",
                        data: {
                            \"time_slot\": {
                                \"start\": moment(event.start).format('YYYY-MM-DD HH:mm'),
                                \"end\": moment(event.end).format('YYYY-MM-DD HH:mm'),
                                \"location\": event.location.id,
                                \"session\": event.session,
                                \"capacity\": event.capacity,
                            }
                        },
                        type: 'POST'
                    }).done(function (data) {
                        event.id = data;
                        \$('#calendar').fullCalendar('updateEvent', event);
                    }).fail(function () {

                    });
                },
                eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                    \$.ajax({
                        url: \"{{ path('admin_session_edit_time_slot') }}\",
                        data: {
                            \"time_slot\": {
                                \"id\": event.id,
                                \"start\": moment(event.start).format('YYYY-MM-DD HH:mm'),
                                \"end\": moment(event.end).format('YYYY-MM-DD HH:mm'),
                                \"location\": event.location.id,
                                \"session\": event.session,
                                \"capacity\": event.capacity,
                            }
                        },
                        type: 'POST'
                    }).done(function () {

                    }).fail(function () {
                        revertFunc();
                    })
                },
                // TODO eventDragStart/Stop disable popover
                eventDragStop: function (event, jsEvent, ui, view) {
                    // if (isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
                    //     \$('#calendar').fullCalendar('removeEvents', event._id);
                    // }
                },
                eventClick: function (event, jsEvent, view) {
                    \$('#timeslot-modal').modal();

                    \$('#timeslot-modal').data('event', event.id);

                    \$('#time_slot_date').val(moment(event.start).format('YYYY-MM-DD'));
                    \$('#time_slot_startTime').val(moment(event.start).format('HH:mm'));
                    \$('#time_slot_endTime').val(moment(event.end).format('HH:mm'));
                    \$('#time_slot_location').val(event.location.id).change();
                    \$('#time_slot_capacity').val(event.capacity);

                    \$('#time_slot_session').val(event.session);
                    \$('#time_slot_start').val(moment(event.start).format('YYYY-MM-DD HH:mm'));
                    \$('#time_slot_end').val(moment(event.end).format('YYYY-MM-DD HH:mm'));

                    \$('#time_slot_assignments').empty();

                    \$.ajax({
                        url: \"{{ path('admin_session_assignments') }}\",
                        data: {
                            \"date\": moment(event.start).format('YYYY-MM-DD'),
                            \"time\": moment(event.start).format('HH:mm')
                        },
                        type: \"GET\",
                    }).done(function (data) {
                        for (var i in data) {
                            \$('#time_slot_assignments').append(\$('<option>', {value: data[i].id}).text(data[i].mentor));
                        }

                        for (var i in event.assignments) {
                            \$('#time_slot_assignments option[value=\"' + event.assignments[i].id + '\"]').prop('selected', true);
                            console.log(event.assignments[i]);
                        }
                    });
                },
                eventLimit: true,
                themeSystem: 'bootstrap3',
                bootstrapGlypicons: {
                    close: 'glyphicon-remove',
                    prev: 'glyphicon-chevron-left',
                    next: 'glyphicon-chevron-right',
                    prevYear: 'glyphicon-backward',
                    nextYear: 'glyphicon-forward'
                },
                businessHours: [
                    {% for day in hours %}
                    {
                        dow: ['{{ day.day }}'],
                        start: '{{ day.startTime|date('H:i') }}',
                        end: '{{ day.endTime|date('H:i') }}'
                    },
                    {% endfor %}
                ],
                events: {
                    url: '{{ path('events') }}',
                    type: 'POST',
                    data: {
                        // put parameters such as filters here
                    },
                    success: function (events) {
                        // console.log(events);
                        // transform raw event data into something fullcalendar can use
                        for (var event in events) {
                            events[event] = transformEvent(events[event]);
                        }
                    }
                },
                eventRender: function (event, element) {
                    var view = \$('#calendar').fullCalendar('getView').type;
                    console.log(view);
                    if (view == 'listMonth') {
                        // console.log(element);
                        var descr = '<td class=\"fc-list-item-title\">'
                            + \"<strong>Location:</strong> \" + event.location.room
                            + '</td>';
                        element.find('.fc-list-item-title').append(descr);
                    } else if(view == 'month') {
                        element.find('.fc-title').remove();
                        element.find('.fc-time').remove();
                        var descr =
                            '<div class=\"no-overflow\"><strong>Title:</strong> ' + event.title + '</div>';

                        element.append(descr);
                    } else {
                        element.find('.fc-title').remove();
                        element.find('.fc-time').remove();
                        var descr =
                            '<div class=\"no-overflow\"><strong>Title:</strong> ' + event.title + '</div>'
                            + \"<strong>Location:</strong> \" + event.location.room + '<br>';
                        if (event.registrations != null) {
                            descr += \"<strong>Capacity:</strong> \" + (event.registrations ? event.registrations : 0) + '/' + event.capacity + '<br>';
                        }
                        // + \"<strong>Time:</strong> \"
                        // + moment(event.start).format(\"HH:mm\") + '-'
                        // + moment(event.end).format(\"HH:mm\") + '<br>';
                        if (event.hasOwnProperty('assignments')) {
                            var names = [];
                            for (var key in event.assignments) {
                                names[key] = event.assignments[key].mentor.firstName;
                            }

                            descr += \"<strong>Mentors:</strong> \" + names.join(\", \");
                        }
                        element.append(descr);

                        // element.popover({
                        //     title: event.title,
                        //     trigger: 'hover',
                        //     placement: 'top',
                        //     container: 'body'
                        // });
                    }
                }
            });

            \$('#timeslot-submit').on('click', function () {
                \$('[name=\"time_slot\"]').submit();
            });

            \$('[name=\"time_slot\"]').on('submit', function (e) {
                e.preventDefault();

                var form = \$('[name=\"time_slot\"]');

                var data = new FormData(this);
                var event = \$('#timeslot-modal').data(\"event\");

                data.append(\"id\", event);
                \$.ajax({
                    url: \$(this).prop('action'),
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    beforeSend: function () {
                        form.hide();
                        \$('#timeslot-loader').show();
                    }
                }).always(function () {
                    \$('#timeslot-loader').hide();
                    form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#timeslot-modal').modal('hide');
                    // need to update the event
                }).fail(function () {

                });
            });

            var updateTimeSlotForm = function() {
                \$.ajax({
                    url: \"{{ path('admin_session_assignments') }}\",
                    data: {
                        \"date\": \$('#time_slot_date').val(),
                        \"time\": \$('#time_slot_startTime').val()
                    },
                    type: \"GET\",
                    beforeSend: function() {
                        \$('#time_slot_assignments').empty();
                    }
                }).done(function (data) {
                    for (var i in data) {
                        \$('#time_slot_assignments').append(\$('<option>', {value: data[i].id}).text(data[i].mentor));
                    }

                    for (var i in event.assignments) {
                        \$('#time_slot_assignments option[value=\"' + event.assignments[i].id + '\"]').prop('selected', true);
                        console.log(event.assignments[i]);
                    }
                });
            };
            \$('#time_slot_date').on('change', updateTimeSlotForm);
            \$('#time_slot_startTime').on('change', updateTimeSlotForm);
            \$('#time_slot_endTime').on('change', updateTimeSlotForm);
        });
    </script>
{% endblock %}
{% block body %}
    <div class=\"row\">
        <div class=\"col-xs-7\">
            <h3>Calendar</h3>
        </div>
        <div class=\"col-xs-3\">
            <div class=\"btn-group\" role=\"group\">
                {#<button id=\"create-session\" type=\"button\" class=\"btn btn-default\">Create Session</button>#}
                <a class=\"btn btn-default\" href=\"{{ path('admin_session_create') }}\">Create Session</a>
                <a id=\"view-requests\" href=\"{{ path('admin_session_requests') }}\" class=\"btn btn-default\">View
                    Requests</a>
            </div>
        </div>
        <div class=\"col-xs-2\">
            <div class=\"input-group\">
                <button id=\"sidebar-toggle\" class=\"btn btn-default\" type=\"button\">Toggle Sidebar</button>
            </div>
        </div>
    </div>

    <div class=\"clearfix\"></div>

    <div class=\"container\">
        <div id=\"row-main\" class=\"row row-eq-height\">
            <div id=\"content\" class=\"col-md-10\">
                <div class=\"panel\">
                    <div class=\"panel-body\">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
            <div id=\"sidebar\" class=\"col-md-2\">
                <div class=\"affix\">
                    <div class=\"panel\">
                        <div class=\"panel-body\">
                            <h3>
                                <small>Unscheduled Sessions</small>
                            </h3>
                            <div id=\"external-events\">
                                {% for session in sessions %}
                                    {% set count = session.repeats - session.timeSlots|length %}
                                    <div id=\"external-{{ session.id }}\" class=\"fc-event label label-default\"
                                         style=\"background-color: {{ session.color }};
                                                 display: inline-block;
                                                 /*overflow:hidden;*/
                                                 /*text-overflow: ellipsis;*/
                                                 max-width: 120px\"
                                         data-event='{
                                                \"id\":\"temp-{{ session.id }}\",
                                                \"title\":\"{{ session.topic }}\",
                                                \"color\":\"{{ session.color }}\",
                                                \"stick\":false,
                                                \"duration\":\"{{ session.defaultDuration|date('%H:%I') }}\",
                                                \"capacity\":\"{{ session.defaultCapacity }}\",
                                                \"location\":{
                                                    \"id\":\"{{ session.defaultLocation.id }}\",
                                                    \"room\":\"{{ session.defaultLocation }}\"
                                                    },
                                                \"session\":\"{{ session.id }}\"
                                                }'
                                         data-counter=\"counter-{{ session.id }}\"
                                         data-count=\"{{ count }}\">
                                        {{ session.topic }}
                                    </div>
                                    <span id=\"counter-{{ session.id }}\" class=\"badge\">{{ count }}</span>
                                    <br>
                                {% endfor %}
                            </div>
                            {#</li>#}
                            {#</ul>#}
                            {#</div>#}
                            {#</div>#}
                            {#<div class=\"row\">#}
                            {#<div class=\"col-xs-12\">#}
                            {#<h3>#}
                            {#<small>Filters</small>#}
                            {#</h3>#}
                            {#<h4>#}
                            {#<small>Course</small>#}
                            {#</h4>#}
                            {#<select multiple class=\"form-control\">#}

                            {#</select>#}
                            {#<h4>#}
                            {#<small>Instructor</small>#}
                            {#</h4>#}
                            {#<select multiple class=\"form-control\">#}

                            {#</select>#}
                            {#<h4>#}
                            {#<small>Room</small>#}
                            {#</h4>#}
                            {#<select multiple class=\"form-control\">#}

                            {#</select>#}
                            {#</div>#}
                            {#</div>#}
                            {#<div class=\"row\">#}
                            {#<div class=\"col-xs-12\"></div>#}
                            {#</div>#}
                        </div>
                        {#<div class=\"panel-footer\"></div>#}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id=\"timeslot-modal\" class=\"modal fade\" data-event>
        <div class=\"modal-dialog modal-lg\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span> <span
                                class=\"sr-only\">close</span></button>
                    <h4 class=\"modal-title\"></h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"timeslot-loader\" class=\"text-center\" style=\"display: none;\">
                        <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                    </div>

                    {{ form_start(time_slot_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.date) }}
                        </div>
                        {{ form_errors(time_slot_form.date) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.date, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.startTime) }}
                        </div>
                        {{ form_errors(time_slot_form.startTime) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.startTime, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.endTime) }}
                        </div>
                        {{ form_errors(time_slot_form.endTime) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.endTime, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.location) }}
                        </div>
                        {{ form_errors(time_slot_form.location) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.location, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.capacity) }}
                        </div>
                        {{ form_errors(time_slot_form.capacity) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.capacity, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <div class=\"control-label\">
                            {{ form_label(time_slot_form.assignments) }}
                        </div>
                        {{ form_errors(time_slot_form.assignments) }}
                        <div class=\"bt-flabels__wrapper\">
                            {{ form_widget(time_slot_form.assignments, {'attr': {'class': 'form-control'}}) }}
                            <span class=\"bt-flabels__error-desc\">Required</span>
                        </div>
                    </div>

                    {{ form_rest(time_slot_form) }}
                    {{ form_end(time_slot_form) }}
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
                    <button id=\"timeslot-submit\" type=\"button\" class=\"btn btn-success\">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{# TODO
    files, quiz creation success, colors, add session on success of creation, no on click for quiz,
    pdf export
#}", "role/admin/session/calendar.html.twig", "/code/templates/role/admin/session/calendar.html.twig");
    }
}
