<?php

/* role/mentor/session/history_by_time.html.twig */
class __TwigTemplate_21d186c84ec6e546fbcd5f278f909f2ec2a7676800988ef61cd40120d3bd3c52 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/session/history_by_time.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/history_by_time.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/history_by_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <h2 class=\"content_title\">Scheduled Sessions</h2>
    <table id=\"sessions\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Description</th>
            <th>Grades</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sessions"]) || array_key_exists("sessions", $context) ? $context["sessions"] : (function () { throw new Twig_Error_Runtime('Variable "sessions" does not exist.', 14, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
            // line 15
            echo "            <tr>
                <td>
                    ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["session"], "sections", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 18
                echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                // line 19
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 20
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "                </td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "description", []), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades", ["id" => twig_get_attribute($this->env, $this->source, $context["session"], "id", [])]), "html", null, true);
            echo "\">
                        <div class=\"btn btn-success\">Edit</div>
                    </a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>
    <br>
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Description</th>
            <th>Grades</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 47, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
            // line 48
            echo "            <tr>
                <td>
                    ";
            // line 50
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["quiz"], "sections", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 51
                echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                // line 52
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 53
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "                </td>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_grades", ["id" => twig_get_attribute($this->env, $this->source, $context["quiz"], "id", [])]), "html", null, true);
            echo "\">
                        <div class=\"btn btn-success\">Edit</div>
                    </a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 69
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 70
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function() {
           \$('#sessions').DataTable({
               'ordering':true,
               'searching': true,
               'paging':true
           });
           \$('#quizzes').DataTable({
               'ordering':true,
               'searching': true,
               'paging':true
           });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/session/history_by_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 70,  231 => 69,  219 => 66,  207 => 60,  202 => 58,  198 => 57,  195 => 56,  180 => 53,  178 => 52,  176 => 51,  159 => 50,  155 => 48,  151 => 47,  135 => 33,  123 => 27,  118 => 25,  114 => 24,  111 => 23,  96 => 20,  94 => 19,  92 => 18,  75 => 17,  71 => 15,  67 => 14,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}
{% block body %}
    <h2 class=\"content_title\">Scheduled Sessions</h2>
    <table id=\"sessions\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Description</th>
            <th>Grades</th>
        </tr>
        </thead>
        <tbody>
        {% for session in sessions %}
            <tr>
                <td>
                    {% for section in session.sections %}
                        {{- section -}}
                        {%- if not loop.last -%}
                            {{- \", \" -}}
                        {%- endif -%}
                    {% endfor %}
                </td>
                <td>{{ session.topic }}</td>
                <td>{{ session.description }}</td>
                <td>
                    <a href=\"{{ path('session_grades', {'id': session.id}) }}\">
                        <div class=\"btn btn-success\">Edit</div>
                    </a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <br>
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Section(s)</th>
            <th>Topic</th>
            <th>Description</th>
            <th>Grades</th>
        </tr>
        </thead>
        <tbody>
        {% for quiz in quizzes %}
            <tr>
                <td>
                    {% for section in quiz.sections %}
                        {{- section -}}
                        {%- if not loop.last -%}
                            {{- \", \" -}}
                        {%- endif -%}
                    {% endfor %}
                </td>
                <td>{{ quiz.topic }}</td>
                <td>{{ quiz.description }}</td>
                <td>
                    <a href=\"{{ path('session_grades', {'id': quiz.id}) }}\">
                        <div class=\"btn btn-success\">Edit</div>
                    </a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function() {
           \$('#sessions').DataTable({
               'ordering':true,
               'searching': true,
               'paging':true
           });
           \$('#quizzes').DataTable({
               'ordering':true,
               'searching': true,
               'paging':true
           });
        });
    </script>
{% endblock %}", "role/mentor/session/history_by_time.html.twig", "/code/templates/role/mentor/session/history_by_time.html.twig");
    }
}
