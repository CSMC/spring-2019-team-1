<?php

/* shared/base.html.twig */
class __TwigTemplate_a78fb74b1b1a7698318c1616983d3d662c930be4bd80ef5df1fa081ffc6444d4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Computer Science Mentor Center</title>

    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 32
        echo "</head>

<body class=\"nav-md\">
    <div class=\"container body\">
        <div class=\"main_container\">
            ";
        // line 37
        echo twig_include($this->env, $context, "shared/component/nav.html.twig");
        echo "

            <!-- page content -->
            <div class=\"right_col\" role=\"main\">
                <div class=\"\">
                    ";
        // line 42
        $this->displayBlock('body', $context, $blocks);
        // line 45
        echo "                </div>
            </div>
            <!-- /page content -->

            ";
        // line 49
        echo twig_include($this->env, $context, "shared/component/footer.html.twig");
        echo "
        </div>
    </div>
    

    ";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 74
        echo "    <script>
        /**
         * Resize function without multiple trigger
         *
         * Usage:
         * \$(window).smartresize(function(){
         *     // code here
         * });
         */
        (function (\$, sr) {
            // debouncing function from John Hann
            // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
            var debounce = function (func, threshold, execAsap) {
                var timeout;

                return function debounced() {
                    var obj = this, args = arguments;

                    function delayed() {
                        if (!execAsap)
                            func.apply(obj, args);
                        timeout = null;
                    }

                    if (timeout)
                        clearTimeout(timeout);
                    else if (execAsap)
                        func.apply(obj, args);

                    timeout = setTimeout(delayed, threshold || 100);
                };
            };

            // smartresize
            jQuery.fn[sr] = function (fn) {
                return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
            };

        })(jQuery, 'smartresize');

        // Sidebar
        \$(function () {
            var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
                \$BODY = \$('body'),
                \$MENU_TOGGLE = \$('#menu_toggle'),
                \$SIDEBAR_MENU = \$('#sidebar-menu'),
                \$SIDEBAR_FOOTER = \$('.sidebar-footer'),
                \$LEFT_COL = \$('.left_col'),
                \$RIGHT_COL = \$('.right_col'),
                \$NAV_MENU = \$('.nav_menu'),
                \$FOOTER = \$('footer');

            // TODO: This is some kind of easy fix, maybe we can improve this
            var setContentHeight = function () {
                // reset height
                \$RIGHT_COL.css('min-height', \$(window).height());

                var bodyHeight = \$BODY.outerHeight(),
                    footerHeight = \$BODY.hasClass('footer_fixed') ? -10 : \$FOOTER.height(),
                    leftColHeight = \$LEFT_COL.eq(1).height() + \$SIDEBAR_FOOTER.height(),
                    contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

                // normalize content
                contentHeight -= \$NAV_MENU.height() + footerHeight;

                \$RIGHT_COL.css('min-height', contentHeight);
            };

            \$SIDEBAR_MENU.find('a').on('click', function (ev) {
                console.log('clicked - sidebar_menu');
                var \$li = \$(this).parent();

                if (\$li.is('.active')) {
                    \$li.removeClass('active active-sm');
                    \$('ul:first', \$li).slideUp(function () {
                        setContentHeight();
                    });
                } else {
                    // prevent closing menu if we are on child menu
                    if (!\$li.parent().is('.child_menu')) {
                        \$SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        \$SIDEBAR_MENU.find('li ul').slideUp();
                    } else {
                        if (\$BODY.is(\".nav-sm\")) {
                            \$SIDEBAR_MENU.find(\"li\").removeClass(\"active active-sm\");
                            \$SIDEBAR_MENU.find(\"li ul\").slideUp();
                        }
                    }
                    \$li.addClass('active');

                    \$('ul:first', \$li).slideDown(function () {
                        setContentHeight();
                    });
                }
            });

            // toggle small or large menu
            \$MENU_TOGGLE.on('click', function () {
                console.log('clicked - menu toggle');

                if (\$BODY.hasClass('nav-md')) {
                    \$SIDEBAR_MENU.find('li.active ul').hide();
                    \$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                } else {
                    \$SIDEBAR_MENU.find('li.active-sm ul').show();
                    \$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                }

                \$BODY.toggleClass('nav-md nav-sm');

                setContentHeight();

                \$('.dataTable').each(function () {
                    \$(this).dataTable().fnDraw();
                });
            });

            // check active menu
            \$SIDEBAR_MENU.find('a[href=\"' + CURRENT_URL + '\"]').parent('li').addClass('current-page');

            \$SIDEBAR_MENU.find('a').filter(function () {
                return this.href == CURRENT_URL;
            }).parent('li').addClass('current-page').parents('ul').slideDown(function () {
                setContentHeight();
            }).parent().addClass('active');

            // recompute content when resizing
            \$(window).smartresize(function () {
                setContentHeight();
            });

            setContentHeight();

            // fixed sidebar
            if (\$.fn.mCustomScrollbar) {
                \$('.menu_fixed').mCustomScrollbar({
                    autoHideScrollbar: true,
                    theme: 'minimal',
                    mouseWheel: {preventDefault: true}
                });
            }
        });


        // Panel toolbox
        \$(function () {
            \$('.collapse-link').on('click', function () {
                var \$BOX_PANEL = \$(this).closest('.x_panel'),
                    \$ICON = \$(this).find('i'),
                    \$BOX_CONTENT = \$BOX_PANEL.find('.x_content');

                // fix for some div with hardcoded fix class
                if (\$BOX_PANEL.attr('style')) {
                    \$BOX_CONTENT.slideToggle(200, function () {
                        \$BOX_PANEL.removeAttr('style');
                    });
                } else {
                    \$BOX_CONTENT.slideToggle(200);
                    \$BOX_PANEL.css('height', 'auto');
                }

                \$ICON.toggleClass('fa-chevron-up fa-chevron-down');
            });

            \$('.close-link').click(function () {
                var \$BOX_PANEL = \$(this).closest('.x_panel');

                \$BOX_PANEL.remove();
            });
        });

        // Accordion
        \$(function () {
            \$(\".expand\").on(\"click\", function () {
                \$(this).next().slideToggle(200);
                \$expand = \$(this).find(\">:first-child\");

                if (\$expand.text() == \"+\") {
                    \$expand.text(\"-\");
                } else {
                    \$expand.text(\"+\");
                }
            });
        });

        \$(document).ready(function() {
            \$('.flash-message').fadeIn(400);
            setTimeout(function() {
                \$('.flash-message').fadeOut(2000);
            }, 5000);
        });
    </script>
</body>
<link rel=\"stylesheet\" type=\"text/css\" href=\"https://xdsoft.net/scripts/datetimepicker/build/jquery.datetimepicker.min.css\"/>
<script src=\"https://xdsoft.net/scripts/datetimepicker/build/jquery.datetimepicker.full.min.js\"></script>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
              integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
              crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
              integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
              crossorigin=\"anonymous\">
        ";
        // line 20
        echo "        <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css\">
        <link rel=\"stylesheet\"
              href=\"https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css\">
        <link rel=\"stylesheet\"
              href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.13/css/all.css\"
              integrity=\"sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp\"
              crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/admin.css"), "html", null, true);
        echo "\"/>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 42
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 43
        echo "
                    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 55
        echo "        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
                integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
                integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
                integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.4.0/jquery.quicksearch.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
        <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
        <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js\"></script>
        <script src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/js/fastclick.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/js/nprogress.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "shared/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 72,  374 => 71,  356 => 55,  347 => 54,  336 => 43,  327 => 42,  315 => 30,  303 => 20,  295 => 13,  286 => 12,  80 => 74,  78 => 54,  70 => 49,  64 => 45,  62 => 42,  54 => 37,  47 => 32,  45 => 12,  32 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Computer Science Mentor Center</title>

    {% block stylesheets %}
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
              integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
              crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
              integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
              crossorigin=\"anonymous\">
        {#FIXME: Maybe move this datatables only where we need it#}
        <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css\">
        <link rel=\"stylesheet\"
              href=\"https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css\">
        <link rel=\"stylesheet\"
              href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.13/css/all.css\"
              integrity=\"sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp\"
              crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/admin.css') }}\"/>
    {% endblock %}
</head>

<body class=\"nav-md\">
    <div class=\"container body\">
        <div class=\"main_container\">
            {{ include('shared/component/nav.html.twig') }}

            <!-- page content -->
            <div class=\"right_col\" role=\"main\">
                <div class=\"\">
                    {% block body %}

                    {% endblock %}
                </div>
            </div>
            <!-- /page content -->

            {{ include('shared/component/footer.html.twig') }}
        </div>
    </div>
    

    {% block javascripts %}
        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
                integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
                integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
                integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
                crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery.quicksearch/2.4.0/jquery.quicksearch.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
        <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
        <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js\"></script>
        <script src=\"{{ asset('build/js/fastclick.js') }}\"></script>
        <script src=\"{{ asset('build/js/nprogress.js') }}\"></script>
    {% endblock %}
    <script>
        /**
         * Resize function without multiple trigger
         *
         * Usage:
         * \$(window).smartresize(function(){
         *     // code here
         * });
         */
        (function (\$, sr) {
            // debouncing function from John Hann
            // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
            var debounce = function (func, threshold, execAsap) {
                var timeout;

                return function debounced() {
                    var obj = this, args = arguments;

                    function delayed() {
                        if (!execAsap)
                            func.apply(obj, args);
                        timeout = null;
                    }

                    if (timeout)
                        clearTimeout(timeout);
                    else if (execAsap)
                        func.apply(obj, args);

                    timeout = setTimeout(delayed, threshold || 100);
                };
            };

            // smartresize
            jQuery.fn[sr] = function (fn) {
                return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
            };

        })(jQuery, 'smartresize');

        // Sidebar
        \$(function () {
            var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
                \$BODY = \$('body'),
                \$MENU_TOGGLE = \$('#menu_toggle'),
                \$SIDEBAR_MENU = \$('#sidebar-menu'),
                \$SIDEBAR_FOOTER = \$('.sidebar-footer'),
                \$LEFT_COL = \$('.left_col'),
                \$RIGHT_COL = \$('.right_col'),
                \$NAV_MENU = \$('.nav_menu'),
                \$FOOTER = \$('footer');

            // TODO: This is some kind of easy fix, maybe we can improve this
            var setContentHeight = function () {
                // reset height
                \$RIGHT_COL.css('min-height', \$(window).height());

                var bodyHeight = \$BODY.outerHeight(),
                    footerHeight = \$BODY.hasClass('footer_fixed') ? -10 : \$FOOTER.height(),
                    leftColHeight = \$LEFT_COL.eq(1).height() + \$SIDEBAR_FOOTER.height(),
                    contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

                // normalize content
                contentHeight -= \$NAV_MENU.height() + footerHeight;

                \$RIGHT_COL.css('min-height', contentHeight);
            };

            \$SIDEBAR_MENU.find('a').on('click', function (ev) {
                console.log('clicked - sidebar_menu');
                var \$li = \$(this).parent();

                if (\$li.is('.active')) {
                    \$li.removeClass('active active-sm');
                    \$('ul:first', \$li).slideUp(function () {
                        setContentHeight();
                    });
                } else {
                    // prevent closing menu if we are on child menu
                    if (!\$li.parent().is('.child_menu')) {
                        \$SIDEBAR_MENU.find('li').removeClass('active active-sm');
                        \$SIDEBAR_MENU.find('li ul').slideUp();
                    } else {
                        if (\$BODY.is(\".nav-sm\")) {
                            \$SIDEBAR_MENU.find(\"li\").removeClass(\"active active-sm\");
                            \$SIDEBAR_MENU.find(\"li ul\").slideUp();
                        }
                    }
                    \$li.addClass('active');

                    \$('ul:first', \$li).slideDown(function () {
                        setContentHeight();
                    });
                }
            });

            // toggle small or large menu
            \$MENU_TOGGLE.on('click', function () {
                console.log('clicked - menu toggle');

                if (\$BODY.hasClass('nav-md')) {
                    \$SIDEBAR_MENU.find('li.active ul').hide();
                    \$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                } else {
                    \$SIDEBAR_MENU.find('li.active-sm ul').show();
                    \$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                }

                \$BODY.toggleClass('nav-md nav-sm');

                setContentHeight();

                \$('.dataTable').each(function () {
                    \$(this).dataTable().fnDraw();
                });
            });

            // check active menu
            \$SIDEBAR_MENU.find('a[href=\"' + CURRENT_URL + '\"]').parent('li').addClass('current-page');

            \$SIDEBAR_MENU.find('a').filter(function () {
                return this.href == CURRENT_URL;
            }).parent('li').addClass('current-page').parents('ul').slideDown(function () {
                setContentHeight();
            }).parent().addClass('active');

            // recompute content when resizing
            \$(window).smartresize(function () {
                setContentHeight();
            });

            setContentHeight();

            // fixed sidebar
            if (\$.fn.mCustomScrollbar) {
                \$('.menu_fixed').mCustomScrollbar({
                    autoHideScrollbar: true,
                    theme: 'minimal',
                    mouseWheel: {preventDefault: true}
                });
            }
        });


        // Panel toolbox
        \$(function () {
            \$('.collapse-link').on('click', function () {
                var \$BOX_PANEL = \$(this).closest('.x_panel'),
                    \$ICON = \$(this).find('i'),
                    \$BOX_CONTENT = \$BOX_PANEL.find('.x_content');

                // fix for some div with hardcoded fix class
                if (\$BOX_PANEL.attr('style')) {
                    \$BOX_CONTENT.slideToggle(200, function () {
                        \$BOX_PANEL.removeAttr('style');
                    });
                } else {
                    \$BOX_CONTENT.slideToggle(200);
                    \$BOX_PANEL.css('height', 'auto');
                }

                \$ICON.toggleClass('fa-chevron-up fa-chevron-down');
            });

            \$('.close-link').click(function () {
                var \$BOX_PANEL = \$(this).closest('.x_panel');

                \$BOX_PANEL.remove();
            });
        });

        // Accordion
        \$(function () {
            \$(\".expand\").on(\"click\", function () {
                \$(this).next().slideToggle(200);
                \$expand = \$(this).find(\">:first-child\");

                if (\$expand.text() == \"+\") {
                    \$expand.text(\"-\");
                } else {
                    \$expand.text(\"+\");
                }
            });
        });

        \$(document).ready(function() {
            \$('.flash-message').fadeIn(400);
            setTimeout(function() {
                \$('.flash-message').fadeOut(2000);
            }, 5000);
        });
    </script>
</body>
<link rel=\"stylesheet\" type=\"text/css\" href=\"https://xdsoft.net/scripts/datetimepicker/build/jquery.datetimepicker.min.css\"/>
<script src=\"https://xdsoft.net/scripts/datetimepicker/build/jquery.datetimepicker.full.min.js\"></script>
</html>
", "shared/base.html.twig", "/code/templates/shared/base.html.twig");
    }
}
