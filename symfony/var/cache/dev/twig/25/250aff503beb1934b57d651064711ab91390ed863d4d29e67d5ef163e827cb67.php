<?php

/* role/mentor/session/schedule_by_time.html.twig */
class __TwigTemplate_f8d2ca97c56c1bb1a2bcf514bbf69dd5d5908de8afff5b835641f1bb327adfef extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/session/schedule_by_time.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/schedule_by_time.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/session/schedule_by_time.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Dates</th>
            <th>Room</th>
            <th>Description</th>
            <th>Details</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["quizzes"]) || array_key_exists("quizzes", $context) ? $context["quizzes"] : (function () { throw new Twig_Error_Runtime('Variable "quizzes" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
            // line 18
            echo "            <tr>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "startDate", []), "m/d/y"), "html", null, true);
            echo "
                    - ";
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "endDate", []), "m/d/y"), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "location", []), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_view", ["id" => twig_get_attribute($this->env, $this->source, $context["quiz"], "id", [])]), "html", null, true);
            echo "\" class=\"btn btn-success\">
                        Details
                    </a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>
    <h2>Sessions</h2>
    <table id=\"sessions\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Date and Time</th>
            <th>Mentor(s)</th>
            <th>Room</th>
            <th>Course(s)</th>
            <th>Instructor(s)</th>
            <th>Description</th>
            <th>Details</th>
            <th>Start</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["timeslots"]) || array_key_exists("timeslots", $context) ? $context["timeslots"] : (function () { throw new Twig_Error_Runtime('Variable "timeslots" does not exist.', 49, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["timeslot"]) {
            // line 50
            echo "            <tr>
                <td>";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "topic", []), "html", null, true);
            echo "</td>
                <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", []), "m/d/y g:i A"), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["timeslot"], "mentors", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
                // line 55
                echo "                        ";
                echo twig_escape_filter($this->env, $context["mentor"], "html", null, true);
                echo "
                        ";
                // line 56
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 57
                    echo "                            ";
                    echo ", ";
                    echo "
                        ";
                }
                // line 59
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                </td>
                <td>";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["timeslot"], "location", []), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 63
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "sections", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
                // line 64
                echo "                        ";
                echo twig_escape_filter($this->env, $context["section"], "html", null, true);
                echo "
                        ";
                // line 65
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 66
                    echo "                            ";
                    echo ", ";
                    echo "
                        ";
                }
                // line 68
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "                </td>
                <td>
                    ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "instructors", []));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["instructor"]) {
                // line 72
                echo "                        ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["instructor"], "firstName", []), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["instructor"], "lastName", []), "html", null, true);
                echo "
                        ";
                // line 73
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                    // line 74
                    echo "                            ";
                    echo ", ";
                    echo "
                        ";
                }
                // line 76
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instructor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "                </td>
                <td>";
            // line 78
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["timeslot"], "session", []), "description", []), "html", null, true);
            echo "</td>
                <td>
                    <a href=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_timeslot_view", ["tid" => twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", [])]), "html", null, true);
            echo "\">
                        <div class=\"btn btn-success\">Details</div>
                    </a>
                </td>
                <td>
                    ";
            // line 85
            if ( !twig_get_attribute($this->env, $this->source, $context["timeslot"], "endTime", [], "any", true, true)) {
                // line 86
                echo "                        <div class=\"btn btn-default\">
                            Ended
                        </div>
                    ";
            } else {
                // line 90
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("swipe_session", ["id" => twig_get_attribute($this->env, $this->source, $context["timeslot"], "id", [])]), "html", null, true);
                echo "\">
                            <div class=\"btn btn-success\">
                                ";
                // line 92
                if (twig_get_attribute($this->env, $this->source, $context["timeslot"], "startTime", [], "any", true, true)) {
                    // line 93
                    echo "                                    Start
                                ";
                } else {
                    // line 95
                    echo "                                    Continue
                                ";
                }
                // line 97
                echo "                            </div>
                        </a>
                    ";
            }
            // line 100
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['timeslot'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "        </tbody>
    </table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 106
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 107
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            \$('#quizzes').DataTable({
                ordering: true,
                searching: true,
                paging: false,
            });

            \$('#sessions').DataTable({
                ordering: true,
                order: [1, 'asc'],
                searching: true,
                paging: false,
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/session/schedule_by_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  367 => 107,  358 => 106,  346 => 103,  338 => 100,  333 => 97,  329 => 95,  325 => 93,  323 => 92,  317 => 90,  311 => 86,  309 => 85,  301 => 80,  296 => 78,  293 => 77,  279 => 76,  273 => 74,  271 => 73,  264 => 72,  247 => 71,  243 => 69,  229 => 68,  223 => 66,  221 => 65,  216 => 64,  199 => 63,  194 => 61,  191 => 60,  177 => 59,  171 => 57,  169 => 56,  164 => 55,  147 => 54,  142 => 52,  138 => 51,  135 => 50,  131 => 49,  111 => 31,  99 => 25,  94 => 23,  90 => 22,  86 => 21,  82 => 20,  78 => 19,  75 => 18,  71 => 17,  54 => 4,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# For use with admins/mentors #}
{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    <h2>Quizzes</h2>
    <table id=\"quizzes\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Dates</th>
            <th>Room</th>
            <th>Description</th>
            <th>Details</th>
        </tr>
        </thead>
        <tbody>
        {% for quiz in quizzes %}
            <tr>
                <td>{{ quiz.topic }}</td>
                <td>{{ quiz.startDate|date('m/d/y') }}
                    - {{ quiz.endDate|date('m/d/y') }}</td>
                <td>{{ quiz.location }}</td>
                <td>{{ quiz.description }}</td>
                <td>
                    <a href=\"{{ path('session_view', {'id': quiz.id}) }}\" class=\"btn btn-success\">
                        Details
                    </a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
    <h2>Sessions</h2>
    <table id=\"sessions\" class=\"table table-bordered table-striped\">
        <thead>
        <tr>
            <th>Topic</th>
            <th>Date and Time</th>
            <th>Mentor(s)</th>
            <th>Room</th>
            <th>Course(s)</th>
            <th>Instructor(s)</th>
            <th>Description</th>
            <th>Details</th>
            <th>Start</th>
        </tr>
        </thead>
        <tbody>
        {% for timeslot in timeslots %}
            <tr>
                <td>{{ timeslot.session.topic }}</td>
                <td>{{ timeslot.startTime|date('m/d/y g:i A') }}</td>
                <td>
                    {% for mentor in timeslot.mentors %}
                        {{ mentor }}
                        {% if not loop.last %}
                            {{ \", \" }}
                        {% endif %}
                    {% endfor %}
                </td>
                <td>{{ timeslot.location }}</td>
                <td>
                    {% for section in timeslot.session.sections %}
                        {{ section }}
                        {% if not loop.last %}
                            {{ \", \" }}
                        {% endif %}
                    {% endfor %}
                </td>
                <td>
                    {% for instructor in timeslot.session.instructors %}
                        {{ instructor.firstName }} {{ instructor.lastName }}
                        {% if not loop.last %}
                            {{ \", \" }}
                        {% endif %}
                    {% endfor %}
                </td>
                <td>{{ timeslot.session.description }}</td>
                <td>
                    <a href=\"{{ path('session_timeslot_view', {'tid': timeslot.id}) }}\">
                        <div class=\"btn btn-success\">Details</div>
                    </a>
                </td>
                <td>
                    {% if timeslot.endTime is not defined %}
                        <div class=\"btn btn-default\">
                            Ended
                        </div>
                    {% else %}
                        <a href=\"{{ path('swipe_session', {'id': timeslot.id}) }}\">
                            <div class=\"btn btn-success\">
                                {% if timeslot.startTime is defined %}
                                    Start
                                {% else %}
                                    Continue
                                {% endif %}
                            </div>
                        </a>
                    {% endif %}
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            \$('#quizzes').DataTable({
                ordering: true,
                searching: true,
                paging: false,
            });

            \$('#sessions').DataTable({
                ordering: true,
                order: [1, 'asc'],
                searching: true,
                paging: false,
            });
        });
    </script>
{% endblock %}", "role/mentor/session/schedule_by_time.html.twig", "/code/templates/role/mentor/session/schedule_by_time.html.twig");
    }
}
