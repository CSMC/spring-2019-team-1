<?php

/* role/admin/department/edit.html.twig */
class __TwigTemplate_dfb2e6ad5d3451b9d70a674bdd71271e4e3cb7aee2d5bb4d575d31c339c669d6 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("role/admin/base.html.twig", "role/admin/department/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "role/admin/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/department/edit.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/department/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    ";
        // line 4
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 25
        echo "    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Department?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteDepartment?id=";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["deleteId"]) || array_key_exists("deleteId", $context) ? $context["deleteId"] : (function () { throw new Twig_Error_Runtime('Variable "deleteId" does not exist.', 41, $this->source); })()), "html", null, true);
        echo "'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <div class=\"title_left\">
                            <h2 style=\"font-family:''\"> Edit Department (";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["departmentName"]) || array_key_exists("departmentName", $context) ? $context["departmentName"] : (function () { throw new Twig_Error_Runtime('Variable "departmentName" does not exist.', 53, $this->source); })()), "html", null, true);
        echo ")</h2>
                            <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                    data-target=\"#myModalDelete\" style=\"float: right\">Delete
                            </button>
                        </div>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        ";
        // line 62
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 62, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 63
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 64
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 65
                echo "                                    <div class=\"flash-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                                        ";
                // line 66
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                                    </div>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                        ";
        // line 72
        echo "                        ";
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 72, $this->source); })()), "vars", []), "valid", [])) {
            // line 73
            echo "                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                ";
            // line 74
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 74, $this->source); })()), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 77
        echo "                        <br/>
                        ";
        // line 78
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 78, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left input_mask"]]);
        echo "
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 81, $this->source); })()), "name", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), "name", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 89, $this->source); })()), "abbreviation", []), 'label');
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 92
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 92, $this->source); })()), "abbreviation", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 97, $this->source); })()), "adminNotes", []), 'label', ["label_attr" => ["style" => "font-weight:200"]]);
        echo "
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 100, $this->source); })()), "adminNotes", []), 'widget', ["attr" => ["class" => "form-control col-md-7 col-xs-12"]]);
        echo "
                            </div>
                        </div>
                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\"
                                    onclick=\"location.href='./list_departments'\">Cancel
                            </button>
                            ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 107, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-success", "data-toggle" => "modal", "data-target" => "#myModalEdit"]]);
        echo "
                        </div>
                        ";
        // line 109
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 109, $this->source); })()), 'form_end');
        echo "
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 115
        $this->displayBlock('javascripts', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 116
        echo "        ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
        <script type=\"text/javascript\">
            ";
        // line 119
        echo "            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });
            ";
        // line 123
        echo "            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            ";
        // line 127
        echo "            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });
        </script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/department/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 127,  265 => 123,  260 => 119,  254 => 116,  236 => 115,  227 => 109,  222 => 107,  212 => 100,  206 => 97,  198 => 92,  192 => 89,  184 => 84,  178 => 81,  172 => 78,  169 => 77,  163 => 74,  160 => 73,  157 => 72,  155 => 71,  148 => 69,  139 => 66,  134 => 65,  130 => 64,  127 => 63,  122 => 62,  111 => 53,  96 => 41,  78 => 25,  56 => 4,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"role/admin/base.html.twig\" %}
{% block body %}
    {#Confirmation Modal Box for saving edited data#}
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalEdit\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: rgba(38,185,154,.88);height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Save</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to save the changes?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-success\" id=\"saveChanges\">Save</button>
                </div>
            </div>
        </div>
    </div>
    {#Confirmation Modal Box for deleting department#}
    <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"myModalDelete\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\" style=\"background: #C82829;height:45px\">
                    <h5 class=\"modal-title\"
                        style=\"color:white; font-size: large;font-family: Helvetica, Arial, sans-serif;\"><b>Delete</b>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"/>
                        <span aria-hidden=\"true\">&times;</span></h5>
                    </button>
                </div>
                <div class=\"modal-body\" style=\"height: 50px\">
                    <p>Are you sure you want to delete the Department?</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Discard</button>
                    <button type=\"button\" class=\"btn btn-danger\" id=\"deleteItem\"
                            onclick=\"location.href='./deleteDepartment?id={{ deleteId }}'\">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"center_col\" role=\"main\">
        <div class=\"row\">
            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                    <div class=\"x_title\">
                        <div class=\"title_left\">
                            <h2 style=\"font-family:''\"> Edit Department ({{ departmentName }})</h2>
                            <button type=\"button\" id=\"deleteBtn\" class=\"btn btn-danger\" data-toggle=\"modal\"
                                    data-target=\"#myModalDelete\" style=\"float: right\">Delete
                            </button>
                        </div>
                        <div class=\"clearfix\"></div>
                    </div>
                    <div class=\"x_content\">
                        {# Displaying Flash message for foreign key constraint violation#}
                        {% for label, messages in app.flashes %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {% for message in messages %}
                                    <div class=\"flash-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            </div>
                        {% endfor %}
                        {# Displaying form errors#}
                        {% if not form.vars.valid %}
                            <div class=\"alert alert-danger\" id=\"errorDiv\">
                                {{ form_errors(form) }}
                            </div>
                        {% endif %}
                        <br/>
                        {{ form_start(form,{'attr':{'class':'form-horizontal form-label-left input_mask'}}) }}
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.name) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.name, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.abbreviation) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.abbreviation, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"
                                   for=\"first-name\">{{ form_label(form.adminNotes,null ,{ 'label_attr': {'style': 'font-weight:200'}}) }}
                            </label>
                            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                {{ form_widget(form.adminNotes, {'attr': {'class': 'form-control col-md-7 col-xs-12'}} ) }}
                            </div>
                        </div>
                        <div class=\"col-md-9 col-sm-9 col-xs-12 col-md-offset-5\">
                            <button type=\"button\" class=\"btn btn-secondary\"
                                    onclick=\"location.href='./list_departments'\">Cancel
                            </button>
                            {{ form_widget(form.submit,{'attr':{'class':'btn btn-success','data-toggle':'modal','data-target':'#myModalEdit'}}) }}
                        </div>
                        {{ form_end(form) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% block javascripts %}
        {{ parent() }}
        <script type=\"text/javascript\">
            {# Preventing default submit for form to display confirmation box#}
            \$('#form_submit').on('click', function (event) {
                event.preventDefault();
            });
            {# Submitting form on confirmation from modal box#}
            \$('#saveChanges').on('click', function () {
                \$('form').submit();
            });
            {# Preventing default delete to display confirmation box#}
            \$('#deleteBtn').on('click', function (event) {
                event.preventDefault();
            });
        </script>
    {% endblock %}
{% endblock %}
", "role/admin/department/edit.html.twig", "/code/templates/role/admin/department/edit.html.twig");
    }
}
