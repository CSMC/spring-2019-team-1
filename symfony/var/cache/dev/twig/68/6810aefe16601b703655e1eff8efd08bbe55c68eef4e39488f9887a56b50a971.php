<?php

/* shared/notifications/session_reminder.html.twig */
class __TwigTemplate_e5482d8bcab3f855e9be835b2341371158788af5de441b1c2c8592f48446f022 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/notifications/session_reminder.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/notifications/session_reminder.html.twig"));

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>UT Dallas Mentor Center Session Reminder</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
</head>

<body>
<h1>CSMC Session Reminder</h1>

<p>Hello ";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mentor"]) || array_key_exists("mentor", $context) ? $context["mentor"] : (function () { throw new Twig_Error_Runtime('Variable "mentor" does not exist.', 13, $this->source); })()), "preferredName", []), "html", null, true);
        echo ",</p>

";
        // line 15
        $context["count"] = twig_length_filter($this->env, (isset($context["assignments"]) || array_key_exists("assignments", $context) ? $context["assignments"] : (function () { throw new Twig_Error_Runtime('Variable "assignments" does not exist.', 15, $this->source); })()));
        // line 16
        $context["assignmentString"] = "assignment|assignments";
        // line 17
        echo "
<p>This is a reminder of your upcoming ";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->transchoice((isset($context["assignmentString"]) || array_key_exists("assignmentString", $context) ? $context["assignmentString"] : (function () { throw new Twig_Error_Runtime('Variable "assignmentString" does not exist.', 18, $this->source); })()), (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 18, $this->source); })())), "html", null, true);
        echo "
    on ";
        // line 19
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["assignmentDate"]) || array_key_exists("assignmentDate", $context) ? $context["assignmentDate"] : (function () { throw new Twig_Error_Runtime('Variable "assignmentDate" does not exist.', 19, $this->source); })()), "l m/d/Y"), "html", null, true);
        echo ":</p>

<ul>
    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["assignments"]) || array_key_exists("assignments", $context) ? $context["assignments"] : (function () { throw new Twig_Error_Runtime('Variable "assignments" does not exist.', 22, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
            // line 23
            echo "        ";
            $context["shift"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["assignment"], "scheduledShift", []), "shift", []);
            // line 24
            echo "        <li>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shift"]) || array_key_exists("shift", $context) ? $context["shift"] : (function () { throw new Twig_Error_Runtime('Variable "shift" does not exist.', 24, $this->source); })()), "startTime", []), "h:i A"), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shift"]) || array_key_exists("shift", $context) ? $context["shift"] : (function () { throw new Twig_Error_Runtime('Variable "shift" does not exist.', 24, $this->source); })()), "endTime", []), "h:i A"), "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "</ul>

<p>Sincerely,</p>
<p>The Computer Science Mentor Center</p>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/notifications/session_reminder.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 26,  72 => 24,  69 => 23,  65 => 22,  59 => 19,  55 => 18,  52 => 17,  50 => 16,  48 => 15,  43 => 13,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"
        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>UT Dallas Mentor Center Session Reminder</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
</head>

<body>
<h1>CSMC Session Reminder</h1>

<p>Hello {{ mentor.preferredName }},</p>

{% set count = assignments | length %}
{% set assignmentString = 'assignment|assignments' %}

<p>This is a reminder of your upcoming {{ assignmentString | transchoice(count) }}
    on {{ assignmentDate | date('l m/d/Y') }}:</p>

<ul>
    {% for assignment in assignments %}
        {% set shift = assignment.scheduledShift.shift %}
        <li>{{ shift.startTime | date('h:i A') }} to {{ shift.endTime | date('h:i A') }}</li>
    {% endfor %}
</ul>

<p>Sincerely,</p>
<p>The Computer Science Mentor Center</p>
</body>
</html>", "shared/notifications/session_reminder.html.twig", "/code/templates/shared/notifications/session_reminder.html.twig");
    }
}
