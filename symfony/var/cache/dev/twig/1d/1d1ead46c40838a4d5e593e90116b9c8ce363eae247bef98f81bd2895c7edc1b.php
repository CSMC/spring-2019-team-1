<?php

/* shared/swipe/session.html.twig */
class __TwigTemplate_4ed68d13502946d70d6d6e145be7c89db2b823bb39f69c6f407a96c47a01cf45 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/swipe/session.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "shared/swipe/session.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/favicon.png"), "html", null, true);
        echo "\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/global.css"), "html", null, true);
        echo "\"/>
    <style>
        /*.panel {*/
        /*margin-bottom: 0;*/
        /*}*/
        /*.table {*/
        /*margin-bottom: 0;*/
        /*}*/
        /*.pagination {*/
        /*margin: 10px 0 0 10px;*/
        /*}*/
    </style>
</head>
<body style=\"background: #FFFFFF url(";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/swipe-background.png"), "html", null, true);
        echo ") no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-6\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <h1>";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 39, $this->source); })()), "name", []), "html", null, true);
        echo "</h1>
                                <h1>
                                    <small>
                                        Mentors: ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 42, $this->source); })()), "mentors", []));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["mentor"]) {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["mentor"], "preferredName", []), "html", null, true);
            if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [])) {
                echo ", ";
            }
            echo " ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mentor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</small>
                                </h1>
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <h2>Welcome! Sign In Here</h2>
                            </div>
                            <div class=\"panel-body\">
                                ";
        // line 57
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 57, $this->source); })()), 'form_start', ["attr" => ["class" => "form-horizontal form-label-left"]]);
        echo "
                                ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 58, $this->source); })()), 'errors');
        echo "
                                <div id=\"success\" class=\"alert alert-success\" style=\"display: none;\"></div>
                                ";
        // line 60
        if ( !twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 60, $this->source); })()), "started", [])) {
            // line 61
            echo "                                    <div id=\"warning\" class=\"alert alert-warning\">The mentor needs to start the
                                        session!
                                    </div>
                                ";
        }
        // line 65
        echo "                                <div class=\"form-group\">
                                    ";
        // line 67
        echo "                                    ";
        // line 68
        echo "                                    ";
        // line 69
        echo "                                    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 69, $this->source); })()), "scancode", []), 'errors');
        echo "
                                    <div>
                                        <div id=\"loader\" class=\"text-center\" style=\"display: none;\">
                                            <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                                        </div>

                                        ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 75, $this->source); })()), "scancode", []), 'widget', ["attr" => ["class" => "form-control", "placeholder" => "Please swipe your Comet Card"]]);
        echo "

                                        <div id=\"no-card-link\" class=\"text-right\">
                                            <a id=\"no-card\" class=\"text-right\" data-toggle=\"modal\"
                                               data-target=\"#no-card-modal\">I don't have my Comet Card</a>
                                        </div>
                                    </div>
                                </div>
                                ";
        // line 83
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["swipe_form"]) || array_key_exists("swipe_form", $context) ? $context["swipe_form"] : (function () { throw new Twig_Error_Runtime('Variable "swipe_form" does not exist.', 83, $this->source); })()), 'form_end');
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xs-3\"></div>
                </div>
            </div>
            <div class=\"col-xs-6\">
                <div class=\"panel panel-default\" style=\"min-height: 95vh; max-height: 98vh\">
                    <div class=\"panel-heading\">
                        Session Roster
                    </div>
                    <table class=\"table table-bordered\">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Time In
                            </th>
                            <th>
                                Signed In?
                            </th>
                            <th>
                                Time Out
                            </th>
                            <th>
                                Signed Out?
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 119
        $context["page_length"] = 20;
        // line 120
        echo "                        ";
        $context["count"] = twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 120, $this->source); })()), "registeredStudents", []));
        // line 121
        echo "                        ";
        $context["empty_seats"] = (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 121, $this->source); })()), "capacity", []) - (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 121, $this->source); })()));
        // line 122
        echo "                        ";
        $context["max"] = (((0 == twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 122, $this->source); })()), "capacity", []) % 20)) ? (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 122, $this->source); })()), "capacity", [])) : (((twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 122, $this->source); })()), "capacity", []) - (twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 122, $this->source); })()), "capacity", []) % (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 122, $this->source); })()))) + (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 122, $this->source); })()))));
        // line 123
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 123, $this->source); })()), "registeredStudents", []));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
            // line 124
            echo "                            <tr class=\"active\" id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["student"], "id", []), "html", null, true);
            echo "\">
                                <td>";
            // line 125
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", []), "html", null, true);
            echo "</td>
                                <td>";
            // line 126
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["student"], "firstName", []), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["student"], "username", []), "html", null, true);
            echo ")</td>

                                ";
            // line 128
            $context["a"] = null;
            // line 129
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 129, $this->source); })()), "attendances", []));
            foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
                // line 130
                echo "                                    ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "id", []) == twig_get_attribute($this->env, $this->source, $context["student"], "id", []))) {
                    // line 131
                    echo "                                        ";
                    $context["a"] = $context["attendance"];
                    // line 132
                    echo "                                    ";
                }
                // line 133
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "                                ";
            if ( !(null === (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new Twig_Error_Runtime('Variable "a" does not exist.', 134, $this->source); })()))) {
                // line 135
                echo "                                    ";
                if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new Twig_Error_Runtime('Variable "a" does not exist.', 135, $this->source); })()), "timeIn", []))) {
                    // line 136
                    echo "                                        <td>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new Twig_Error_Runtime('Variable "a" does not exist.', 136, $this->source); })()), "timeIn", []), "g:i A"), "html", null, true);
                    echo "</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    ";
                } else {
                    // line 139
                    echo "                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    ";
                }
                // line 142
                echo "
                                    ";
                // line 143
                if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new Twig_Error_Runtime('Variable "a" does not exist.', 143, $this->source); })()), "timeOut", []))) {
                    // line 144
                    echo "                                        <td>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["a"]) || array_key_exists("a", $context) ? $context["a"] : (function () { throw new Twig_Error_Runtime('Variable "a" does not exist.', 144, $this->source); })()), "timeOut", []), "g:i A"), "html", null, true);
                    echo "</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    ";
                } else {
                    // line 147
                    echo "                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    ";
                }
                // line 150
                echo "                                ";
            } else {
                // line 151
                echo "                                    <td></td>
                                    <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    <td></td>
                                    <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                ";
            }
            // line 156
            echo "                            </tr>
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "                        ";
        // line 159
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 159, $this->source); })()), "attendances", []));
        foreach ($context['_seq'] as $context["_key"] => $context["attendance"]) {
            // line 160
            echo "                            ";
            $context["found"] = false;
            // line 161
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["session"]) || array_key_exists("session", $context) ? $context["session"] : (function () { throw new Twig_Error_Runtime('Variable "session" does not exist.', 161, $this->source); })()), "registeredStudents", []));
            foreach ($context['_seq'] as $context["_key"] => $context["student"]) {
                // line 162
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, $context["student"], "id", []) == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "id", []))) {
                    // line 163
                    echo "                                    ";
                    $context["found"] = true;
                    // line 164
                    echo "                                ";
                }
                // line 165
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['student'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 166
            echo "
                            ";
            // line 167
            if ( !(isset($context["found"]) || array_key_exists("found", $context) ? $context["found"] : (function () { throw new Twig_Error_Runtime('Variable "found" does not exist.', 167, $this->source); })())) {
                // line 168
                echo "                                <tr id=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "id", []), "html", null, true);
                echo "\">
                                    ";
                // line 169
                $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 169, $this->source); })()) + 1);
                // line 170
                echo "                                    <td>";
                echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 170, $this->source); })()), "html", null, true);
                echo "</td>
                                    <td>";
                // line 171
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "firstName", []), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attendance"], "user", []), "username", []), "html", null, true);
                echo ")</td>
                                    ";
                // line 172
                if ( !(null === twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []))) {
                    // line 173
                    echo "                                        <td>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeIn", []), "g:i A"), "html", null, true);
                    echo "</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    ";
                } else {
                    // line 176
                    echo "                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    ";
                }
                // line 179
                echo "
                                    ";
                // line 180
                if ( !(null === twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []))) {
                    // line 181
                    echo "                                        <td>";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attendance"], "timeOut", []), "g:i A"), "html", null, true);
                    echo "</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    ";
                } else {
                    // line 184
                    echo "                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    ";
                }
                // line 187
                echo "                                </tr>
                            ";
            }
            // line 189
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendance'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 190
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 190, $this->source); })()) + 1), (isset($context["max"]) || array_key_exists("max", $context) ? $context["max"] : (function () { throw new Twig_Error_Runtime('Variable "max" does not exist.', 190, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 191
            echo "                            <tr class=\"active\" id=\"temp-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">
                                <td>";
            // line 192
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"register-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"register-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"register-modal-label\">No Comet Card Sign In</h4>
                    </div>
                    <div class=\"modal-body\">
                        <div id=\"register-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                        <div id=\"register-warning\" class=\"alert alert-warning\">
                            <strong>Card not registered!</strong>
                            Please enter your NetID and password to register this card.
                        </div>
                        <div id=\"register-loader\" class=\"text-center\" style=\"display: none;\">
                            <img src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                        </div>
                        ";
        // line 224
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 224, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                ";
        // line 227
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 227, $this->source); })()), "username", []), 'label');
        echo "
                            </div>
                            ";
        // line 229
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 229, $this->source); })()), "username", []), 'errors');
        echo "
                            <div class=\"bt-flabels__wrapper\">
                                ";
        // line 231
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 231, $this->source); })()), "username", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                ";
        // line 237
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 237, $this->source); })()), "password", []), 'label');
        echo "
                            </div>
                            ";
        // line 239
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 239, $this->source); })()), "password", []), 'errors');
        echo "
                            <div class=\"bt-flabels__wrapper\">
                                ";
        // line 241
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 241, $this->source); })()), "password", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        ";
        // line 245
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["register_form"]) || array_key_exists("register_form", $context) ? $context["register_form"] : (function () { throw new Twig_Error_Runtime('Variable "register_form" does not exist.', 245, $this->source); })()), 'form_end');
        echo "
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button id=\"register-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"no-card-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"no-card-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"no-card-modal-label\">Register Comet Card</h4>
                    </div>
                    <div class=\"modal-body\">
                        <p>
                            Having your Comet Card is a requirement to attend sessions hosted by the CSMC.
                            However, we will allow you <b style=\"color: red;\">one</b> sign in without your card per
                            semester.
                            If you choose to sign in with your Comet Card for this session, you will not be able to
                            sign in without your Comet Card again this semester.
                            <b>Your professor will also be notified that you did not have your Comet Card.</b>
                        </p>
                        <div id=\"no-card-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                        <div id=\"no-card-loader\" class=\"text-center\" style=\"display: none;\">
                            <img src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/ajax-loader.gif"), "html", null, true);
        echo "\">
                        </div>
                        ";
        // line 277
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 277, $this->source); })()), 'form_start', ["attr" => ["class" => "bt-flabels js-flabels"]]);
        echo "
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                ";
        // line 280
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 280, $this->source); })()), "reason", []), 'label');
        echo "
                            </div>
                            ";
        // line 282
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 282, $this->source); })()), "reason", []), 'errors');
        echo "
                            <div class=\"bt-flabels__wrapper\">
                                ";
        // line 284
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 284, $this->source); })()), "reason", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                ";
        // line 290
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 290, $this->source); })()), "username", []), 'label');
        echo "
                            </div>
                            ";
        // line 292
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 292, $this->source); })()), "username", []), 'errors');
        echo "
                            <div class=\"bt-flabels__wrapper\">
                                ";
        // line 294
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 294, $this->source); })()), "username", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                ";
        // line 300
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 300, $this->source); })()), "password", []), 'label');
        echo "
                            </div>
                            ";
        // line 302
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 302, $this->source); })()), "password", []), 'errors');
        echo "
                            <div class=\"bt-flabels__wrapper\">
                                ";
        // line 304
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 304, $this->source); })()), "password", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        ";
        // line 308
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["no_card_form"]) || array_key_exists("no_card_form", $context) ? $context["no_card_form"] : (function () { throw new Twig_Error_Runtime('Variable "no_card_form" does not exist.', 308, $this->source); })()), 'form_end');
        echo "
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button id=\"no-card-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"error-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"error-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"error-modal-label\">Error</h4>
                    </div>
                    <div class=\"modal-body\">
                        <div id=\"error-message\" class=\"alert alert-danger\" role=\"alert\">

                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        ";
        // line 334
        echo "                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Acknowledge</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
    <script src=\"https://cdn.datatables.net/plug-ins/1.10.16/features/pageResize/dataTables.pageResize.min.js\"></script>
    <script>
        \$(function () {
            var table = \$('.table').DataTable({
                ordering: false,
                info: false,
                searching: false,
                lengthChange: false,
                pageLength: ";
        // line 362
        echo twig_escape_filter($this->env, (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 362, $this->source); })()), "html", null, true);
        echo "
                // pageResize: true
            });

            // \$('.dataTables_length').css('display', 'none');
            var registered_students = ";
        // line 367
        echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 367, $this->source); })()), "html", null, true);
        echo ";
            var next = registered_students + 1;

            var check_out_student = function (id, time_out) {
                var row = \$('#' + id);

                row.children('td:nth-child(5)').html(time_out);
                row.children('td:nth-child(6)').html('<span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span>');

                var page = Math.floor(row.children('td:first-child').text() / ";
        // line 376
        echo twig_escape_filter($this->env, (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 376, $this->source); })()), "html", null, true);
        echo ");
                table.page(page).draw(false);
            };

            var check_in_student = function (id, name, time_in) {
                var row = \$('#' + id);
                var page = 0;
                if (row.length) {
                    row.removeClass('active');
                    page = Math.floor(row.children('td:first-child').text() / ";
        // line 385
        echo twig_escape_filter($this->env, (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 385, $this->source); })()), "html", null, true);
        echo ");
                } else {
                    row = \$('#temp-' + next);
                    row.prop('id', id);
                    row.removeClass('active');
                    row.children('td:nth-child(2)').html(name);
                    row.children('td:nth-child(6)').html('<span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span>');

                    page = Math.floor(next / ";
        // line 393
        echo twig_escape_filter($this->env, (isset($context["page_length"]) || array_key_exists("page_length", $context) ? $context["page_length"] : (function () { throw new Twig_Error_Runtime('Variable "page_length" does not exist.', 393, $this->source); })()), "html", null, true);
        echo ");

                    next++;
                }

                row.children('td:nth-child(3)').html(time_in);
                row.children('td:nth-child(4)').html('<span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span>');

                table.page(page).draw(false);
            };

            var success = \$('#success');
            var no_card_error = \$('#no-card-error');
            var register_warning = \$('#register-warning');
            var register_error = \$('#register-error');

            var loader = \$('#loader');
            // TODO consider putting a different loader for these 2 in footer of modal
            var no_card_loader = \$('#no-card-loader');
            var register_loader = \$('#register-loader');

            var input = \$('#swipe_scancode');
            input.prop('autofocus', true);
            input.get(0).focus();

            var no_card_link = \$('#no-card-link');

            var swipe_form = \$('[name=\"swipe\"]');
            var no_card_form = \$('[name=\"no_card\"]');
            var register_form = \$('[name=\"card_register\"]');

            swipe_form.on('submit', function (e) {
                e.preventDefault();

                var swipe = input.val();

                register_form.children('#card_register_swipe').val(swipe);

                \$.ajax({
                    url: swipe_form.prop('action'),
                    data: {
                        'scancode': swipe,
                        'session': \$('#swipe_session').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        input.hide();
                        no_card_link.hide();
                        loader.show();
                    }
                }).always(function () {
                    loader.hide();
                    input.show();
                    no_card_link.show();
                }).done(function (data, textStatus, jqXHR) {
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    ";
        // line 452
        echo "                    ";
        // line 453
        echo "                    switch (jqXHR.responseJSON.message) {
                        case 'session_start':
                            \$('#warning').hide();
                            success.html('<strong>Success!</strong> Session started!');
                            break;
                        case 'session_end':
                            // idk what to do here
                            success.html('<strong>Session ended!</strong>');
                            break;
                        case 'attendee_in':
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                        case 'attendee_out':
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user_name + '!');
                            check_out_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.time_out);
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'malformed_scan':
                            // rescan card again
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Scan failed!</strong> Please scan your card again.');
                            break;
                        case 'unregistered_user':
                            // register card
                            \$('#register-modal').modal('show');
                            break;
                        case 'no_user':
                        ";
        // line 484
        echo "                            break;
                        case 'ineligible':
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Not eligible for session!</strong> You are not on the roster for any of the sections listed for this session.');
                            break;
                        case 'invalid':
                            // shouldn't happen
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        case 'session_needs_starting':
                            \$('#error-modal-label').modal('show');
                            \$('#error-message').html('<strong>Session not started!</strong> Mentor needs to swipe first.');
                        default:
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });

                this.reset();
            });

            no_card_form.on('submit', function (e) {
                e.preventDefault();

                var username = \$('#no_card_username').val();
                var password = \$('#no_card_password').val();
                var session = \$('#no_card_session').val();
                \$.ajax({
                    url: no_card_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': session,
                    },
                    type: 'POST',
                    beforeSend: function () {
                        no_card_form.hide();
                        no_card_loader.show();
                    }
                }).always(function () {
                    no_card_loader.hide();
                    no_card_form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#no-card-modal').modal('hide');
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    ";
        // line 533
        echo "                    switch (jqXHR.responseJSON) {
                        case 'attendee_in':
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                            ";
        // line 539
        echo "                            ";
        // line 540
        echo "                            ";
        // line 541
        echo "                            ";
        // line 542
        echo "                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'no_user':
                        ";
        // line 547
        echo "                            break;
                        case 'bad_credentials':
                            // shouldn't happen
                            no_card_error.show();
                            no_card_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        default:
                            no_card_error.show();
                            no_card_error.html('<strong>We don\\'t really know what went wrong!</strong> Please try again, but if this message continues please let the mentor know!');
                    }
                });
            });

            \$('#no-card-submit').on('click', function () {
                no_card_form.submit();
            });

            register_form.on('submit', function (e) {
                e.preventDefault();

                var username = \$('#card_register_username').val();
                var password = \$('#card_register_password').val();
                var session = \$('#card_register_session').val();
                var swipe = \$('#card_register_swipe').val();
                \$.ajax({
                    url: register_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': session,
                        'swipe': swipe
                    },
                    type: 'POST',
                    beforeSend: function () {
                        register_form.hide();
                        register_loader.show();
                    }
                }).always(function () {
                    register_loader.hide();
                    register_form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#register-modal').modal('hide');
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    ";
        // line 593
        echo "                    switch (jqXHR.responseJSON) {
                        case 'attendee_in':
                        ";
        // line 596
        echo "                            success.html('<strong>Success!</strong> Your card has been registered and welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                            ";
        // line 600
        echo "                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    register_warning.hide();
                    register_error.show();

                    switch (jqXHR.responseJSON) {
                        case 'unregistered_user':
                        ";
        // line 608
        echo "                            register_error.html('<strong>Card could not be registered!</strong> Please try again.');
                            break;
                        case 'no_user':
                        ";
        // line 612
        echo "                            break;
                        case 'ineligible':
                            // hide modal
                            // normal error
                            break;
                        case 'bad_credentials':
                            register_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        case 'invalid':
                        ";
        // line 622
        echo "                            register_error.html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            register_error.html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });
            });

            \$('#register-submit').on('click', function () {
                register_form.submit();
            });

            no_card_form.parsley({
                errorsMessagesDisabled: true,
            });

            register_form.parsley({
                errorsMessagesDisabled: true,
            });

            \$('#no-card').on('click', function () {

            });

            \$('#register-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            \$('#no-card-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            \$('#error-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            // floating labels
            var floatingLabel;

            floatingLabel = function (onload) {
                var \$input;
                \$input = \$(this);
                if (onload) {
                    \$.each(\$('.bt-flabels__wrapper input'), function (index, value) {
                        var \$current_input;
                        \$current_input = \$(value);
                        if (\$current_input.val()) {
                            \$current_input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                        }
                    });
                }

                setTimeout((function () {
                    if (\$input.val()) {
                        \$input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                    } else {
                        \$input.closest('.bt-flabels__wrapper').removeClass('bt-flabel__float');
                    }
                }), 1);
            };

            \$('.bt-flabels__wrapper input').keydown(floatingLabel);
            \$('.bt-flabels__wrapper input').change(floatingLabel);

            window.addEventListener('load', floatingLabel(true), false);
            var error = function () {
                \$.each(this.fields, function (key, field) {
                    if (field.validationResult !== true) {
                        field.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                    }
                });
            };

            var validated = function () {
                if (this.validationResult === true) {
                    this.\$element.closest('.bt-flabels__wrapper').removeClass('bt-flabels__error');
                } else {
                    this.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                }
            };

            no_card_form.parsley().on('form:error', error);
            no_card_form.parsley().on('field:validated', validated);

            register_form.parsley().on('form:error', error);
            register_form.parsley().on('field:validated', validated);
            // end floating labels
        });

        \$(function() {
            
        });
    </script>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "shared/swipe/session.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  982 => 622,  971 => 612,  966 => 608,  957 => 600,  952 => 596,  948 => 593,  901 => 547,  895 => 542,  893 => 541,  891 => 540,  889 => 539,  882 => 533,  832 => 484,  800 => 453,  798 => 452,  737 => 393,  726 => 385,  714 => 376,  702 => 367,  694 => 362,  664 => 334,  636 => 308,  629 => 304,  624 => 302,  619 => 300,  610 => 294,  605 => 292,  600 => 290,  591 => 284,  586 => 282,  581 => 280,  575 => 277,  570 => 275,  537 => 245,  530 => 241,  525 => 239,  520 => 237,  511 => 231,  506 => 229,  501 => 227,  495 => 224,  490 => 222,  466 => 200,  452 => 192,  447 => 191,  442 => 190,  436 => 189,  432 => 187,  427 => 184,  420 => 181,  418 => 180,  415 => 179,  410 => 176,  403 => 173,  401 => 172,  395 => 171,  390 => 170,  388 => 169,  383 => 168,  381 => 167,  378 => 166,  372 => 165,  369 => 164,  366 => 163,  363 => 162,  358 => 161,  355 => 160,  350 => 159,  348 => 158,  333 => 156,  326 => 151,  323 => 150,  318 => 147,  311 => 144,  309 => 143,  306 => 142,  301 => 139,  294 => 136,  291 => 135,  288 => 134,  282 => 133,  279 => 132,  276 => 131,  273 => 130,  268 => 129,  266 => 128,  259 => 126,  255 => 125,  250 => 124,  232 => 123,  229 => 122,  226 => 121,  223 => 120,  221 => 119,  182 => 83,  171 => 75,  165 => 72,  158 => 69,  156 => 68,  154 => 67,  151 => 65,  145 => 61,  143 => 60,  138 => 58,  134 => 57,  84 => 42,  78 => 39,  67 => 31,  51 => 18,  38 => 8,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('build/images/favicon.png') }}\">

    <title>Welcome!</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"
          integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\"
          integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\"
          crossorigin=\"anonymous\">
    <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('build/css/global.css') }}\"/>
    <style>
        /*.panel {*/
        /*margin-bottom: 0;*/
        /*}*/
        /*.table {*/
        /*margin-bottom: 0;*/
        /*}*/
        /*.pagination {*/
        /*margin: 10px 0 0 10px;*/
        /*}*/
    </style>
</head>
<body style=\"background: #FFFFFF url({{ asset('build/images/swipe-background.png') }}) no-repeat right top;\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-xs-6\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <h1>{{ session.name }}</h1>
                                <h1>
                                    <small>
                                        Mentors: {% for mentor in session.mentors %} {{- mentor.preferredName -}} {% if not loop.last %} {{- ', ' -}} {% endif %} {% endfor %}</small>
                                </h1>
                            </div>
                        </div>
                        <div style=\"height: 15vh\"></div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-xs-3\"></div>
                    <div class=\"col-xs-6\">
                        <div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                                <h2>Welcome! Sign In Here</h2>
                            </div>
                            <div class=\"panel-body\">
                                {{ form_start(swipe_form, {'attr': {'class': 'form-horizontal form-label-left'}}) }}
                                {{ form_errors(swipe_form) }}
                                <div id=\"success\" class=\"alert alert-success\" style=\"display: none;\"></div>
                                {% if not session.started %}
                                    <div id=\"warning\" class=\"alert alert-warning\">The mentor needs to start the
                                        session!
                                    </div>
                                {% endif %}
                                <div class=\"form-group\">
                                    {#<div class=\"control-label\">#}
                                    {#{{ form_label(form.scancode) }}#}
                                    {#</div>#}
                                    {{ form_errors(swipe_form.scancode) }}
                                    <div>
                                        <div id=\"loader\" class=\"text-center\" style=\"display: none;\">
                                            <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                                        </div>

                                        {{ form_widget(swipe_form.scancode, {'attr': {'class': 'form-control', 'placeholder': 'Please swipe your Comet Card'}}) }}

                                        <div id=\"no-card-link\" class=\"text-right\">
                                            <a id=\"no-card\" class=\"text-right\" data-toggle=\"modal\"
                                               data-target=\"#no-card-modal\">I don't have my Comet Card</a>
                                        </div>
                                    </div>
                                </div>
                                {{ form_end(swipe_form) }}
                            </div>
                        </div>
                    </div>
                    <div class=\"col-xs-3\"></div>
                </div>
            </div>
            <div class=\"col-xs-6\">
                <div class=\"panel panel-default\" style=\"min-height: 95vh; max-height: 98vh\">
                    <div class=\"panel-heading\">
                        Session Roster
                    </div>
                    <table class=\"table table-bordered\">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Time In
                            </th>
                            <th>
                                Signed In?
                            </th>
                            <th>
                                Time Out
                            </th>
                            <th>
                                Signed Out?
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {% set page_length = 20 %}
                        {% set count = session.registeredStudents|length %}
                        {% set empty_seats = session.capacity - count %}
                        {% set max = (session.capacity is divisible by(20)) ? (session.capacity) : ((session.capacity - (session.capacity % page_length)) + page_length) %}
                        {% for student in session.registeredStudents %}
                            <tr class=\"active\" id=\"{{ student.id }}\">
                                <td>{{ loop.index }}</td>
                                <td>{{ student.firstName }} ({{- student.username -}})</td>

                                {% set a = null %}
                                {% for attendance in session.attendances %}
                                    {% if attendance.user.id == student.id %}
                                        {% set a = attendance %}
                                    {% endif %}
                                {% endfor %}
                                {% if a is not null %}
                                    {% if a.timeIn is not null %}
                                        <td>{{ a.timeIn|date('g:i A') }}</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    {% else %}
                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    {% endif %}

                                    {% if a.timeOut is not null %}
                                        <td>{{ a.timeOut|date('g:i A') }}</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    {% else %}
                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    {% endif %}
                                {% else %}
                                    <td></td>
                                    <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    <td></td>
                                    <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                {% endif %}
                            </tr>
                        {% endfor %}
                        {# TODO add students who have attended but not registered on page load #}
                        {% for attendance in session.attendances %}
                            {% set found = false %}
                            {% for student in session.registeredStudents %}
                                {% if student.id == attendance.user.id %}
                                    {% set found = true %}
                                {% endif %}
                            {% endfor %}

                            {% if not found %}
                                <tr id=\"{{ attendance.user.id }}\">
                                    {% set count = count + 1 %}
                                    <td>{{ count }}</td>
                                    <td>{{ attendance.user.firstName }} ({{- attendance.user.username -}})</td>
                                    {% if attendance.timeIn is not null %}
                                        <td>{{ attendance.timeIn|date('g:i A') }}</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    {% else %}
                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    {% endif %}

                                    {% if attendance.timeOut is not null %}
                                        <td>{{ attendance.timeOut|date('g:i A') }}</td>
                                        <td><span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span></td>
                                    {% else %}
                                        <td></td>
                                        <td><span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span></td>
                                    {% endif %}
                                </tr>
                            {% endif %}
                        {% endfor %}
                        {% for i in range(count + 1, max) %}
                            <tr class=\"active\" id=\"temp-{{ i }}\">
                                <td>{{ i }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"register-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"register-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"register-modal-label\">No Comet Card Sign In</h4>
                    </div>
                    <div class=\"modal-body\">
                        <div id=\"register-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                        <div id=\"register-warning\" class=\"alert alert-warning\">
                            <strong>Card not registered!</strong>
                            Please enter your NetID and password to register this card.
                        </div>
                        <div id=\"register-loader\" class=\"text-center\" style=\"display: none;\">
                            <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                        </div>
                        {{ form_start(register_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                {{ form_label(register_form.username) }}
                            </div>
                            {{ form_errors(register_form.username) }}
                            <div class=\"bt-flabels__wrapper\">
                                {{ form_widget(register_form.username, {'attr': {'class': 'form-control'}}) }}
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                {{ form_label(register_form.password) }}
                            </div>
                            {{ form_errors(register_form.password) }}
                            <div class=\"bt-flabels__wrapper\">
                                {{ form_widget(register_form.password, {'attr': {'class': 'form-control'}}) }}
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        {{ form_end(register_form) }}
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button id=\"register-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"no-card-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"no-card-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"no-card-modal-label\">Register Comet Card</h4>
                    </div>
                    <div class=\"modal-body\">
                        <p>
                            Having your Comet Card is a requirement to attend sessions hosted by the CSMC.
                            However, we will allow you <b style=\"color: red;\">one</b> sign in without your card per
                            semester.
                            If you choose to sign in with your Comet Card for this session, you will not be able to
                            sign in without your Comet Card again this semester.
                            <b>Your professor will also be notified that you did not have your Comet Card.</b>
                        </p>
                        <div id=\"no-card-error\" class=\"alert alert-danger\" style=\"display: none;\"></div>
                        <div id=\"no-card-loader\" class=\"text-center\" style=\"display: none;\">
                            <img src=\"{{ asset('build/images/ajax-loader.gif') }}\">
                        </div>
                        {{ form_start(no_card_form, { 'attr': {'class': 'bt-flabels js-flabels'} }) }}
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                {{ form_label(no_card_form.reason) }}
                            </div>
                            {{ form_errors(no_card_form.reason) }}
                            <div class=\"bt-flabels__wrapper\">
                                {{ form_widget(no_card_form.reason, {'attr': {'class': 'form-control'}}) }}
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                {{ form_label(no_card_form.username) }}
                            </div>
                            {{ form_errors(no_card_form.username) }}
                            <div class=\"bt-flabels__wrapper\">
                                {{ form_widget(no_card_form.username, {'attr': {'class': 'form-control'}}) }}
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"control-label\">
                                {{ form_label(no_card_form.password) }}
                            </div>
                            {{ form_errors(no_card_form.password) }}
                            <div class=\"bt-flabels__wrapper\">
                                {{ form_widget(no_card_form.password, {'attr': {'class': 'form-control'}}) }}
                                <span class=\"bt-flabels__error-desc\">Required</span>
                            </div>
                        </div>
                        {{ form_end(no_card_form) }}
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                        <button id=\"no-card-submit\" type=\"button\" class=\"btn btn-success\">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"modal fade\" id=\"error-modal\" tabindex=\"-1\" role=\"dialog\"
             aria-labelledby=\"error-modal-label\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span
                                    aria-hidden=\"true\">&times;</span></button>
                        <h4 class=\"modal-title\" id=\"error-modal-label\">Error</h4>
                    </div>
                    <div class=\"modal-body\">
                        <div id=\"error-message\" class=\"alert alert-danger\" role=\"alert\">

                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        {#<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>#}
                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Acknowledge</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
            integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
            integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"
            integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js\"></script>
    <script src=\"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js\"></script>
    <script src=\"https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js\"></script>
    <script src=\"https://cdn.datatables.net/plug-ins/1.10.16/features/pageResize/dataTables.pageResize.min.js\"></script>
    <script>
        \$(function () {
            var table = \$('.table').DataTable({
                ordering: false,
                info: false,
                searching: false,
                lengthChange: false,
                pageLength: {{ page_length }}
                // pageResize: true
            });

            // \$('.dataTables_length').css('display', 'none');
            var registered_students = {{ count }};
            var next = registered_students + 1;

            var check_out_student = function (id, time_out) {
                var row = \$('#' + id);

                row.children('td:nth-child(5)').html(time_out);
                row.children('td:nth-child(6)').html('<span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span>');

                var page = Math.floor(row.children('td:first-child').text() / {{ page_length }});
                table.page(page).draw(false);
            };

            var check_in_student = function (id, name, time_in) {
                var row = \$('#' + id);
                var page = 0;
                if (row.length) {
                    row.removeClass('active');
                    page = Math.floor(row.children('td:first-child').text() / {{ page_length }});
                } else {
                    row = \$('#temp-' + next);
                    row.prop('id', id);
                    row.removeClass('active');
                    row.children('td:nth-child(2)').html(name);
                    row.children('td:nth-child(6)').html('<span class=\"glyphicon glyphicon-remove\" style=\"color: red;\"></span>');

                    page = Math.floor(next / {{ page_length }});

                    next++;
                }

                row.children('td:nth-child(3)').html(time_in);
                row.children('td:nth-child(4)').html('<span class=\"glyphicon glyphicon-ok\" style=\"color: green;\"></span>');

                table.page(page).draw(false);
            };

            var success = \$('#success');
            var no_card_error = \$('#no-card-error');
            var register_warning = \$('#register-warning');
            var register_error = \$('#register-error');

            var loader = \$('#loader');
            // TODO consider putting a different loader for these 2 in footer of modal
            var no_card_loader = \$('#no-card-loader');
            var register_loader = \$('#register-loader');

            var input = \$('#swipe_scancode');
            input.prop('autofocus', true);
            input.get(0).focus();

            var no_card_link = \$('#no-card-link');

            var swipe_form = \$('[name=\"swipe\"]');
            var no_card_form = \$('[name=\"no_card\"]');
            var register_form = \$('[name=\"card_register\"]');

            swipe_form.on('submit', function (e) {
                e.preventDefault();

                var swipe = input.val();

                register_form.children('#card_register_swipe').val(swipe);

                \$.ajax({
                    url: swipe_form.prop('action'),
                    data: {
                        'scancode': swipe,
                        'session': \$('#swipe_session').val()
                    },
                    type: 'POST',
                    beforeSend: function () {
                        input.hide();
                        no_card_link.hide();
                        loader.show();
                    }
                }).always(function () {
                    loader.hide();
                    input.show();
                    no_card_link.show();
                }).done(function (data, textStatus, jqXHR) {
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    {# TODO need to handle session start and end #}
                    {# TODO #}
                    switch (jqXHR.responseJSON.message) {
                        case 'session_start':
                            \$('#warning').hide();
                            success.html('<strong>Success!</strong> Session started!');
                            break;
                        case 'session_end':
                            // idk what to do here
                            success.html('<strong>Session ended!</strong>');
                            break;
                        case 'attendee_in':
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                        case 'attendee_out':
                            success.html('<strong>Success!</strong> Goodbye ' + jqXHR.responseJSON.user_name + '!');
                            check_out_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.time_out);
                            break;
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'malformed_scan':
                            // rescan card again
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Scan failed!</strong> Please scan your card again.');
                            break;
                        case 'unregistered_user':
                            // register card
                            \$('#register-modal').modal('show');
                            break;
                        case 'no_user':
                        {# TODO figure out how to handle #}
                            break;
                        case 'ineligible':
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Not eligible for session!</strong> You are not on the roster for any of the sections listed for this session.');
                            break;
                        case 'invalid':
                            // shouldn't happen
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        case 'session_needs_starting':
                            \$('#error-modal-label').modal('show');
                            \$('#error-message').html('<strong>Session not started!</strong> Mentor needs to swipe first.');
                        default:
                            \$('#error-modal').modal('show');
                            \$('#error-message').html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });

                this.reset();
            });

            no_card_form.on('submit', function (e) {
                e.preventDefault();

                var username = \$('#no_card_username').val();
                var password = \$('#no_card_password').val();
                var session = \$('#no_card_session').val();
                \$.ajax({
                    url: no_card_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': session,
                    },
                    type: 'POST',
                    beforeSend: function () {
                        no_card_form.hide();
                        no_card_loader.show();
                    }
                }).always(function () {
                    no_card_loader.hide();
                    no_card_form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#no-card-modal').modal('hide');
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    {# TODO combine with normal swipe success #}
                    switch (jqXHR.responseJSON) {
                        case 'attendee_in':
                            success.html('<strong>Success!</strong> Welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                            {# TODO figure out how to handle 'swipe out' for netid sign ins #}
                            {#case 'attendee_out':#}
                            {#success.html('<strong>Success!</strong> Goodbye {{ 'person' }}!');#}
                            {#break;#}
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.responseJSON) {
                        case 'no_user':
                        {# TODO figure out how to handle #}
                            break;
                        case 'bad_credentials':
                            // shouldn't happen
                            no_card_error.show();
                            no_card_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        default:
                            no_card_error.show();
                            no_card_error.html('<strong>We don\\'t really know what went wrong!</strong> Please try again, but if this message continues please let the mentor know!');
                    }
                });
            });

            \$('#no-card-submit').on('click', function () {
                no_card_form.submit();
            });

            register_form.on('submit', function (e) {
                e.preventDefault();

                var username = \$('#card_register_username').val();
                var password = \$('#card_register_password').val();
                var session = \$('#card_register_session').val();
                var swipe = \$('#card_register_swipe').val();
                \$.ajax({
                    url: register_form.prop('action'),
                    data: {
                        'username': username,
                        'password': password,
                        'session': session,
                        'swipe': swipe
                    },
                    type: 'POST',
                    beforeSend: function () {
                        register_form.hide();
                        register_loader.show();
                    }
                }).always(function () {
                    register_loader.hide();
                    register_form.show();
                }).done(function (data, textStatus, jqXHR) {
                    \$('#register-modal').modal('hide');
                    success.show();
                    success.delay(9000).hide(\"fade\", {}, 1000, null);

                    {# success implies card was registered #}
                    switch (jqXHR.responseJSON) {
                        case 'attendee_in':
                        {# TODO better message #}
                            success.html('<strong>Success!</strong> Your card has been registered and welcome ' + jqXHR.responseJSON.user_name + '!');
                            check_in_student(jqXHR.responseJSON.user_id, jqXHR.responseJSON.user_name, jqXHR.responseJSON.time_in);
                            break;
                            {# other success messages should NOT occur, if they do something went really wrong #}
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    register_warning.hide();
                    register_error.show();

                    switch (jqXHR.responseJSON) {
                        case 'unregistered_user':
                        {# shouldn't happen #}
                            register_error.html('<strong>Card could not be registered!</strong> Please try again.');
                            break;
                        case 'no_user':
                        {# TODO figure out how to handle #}
                            break;
                        case 'ineligible':
                            // hide modal
                            // normal error
                            break;
                        case 'bad_credentials':
                            register_error.html('<strong>Invalid Credentials!</strong> Please try again.');
                            break;
                        case 'invalid':
                        {# shouldn't happen #}
                            register_error.html('<strong>Something went wrong!</strong> Please try again.');
                            break;
                        default:
                            register_error.html('<strong>Something went wrong!</strong> Please try again. If this message continues please let the mentor know!');
                    }
                });
            });

            \$('#register-submit').on('click', function () {
                register_form.submit();
            });

            no_card_form.parsley({
                errorsMessagesDisabled: true,
            });

            register_form.parsley({
                errorsMessagesDisabled: true,
            });

            \$('#no-card').on('click', function () {

            });

            \$('#register-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            \$('#no-card-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            \$('#error-modal').on('hidden.bs.modal', function (e) {
                input.get(0).focus();
            });

            // floating labels
            var floatingLabel;

            floatingLabel = function (onload) {
                var \$input;
                \$input = \$(this);
                if (onload) {
                    \$.each(\$('.bt-flabels__wrapper input'), function (index, value) {
                        var \$current_input;
                        \$current_input = \$(value);
                        if (\$current_input.val()) {
                            \$current_input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                        }
                    });
                }

                setTimeout((function () {
                    if (\$input.val()) {
                        \$input.closest('.bt-flabels__wrapper').addClass('bt-flabel__float');
                    } else {
                        \$input.closest('.bt-flabels__wrapper').removeClass('bt-flabel__float');
                    }
                }), 1);
            };

            \$('.bt-flabels__wrapper input').keydown(floatingLabel);
            \$('.bt-flabels__wrapper input').change(floatingLabel);

            window.addEventListener('load', floatingLabel(true), false);
            var error = function () {
                \$.each(this.fields, function (key, field) {
                    if (field.validationResult !== true) {
                        field.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                    }
                });
            };

            var validated = function () {
                if (this.validationResult === true) {
                    this.\$element.closest('.bt-flabels__wrapper').removeClass('bt-flabels__error');
                } else {
                    this.\$element.closest('.bt-flabels__wrapper').addClass('bt-flabels__error');
                }
            };

            no_card_form.parsley().on('form:error', error);
            no_card_form.parsley().on('field:validated', validated);

            register_form.parsley().on('form:error', error);
            register_form.parsley().on('field:validated', validated);
            // end floating labels
        });

        \$(function() {
            
        });
    </script>
</body>
</html>", "shared/swipe/session.html.twig", "/code/templates/shared/swipe/session.html.twig");
    }
}
