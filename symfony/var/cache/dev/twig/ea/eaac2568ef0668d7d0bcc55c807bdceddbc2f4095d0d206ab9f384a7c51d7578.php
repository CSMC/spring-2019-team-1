<?php

/* role/student/session/schedule_by_course.html.twig */
class __TwigTemplate_4abefe695e3ae2e6aa8a18d180dda83332450d16b94b1a5a10f714fccc0478f1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/student/session/schedule_by_course.html.twig", 2);
        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/schedule_by_course.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/student/session/schedule_by_course.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "shared/component/flash_messages.html.twig");
        echo "
    ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new Twig_Error_Runtime('Variable "courses" does not exist.', 5, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
            // line 6
            echo "        <h3>
            ";
            // line 7
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "department", []), "abbreviation", []), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "number", []), "html", null, true);
            echo "
            : ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "course", []), "name", []), "html", null, true);
            echo "
        </h3>
        <h2 class=\"content_title\">Quizzes</h2>
        <table id=\"quizzes-";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "id", []), "html", null, true);
            echo "\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Topic</th>
                <th>Dates</th>
                <th>Room</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "quizzes", [], "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["quiz"]) {
                // line 22
                echo "                <tr>
                    <td>";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "topic", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 24
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "startDate", []), "m/d/y"), "html", null, true);
                echo "
                        - ";
                // line 25
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "endDate", []), "m/d/y"), "html", null, true);
                echo "</td>
                    <td>";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "room", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["quiz"], "description", []), "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quiz'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "            </tbody>
        </table>
        <h2>Sessions</h2>
        <table id=\"sessions-";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "id", []), "html", null, true);
            echo "\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Topic</th>
                <th>Description</th>
                <th>Register</th>
            </tr>
            </thead>
            <tbody>
            ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["course"], "sessions", [], "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["session"]) {
                // line 43
                echo "                <tr>
                    <td>";
                // line 44
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "topic", []), "html", null, true);
                echo "</td>
                    <td>";
                // line 45
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["session"], "description", []), "html", null, true);
                echo "</td>
                    <td>
                        <a class=\"btn btn-success\" href=\"";
                // line 47
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("session_register_timeslot", ["sid" => twig_get_attribute($this->env, $this->source, $context["session"], "id", [])]), "html", null, true);
                echo "\">
                            Register
                        </a>
                    </td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['session'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "            </tbody>
        </table>
        <br>
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 57
            echo "        <div class=\"alert alert-danger\">You are not currently enrolled in any courses. If you believe this is an
            error please contact your instructor(s).
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 62
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 63
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        \$(function () {
            ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) || array_key_exists("courses", $context) ? $context["courses"] : (function () { throw new Twig_Error_Runtime('Variable "courses" does not exist.', 66, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
            // line 67
            echo "            \$('#quizzes-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "id", []), "html", null, true);
            echo "').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });

            \$('#sessions-";
            // line 74
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["course"], "section", [], "array"), "id", []), "html", null, true);
            echo "').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/student/session/schedule_by_course.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 81,  225 => 74,  214 => 67,  210 => 66,  203 => 63,  194 => 62,  177 => 57,  169 => 53,  157 => 47,  152 => 45,  148 => 44,  145 => 43,  141 => 42,  129 => 33,  124 => 30,  115 => 27,  111 => 26,  107 => 25,  103 => 24,  99 => 23,  96 => 22,  92 => 21,  79 => 11,  73 => 8,  67 => 7,  64 => 6,  59 => 5,  54 => 4,  45 => 3,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# For use with instructors/students #}
{% extends 'shared/base.html.twig' %}
{% block body %}
    {{ include('shared/component/flash_messages.html.twig') }}
    {% for course in courses %}
        <h3>
            {{ course['section'].course.department.abbreviation }} {{ course['section'].course.number }}
            : {{ course['section'].course.name }}
        </h3>
        <h2 class=\"content_title\">Quizzes</h2>
        <table id=\"quizzes-{{ course['section'].id }}\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Topic</th>
                <th>Dates</th>
                <th>Room</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            {% for quiz in course['quizzes'] %}
                <tr>
                    <td>{{ quiz.topic }}</td>
                    <td>{{ quiz.startDate|date('m/d/y') }}
                        - {{ quiz.endDate|date('m/d/y') }}</td>
                    <td>{{ quiz.room }}</td>
                    <td>{{ quiz.description }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
        <h2>Sessions</h2>
        <table id=\"sessions-{{ course['section'].id }}\" class=\"table table-bordered table-striped\">
            <thead>
            <tr>
                <th>Topic</th>
                <th>Description</th>
                <th>Register</th>
            </tr>
            </thead>
            <tbody>
            {% for session in course['sessions'] %}
                <tr>
                    <td>{{ session.topic }}</td>
                    <td>{{ session.description }}</td>
                    <td>
                        <a class=\"btn btn-success\" href=\"{{ path('session_register_timeslot', {'sid': session.id}) }}\">
                            Register
                        </a>
                    </td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
        <br>
    {% else %}
        <div class=\"alert alert-danger\">You are not currently enrolled in any courses. If you believe this is an
            error please contact your instructor(s).
        </div>
    {% endfor %}
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script>
        \$(function () {
            {% for course in courses %}
            \$('#quizzes-{{ course['section'].id }}').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });

            \$('#sessions-{{ course['section'].id }}').DataTable({
                paging: false,
                ordering: true,
                order: [[0, 'asc']],
                searching: true
            });
            {% endfor %}
        });
    </script>
{% endblock %}", "role/student/session/schedule_by_course.html.twig", "/code/templates/role/student/session/schedule_by_course.html.twig");
    }
}
