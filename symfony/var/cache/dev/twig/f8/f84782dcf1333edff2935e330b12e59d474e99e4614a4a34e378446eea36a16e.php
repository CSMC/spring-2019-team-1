<?php

/* role/mentor/edit_profile.html.twig */
class __TwigTemplate_f28dd3d56e046183b2b98633ed233a8752e64c545fc942e63397ffd63b792a99 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("shared/base.html.twig", "role/mentor/edit_profile.html.twig", 1);
        $this->blocks = [
            'form_errors' => [$this, 'block_form_errors'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "shared/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/edit_profile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/mentor/edit_profile.html.twig"));

        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->source); })()), [0 => $this->getTemplateName()], true);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        echo "    ";
        ob_start();
        // line 7
        echo "        ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 7, $this->source); })())) == 1)) {
            // line 8
            echo "            <div class=\"alert alert-danger\" role=\"alert\">
                ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 9, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "                    <strong>Error: </strong> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "message", []), "html", null, true);
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "            </div>
        ";
        } elseif ((twig_length_filter($this->env,         // line 13
(isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 13, $this->source); })())) > 1)) {
            // line 14
            echo "            <div class=\"alert alert-danger\" role=\"alert\">
                <strong>Errors: </strong>
                <ul>
                    ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new Twig_Error_Runtime('Variable "errors" does not exist.', 17, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 18
                echo "                        <li>WAHOO!! ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "message", []), "html", null, true);
                echo "</li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "                </ul>
            </div>
        ";
        }
        // line 23
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 27
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.css\">
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/edit_profile.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
    <style>
        .affirm{
            color:white;
            cursor: pointer;
            float:right;
            padding-right:20px;
            font-size:16px;
            font-weight:normal
        }
        .affirm:hover{
            color:blue;
        }
    </style>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 47
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 48
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\"
            src=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.js\"></script>
    <script src=\"https://unpkg.com/imask\"></script>
    <script>
        let phoneNumberMaskOptions = {
            mask: '(000) 000-0000'
        };

        let phoneNumberMask = new IMask(document.getElementById('profile_phoneNumber'), phoneNumberMaskOptions);
        let preferredPhoneNumberMask = new IMask(document.getElementById('profile_notificationPreferences_preferredPhoneNumber'), phoneNumberMaskOptions);

        // This must be checked on form submit and not when submit button is clicked, otherwise the browser can cancel
        // the submission because of required fields not being filled and the mask will break
        \$('form').on('submit', function () {
            // Submit unmasked value by changing the mask format
            // This is dirty but there doesn't seem to be a better way of doing it with this library
            // I still like it because it's plain js and the as-you-type support is good
            let emptyMaskOptions = {mask: '0000000000'};
            phoneNumberMask.updateOptions(emptyMaskOptions);
            preferredPhoneNumberMask.updateOptions(emptyMaskOptions);
        });

        // Integer mask for notification days
        new IMask(document.getElementById('profile_notificationPreferences_sessionReminderAdvanceDays'),
            {
                mask: Number,
                scale: 0,
                signed: false,
                max: 100
            });
    </script>
    <script>
        \$(function () {
            \$('.expertise-slider').on('change', function () {
                var sliderVal = \$(this).find(\"input\").val();
                \$(this).next().html(sliderVal);
            });
        });

        Dropzone.autoDiscover = false;
        \$(function () {
            \$('.dropzone').dropzone({
                url: \"";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("save_profile_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 92, $this->source); })()), "username", [])]), "html", null, true);
        echo "\",
                autoProcessQueue: false,
                maxFiles: 1,
                createImageThumbnails: true,
                thumbnailWidth: 250,
                thumbnailHeight: 250,
                previewTemplate: document.querySelector('#template-container').innerHTML,
                init: function () {

                    var element = \$('#' + this.element.id);
                    if (element.data('has-image')) {
                    }
                },
                addedfile: function (file) {
                    var dropzone = this;
                    //remove previous uploaded file
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                    if (this.element === this.previewsContainer) {
                        this.element.classList.add(\"dz-started\");
                    }
                    if (this.previewsContainer) {
                        file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());

                        this.previewsContainer.appendChild(file.previewElement);

                        var size = file.previewElement.querySelector(\"[data-dz-size]\");
                        if (size) {
                            size.innerHTML = this.filesize(file.size);
                        }

                        if (this.options.addRemoveLinks) {
                            file._removeLink = Dropzone.createElement(\"<a class=\\\"dz-remove\\\" href=\\\"javascript:undefined;\\\" data-dz-remove>\" + this.options.dictRemoveFile + \"</a>\");
                            file.previewElement.appendChild(file._removeLink);
                        }

                        var removeFileEvent = function removeFileEvent(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (file.status === Dropzone.UPLOADING) {
                                return Dropzone.confirm(_this2.options.dictCancelUploadConfirmation, function () {
                                    return this.removeFile(file);
                                });
                            } else {
                                if (this.options.dictRemoveFileConfirmation) {
                                    return Dropzone.confirm(_this2.options.dictRemoveFileConfirmation, function () {
                                        return this.removeFile(file);
                                    });
                                } else {
                                    return this.removeFile(file);
                                }
                            }
                        };

                        var remove = file.previewElement.querySelector(\"[data-dz-remove]\");
                        if (remove) {
                            remove.addEventListener(\"click\", removeFileEvent);
                        }
                    }
                    //if (!\$(dropzone.element).data('has-image')) {
                    \$(file.previewElement).off().on('click', function () {
                        \$('#modal-image').data('active', dropzone.element.id);
                        \$('#modal-image').cropper('replace', file.dataURL);
                        \$('#modal').modal('show');
                    });
                    //}
                },
                thumbnail: function (file, dataUrl) {
                    if (file.previewElement) {
                        file.previewElement.classList.remove('dz-file-preview');

                        var thumbnail = file.previewElement.querySelector(\"[data-dz-thumbnail]\");
                        if (thumbnail) {
                            thumbnail.alt = file.name;
                            thumbnail.src = dataUrl;
                        }

                        return setTimeout(function () {
                            return file.previewElement.classList.add(\"dz-image-preview\");
                        }, 1);
                    }
                },
                sending: function (file, xhr, formData) {
                    var element = \$('#' + this.element.id);

                    var crop = element.data('crop');
                    var canvas = element.data('canvas');
                    var image = element.data('image');
                    var user = element.data('user');

                    formData.append(\"crop\", crop);
                    formData.append(\"canvas\", canvas);
                    formData.append(\"image\", image);
                    formData.append(\"user\", user);
                },
                complete: function (file) {
                    //\$(file.previewElement).off('click');
                    //\$(file.previewElement.querySelector(\"[data-dz-thumbnail]\")).css('border', '2px solid green');
                },
                success: function () {
                    //this.removeAllFiles();
                }
            });

            var minWidth = window.innerWidth < 500 ? window.innerWidth * 0.8 : window.innerWidth * 0.6;
            var minHeight = window.innerHeight < 500 ? window.innerHeight * 0.8 : window.innerHeight * 0.6;

            \$('#modal-image').cropper({
                aspectRatio: 1,
                viewMode: 2,
                minContainerWidth: minWidth,
                minContainerHeight: minHeight,
                responsive: true,
                ready: function () {
                    \$('.cropper-container').css('margin', 'auto');
                    \$('.modal-content').css('width', minWidth + 30);
                    \$('#modal .btn-success').off('click').on('click', function (event) {
                        var active = \$('#modal-image').data('active');
                        var dropzone = \$('#' + active).get(0).dropzone;
                        var file = dropzone.getQueuedFiles()[0];
                        var thumb = \$('#modal-image').cropper('getCroppedCanvas').toDataURL('image/jpeg');
                        dropzone.emit(\"thumbnail\", file, thumb);

                        var crop = JSON.stringify(\$('#modal-image').cropper('getCropBoxData'));
                        var canvas = JSON.stringify(\$('#modal-image').cropper('getCanvasData'));
                        var image = JSON.stringify(\$('#modal-image').cropper('getImageData'));
                        \$('#' + active).data('crop', crop);
                        \$('#' + active).data('canvas', canvas);
                        \$('#' + active).data('image', image);

                        //dropzone.processQueue();

                        \$('#modal').modal('hide');
                    });
                }
            });

            \$('#modal').on('show.bs.modal', function (event) {
                var active = \$('#modal-image').data('active');

                var crop = \$('#' + active).data('crop');
                var canvas = \$('#' + active).data('canvas');
                var image = \$('#' + active).data('image');
                if (crop && canvas && image) {
                    \$('#modal-image').cropper('setCropBoxData', JSON.parse(crop));
                    \$('#modal-image').cropper('setCanvasData', JSON.parse(canvas));
                    \$('#modal-image').cropper('setImageData', JSON.parse(image));
                }
            });
            \$('#modal').on('hidden.bs.modal', function (event) {
                \$('#modal').data('bs.modal', null);
            });

            \$('.panel .glyphicon-trash').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.removeAllFiles();
                // TODO remove from server as well
            });

            \$('.panel .glyphicon-open').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.hiddenFileInput.click();
            });

            \$('.panel .fa-compress').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.getQueuedFiles()[0].previewElement.click();
            });

            \$('#profile_submit').on('click', function (event) {
                if (!isEmpty(\$('.dropzone').get(0).dropzone.files)) {
                    if (\$('.dropzone').data(\"crop\")) {
                        \$('.dropzone').get(0).dropzone.processQueue();
                        return true;
                    }
                    else {
                        if (confirm('Uncropped picture will not be saved, crop your picture before submit?')) {
                            \$('.dropzone').get(0).dropzone.getQueuedFiles()[0].previewElement.click();
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }

                function isEmpty(obj) {
                    for (var key in obj) {
                        if (obj.hasOwnProperty(key))
                            return false;
                    }
                    return true;
                }
            });
        });

        \$('#upload_new_profile_picture').on('click', function () {
            \$('.profile-picture-frame').get(0).style.display = 'none';
            \$('.pic-upload-panel').get(0).style.display = 'block';
            \$('#upload_new_profile_picture').get(0).style.display = 'none';
        });
    </script>
    ";
        // line 303
        echo "    <script>
        \$('#affirm').click(function(){
            document.getElementById('warninglable').style.display='none';
        })
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 309
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 310
        echo "    <div class=\"\">
        <div class=\"container\">
            <!-- new code start-->
            ";
        // line 313
        if ((isset($context["isInUse"]) || array_key_exists("isInUse", $context))) {
            // line 314
            echo "                <div id = 'warninglable' style='background:#ff8080;padding-bottom:8px;';>
                    <div  style = 'display:block;color:black;text-align:center;font-size:16px;font-weight:bold'>
                    Perferred name is already in use. Please choose another one instead! Then click submit again.
                        <div class='affirm' id = 'affirm'><u>Affirm</u></div>
                    </div>
                </div>
            ";
        }
        // line 321
        echo "            <!-- new code end-->
            <div class=\"row pt\">
                <br>
                <br>
                <div class=\"col-md-6\">

                    <div class=\"row\">
                        <div class=\"col-md-12 info-board\">
                            <div class=\"main-name\">
                                ";
        // line 330
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 330, $this->source); })()), 'form_start', ["attr" => ["class" => "form-group"]]);
        echo "

                                <fieldset>
                                    <legend>";
        // line 333
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 333, $this->source); })()), "firstName", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 333, $this->source); })()), "lastName", []), "html", null, true);
        echo "</legend>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">";
        // line 336
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 336, $this->source); })()), "preferredName", []), 'label');
        echo ":</span>
                                        <div class=\"alert alert-info\">
                                            <strong>Note: </strong>
                                            If the preferred name is left blank, your first name will be used by
                                            default
                                        </div>
                                        ";
        // line 342
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 342, $this->source); })()), "preferredName", []), 'errors');
        echo "
                                        ";
        // line 343
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 343, $this->source); })()), "preferredName", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">";
        // line 347
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 347, $this->source); })()), "birthDate", []), 'label');
        echo ":</span>
                                        ";
        // line 348
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 348, $this->source); })()), "birthDate", []), 'errors');
        echo "
                                        ";
        // line 349
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 349, $this->source); })()), "birthDate", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">";
        // line 353
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 353, $this->source); })()), "expectedGraduationSemester", []), 'label');
        echo "
                                            :</span>
                                        ";
        // line 356
        echo "                                        ";
        // line 357
        echo "                                        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 357, $this->source); })()), 'errors');
        echo "
                                        <div>
                                            ";
        // line 359
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 359, $this->source); })()), "expectedGraduationSemester", []), "season", []), 'widget', ["attr" => ["class" => "grad-sem-dropdown form-control inline-block"]]);
        echo "
                                            ";
        // line 360
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 360, $this->source); })()), "expectedGraduationSemester", []), "year", []), 'widget', ["attr" => ["class" => "grad-sem-dropdown form-control inline-block"]]);
        echo "
                                        </div>
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">";
        // line 365
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 365, $this->source); })()), "phoneNumber", []), 'label');
        echo ":</span>
                                        ";
        // line 366
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 366, $this->source); })()), "phoneNumber", []), 'errors');
        echo "
                                        ";
        // line 367
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 367, $this->source); })()), "phoneNumber", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                    </div>

                                    <div class=\"attr-field\">
                                        <p class=\"field-label\"> Course Expertise Levels: </p>
                                        ";
        // line 372
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 372, $this->source); })()), "specialties", []));
        foreach ($context['_seq'] as $context["_key"] => $context["specialty"]) {
            // line 373
            echo "                                            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["specialty"], 'label');
            echo "
                                            ";
            // line 374
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["specialty"], 'errors');
            echo "
                                            ";
            // line 375
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["specialty"], 'widget', ["attr" => ["class" => "expertise-slider col-md-11"]]);
            echo "
                                            <span class=\"expertise-level inline-block col-md-1\">";
            // line 376
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["specialty"], "vars", []), "value", []), "rating", []), "html", null, true);
            echo "</span>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['specialty'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 378
        echo "                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">";
        // line 381
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 381, $this->source); })()), "dietaryRestrictions", []), 'label');
        echo ":</span>
                                        ";
        // line 382
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 382, $this->source); })()), "dietaryRestrictions", []), 'errors');
        echo "
                                        ";
        // line 383
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 383, $this->source); })()), "dietaryRestrictions", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                    </div>

                                    ";
        // line 386
        if ((isset($context["isAdmin"]) || array_key_exists("isAdmin", $context) ? $context["isAdmin"] : (function () { throw new Twig_Error_Runtime('Variable "isAdmin" does not exist.', 386, $this->source); })())) {
            // line 387
            echo "                                        <div class=\"attr-field\">
                                            <span class=\"field-label\">";
            // line 388
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 388, $this->source); })()), "adminNotes", []), 'label');
            echo ":</span>
                                            ";
            // line 389
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 389, $this->source); })()), "adminNotes", []), 'errors');
            echo "
                                            ";
            // line 390
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 390, $this->source); })()), "adminNotes", []), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "
                                        </div>
                                    ";
        }
        // line 393
        echo "                                </fieldset>

                                <fieldset>
                                    <legend>Notification Preferences:</legend>

                                    <fieldset>
                                        <legend><span class=\"field-label\">Preferred Delivery Method:</span></legend>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            ";
        // line 402
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 402, $this->source); })()), "notificationPreferences", []), "useEmail", []), 'errors');
        echo "
                                            ";
        // line 403
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 403, $this->source); })()), "notificationPreferences", []), "useEmail", []), 'widget');
        echo "
                                            Email
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            ";
        // line 408
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 408, $this->source); })()), "notificationPreferences", []), "preferredEmail", []), 'errors');
        echo "
                                            ";
        // line 409
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 409, $this->source); })()), "notificationPreferences", []), "preferredEmail", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                        </div>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            ";
        // line 413
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 413, $this->source); })()), "notificationPreferences", []), "usePhoneNumber", []), 'widget');
        echo "
                                            Text
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            ";
        // line 418
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 418, $this->source); })()), "notificationPreferences", []), "preferredPhoneNumber", []), 'errors');
        echo "
                                            ";
        // line 419
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 419, $this->source); })()), "notificationPreferences", []), "preferredPhoneNumber", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                        </div>
                                        <div class=\"attr-field-compact form-indent-2\">
                                            ";
        // line 422
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 422, $this->source); })()), "notificationPreferences", []), "preferredPhoneNumberCarrier", []), 'errors');
        echo "
                                            ";
        // line 423
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 423, $this->source); })()), "notificationPreferences", []), "preferredPhoneNumberCarrier", []), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend><span
                                                    class=\"field-label\">I Would Like to Receive Notifications:</span>
                                        </legend>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            ";
        // line 433
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 433, $this->source); })()), "notificationPreferences", []), "notifyWhenAssigned", []), 'widget');
        echo " When I'm
                                            assigned to a session
                                        </div>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            ";
        // line 438
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 438, $this->source); })()), "notificationPreferences", []), "notifyBeforeSession", []), 'widget');
        echo " Before a
                                            session
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            ";
        // line 443
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 443, $this->source); })()), "notificationPreferences", []), "sessionReminderAdvanceDays", []), 'errors');
        echo "
                                            ";
        // line 444
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 444, $this->source); })()), "notificationPreferences", []), "sessionReminderAdvanceDays", []), 'widget', ["attr" => ["class" => "form-control form-control-inline"]]);
        echo "
                                            day(s)
                                            before a session
                                        </div>
                                    </fieldset>
                                </fieldset>

                                <div class=\"attr-field-compact container\">
                                    <a href=\"";
        // line 452
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 452, $this->source); })()), "username", [])]), "html", null, true);
        echo "\"
                                       class=\"btn btn-default\">
                                        Cancel
                                    </a>
                                    ";
        // line 456
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 456, $this->source); })()), "submit", []), 'label');
        echo "
                                    ";
        // line 457
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 457, $this->source); })()), "submit", []), 'widget', ["attr" => ["class" => "btn btn-info btn-ok"]]);
        echo "
                                </div>

                                ";
        // line 460
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 460, $this->source); })()), 'form_end');
        echo "

                            </div>
                        </div>

                    </div>


                </div>
                <div class=\"col-md-6\">
                    ";
        // line 471
        echo "                    ";
        $context["pictureRequest"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 471, $this->source); })()), "profile", []), "profilePictureModificationRequest", []);
        // line 472
        echo "                    ";
        if ( !(null === (isset($context["pictureRequest"]) || array_key_exists("pictureRequest", $context) ? $context["pictureRequest"] : (function () { throw new Twig_Error_Runtime('Variable "pictureRequest" does not exist.', 472, $this->source); })()))) {
            // line 473
            echo "                        <div class=\"panel panel-default change-req-panel profile-picture-frame\">
                            <p class=\"change-req-label\">Pending Approval:</p>
                            <img class=\"profile-picture\"
                                 src=\"";
            // line 476
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_requested_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 476, $this->source); })()), "username", [])]), "html", null, true);
            echo "\">
                        </div>
                    ";
        } else {
            // line 479
            echo "                        <div class=\"profile-picture-frame\">
                            ";
            // line 480
            if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 480, $this->source); })()), "profilePicture", []))) {
                // line 481
                echo "                                <img class=\"profile-picture\"
                                     src=\"";
                // line 482
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_image", ["username" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 482, $this->source); })()), "username", [])]), "html", null, true);
                echo "\">
                            ";
            } else {
                // line 484
                echo "                                ";
                // line 485
                echo "                                <img class=\"profile-picture\" src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/images/user.png"), "html", null, true);
                echo "\">
                            ";
            }
            // line 487
            echo "                        </div>
                    ";
        }
        // line 489
        echo "
                    <button type=\"button\" id=\"upload_new_profile_picture\" class=\"btn btn-info btn-block btn-edit\">Upload
                        New Profile Picture
                    </button>

                    <div class=\"panel panel-default pic-upload-panel\" style=\"display: none;\">
                        <div class=\"panel-heading\">
                        </div>
                        <div class=\"panel-body\">
                            <div id=\"";
        // line 498
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 498, $this->source); })()), "id", []), "html", null, true);
        echo "\" class=\"dropzone\"
                                 style=\"width: 100%; height: 0; min-height: 0; padding-bottom: calc(100% - 24px)\"
                                 data-crop
                                 data-canvas data-image data-user=\"";
        // line 501
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 501, $this->source); })()), "id", []), "html", null, true);
        echo "\" data-username=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 501, $this->source); })()), "username", []), "html", null, true);
        echo "\"
                                 data-has-image=\"";
        // line 502
        echo twig_escape_filter($this->env,  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 502, $this->source); })()), "profile", []), "image", [])), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"panel-footer\">
                            <div class=\"row\">
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
        // line 508
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 508, $this->source); })()), "id", []), "html", null, true);
        echo "\">
                                        <span class=\"glyphicon glyphicon-open\"></span>
                                        <span class=\"sr-only\">Upload</span>
                                    </button>
                                </div>
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
        // line 514
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 514, $this->source); })()), "id", []), "html", null, true);
        echo "\">
                                        <span class=\"glyphicon glyphicon-trash\"></span>
                                        <span class=\"sr-only\">Trash</span>
                                    </button>
                                </div>
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"";
        // line 520
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 520, $this->source); })()), "id", []), "html", null, true);
        echo "\">
                                        <span class=\"fas fa-compress\"></span>
                                        <span class=\"sr-only\">Crop</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id=\"template-container\" hidden>
        ";
        // line 535
        echo "        <div class=\"dz-preview dz-file-preview\"
             style=\"width: calc(100% - 32px); height: 0; min-height: 0; padding-bottom: calc(100% - 32px);\">
            <div class=\"dz-image\" style=\"width: 100%; height: 0; min-height: 0; padding-bottom: 100%;\">
                <img data-dz-thumbnail style=\"width: 100%; border: 2px solid red; border-radius: 20px\"/>
            </div>
            <div class=\"dz-details\">
                <div class=\"dz-size\">
                    <span data-dz-size></span>
                </div>
                ";
        // line 545
        echo "                ";
        // line 546
        echo "                ";
        // line 547
        echo "            </div>
            <div class=\"dz-error-message\">
                <span data-dz-errormessage></span>
            </div>
        </div>
    </div>

    <div id=\"modal\" class=\"modal fade\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">

                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-xs-12\">
                            <img id=\"modal-image\" data-active>
                        </div>
                    </div>

                </div>
                <div class=\"modal-footer\">
                    <button class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button class=\"btn btn-success\">Accept</button>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "role/mentor/edit_profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  883 => 547,  881 => 546,  879 => 545,  868 => 535,  851 => 520,  842 => 514,  833 => 508,  824 => 502,  818 => 501,  812 => 498,  801 => 489,  797 => 487,  791 => 485,  789 => 484,  784 => 482,  781 => 481,  779 => 480,  776 => 479,  770 => 476,  765 => 473,  762 => 472,  759 => 471,  746 => 460,  740 => 457,  736 => 456,  729 => 452,  718 => 444,  714 => 443,  706 => 438,  698 => 433,  685 => 423,  681 => 422,  675 => 419,  671 => 418,  663 => 413,  656 => 409,  652 => 408,  644 => 403,  640 => 402,  629 => 393,  623 => 390,  619 => 389,  615 => 388,  612 => 387,  610 => 386,  604 => 383,  600 => 382,  596 => 381,  591 => 378,  583 => 376,  579 => 375,  575 => 374,  570 => 373,  566 => 372,  558 => 367,  554 => 366,  550 => 365,  542 => 360,  538 => 359,  532 => 357,  530 => 356,  525 => 353,  518 => 349,  514 => 348,  510 => 347,  503 => 343,  499 => 342,  490 => 336,  482 => 333,  476 => 330,  465 => 321,  456 => 314,  454 => 313,  449 => 310,  440 => 309,  425 => 303,  219 => 92,  171 => 48,  162 => 47,  137 => 31,  129 => 27,  120 => 26,  109 => 23,  104 => 20,  95 => 18,  91 => 17,  86 => 14,  84 => 13,  81 => 12,  72 => 10,  68 => 9,  65 => 8,  62 => 7,  59 => 6,  50 => 5,  40 => 1,  38 => 4,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'shared/base.html.twig' %}

{# Customize how form errors are rendered for this form #}
{% form_theme form _self %}
{% block form_errors %}
    {% spaceless %}
        {% if errors|length == 1 %}
            <div class=\"alert alert-danger\" role=\"alert\">
                {% for error in errors %}
                    <strong>Error: </strong> {{ error.message }}
                {% endfor %}
            </div>
        {% elseif errors|length > 1 %}
            <div class=\"alert alert-danger\" role=\"alert\">
                <strong>Errors: </strong>
                <ul>
                    {% for error in errors %}
                        <li>WAHOO!! {{ error.message }}</li>
                    {% endfor %}
                </ul>
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock form_errors %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" type=\"text/css\"
          href=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.css\">
    <link href=\"{{ asset('build/css/edit_profile.css') }}\" rel=\"stylesheet\"/>
    <style>
        .affirm{
            color:white;
            cursor: pointer;
            float:right;
            padding-right:20px;
            font-size:16px;
            font-weight:normal
        }
        .affirm:hover{
            color:blue;
        }
    </style>
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\"
            src=\"https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.6/cropper.min.js\"></script>
    <script src=\"https://unpkg.com/imask\"></script>
    <script>
        let phoneNumberMaskOptions = {
            mask: '(000) 000-0000'
        };

        let phoneNumberMask = new IMask(document.getElementById('profile_phoneNumber'), phoneNumberMaskOptions);
        let preferredPhoneNumberMask = new IMask(document.getElementById('profile_notificationPreferences_preferredPhoneNumber'), phoneNumberMaskOptions);

        // This must be checked on form submit and not when submit button is clicked, otherwise the browser can cancel
        // the submission because of required fields not being filled and the mask will break
        \$('form').on('submit', function () {
            // Submit unmasked value by changing the mask format
            // This is dirty but there doesn't seem to be a better way of doing it with this library
            // I still like it because it's plain js and the as-you-type support is good
            let emptyMaskOptions = {mask: '0000000000'};
            phoneNumberMask.updateOptions(emptyMaskOptions);
            preferredPhoneNumberMask.updateOptions(emptyMaskOptions);
        });

        // Integer mask for notification days
        new IMask(document.getElementById('profile_notificationPreferences_sessionReminderAdvanceDays'),
            {
                mask: Number,
                scale: 0,
                signed: false,
                max: 100
            });
    </script>
    <script>
        \$(function () {
            \$('.expertise-slider').on('change', function () {
                var sliderVal = \$(this).find(\"input\").val();
                \$(this).next().html(sliderVal);
            });
        });

        Dropzone.autoDiscover = false;
        \$(function () {
            \$('.dropzone').dropzone({
                url: \"{{ path('save_profile_image', {'username': user.username}) }}\",
                autoProcessQueue: false,
                maxFiles: 1,
                createImageThumbnails: true,
                thumbnailWidth: 250,
                thumbnailHeight: 250,
                previewTemplate: document.querySelector('#template-container').innerHTML,
                init: function () {

                    var element = \$('#' + this.element.id);
                    if (element.data('has-image')) {
                    }
                },
                addedfile: function (file) {
                    var dropzone = this;
                    //remove previous uploaded file
                    if (this.files[1] != null) {
                        this.removeFile(this.files[0]);
                    }
                    if (this.element === this.previewsContainer) {
                        this.element.classList.add(\"dz-started\");
                    }
                    if (this.previewsContainer) {
                        file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());

                        this.previewsContainer.appendChild(file.previewElement);

                        var size = file.previewElement.querySelector(\"[data-dz-size]\");
                        if (size) {
                            size.innerHTML = this.filesize(file.size);
                        }

                        if (this.options.addRemoveLinks) {
                            file._removeLink = Dropzone.createElement(\"<a class=\\\"dz-remove\\\" href=\\\"javascript:undefined;\\\" data-dz-remove>\" + this.options.dictRemoveFile + \"</a>\");
                            file.previewElement.appendChild(file._removeLink);
                        }

                        var removeFileEvent = function removeFileEvent(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (file.status === Dropzone.UPLOADING) {
                                return Dropzone.confirm(_this2.options.dictCancelUploadConfirmation, function () {
                                    return this.removeFile(file);
                                });
                            } else {
                                if (this.options.dictRemoveFileConfirmation) {
                                    return Dropzone.confirm(_this2.options.dictRemoveFileConfirmation, function () {
                                        return this.removeFile(file);
                                    });
                                } else {
                                    return this.removeFile(file);
                                }
                            }
                        };

                        var remove = file.previewElement.querySelector(\"[data-dz-remove]\");
                        if (remove) {
                            remove.addEventListener(\"click\", removeFileEvent);
                        }
                    }
                    //if (!\$(dropzone.element).data('has-image')) {
                    \$(file.previewElement).off().on('click', function () {
                        \$('#modal-image').data('active', dropzone.element.id);
                        \$('#modal-image').cropper('replace', file.dataURL);
                        \$('#modal').modal('show');
                    });
                    //}
                },
                thumbnail: function (file, dataUrl) {
                    if (file.previewElement) {
                        file.previewElement.classList.remove('dz-file-preview');

                        var thumbnail = file.previewElement.querySelector(\"[data-dz-thumbnail]\");
                        if (thumbnail) {
                            thumbnail.alt = file.name;
                            thumbnail.src = dataUrl;
                        }

                        return setTimeout(function () {
                            return file.previewElement.classList.add(\"dz-image-preview\");
                        }, 1);
                    }
                },
                sending: function (file, xhr, formData) {
                    var element = \$('#' + this.element.id);

                    var crop = element.data('crop');
                    var canvas = element.data('canvas');
                    var image = element.data('image');
                    var user = element.data('user');

                    formData.append(\"crop\", crop);
                    formData.append(\"canvas\", canvas);
                    formData.append(\"image\", image);
                    formData.append(\"user\", user);
                },
                complete: function (file) {
                    //\$(file.previewElement).off('click');
                    //\$(file.previewElement.querySelector(\"[data-dz-thumbnail]\")).css('border', '2px solid green');
                },
                success: function () {
                    //this.removeAllFiles();
                }
            });

            var minWidth = window.innerWidth < 500 ? window.innerWidth * 0.8 : window.innerWidth * 0.6;
            var minHeight = window.innerHeight < 500 ? window.innerHeight * 0.8 : window.innerHeight * 0.6;

            \$('#modal-image').cropper({
                aspectRatio: 1,
                viewMode: 2,
                minContainerWidth: minWidth,
                minContainerHeight: minHeight,
                responsive: true,
                ready: function () {
                    \$('.cropper-container').css('margin', 'auto');
                    \$('.modal-content').css('width', minWidth + 30);
                    \$('#modal .btn-success').off('click').on('click', function (event) {
                        var active = \$('#modal-image').data('active');
                        var dropzone = \$('#' + active).get(0).dropzone;
                        var file = dropzone.getQueuedFiles()[0];
                        var thumb = \$('#modal-image').cropper('getCroppedCanvas').toDataURL('image/jpeg');
                        dropzone.emit(\"thumbnail\", file, thumb);

                        var crop = JSON.stringify(\$('#modal-image').cropper('getCropBoxData'));
                        var canvas = JSON.stringify(\$('#modal-image').cropper('getCanvasData'));
                        var image = JSON.stringify(\$('#modal-image').cropper('getImageData'));
                        \$('#' + active).data('crop', crop);
                        \$('#' + active).data('canvas', canvas);
                        \$('#' + active).data('image', image);

                        //dropzone.processQueue();

                        \$('#modal').modal('hide');
                    });
                }
            });

            \$('#modal').on('show.bs.modal', function (event) {
                var active = \$('#modal-image').data('active');

                var crop = \$('#' + active).data('crop');
                var canvas = \$('#' + active).data('canvas');
                var image = \$('#' + active).data('image');
                if (crop && canvas && image) {
                    \$('#modal-image').cropper('setCropBoxData', JSON.parse(crop));
                    \$('#modal-image').cropper('setCanvasData', JSON.parse(canvas));
                    \$('#modal-image').cropper('setImageData', JSON.parse(image));
                }
            });
            \$('#modal').on('hidden.bs.modal', function (event) {
                \$('#modal').data('bs.modal', null);
            });

            \$('.panel .glyphicon-trash').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.removeAllFiles();
                // TODO remove from server as well
            });

            \$('.panel .glyphicon-open').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.hiddenFileInput.click();
            });

            \$('.panel .fa-compress').closest(\"button\").on('click', function (event) {
                var dropzone = \$('#' + \$(this).data('dropzone')).get(0).dropzone;
                dropzone.getQueuedFiles()[0].previewElement.click();
            });

            \$('#profile_submit').on('click', function (event) {
                if (!isEmpty(\$('.dropzone').get(0).dropzone.files)) {
                    if (\$('.dropzone').data(\"crop\")) {
                        \$('.dropzone').get(0).dropzone.processQueue();
                        return true;
                    }
                    else {
                        if (confirm('Uncropped picture will not be saved, crop your picture before submit?')) {
                            \$('.dropzone').get(0).dropzone.getQueuedFiles()[0].previewElement.click();
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }

                function isEmpty(obj) {
                    for (var key in obj) {
                        if (obj.hasOwnProperty(key))
                            return false;
                    }
                    return true;
                }
            });
        });

        \$('#upload_new_profile_picture').on('click', function () {
            \$('.profile-picture-frame').get(0).style.display = 'none';
            \$('.pic-upload-panel').get(0).style.display = 'block';
            \$('#upload_new_profile_picture').get(0).style.display = 'none';
        });
    </script>
    {#<script>
        \$(document).ready(function () {
            setTimeout(displaydely,5000);
        });
        function displaydely(){
            document.getElementById('warninglable').style.display='none';
        }
    </script>#}
    <script>
        \$('#affirm').click(function(){
            document.getElementById('warninglable').style.display='none';
        })
    </script>
{% endblock %}
{% block body %}
    <div class=\"\">
        <div class=\"container\">
            <!-- new code start-->
            {% if isInUse is defined %}
                <div id = 'warninglable' style='background:#ff8080;padding-bottom:8px;';>
                    <div  style = 'display:block;color:black;text-align:center;font-size:16px;font-weight:bold'>
                    Perferred name is already in use. Please choose another one instead! Then click submit again.
                        <div class='affirm' id = 'affirm'><u>Affirm</u></div>
                    </div>
                </div>
            {% endif %}
            <!-- new code end-->
            <div class=\"row pt\">
                <br>
                <br>
                <div class=\"col-md-6\">

                    <div class=\"row\">
                        <div class=\"col-md-12 info-board\">
                            <div class=\"main-name\">
                                {{ form_start(form, {'attr': {'class': 'form-group'}}) }}

                                <fieldset>
                                    <legend>{{ user.firstName }} {{ user.lastName }}</legend>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">{{ form_label(form.preferredName) }}:</span>
                                        <div class=\"alert alert-info\">
                                            <strong>Note: </strong>
                                            If the preferred name is left blank, your first name will be used by
                                            default
                                        </div>
                                        {{ form_errors(form.preferredName) }}
                                        {{ form_widget(form.preferredName, {'attr': {'class': 'form-control'}}) }}
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">{{ form_label(form.birthDate) }}:</span>
                                        {{ form_errors(form.birthDate) }}
                                        {{ form_widget(form.birthDate, {'attr': {'class': 'form-control'}}) }}
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">{{ form_label(form.expectedGraduationSemester) }}
                                            :</span>
                                        {#FIXME:This is a workaround, since symfony won't let me display these errors on the right field, see https://github.com/symfony/symfony/issues/19598 #}
                                        {#If we ever need to display form errors, this will break#}
                                        {{ form_errors(form) }}
                                        <div>
                                            {{ form_widget(form.expectedGraduationSemester.season, {'attr': {'class': 'grad-sem-dropdown form-control inline-block'}}) }}
                                            {{ form_widget(form.expectedGraduationSemester.year, {'attr': {'class': 'grad-sem-dropdown form-control inline-block'}}) }}
                                        </div>
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">{{ form_label(form.phoneNumber) }}:</span>
                                        {{ form_errors(form.phoneNumber) }}
                                        {{ form_widget(form.phoneNumber, {'attr': {'class': 'form-control'}}) }}
                                    </div>

                                    <div class=\"attr-field\">
                                        <p class=\"field-label\"> Course Expertise Levels: </p>
                                        {% for specialty in form.specialties %}
                                            {{ form_label(specialty) }}
                                            {{ form_errors(specialty) }}
                                            {{ form_widget(specialty, {'attr': {'class': 'expertise-slider col-md-11'}}) }}
                                            <span class=\"expertise-level inline-block col-md-1\">{{ specialty.vars.value.rating }}</span>
                                        {% endfor %}
                                    </div>

                                    <div class=\"attr-field\">
                                        <span class=\"field-label\">{{ form_label(form.dietaryRestrictions) }}:</span>
                                        {{ form_errors(form.dietaryRestrictions) }}
                                        {{ form_widget(form.dietaryRestrictions, {'attr': {'class': 'form-control'}}) }}
                                    </div>

                                    {% if isAdmin %}
                                        <div class=\"attr-field\">
                                            <span class=\"field-label\">{{ form_label(form.adminNotes) }}:</span>
                                            {{ form_errors(form.adminNotes) }}
                                            {{ form_widget(form.adminNotes, {'attr': {'class': 'form-control'}}) }}
                                        </div>
                                    {% endif %}
                                </fieldset>

                                <fieldset>
                                    <legend>Notification Preferences:</legend>

                                    <fieldset>
                                        <legend><span class=\"field-label\">Preferred Delivery Method:</span></legend>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            {{ form_errors(form.notificationPreferences.useEmail) }}
                                            {{ form_widget(form.notificationPreferences.useEmail) }}
                                            Email
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            {{ form_errors(form.notificationPreferences.preferredEmail) }}
                                            {{ form_widget(form.notificationPreferences.preferredEmail, {'attr': {'class': 'form-control'}}) }}
                                        </div>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            {{ form_widget(form.notificationPreferences.usePhoneNumber) }}
                                            Text
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            {{ form_errors(form.notificationPreferences.preferredPhoneNumber) }}
                                            {{ form_widget(form.notificationPreferences.preferredPhoneNumber, {'attr': {'class': 'form-control'}}) }}
                                        </div>
                                        <div class=\"attr-field-compact form-indent-2\">
                                            {{ form_errors(form.notificationPreferences.preferredPhoneNumberCarrier) }}
                                            {{ form_widget(form.notificationPreferences.preferredPhoneNumberCarrier, {'attr': {'class': 'form-control'}}) }}
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend><span
                                                    class=\"field-label\">I Would Like to Receive Notifications:</span>
                                        </legend>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            {{ form_widget(form.notificationPreferences.notifyWhenAssigned) }} When I'm
                                            assigned to a session
                                        </div>

                                        <div class=\"attr-field-compact form-indent-1\">
                                            {{ form_widget(form.notificationPreferences.notifyBeforeSession) }} Before a
                                            session
                                        </div>

                                        <div class=\"attr-field-compact form-indent-2\">
                                            {{ form_errors(form.notificationPreferences.sessionReminderAdvanceDays) }}
                                            {{ form_widget(form.notificationPreferences.sessionReminderAdvanceDays, {'attr': {'class': 'form-control form-control-inline'}}) }}
                                            day(s)
                                            before a session
                                        </div>
                                    </fieldset>
                                </fieldset>

                                <div class=\"attr-field-compact container\">
                                    <a href=\"{{ path(\"profile\", {\"username\": user.username}) }}\"
                                       class=\"btn btn-default\">
                                        Cancel
                                    </a>
                                    {{ form_label(form.submit) }}
                                    {{ form_widget(form.submit, {'attr': {'class': 'btn btn-info btn-ok'}}) }}
                                </div>

                                {{ form_end(form) }}

                            </div>
                        </div>

                    </div>


                </div>
                <div class=\"col-md-6\">
                    {#Change requested alert for profile picture#}
                    {% set pictureRequest = user.profile.profilePictureModificationRequest %}
                    {% if pictureRequest is not null %}
                        <div class=\"panel panel-default change-req-panel profile-picture-frame\">
                            <p class=\"change-req-label\">Pending Approval:</p>
                            <img class=\"profile-picture\"
                                 src=\"{{ path('profile_requested_image', {'username': user.username}) }}\">
                        </div>
                    {% else %}
                        <div class=\"profile-picture-frame\">
                            {% if user.profilePicture is not null %}
                                <img class=\"profile-picture\"
                                     src=\"{{ path('profile_image', {'username': user.username}) }}\">
                            {% else %}
                                {#Use the placeholder image#}
                                <img class=\"profile-picture\" src=\"{{ asset('build/images/user.png') }}\">
                            {% endif %}
                        </div>
                    {% endif %}

                    <button type=\"button\" id=\"upload_new_profile_picture\" class=\"btn btn-info btn-block btn-edit\">Upload
                        New Profile Picture
                    </button>

                    <div class=\"panel panel-default pic-upload-panel\" style=\"display: none;\">
                        <div class=\"panel-heading\">
                        </div>
                        <div class=\"panel-body\">
                            <div id=\"{{ user.id }}\" class=\"dropzone\"
                                 style=\"width: 100%; height: 0; min-height: 0; padding-bottom: calc(100% - 24px)\"
                                 data-crop
                                 data-canvas data-image data-user=\"{{ user.id }}\" data-username=\"{{ user.username }}\"
                                 data-has-image=\"{{ user.profile.image is not null }}\">
                            </div>
                        </div>
                        <div class=\"panel-footer\">
                            <div class=\"row\">
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                                        <span class=\"glyphicon glyphicon-open\"></span>
                                        <span class=\"sr-only\">Upload</span>
                                    </button>
                                </div>
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                                        <span class=\"glyphicon glyphicon-trash\"></span>
                                        <span class=\"sr-only\">Trash</span>
                                    </button>
                                </div>
                                <div class=\"col-xs-4\">
                                    <button class=\"btn btn-default\" style=\"width: 100%;\" data-dropzone=\"{{ user.id }}\">
                                        <span class=\"fas fa-compress\"></span>
                                        <span class=\"sr-only\">Crop</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id=\"template-container\" hidden>
        {# TODO add progress bar and indication if uploaded or not #}
        <div class=\"dz-preview dz-file-preview\"
             style=\"width: calc(100% - 32px); height: 0; min-height: 0; padding-bottom: calc(100% - 32px);\">
            <div class=\"dz-image\" style=\"width: 100%; height: 0; min-height: 0; padding-bottom: 100%;\">
                <img data-dz-thumbnail style=\"width: 100%; border: 2px solid red; border-radius: 20px\"/>
            </div>
            <div class=\"dz-details\">
                <div class=\"dz-size\">
                    <span data-dz-size></span>
                </div>
                {#<div class=\"dz-filename\">#}
                {#<span data-dz-name></span>#}
                {#</div>#}
            </div>
            <div class=\"dz-error-message\">
                <span data-dz-errormessage></span>
            </div>
        </div>
    </div>

    <div id=\"modal\" class=\"modal fade\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">

                </div>
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-xs-12\">
                            <img id=\"modal-image\" data-active>
                        </div>
                    </div>

                </div>
                <div class=\"modal-footer\">
                    <button class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    <button class=\"btn btn-success\">Accept</button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "role/mentor/edit_profile.html.twig", "/code/templates/role/mentor/edit_profile.html.twig");
    }
}
