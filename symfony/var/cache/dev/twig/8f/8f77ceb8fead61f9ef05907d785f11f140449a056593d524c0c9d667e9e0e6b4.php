<?php

/* role/admin/component/nav.html.twig */
class __TwigTemplate_0f94b21d554b8895009e7ebada5086ef0c012dac958e7d292b239af5fe79474e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/component/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "role/admin/component/nav.html.twig"));

        // line 1
        echo "<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a href=\"index.html\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>CSMC Admin!</span></a>
        </div>

        <div class=\"clearfix\"></div>

        ";
        // line 10
        echo "        ";
        // line 11
        echo "        ";
        // line 12
        echo "        ";
        // line 13
        echo "        ";
        // line 14
        echo "        ";
        // line 15
        echo "        ";
        // line 16
        echo "        ";
        // line 17
        echo "        ";
        // line 18
        echo "        ";
        // line 19
        echo "        ";
        // line 20
        echo "        ";
        // line 21
        echo "
        ";
        // line 23
        echo "
        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                <h3>General</h3>
                <ul class=\"nav side-menu\">
                    <li><a href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home");
        echo "\"><i class=\"fa fa-home\"></i> Home <span class=\"fa fa-chevron-down\"></span></a>

                    </li>
                    <li><a><i class=\"fa fa-cog\"></i> System Set Up <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_ip_list");
        echo "\">IPs</a></li>
                            <li><a href=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_room_list");
        echo "\">Rooms</a></li>
                            <li><a href=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_semester_list");
        echo "\">Semesters</a></li>
                            <li><a href=\"";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_department_list");
        echo "\">Departments</a></li>
                            <li><a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_course_list");
        echo "\">Courses</a></li>
                            <li><a href=\"";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_section_list");
        echo "\">Sections</a></li>
                            <li><a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_hours_list");
        echo "\">Operation Hours</a></li>
                            <li><a href=\"#";
        // line 41
        echo "\">Schedule Time</a></li>
                        </ul>
                    </li>

                    <li><a><i class=\"fa fa-user\"></i> Users <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_list");
        echo "\">User</a></li>
                            <li><a href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_user_group_list");
        echo "\">User Groups</a></li>
                            <li><a href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_role_list");
        echo "\">Roles</a></li>
                        </ul>
                    </li>

                    <li><a><i class=\"fa fa-users\"></i> Mentor Management <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li id=\"occurrence-summary-tab\"><a href=\"";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_view_occurrence_summary");
        echo "\">Occurrences</a></li>
                            <li id=\"mentor-summary-tab\"><a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_view_mentor_summary");
        echo "\">Mentor Summary</a></li>
                            <li><a href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_view_mentor_specialty");
        echo "\">Specialty</a></li>
                            <li><a href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("behavior_occurrence_submission");
        echo "\">Submit Occurrence</a></li>
                            <li><a href=\"";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_view_mentor_summary_settings");
        echo "\">Settings</a></li>
                        </ul>
                    </li>

                    <li><a href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_announcement_list");
        echo "\"><i class=\"fa fa-bullhorn\"></i> Announcements</a>
                    </li>

                    <li><a>
                            <i class=\"fa fa-clock-o\"></i> Session Schedule ";
        // line 68
        echo "                            <span class=\"fa fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_calendar");
        echo "\">Calendar</a></li>
                            <li><a href=\"";
        // line 72
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_session_requests");
        echo "\">Requests</a></li>
                            <li><a href=\"#";
        // line 73
        echo "\">Holidays</a></li>
                        </ul>
                    </li>

                    <li>
                        <a>
                            <i class=\"fa fa-suitcase\"></i> Staffing <span class=\"fa fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"";
        // line 82
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_schedule_calendar");
        echo "\">Calendar</a></li>
                            <li><a href=\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_schedule_absences");
        echo "\">Absences</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                    <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                       aria-expanded=\"false\">
                        <img src=\"images/img.jpg\" alt=\"\">CSMC Admin
                        <span class=\" fa fa-angle-down\"></span>
                    </a>
                    <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                        <li><a href=\"javascript:;\"> Profile</a></li>
                        <li>
                            <a href=\"javascript:;\">
                                <span class=\"badge bg-red pull-right\">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href=\"javascript:;\">Help</a></li>
                        <li><a href=\"login.html\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "role/admin/component/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 83,  187 => 82,  176 => 73,  172 => 72,  168 => 71,  163 => 68,  156 => 63,  149 => 59,  145 => 58,  141 => 57,  137 => 56,  133 => 55,  124 => 49,  120 => 48,  116 => 47,  108 => 41,  104 => 40,  100 => 39,  96 => 38,  92 => 37,  88 => 36,  84 => 35,  80 => 34,  72 => 29,  64 => 23,  61 => 21,  59 => 20,  57 => 19,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  47 => 14,  45 => 13,  43 => 12,  41 => 11,  39 => 10,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"col-md-3 left_col\">
    <div class=\"left_col scroll-view\">
        <div class=\"navbar nav_title\" style=\"border: 0;\">
            <a href=\"index.html\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>CSMC Admin!</span></a>
        </div>

        <div class=\"clearfix\"></div>

        {#<!-- menu profile quick info -->#}
        {#<div class=\"profile clearfix\">#}
        {#<div class=\"profile_pic\">#}
        {#<img src=\"images/img.jpg\" alt=\"...\" class=\"img-circle profile_img\">#}
        {#</div>#}
        {#<div class=\"profile_info\">#}
        {#<span>Welcome,</span>#}
        {#<h2>John Doe</h2>#}
        {#</div>#}
        {#<div class=\"clearfix\"></div>#}
        {#</div>#}
        {#<!-- /menu profile quick info -->#}

        {#<br />#}

        <!-- sidebar menu -->
        <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
            <div class=\"menu_section\">
                <h3>General</h3>
                <ul class=\"nav side-menu\">
                    <li><a href=\"{{ path('home') }}\"><i class=\"fa fa-home\"></i> Home <span class=\"fa fa-chevron-down\"></span></a>

                    </li>
                    <li><a><i class=\"fa fa-cog\"></i> System Set Up <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_ip_list') }}\">IPs</a></li>
                            <li><a href=\"{{ path('admin_room_list') }}\">Rooms</a></li>
                            <li><a href=\"{{ path('admin_semester_list') }}\">Semesters</a></li>
                            <li><a href=\"{{ path('admin_department_list') }}\">Departments</a></li>
                            <li><a href=\"{{ path('admin_course_list') }}\">Courses</a></li>
                            <li><a href=\"{{ path('admin_section_list') }}\">Sections</a></li>
                            <li><a href=\"{{ path('admin_hours_list') }}\">Operation Hours</a></li>
                            <li><a href=\"#{#{ path('admin_schedule_list') }#}\">Schedule Time</a></li>
                        </ul>
                    </li>

                    <li><a><i class=\"fa fa-user\"></i> Users <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_user_list') }}\">User</a></li>
                            <li><a href=\"{{ path('admin_user_group_list') }}\">User Groups</a></li>
                            <li><a href=\"{{ path('admin_role_list') }}\">Roles</a></li>
                        </ul>
                    </li>

                    <li><a><i class=\"fa fa-users\"></i> Mentor Management <span class=\"fa fa-chevron-down\"></span></a>
                        <ul class=\"nav child_menu\">
                            <li id=\"occurrence-summary-tab\"><a href=\"{{ path('admin_view_occurrence_summary') }}\">Occurrences</a></li>
                            <li id=\"mentor-summary-tab\"><a href=\"{{ path('admin_view_mentor_summary') }}\">Mentor Summary</a></li>
                            <li><a href=\"{{ path('admin_view_mentor_specialty') }}\">Specialty</a></li>
                            <li><a href=\"{{ path('behavior_occurrence_submission') }}\">Submit Occurrence</a></li>
                            <li><a href=\"{{ path('admin_view_mentor_summary_settings') }}\">Settings</a></li>
                        </ul>
                    </li>

                    <li><a href=\"{{ path('admin_announcement_list') }}\"><i class=\"fa fa-bullhorn\"></i> Announcements</a>
                    </li>

                    <li><a>
                            <i class=\"fa fa-clock-o\"></i> Session Schedule {#<span class=\"badge bg-green\">6</span>#}
                            <span class=\"fa fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_session_calendar') }}\">Calendar</a></li>
                            <li><a href=\"{{ path('admin_session_requests') }}\">Requests</a></li>
                            <li><a href=\"#{#{ path('admin_holiday_list') }#}\">Holidays</a></li>
                        </ul>
                    </li>

                    <li>
                        <a>
                            <i class=\"fa fa-suitcase\"></i> Staffing <span class=\"fa fa-chevron-down\"></span>
                        </a>
                        <ul class=\"nav child_menu\">
                            <li><a href=\"{{ path('admin_schedule_calendar') }}\">Calendar</a></li>
                            <li><a href=\"{{ path('admin_schedule_absences') }}\">Absences</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class=\"sidebar-footer hidden-small\">
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
            </a>
            <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"login.html\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class=\"top_nav\">
    <div class=\"nav_menu\">
        <nav>
            <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
            </div>

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                    <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\"
                       aria-expanded=\"false\">
                        <img src=\"images/img.jpg\" alt=\"\">CSMC Admin
                        <span class=\" fa fa-angle-down\"></span>
                    </a>
                    <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                        <li><a href=\"javascript:;\"> Profile</a></li>
                        <li>
                            <a href=\"javascript:;\">
                                <span class=\"badge bg-red pull-right\">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href=\"javascript:;\">Help</a></li>
                        <li><a href=\"login.html\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
", "role/admin/component/nav.html.twig", "/code/templates/role/admin/component/nav.html.twig");
    }
}
