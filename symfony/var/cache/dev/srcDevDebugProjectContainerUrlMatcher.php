<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                if (0 === strpos($pathinfo, '/admin/announcement')) {
                    // admin_announcement_list
                    if ('/admin/announcement' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\AnnouncementController::listAnnouncements',  '_route' => 'admin_announcement_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_announcement_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_announcement_list'));
                        }

                        return $ret;
                    }
                    not_admin_announcement_list:

                    // admin_announcement_create
                    if ('/admin/announcement/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\AnnouncementController::addNewAnnouncement',  '_route' => 'admin_announcement_create',);
                    }

                    // admin_announcement_edit
                    if ('/admin/announcement/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\AnnouncementController::editAnnouncement',  '_route' => 'admin_announcement_edit',);
                    }

                    // admin_announcement_delete
                    if ('/admin/announcement/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\AnnouncementController::deleteAnnouncement',  '_route' => 'admin_announcement_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/course')) {
                    // admin_course_list
                    if ('/admin/course' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\CourseController::listCourses',  '_route' => 'admin_course_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_course_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_course_list'));
                        }

                        return $ret;
                    }
                    not_admin_course_list:

                    // admin_course_create
                    if ('/admin/course/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\CourseController::addNewCourse',  '_route' => 'admin_course_create',);
                    }

                    // admin_course_edit
                    if (0 === strpos($pathinfo, '/admin/course/edit') && preg_match('#^/admin/course/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_course_edit']), array (  '_controller' => 'App\\Controller\\Admin\\CourseController::editCourse',));
                    }

                    // admin_course_delete
                    if ('/admin/course/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\CourseController::deleteCourse',  '_route' => 'admin_course_delete',);
                    }

                    // admin_course_roster_upload
                    if ('/admin/course/roster/upload' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\CourseController::courseRosterUploadAction',  '_route' => 'admin_course_roster_upload',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/department')) {
                    // admin_department_create
                    if ('/admin/department/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\DepartmentController::AddDepartmentAction',  '_route' => 'admin_department_create',);
                    }

                    // admin_department_list
                    if ('/admin/department' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\DepartmentController::ListDepartmentsFunction',  '_route' => 'admin_department_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_department_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_department_list'));
                        }

                        return $ret;
                    }
                    not_admin_department_list:

                    // admin_department_edit
                    if (0 === strpos($pathinfo, '/admin/department/edit') && preg_match('#^/admin/department/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_department_edit']), array (  '_controller' => 'App\\Controller\\Admin\\DepartmentController::EditDepartmentFunction',));
                    }

                    // admin_department_delete
                    if ('/admin/department/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\DepartmentController::deleteDepartmentFunction',  '_route' => 'admin_department_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/holiday')) {
                    // admin_holiday_create
                    if ('/admin/holiday/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\HolidaysController::addHolidaysAction',  '_route' => 'admin_holiday_create',);
                    }

                    // admin_holiday_list
                    if ('/admin/holiday' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\HolidaysController::listHolidaysFunction',  '_route' => 'admin_holiday_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_holiday_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_holiday_list'));
                        }

                        return $ret;
                    }
                    not_admin_holiday_list:

                    // admin_holiday_edit
                    if ('/admin/holiday/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\HolidaysController::editHolidayFunction',  '_route' => 'admin_holiday_edit',);
                    }

                    // admin_holiday_delete
                    if ('/admin/holiday/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\HolidaysController::deleteHolidayFunction',  '_route' => 'admin_holiday_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/hours')) {
                    // admin_hours_list
                    if ('/admin/hours' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\OperationHoursController::listOperationHours',  '_route' => 'admin_hours_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_hours_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_hours_list'));
                        }

                        return $ret;
                    }
                    not_admin_hours_list:

                    // admin_hours_edit
                    if ('/admin/hours/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\OperationHoursController::editOperationHours',  '_route' => 'admin_hours_edit',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/ip')) {
                    // admin_ip_list
                    if ('/admin/ip' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\IpController::addressShowAll',  '_route' => 'admin_ip_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_ip_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_ip_list'));
                        }

                        return $ret;
                    }
                    not_admin_ip_list:

                    // admin_ip_create
                    if ('/admin/ip/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\IpController::addAddress',  '_route' => 'admin_ip_create',);
                    }

                    // admin_ip_edit
                    if ('/admin/ip/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\IpController::addressEdit',  '_route' => 'admin_ip_edit',);
                    }

                    // admin_ip_delete
                    if ('/admin/ip/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\IpController::deleteAddress',  '_route' => 'admin_ip_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/role')) {
                    // admin_role_list
                    if ('/admin/role' === $trimmedPathinfo) {
                        $ret = array (  '_controller' => 'App\\Controller\\Admin\\RoleController::roleShowAll',  '_route' => 'admin_role_list',);
                        if ('/' === substr($pathinfo, -1)) {
                            // no-op
                        } elseif ('GET' !== $canonicalMethod) {
                            goto not_admin_role_list;
                        } else {
                            return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_role_list'));
                        }

                        return $ret;
                    }
                    not_admin_role_list:

                    // admin_role_create
                    if ('/admin/role/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoleController::addNewRole',  '_route' => 'admin_role_create',);
                    }

                    // admin_role_edit
                    if ('/admin/role/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoleController::editRole',  '_route' => 'admin_role_edit',);
                    }

                    // admin_role_delete
                    if ('/admin/role/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoleController::deleteRole',  '_route' => 'admin_role_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/room')) {
                    // admin_room_list
                    if ('/admin/room/list' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoomsController::roomShowAll',  '_route' => 'admin_room_list',);
                    }

                    // admin_room_create
                    if ('/admin/room/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoomsController::roomAdd',  '_route' => 'admin_room_create',);
                    }

                    // admin_room_edit
                    if ('/admin/room/edit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoomsController::roomEdit',  '_route' => 'admin_room_edit',);
                    }

                    // admin_room_delete
                    if ('/admin/room/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\RoomsController::roomDelete',  '_route' => 'admin_room_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/s')) {
                    if (0 === strpos($pathinfo, '/admin/schedule')) {
                        // admin_schedule_calendar
                        if ('/admin/schedule/calendar' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::calendarAction',  '_route' => 'admin_schedule_calendar',);
                        }

                        // admin_schedule_absences
                        if ('/admin/schedule/absences' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::absencesAction',  '_route' => 'admin_schedule_absences',);
                        }

                        if (0 === strpos($pathinfo, '/admin/schedule/ajax/shift')) {
                            // admin_schedule_ajax_shift
                            if ('/admin/schedule/ajax/shift' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::ajaxShiftAction',  '_route' => 'admin_schedule_ajax_shift',);
                            }

                            // admin_schedule_ajax_shift_remove
                            if ('/admin/schedule/ajax/shift/remove' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::ajaxShiftRemoveAction',  '_route' => 'admin_schedule_ajax_shift_remove',);
                            }

                            // admin_schedule_ajax_shift_leader
                            if ('/admin/schedule/ajax/shift/leader' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::ajaxShiftLeaderAction',  '_route' => 'admin_schedule_ajax_shift_leader',);
                            }

                        }

                        // admin_schedule_timesheets
                        if ('/admin/schedule/timesheets' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::timesheetsAction',  '_route' => 'admin_schedule_timesheets',);
                        }

                        // admin_schedule_update
                        if ('/admin/schedule/update' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleController::updateAction',  '_route' => 'admin_schedule_update',);
                        }

                        // admin_schedule_list
                        if ('/admin/schedule' === $trimmedPathinfo) {
                            $ret = array (  '_controller' => 'App\\Controller\\Admin\\ScheduleTimeController::listScheduleTime',  '_route' => 'admin_schedule_list',);
                            if ('/' === substr($pathinfo, -1)) {
                                // no-op
                            } elseif ('GET' !== $canonicalMethod) {
                                goto not_admin_schedule_list;
                            } else {
                                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_schedule_list'));
                            }

                            return $ret;
                        }
                        not_admin_schedule_list:

                        // admin_schedule_edit
                        if ('/admin/schedule/edit' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\ScheduleTimeController::editScheduleTime',  '_route' => 'admin_schedule_edit',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/admin/se')) {
                        if (0 === strpos($pathinfo, '/admin/section')) {
                            // admin_section_list
                            if ('/admin/section' === $trimmedPathinfo) {
                                $ret = array (  '_controller' => 'App\\Controller\\Admin\\SectionController::listSections',  '_route' => 'admin_section_list',);
                                if ('/' === substr($pathinfo, -1)) {
                                    // no-op
                                } elseif ('GET' !== $canonicalMethod) {
                                    goto not_admin_section_list;
                                } else {
                                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_section_list'));
                                }

                                return $ret;
                            }
                            not_admin_section_list:

                            // admin_section_create
                            if ('/admin/section/create' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SectionController::addNewSection',  '_route' => 'admin_section_create',);
                            }

                            // admin_section_edit
                            if (0 === strpos($pathinfo, '/admin/section/edit') && preg_match('#^/admin/section/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_section_edit']), array (  '_controller' => 'App\\Controller\\Admin\\SectionController::editSection',));
                            }

                            // admin_section_delete
                            if ('/admin/section/delete' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SectionController::deleteSection',  '_route' => 'admin_section_delete',);
                            }

                            // admin_section_roster
                            if (0 === strpos($pathinfo, '/admin/section/roster') && preg_match('#^/admin/section/roster/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_section_roster']), array (  '_controller' => 'App\\Controller\\Admin\\SectionController::rosterAction',));
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/semester')) {
                            // admin_semester_list
                            if ('/admin/semester' === $trimmedPathinfo) {
                                $ret = array (  '_controller' => 'App\\Controller\\Admin\\SemesterController::semesterShowAll',  '_route' => 'admin_semester_list',);
                                if ('/' === substr($pathinfo, -1)) {
                                    // no-op
                                } elseif ('GET' !== $canonicalMethod) {
                                    goto not_admin_semester_list;
                                } else {
                                    return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_semester_list'));
                                }

                                return $ret;
                            }
                            not_admin_semester_list:

                            // admin_semester_create
                            if ('/admin/semester/create' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SemesterController::addSemester',  '_route' => 'admin_semester_create',);
                            }

                            // admin_semester_edit
                            if ('/admin/semester/edit' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SemesterController::semesterEdit',  '_route' => 'admin_semester_edit',);
                            }

                            // admin_semester_delete
                            if ('/admin/semester/delete' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SemesterController::deleteSemester',  '_route' => 'admin_semester_delete',);
                            }

                        }

                        elseif (0 === strpos($pathinfo, '/admin/session')) {
                            // admin_session_calendar
                            if ('/admin/session/calendar' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::calendarAction',  '_route' => 'admin_session_calendar',);
                            }

                            if (0 === strpos($pathinfo, '/admin/session/create')) {
                                // admin_session_create_scheduled
                                if ('/admin/session/create/scheduled' === $pathinfo) {
                                    return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::createScheduledSessionAction',  '_route' => 'admin_session_create_scheduled',);
                                }

                                // admin_session_create_quiz
                                if ('/admin/session/create/quiz' === $pathinfo) {
                                    return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::createQuizAction',  '_route' => 'admin_session_create_quiz',);
                                }

                                // admin_session_create_time_slot
                                if ('/admin/session/create/timeslot' === $pathinfo) {
                                    return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::createTimeSlotAction',  '_route' => 'admin_session_create_time_slot',);
                                }

                                // admin_session_create
                                if ('/admin/session/create' === $pathinfo) {
                                    return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::sessionCreateAction',  '_route' => 'admin_session_create',);
                                }

                                // admin_session_create_from_request
                                if (preg_match('#^/admin/session/create/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_create_from_request']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::sessionCreateFromRequestAction',));
                                }

                            }

                            elseif (0 === strpos($pathinfo, '/admin/session/edit')) {
                                // admin_session_edit_scheduled
                                if (0 === strpos($pathinfo, '/admin/session/edit/scheduled') && preg_match('#^/admin/session/edit/scheduled/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_edit_scheduled']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::editScheduledSessionAction',));
                                }

                                // admin_session_edit_quiz
                                if (0 === strpos($pathinfo, '/admin/session/edit/quiz') && preg_match('#^/admin/session/edit/quiz/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_edit_quiz']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::editQuizAction',));
                                }

                                // admin_session_edit_time_slot
                                if ('/admin/session/edit/timeslot' === $pathinfo) {
                                    return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::editTimeSlotAction',  '_route' => 'admin_session_edit_time_slot',);
                                }

                                // admin_session_edit
                                if (preg_match('#^/admin/session/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_edit']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::sessionEditAction',));
                                }

                            }

                            elseif (0 === strpos($pathinfo, '/admin/session/re')) {
                                // admin_session_session_request_edit
                                if (0 === strpos($pathinfo, '/admin/session/request/edit') && preg_match('#^/admin/session/request/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_session_request_edit']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::sessionRequestEditAction',));
                                }

                                if (0 === strpos($pathinfo, '/admin/session/requests')) {
                                    // admin_session_requests
                                    if ('/admin/session/requests' === $pathinfo) {
                                        return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::requestsAction',  '_route' => 'admin_session_requests',);
                                    }

                                    // admin_session_requests_feed
                                    if (0 === strpos($pathinfo, '/admin/session/requests/feed') && preg_match('#^/admin/session/requests/feed/(?P<status>[^/]++)$#sD', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_requests_feed']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::requestsFeedAction',));
                                    }

                                }

                                // admin_session_register
                                if (0 === strpos($pathinfo, '/admin/session/register') && preg_match('#^/admin/session/register/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_register']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::registerAction',));
                                }

                            }

                            // admin_session_assignments
                            if ('/admin/session/assignments' === $pathinfo) {
                                return array (  '_controller' => 'App\\Controller\\Admin\\SessionController::assignmentsFeedAction',  '_route' => 'admin_session_assignments',);
                            }

                            // admin_session_attend
                            if (0 === strpos($pathinfo, '/admin/session/attend') && preg_match('#^/admin/session/attend/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_attend']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::attendAction',));
                            }

                            // admin_session_grades
                            if (0 === strpos($pathinfo, '/admin/session/grades') && preg_match('#^/admin/session/grades/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_session_grades']), array (  '_controller' => 'App\\Controller\\Admin\\SessionController::gradesAction',));
                            }

                        }

                    }

                    // admin_subject_ajax_color
                    if ('/admin/subject/ajax/color' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\SubjectController::ajaxColorAction',  '_route' => 'admin_subject_ajax_color',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/user')) {
                    // admin_user_list
                    if (preg_match('#^/admin/user(?:/(?P<role>all|admin|instructor|mentor|student))?$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_user_list']), array (  'role' => 'all',  '_controller' => 'App\\Controller\\Admin\\UserController::userShowAll',));
                    }

                    if (0 === strpos($pathinfo, '/admin/user/userShow')) {
                        // admin_user_userShowInstructor
                        if ('/admin/user/userShowInstructor' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserController::userShowInstructor',  '_route' => 'admin_user_userShowInstructor',);
                        }

                        // admin_user_userShowMentor
                        if ('/admin/user/userShowMentor' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserController::userShowMentor',  '_route' => 'admin_user_userShowMentor',);
                        }

                        // admin_user_userShowAdmin
                        if ('/admin/user/userShowAdmin' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserController::userShowAdmin',  '_route' => 'admin_user_userShowAdmin',);
                        }

                        // admin_user_userShowStudent
                        if ('/admin/user/userShowStudent' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserController::userShowStudent',  '_route' => 'admin_user_userShowStudent',);
                        }

                    }

                    // admin_user_create
                    if ('/admin/user/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\UserController::addNewUser',  '_route' => 'admin_user_create',);
                    }

                    // admin_user_edit
                    if (0 === strpos($pathinfo, '/admin/user/edit') && preg_match('#^/admin/user/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_user_edit']), array (  '_controller' => 'App\\Controller\\Admin\\UserController::editUser',));
                    }

                    // admin_user_delete
                    if ('/admin/user/delete' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Admin\\UserController::deleteUser',  '_route' => 'admin_user_delete',);
                    }

                    if (0 === strpos($pathinfo, '/admin/user/group')) {
                        // admin_user_group_list
                        if ('/admin/user/group' === $trimmedPathinfo) {
                            $ret = array (  '_controller' => 'App\\Controller\\Admin\\UserGroupController::userGroupShowAll',  '_route' => 'admin_user_group_list',);
                            if ('/' === substr($pathinfo, -1)) {
                                // no-op
                            } elseif ('GET' !== $canonicalMethod) {
                                goto not_admin_user_group_list;
                            } else {
                                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_user_group_list'));
                            }

                            return $ret;
                        }
                        not_admin_user_group_list:

                        // admin_user_group_create
                        if ('/admin/user/group/create' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserGroupController::userGroupAdd',  '_route' => 'admin_user_group_create',);
                        }

                        // admin_user_group_edit
                        if ('/admin/user/group/edit' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserGroupController::userGroupEdit',  '_route' => 'admin_user_group_edit',);
                        }

                        // admin_user_group_delete
                        if ('/admin/user/group/delete' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\Admin\\UserGroupController::deleteUserGroup',  '_route' => 'admin_user_group_delete',);
                        }

                    }

                }

                // admin_home
                if ('/admin' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'App\\Controller\\AdminController::homeAction',  '_route' => 'admin_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_admin_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'admin_home'));
                    }

                    return $ret;
                }
                not_admin_home:

                // admin_cropper
                if ('/admin/cropper' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\AdminController::cropperAction',  '_route' => 'admin_cropper',);
                }

                if (0 === strpos($pathinfo, '/admin/a')) {
                    if (0 === strpos($pathinfo, '/admin/ajax')) {
                        // admin_image_upload
                        if ('/admin/ajax/image_upload' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::mentorFileUpload',  '_route' => 'admin_image_upload',);
                        }

                        // admin_count_pending_mod_requests
                        if ('/admin/ajax/count_pending_mod_requests' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::countPendingModRequests',  '_route' => 'admin_count_pending_mod_requests',);
                        }

                        // admin_count_pending_occurrences
                        if ('/admin/ajax/count_pending_occurrences' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::countPendingOccurrences',  '_route' => 'admin_count_pending_occurrences',);
                        }

                        // admin_ajax_get_occurrence_details
                        if ('/admin/ajax/get_occurrence_details' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::getOccurrenceDetails',  '_route' => 'admin_ajax_get_occurrence_details',);
                        }

                        // admin_update_occurrence
                        if ('/admin/ajax/update_occurrence' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::saveOccurrenceAction',  '_route' => 'admin_update_occurrence',);
                        }

                        // admin_ajax_penalty_configuration
                        if ('/admin/ajax/penalty/configuration' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::savePenaltyConfigurationAction',  '_route' => 'admin_ajax_penalty_configuration',);
                        }

                        // admin_modify_specialties
                        if ('/admin/ajax/modify_specialties' === $pathinfo) {
                            return array (  '_controller' => 'App\\Controller\\AdminController::modifySpecialties',  '_route' => 'admin_modify_specialties',);
                        }

                    }

                    // admin_approve_mod_request
                    if (0 === strpos($pathinfo, '/admin/approve_profile_modification_request') && preg_match('#^/admin/approve_profile_modification_request/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_approve_mod_request']), array (  '_controller' => 'App\\Controller\\AdminController::approveProfileModificationRequest',));
                    }

                    // admin_approve_occurrence
                    if ('/admin/approve_occurrence' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\AdminController::approveOccurrence',  '_route' => 'admin_approve_occurrence',);
                    }

                }

                // admin_view_occurrence_summary
                if ('/admin/occurrence_summary' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\AdminController::viewMentorSummaryOccurrences',  '_route' => 'admin_view_occurrence_summary',);
                }

                if (0 === strpos($pathinfo, '/admin/view_mentor_summary')) {
                    // admin_view_mentor_summary
                    if ('/admin/view_mentor_summary' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\AdminController::viewMentorSummary',  '_route' => 'admin_view_mentor_summary',);
                    }

                    // admin_view_mentor_summary_settings
                    if ('/admin/view_mentor_summary_settings' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\AdminController::viewMentorSummarySettings',  '_route' => 'admin_view_mentor_summary_settings',);
                    }

                }

                // admin_view_mentor_specialty
                if ('/admin/view_mentor_specialty' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\AdminController::viewMentorSpecialty',  '_route' => 'admin_view_mentor_specialty',);
                }

                // admin_reject_mod_request
                if (0 === strpos($pathinfo, '/admin/reject_profile_modification_request') && preg_match('#^/admin/reject_profile_modification_request/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'admin_reject_mod_request']), array (  '_controller' => 'App\\Controller\\AdminController::rejectProfileModificationRequest',));
                }

                // admin_reject_occurrence
                if ('/admin/reject_occurrence' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\AdminController::rejectOccurrence',  '_route' => 'admin_reject_occurrence',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ajax')) {
                if (0 === strpos($pathinfo, '/ajax/schedule')) {
                    // absence_cancel_ajax
                    if ('/ajax/schedule/absence/cancel' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceCancelAjaxAction',  '_route' => 'absence_cancel_ajax',);
                    }

                    // absence_market_ajax
                    if ('/ajax/schedule/absence/market' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceMarketAjaxAction',  '_route' => 'absence_market_ajax',);
                    }

                    // shift_assignment_date_ajax
                    if ('/ajax/schedule/shift/assignment/date' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftAssignmentDateAjax',  '_route' => 'shift_assignment_date_ajax',);
                    }

                    // shift_assignment_time_ajax
                    if ('/ajax/schedule/shift/assignment/time' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftAssignmentTimeAjax',  '_route' => 'shift_assignment_time_ajax',);
                    }

                }

                // session_grades_edit_ajax
                if ('/ajax/session/grades' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionHistoryController::sessionGradesEditAjaxAction',  '_route' => 'session_grades_edit_ajax',);
                }

                // user_reset_card
                if ('/ajax/user/reset_card' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\User\\UserController::userResetCardAction',  '_route' => 'user_reset_card',);
                }

            }

            // session_attend
            if (0 === strpos($pathinfo, '/attend') && preg_match('#^/attend/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_attend']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionHistoryController::attendAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/s')) {
            // behavior_occurrence_submission
            if ('/submit_behavior_occurrence' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\BehaviorOccurrenceSubmissionController::submitBehavorOccurrence',  '_route' => 'behavior_occurrence_submission',);
            }

            if (0 === strpos($pathinfo, '/schedule')) {
                if (0 === strpos($pathinfo, '/schedule/absence')) {
                    // absence
                    if ('/schedule/absence' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceAction',  '_route' => 'absence',);
                    }

                    // absence_create
                    if ('/schedule/absence/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceCreateAction',  '_route' => 'absence_create',);
                    }

                    // absence_edit
                    if (0 === strpos($pathinfo, '/schedule/absence/edit') && preg_match('#^/schedule/absence/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'absence_edit']), array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceEditAction',));
                    }

                    // absence_market
                    if ('/schedule/absence/market' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\AbsenceController::absenceMarketAction',  '_route' => 'absence_market',);
                    }

                }

                // schedule
                if ('/schedule' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ScheduleController::scheduleAction',  '_route' => 'schedule',);
                }

                // schedule_weekly
                if (0 === strpos($pathinfo, '/schedule/weekly') && preg_match('#^/schedule/weekly(?:/(?P<date>\\d{4}-\\d{2}-\\d{2}))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'schedule_weekly']), array (  'date' => NULL,  '_controller' => 'App\\Controller\\Entity\\Schedule\\ScheduleController::scheduleWeeklyAction',));
                }

                // schedule_upload
                if ('/schedule/upload' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ScheduleController::scheduleCreateAction',  '_route' => 'schedule_upload',);
                }

                // schedule_timesheet
                if (0 === strpos($pathinfo, '/schedule/timesheet') && preg_match('#^/schedule/timesheet(?:/(?P<date>\\d{4}-\\d{2}-\\d{2}))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'schedule_timesheet']), array (  'date' => NULL,  '_controller' => 'App\\Controller\\Entity\\Schedule\\ScheduleController::scheduleTimeSheetAction',));
                }

                if (0 === strpos($pathinfo, '/schedule/shift')) {
                    // shift_create_subject
                    if ('/schedule/shift/create/subject' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftCreateSubjectAction',  '_route' => 'shift_create_subject',);
                    }

                    // shift_create_time
                    if ('/schedule/shift/create/time' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftCreateTimeAction',  '_route' => 'shift_create_time',);
                    }

                    // shift_edit
                    if (0 === strpos($pathinfo, '/schedule/shift/edit') && preg_match('#^/schedule/shift/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'shift_edit']), array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftEditAction',));
                    }

                    // shift_time_feed
                    if ('/schedule/shift/time/feed' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Schedule\\ShiftController::shiftTimeFeed',  '_route' => 'shift_time_feed',);
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/session')) {
                // session
                if (preg_match('#^/session(?:/(?P<month>\\d{2})(?:/(?P<year>\\d{4}))?)?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'session']), array (  'month' => NULL,  'year' => NULL,  '_controller' => 'App\\Controller\\Entity\\Session\\SessionController::sessionAction',));
                }

                // session_view
                if (0 === strpos($pathinfo, '/session/view') && preg_match('#^/session/view/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_view']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionController::sessionViewAction',));
                }

                // session_timeslot_view
                if (0 === strpos($pathinfo, '/session/timeslot/view') && preg_match('#^/session/timeslot/view/(?P<tid>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_timeslot_view']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionController::sessionTimeslotViewAction',));
                }

                // session_schedule
                if ('/session/schedule' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionController::sessionScheduleAction',  '_route' => 'session_schedule',);
                }

                // session_history
                if ('/session/history' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionHistoryController::sessionHistoryAction',  '_route' => 'session_history',);
                }

                if (0 === strpos($pathinfo, '/session/grades')) {
                    // session_grades
                    if (preg_match('#^/session/grades/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_grades']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionHistoryController::sessionGradesAction',));
                    }

                    // session_grades_download
                    if (preg_match('#^/session/grades/(?P<id>[^/]++)/download$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_grades_download']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionHistoryController::sessionGradesDownloadAction',));
                    }

                }

                elseif (0 === strpos($pathinfo, '/session/register')) {
                    // session_register_timeslot
                    if (preg_match('#^/session/register/(?P<sid>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_register_timeslot']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRegisterController::sessionRegisterTimeslotAction',));
                    }

                    // session_register_form
                    if (0 === strpos($pathinfo, '/session/register/timeslot') && preg_match('#^/session/register/timeslot/(?P<tid>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_register_form']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRegisterController::sessionRegisterFormAction',));
                    }

                }

                elseif (0 === strpos($pathinfo, '/session/request')) {
                    // session_request
                    if ('/session/request' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRequestController::sessionRequestAction',  '_route' => 'session_request',);
                    }

                    // session_request_view
                    if (0 === strpos($pathinfo, '/session/request/view') && preg_match('#^/session/request/view/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_request_view']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRequestController::sessionRequestViewAction',));
                    }

                    // session_request_create
                    if ('/session/request/create' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRequestController::sessionRequestCreateAction',  '_route' => 'session_request_create',);
                    }

                    // session_request_edit
                    if (0 === strpos($pathinfo, '/session/request/edit') && preg_match('#^/session/request/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, ['_route' => 'session_request_edit']), array (  '_controller' => 'App\\Controller\\Entity\\Session\\SessionRequestController::sessionRequestEdit',));
                    }

                }

            }

            elseif (0 === strpos($pathinfo, '/swipe')) {
                // swipe_walk_in
                if ('/swipe/walk_in' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\SwipeController::swipeWalkInAction',  '_route' => 'swipe_walk_in',);
                }

                if (0 === strpos($pathinfo, '/swipe/ajax')) {
                    // swipe_ajax_walk_in
                    if ('/swipe/ajax/walk_in' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\SwipeController::swipeAjaxWalkInAction',  '_route' => 'swipe_ajax_walk_in',);
                    }

                    // swipe_ajax_entry
                    if ('/swipe/ajax/entry' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\SwipeController::swipeAjaxWalkInEntryAction',  '_route' => 'swipe_ajax_entry',);
                    }

                    // swipe_ajax_exit
                    if ('/swipe/ajax/exit' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\SwipeController::swipeAjaxWalkInExitAction',  '_route' => 'swipe_ajax_exit',);
                    }

                    // swipe_ajax_session
                    if ('/swipe/ajax/session' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\SwipeController::swipeAjaxSessionAction',  '_route' => 'swipe_ajax_session',);
                    }

                    // swipe_ajax_register
                    if ('/swipe/ajax/register' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\SwipeController::swipeAjaxRegisterAction',  '_route' => 'swipe_ajax_register',);
                    }

                }

                // swipe_session
                if (0 === strpos($pathinfo, '/swipe/session') && preg_match('#^/swipe/session/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'swipe_session']), array (  '_controller' => 'App\\Controller\\SwipeController::swipeSessionAction',));
                }

            }

        }

        // home
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\DefaultController::index',  '_route' => 'home',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_home;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'home'));
            }

            return $ret;
        }
        not_home:

        if (0 === strpos($pathinfo, '/d')) {
            if (0 === strpos($pathinfo, '/dev')) {
                // dev_home
                if ('/dev' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'App\\Controller\\DeveloperController::homeAction',  '_route' => 'dev_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_dev_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'dev_home'));
                    }

                    return $ret;
                }
                not_dev_home:

                if (0 === strpos($pathinfo, '/dev/files')) {
                    // dev_files
                    if ('/dev/files' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DeveloperController::fileViewAction',  '_route' => 'dev_files',);
                    }

                    // dev_files_feed
                    if ('/dev/files/feed' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DeveloperController::filesFeedAction',  '_route' => 'dev_files_feed',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/dev/swipes')) {
                    // dev_swipes
                    if ('/dev/swipes' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DeveloperController::swipeViewAction',  '_route' => 'dev_swipes',);
                    }

                    // dev_swipes_feed
                    if ('/dev/swipes/feed' === $pathinfo) {
                        return array (  '_controller' => 'App\\Controller\\DeveloperController::swipesFeedAction',  '_route' => 'dev_swipes_feed',);
                    }

                }

            }

            // display
            if ('/display' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\DisplayController::displayAction',  '_route' => 'display',);
            }

            // download
            if (0 === strpos($pathinfo, '/download') && preg_match('#^/download/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'download']), array (  '_controller' => 'App\\Controller\\FileController::downloadAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/course')) {
            // course
            if ('/course' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Entity\\Course\\CourseController::courseAction',  '_route' => 'course',);
            }

            // section
            if ('/course/section' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Entity\\Course\\SectionController::sectionAction',  '_route' => 'section',);
            }

            if (0 === strpos($pathinfo, '/course/section/view')) {
                // section_roster
                if (preg_match('#^/course/section/view/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'section_roster']), array (  '_controller' => 'App\\Controller\\Entity\\Course\\SectionController::sectionRosterAction',));
                }

                // section_roster_download
                if (preg_match('#^/course/section/view/(?P<id>[^/]++)/roster$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'section_roster_download']), array (  '_controller' => 'App\\Controller\\Entity\\Course\\SectionController::sectionRosterDownloadAction',));
                }

            }

        }

        elseif (0 === strpos($pathinfo, '/feedback')) {
            // feedback
            if ('/feedback' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Entity\\Feedback\\FeedbackController::feedbackAction',  '_route' => 'feedback',);
            }

            // feedback_type
            if ('/feedback/type' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Entity\\Feedback\\FeedbackController::feedbackTypeAction',  '_route' => 'feedback_type',);
            }

            // feedback_view
            if ('/feedback/view' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Entity\\Feedback\\FeedbackController::feedbackViewAction',  '_route' => 'feedback_view',);
            }

        }

        // file
        if ('/file' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\FileController::fileAction',  '_route' => 'file',);
        }

        if (0 === strpos($pathinfo, '/user/info')) {
            // user_info_view
            if (0 === strpos($pathinfo, '/user/info/view') && preg_match('#^/user/info/view(?:/(?P<id>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'user_info_view']), array (  'id' => NULL,  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoViewAction',));
            }

            // user_info_edit
            if (0 === strpos($pathinfo, '/user/info/edit') && preg_match('#^/user/info/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'user_info_edit']), array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoEditAction',));
            }

            if (0 === strpos($pathinfo, '/user/info/dietary_restriction')) {
                // user_info_dietary_restriction
                if ('/user/info/dietary_restriction' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoDietaryRestrictionAction',  '_route' => 'user_info_dietary_restriction',);
                }

                // user_info_dietary_restriction_create
                if ('/user/info/dietary_restriction/create' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoDietaryRestrictionCreateAction',  '_route' => 'user_info_dietary_restriction_create',);
                }

                // user_info_dietary_restriction_edit
                if (0 === strpos($pathinfo, '/user/info/dietary_restriction/edit') && preg_match('#^/user/info/dietary_restriction/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'user_info_dietary_restriction_edit']), array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoDietaryRestrictionEditAction',));
                }

                // user_info_dietary_restriction__category_create
                if ('/user/info/dietary_restriction_category/create' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoDietaryRestrictionCategoryCreateAction',  '_route' => 'user_info_dietary_restriction__category_create',);
                }

                // user_info_dietary_restriction_category_edit
                if (0 === strpos($pathinfo, '/user/info/dietary_restriction_category/edit') && preg_match('#^/user/info/dietary_restriction_category/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'user_info_dietary_restriction_category_edit']), array (  '_controller' => 'App\\Controller\\Entity\\User\\InfoController::infoDietaryRestrictionCategoryEditAction',));
                }

            }

        }

        // user_profile
        if ('/user/profile' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\Entity\\User\\UserController::userProfileAction',  '_route' => 'user_profile',);
        }

        // events
        if ('/events' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\EventController::eventFeedAction',  '_route' => 'events',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // profile
            if (preg_match('#^/profile/(?P<username>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'profile']), array (  '_controller' => 'App\\Controller\\ProfileController::viewProfile',));
            }

            // edit_profile
            if (preg_match('#^/profile/(?P<username>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'edit_profile']), array (  '_controller' => 'App\\Controller\\ProfileController::editProfile',));
            }

            // save_profile_image
            if (preg_match('#^/profile/(?P<username>[^/]++)/save_image$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'save_profile_image']), array (  '_controller' => 'App\\Controller\\ProfileController::saveImageAction',));
            }

            // update_profile_image
            if (preg_match('#^/profile/(?P<username>[^/]++)/update_image$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'update_profile_image']), array (  '_controller' => 'App\\Controller\\ProfileController::updateImageAction',));
            }

            // profile_image
            if (preg_match('#^/profile/(?P<username>[^/]++)/image$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'profile_image']), array (  '_controller' => 'App\\Controller\\ProfileController::imageAction',));
            }

            // profile_requested_image
            if (preg_match('#^/profile/(?P<username>[^/]++)/new_image$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'profile_requested_image']), array (  '_controller' => 'App\\Controller\\ProfileController::requestedProfileImage',));
            }

            // origin_requested_image
            if (preg_match('#^/profile/(?P<username>[^/]++)/origin_requested_image$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'origin_requested_image']), array (  '_controller' => 'App\\Controller\\ProfileController::originRequestedImage',));
            }

        }

        elseif (0 === strpos($pathinfo, '/report')) {
            // report_generator
            if ('/report/generator' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ReportController::reportGeneratorAction',  '_route' => 'report_generator',);
            }

            // report
            if ('/report' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ReportController::reportAction',  '_route' => 'report',);
            }

            // report_walkin_download
            if (0 === strpos($pathinfo, '/report/walkin') && preg_match('#^/report/walkin/(?P<id>[^/]++)/download$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'report_walkin_download']), array (  '_controller' => 'App\\Controller\\ReportController::reportWalkInDownloadAction',));
            }

            // report_section_grades_download
            if (0 === strpos($pathinfo, '/report/section') && preg_match('#^/report/section/(?P<id>[^/]++)/grades/download$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => 'report_section_grades_download']), array (  '_controller' => 'App\\Controller\\ReportController::reportSectionGradesDownloadAction',));
            }

            // report_semester
            if ('/report/semester' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ReportController::reportSemester',  '_route' => 'report_semester',);
            }

        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
        }

        // user_logout
        if ('/logout' === $pathinfo) {
            return ['_route' => 'user_logout'];
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
