<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'app.email_service' shared autowired service.

return $this->services['app.email_service'] = new \App\Utils\EmailNotificationsService($this->getEnv('SMTP_USERNAME'), $this->getEnv('SMTP_PASSWORD'), $this->getEnv('FROM_EMAIL'));
