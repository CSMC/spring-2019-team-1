<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\Admin\CourseController' shared autowired service.

return $this->services['App\Controller\Admin\CourseController'] = new \App\Controller\Admin\CourseController();
