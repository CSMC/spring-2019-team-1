<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'App\Validator\Constraints\ScancodeValidator' shared autowired service.

return $this->services['App\Validator\Constraints\ScancodeValidator'] = new \App\Validator\Constraints\ScancodeValidator();
