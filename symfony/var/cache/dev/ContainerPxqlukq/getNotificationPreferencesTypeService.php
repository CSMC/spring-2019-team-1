<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'App\Form\NotificationPreferencesType' shared autowired service.

return $this->services['App\Form\NotificationPreferencesType'] = new \App\Form\NotificationPreferencesType();
