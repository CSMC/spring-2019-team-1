<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'App\Controller\Entity\Session\SessionController' shared autowired service.

return $this->services['App\Controller\Entity\Session\SessionController'] = new \App\Controller\Entity\Session\SessionController();
