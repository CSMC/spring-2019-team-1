<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'form.registry' shared service.

return $this->services['form.registry'] = new \Symfony\Component\Form\FormRegistry([0 => new \Symfony\Component\Form\Extension\DependencyInjection\DependencyInjectionExtension(new \Symfony\Component\DependencyInjection\ServiceLocator(['App\\Form\\AbsenceType' => function () {
    return ${($_ = isset($this->services['App\Form\AbsenceType']) ? $this->services['App\Form\AbsenceType'] : ($this->services['App\Form\AbsenceType'] = new \App\Form\AbsenceType())) && false ?: '_'};
}, 'App\\Form\\AnnouncementType' => function () {
    return ${($_ = isset($this->services['App\Form\AnnouncementType']) ? $this->services['App\Form\AnnouncementType'] : ($this->services['App\Form\AnnouncementType'] = new \App\Form\AnnouncementType())) && false ?: '_'};
}, 'App\\Form\\CardRegisterType' => function () {
    return ${($_ = isset($this->services['App\Form\CardRegisterType']) ? $this->services['App\Form\CardRegisterType'] : ($this->services['App\Form\CardRegisterType'] = new \App\Form\CardRegisterType())) && false ?: '_'};
}, 'App\\Form\\GraduationSemesterType' => function () {
    return ${($_ = isset($this->services['App\Form\GraduationSemesterType']) ? $this->services['App\Form\GraduationSemesterType'] : ($this->services['App\Form\GraduationSemesterType'] = new \App\Form\GraduationSemesterType())) && false ?: '_'};
}, 'App\\Form\\NoCardType' => function () {
    return ${($_ = isset($this->services['App\Form\NoCardType']) ? $this->services['App\Form\NoCardType'] : ($this->services['App\Form\NoCardType'] = new \App\Form\NoCardType())) && false ?: '_'};
}, 'App\\Form\\NotificationPreferencesType' => function () {
    return ${($_ = isset($this->services['App\Form\NotificationPreferencesType']) ? $this->services['App\Form\NotificationPreferencesType'] : ($this->services['App\Form\NotificationPreferencesType'] = new \App\Form\NotificationPreferencesType())) && false ?: '_'};
}, 'App\\Form\\ProfileType' => function () {
    return ${($_ = isset($this->services['App\Form\ProfileType']) ? $this->services['App\Form\ProfileType'] : ($this->services['App\Form\ProfileType'] = new \App\Form\ProfileType())) && false ?: '_'};
}, 'App\\Form\\QuizType' => function () {
    return ${($_ = isset($this->services['App\Form\QuizType']) ? $this->services['App\Form\QuizType'] : ($this->services['App\Form\QuizType'] = new \App\Form\QuizType())) && false ?: '_'};
}, 'App\\Form\\RequestType' => function () {
    return ${($_ = isset($this->services['App\Form\RequestType']) ? $this->services['App\Form\RequestType'] : ($this->services['App\Form\RequestType'] = new \App\Form\RequestType())) && false ?: '_'};
}, 'App\\Form\\ScheduledSessionType' => function () {
    return ${($_ = isset($this->services['App\Form\ScheduledSessionType']) ? $this->services['App\Form\ScheduledSessionType'] : ($this->services['App\Form\ScheduledSessionType'] = new \App\Form\ScheduledSessionType())) && false ?: '_'};
}, 'App\\Form\\SessionType' => function () {
    return ${($_ = isset($this->services['App\Form\SessionType']) ? $this->services['App\Form\SessionType'] : ($this->services['App\Form\SessionType'] = new \App\Form\SessionType())) && false ?: '_'};
}, 'App\\Form\\SpecialtyType' => function () {
    return ${($_ = isset($this->services['App\Form\SpecialtyType']) ? $this->services['App\Form\SpecialtyType'] : ($this->services['App\Form\SpecialtyType'] = new \App\Form\SpecialtyType())) && false ?: '_'};
}, 'App\\Form\\SwipeType' => function () {
    return ${($_ = isset($this->services['App\Form\SwipeType']) ? $this->services['App\Form\SwipeType'] : ($this->services['App\Form\SwipeType'] = new \App\Form\SwipeType())) && false ?: '_'};
}, 'App\\Form\\TimeSlotType' => function () {
    return ${($_ = isset($this->services['App\Form\TimeSlotType']) ? $this->services['App\Form\TimeSlotType'] : ($this->services['App\Form\TimeSlotType'] = new \App\Form\TimeSlotType())) && false ?: '_'};
}, 'App\\Form\\WalkInEntryType' => function () {
    return ${($_ = isset($this->services['App\Form\WalkInEntryType']) ? $this->services['App\Form\WalkInEntryType'] : ($this->services['App\Form\WalkInEntryType'] = new \App\Form\WalkInEntryType())) && false ?: '_'};
}, 'App\\Form\\WalkInExitType' => function () {
    return ${($_ = isset($this->services['App\Form\WalkInExitType']) ? $this->services['App\Form\WalkInExitType'] : ($this->services['App\Form\WalkInExitType'] = new \App\Form\WalkInExitType())) && false ?: '_'};
}, 'Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType' => function () {
    return ${($_ = isset($this->services['form.type.entity']) ? $this->services['form.type.entity'] : $this->load('getForm_Type_EntityService.php')) && false ?: '_'};
}, 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType' => function () {
    return ${($_ = isset($this->services['form.type.choice']) ? $this->services['form.type.choice'] : $this->load('getForm_Type_ChoiceService.php')) && false ?: '_'};
}, 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => function () {
    return ${($_ = isset($this->services['form.type.form']) ? $this->services['form.type.form'] : $this->load('getForm_Type_FormService.php')) && false ?: '_'};
}]), ['Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => new RewindableGenerator(function () {
    yield 0 => ${($_ = isset($this->services['form.type_extension.form.transformation_failure_handling']) ? $this->services['form.type_extension.form.transformation_failure_handling'] : $this->load('getForm_TypeExtension_Form_TransformationFailureHandlingService.php')) && false ?: '_'};
    yield 1 => ${($_ = isset($this->services['form.type_extension.form.http_foundation']) ? $this->services['form.type_extension.form.http_foundation'] : $this->load('getForm_TypeExtension_Form_HttpFoundationService.php')) && false ?: '_'};
    yield 2 => ${($_ = isset($this->services['form.type_extension.form.validator']) ? $this->services['form.type_extension.form.validator'] : $this->load('getForm_TypeExtension_Form_ValidatorService.php')) && false ?: '_'};
    yield 3 => ${($_ = isset($this->services['form.type_extension.upload.validator']) ? $this->services['form.type_extension.upload.validator'] : $this->load('getForm_TypeExtension_Upload_ValidatorService.php')) && false ?: '_'};
    yield 4 => ${($_ = isset($this->services['form.type_extension.csrf']) ? $this->services['form.type_extension.csrf'] : $this->load('getForm_TypeExtension_CsrfService.php')) && false ?: '_'};
    yield 5 => ${($_ = isset($this->services['form.type_extension.form.data_collector']) ? $this->services['form.type_extension.form.data_collector'] : $this->load('getForm_TypeExtension_Form_DataCollectorService.php')) && false ?: '_'};
}, 6), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RepeatedType' => new RewindableGenerator(function () {
    yield 0 => ${($_ = isset($this->services['form.type_extension.repeated.validator']) ? $this->services['form.type_extension.repeated.validator'] : ($this->services['form.type_extension.repeated.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension())) && false ?: '_'};
}, 1), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\SubmitType' => new RewindableGenerator(function () {
    yield 0 => ${($_ = isset($this->services['form.type_extension.submit.validator']) ? $this->services['form.type_extension.submit.validator'] : ($this->services['form.type_extension.submit.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension())) && false ?: '_'};
}, 1)], new RewindableGenerator(function () {
    yield 0 => ${($_ = isset($this->services['form.type_guesser.validator']) ? $this->services['form.type_guesser.validator'] : $this->load('getForm_TypeGuesser_ValidatorService.php')) && false ?: '_'};
    yield 1 => ${($_ = isset($this->services['form.type_guesser.doctrine']) ? $this->services['form.type_guesser.doctrine'] : $this->load('getForm_TypeGuesser_DoctrineService.php')) && false ?: '_'};
}, 2), NULL)], ${($_ = isset($this->services['form.resolved_type_factory']) ? $this->services['form.resolved_type_factory'] : $this->load('getForm_ResolvedTypeFactoryService.php')) && false ?: '_'});
